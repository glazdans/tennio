#!/bin/sh

# supervisor setup for centOS 6

yum install python-setuptools -y
easy_install pip

easy_install supervisor

echo_supervisord_conf > /etc/supervisord.conf
mkdir /etc/supervisord.d
echo "[include]
files = /etc/supervisord.d/*.conf" >> /etc/supervisord.conf

cp supervisord /etc/init.d/
chmod +x /etc/init.d/supervisord
chkconfig --add supervisord
service supervisord start

# then you add your program configs to /etc/supervisord.d/
supervisorctl reread
supervisorctl start program_name
