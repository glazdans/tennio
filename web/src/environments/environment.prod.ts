export const environment = {
  production: true,
  apiUrl: "https://api.tennio.com",
  host: "api.tennio.com",
  wsUrl: "wss://api.tennio.com",
  gameUrl: "https://game.tennio.com"
};
