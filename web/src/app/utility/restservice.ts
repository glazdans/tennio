import {Http, Headers} from "@angular/http";
import {LoginService} from "../component/login/login.service";
import {Router} from "@angular/router";
import {AnonymousRestService} from "./anonymousrestservice";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ObservableInput} from "rxjs/Observable";

@Injectable()
export class RestService extends AnonymousRestService {
    protected loginService: LoginService;

    constructor(http: Http, router: Router, loginService: LoginService) {
        super(http, router);
        this.loginService = loginService;
    }

    protected getHeaders(options: any, post: boolean = false) {
        let o = options || {headers: new Headers()};

        if (this.loginService.isLoggedIn()) {
            o.headers.append("Authorization", this.loginService.getJwt());
        }

        if (post) {
            o.headers.append('Content-Type', 'application/json');
        }

        return o;
    }

    public onError(err: any, caught: Observable<any>): ObservableInput<{}> {
        if (err.status === 401) {
            this.loginService.logout();
        }

        return Observable.empty();
    }
}