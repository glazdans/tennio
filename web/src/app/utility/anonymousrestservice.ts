import {Response, Headers, RequestOptionsArgs, Http} from "@angular/http";
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {ObservableInput} from "rxjs/Observable";
import {Tennio} from "./Tennio";

@Injectable()
export class AnonymousRestService {
    constructor(protected http: Http, protected router: Router) {
    }

    protected extractData(res: Response) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Bad response status: ' + res.status);
        }
        let body:any;
        if (res.text().toString().length) {
            body = res.json();
        }

        return body || {};
    }

    public get(url: string, options?: RequestOptionsArgs): Observable<any> {
        return this.http.get(url, this.getHeaders(options)).catch(Tennio.closure(this, this.onError)).map(this.extractData);
    };

    protected getHeaders(options:any, post: boolean = false) {
        let o = options || {headers: new Headers()};

        if(post) {
            o.headers.append('Content-Type', 'application/json');
        }

        return o;
    }

    public post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
        if(typeof body == "object") {
            body = JSON.stringify(body);
        }
        return this.http.post(url, body, this.getHeaders(options, !!body.length)).map(this.extractData).catch(this.onError);
    };

    public delete(url: string, options?: RequestOptionsArgs): Observable<any> {
        return this.http.delete(url, this.getHeaders(options, true)).map(this.extractData).catch(this.onError);
    };

    public onError(err: any, caught: Observable<any>): ObservableInput<{}> {
        return caught;
    }
}