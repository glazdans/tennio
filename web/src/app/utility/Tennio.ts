import {isNumeric} from "rxjs/util/isNumeric";
import {Notification, NotificationType, NotificationAttributeType} from "../component/menu/notification";
import {isBoolean} from "util";
/**
 *
 * Global function module/library Tennio
 *
 */

export module Tennio {
    export function closure($this:any, fn:any, ...aargs: any[]) {
        if (typeof fn != 'function') {
            throw new Error('Closure function is not fun');
        }
        return function (...bargs: any[]) {
            $this = $this || this;
            return fn.apply($this, aargs.concat(bargs));
        };
    }

    export function compareEnum($enum : { [s: number]: string }, val1:any, val2:any) {
        return +$enum[val1] == val2 || $enum[val1] == $enum[val2];
    }

    export function objLen(obj: any) {
        return Object.keys(obj).length;
    }

    export function enumNames($enum : { [s: number]: string }) {
        let keys = Object.keys($enum),
            names = [];

        for(let i in keys) {
            if(!isNumeric(keys[i])) {
                names.push(keys[i]);
            }
        }

        return names;
    }

    export function enumValues($enum : { [s: number]: string }) {
        let keys = Object.keys($enum),
            values = [];

        for(let i in keys) {
            if(isNumeric(keys[i])) {
                values.push(keys[i]);
            } else {
                break
            }
        }

        return values;
    }

    export function getSomethingById<V>(somethings: any[], id: number): V {
        for(let something of somethings) {
            if(something.id == id) {
                return something;
            }
        }
        return null;
    }

    export function killConsole() {
        console.log = () => {
        };
    }
    
    export class N {
        static checkType(n : Notification, nt: NotificationType) : boolean {
            return Tennio.compareEnum(NotificationType, n.notificationType, nt);
        }
        static onFindAttr(n : Notification, nat: NotificationAttributeType, cb: (string) => boolean) : void {
            for(let attr in n.notificationAttributes){
                if(Tennio.compareEnum(NotificationAttributeType, attr, nat)) {
                    let value = n.notificationAttributes[attr];
                    let shouldContinue = cb(value);
                    if(isBoolean(shouldContinue) && !shouldContinue){
                        break;
                    }
                }
            }
        }

        static findOneAttr<T>(n: Notification, nat: NotificationAttributeType): T {
            for (let attr in n.notificationAttributes) {
                if (Tennio.compareEnum(NotificationAttributeType, attr, nat)) {
                    return n.notificationAttributes[attr];
                }
            }
        }
    }
}