import {Injectable} from "@angular/core";
import {Tennio} from "./Tennio";
declare let moment: any;

@Injectable()
export class TimeService {
    public timeString: string;
    public timestamp: number;
    public dateTimeString: string;
    public dateString: string;

    private timeOffset: number = 0;

    static CANCER_LIMIT: number = 3500;

    get isCancerTime() {
        return this.timeOffset > TimeService.CANCER_LIMIT;
    }

    constructor() {
        this.updateTime();
    }

    setServerTime(serverTime: number) {
        let localTime = Date.now();
        this.timeOffset = Math.abs(localTime - serverTime);
        if (this.timeOffset < TimeService.CANCER_LIMIT) {
            this.timeOffset = 0;
        }
    }

    updateTime() {
        let cMoment = moment();
        this.dateString = cMoment.format('ll');
        this.timeString = cMoment.format('LT');
        this.timestamp = +cMoment.unix();
        this.dateTimeString = cMoment.format('LLL');

        // updates every minute at 0 seconds
        setTimeout(Tennio.closure(this, this.updateTime), (cMoment.startOf('minute').unix() + 60 - this.timestamp) * 1000);
    }
}