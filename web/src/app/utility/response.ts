export declare class Response<T> {
    public object:T;
    public code: ResponseCode;
    public message: string;
}

export enum ResponseCode {
    OK, FOUND, ERROR
}