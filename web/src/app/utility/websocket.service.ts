import {Injectable} from "@angular/core";
import {LoginService} from "../component/login/login.service";
import {Tennio} from "./Tennio";
import {environment} from "../../environments/environment";
import {SnotifyService} from "ng-snotify";

declare let Stomp: any;

@Injectable()
export class WebSocketService {
    private callbacks: {[destination: string]: ((message: string) => any)[]} = {};
    private stompSubs: any = {};
    private stompClient: any;
    private connectionPromise: any = null;
    private wsDiedMsg: string = "Lost connection to server...";

    constructor(private loginService: LoginService, private snotifyService: SnotifyService) {
        loginService.registerOnLogoutCallback(Tennio.closure(this, this.onLogout));
        loginService.registerOnLoginCallback(Tennio.closure(this, this.onLogin));
    }

    private getStompClient(): any {
        return new Promise((resolve, reject) => {
            let client = this.stompClient;
            if (!client) {
                let socket = new WebSocket(environment.wsUrl +
                    "/hello?jwt=" + this.loginService.getJwt());
                client = Stomp.over(socket);
                console.log("!!!!NEW SOCKET!!!!");
                client.debug = null;
                this.stompClient = client;
            }
            if (client.connected) {
                resolve(client);
            } else {
                if (!this.connectionPromise) {
                    this.connectionPromise = new Promise((resolve, reject) => {
                        client.connect({}, (frame: any) => {
                            console.log("CONNECTED TO WS");
                            resolve(client);
                        }, () => {
                            reject(this.snotifyService.error(this.wsDiedMsg, "", {timeout: 0, titleMaxLength: 2323}))
                        });
                    }).catch(() => reject(this.snotifyService.error(this.wsDiedMsg, "", {timeout: 0, titleMaxLength: 2323})));
                }

                this.connectionPromise.then((client: any) => {
                    resolve(client);
                });
            }
        });
    }

    public onLogout() {
        if (this.stompClient.connected) {
            for (let i in this.stompSubs) {
                this.stompSubs[i].unsubscribe();
            }
            this.stompSubs = {};
            this.stompClient.disconnect();
            this.stompClient = null;
            this.connectionPromise = null;
        }
    }

    public onLogin() {
        if (Tennio.objLen(this.callbacks)) {
            console.clear();
            this.getStompClient().then((client: any) => {
                console.log("REINITIALIZING WS SUBSCRIPTIONS");
                for (let dest in this.callbacks) {
                    client.subscribe(dest, this.handleMessages.apply(this, [dest]));
                }
            });
        }
    }

    handleMessages(destination: string) {
        return (message: any) => {
            let json = JSON.parse(message.body);
            for (let callback of this.callbacks[destination]) {
                callback(json);
            }
        }
    }

    private unsubscribe(destination: string, id: number) {
        if (this.callbacks[destination] instanceof Array) {
            if (this.callbacks[destination][id]) {
                this.callbacks[destination].splice(id, 1);
            }
        }
        if (this.stompSubs[destination]) {
            this.stompSubs[destination].unsubscribe();
            delete this.stompSubs[destination];
        }
    }

    subscribe(destination: string, callback: (message: string) => any, data?: any): WebSocketSubscription {
        if (this.callbacks[destination]) {
            this.callbacks[destination][this.callbacks[destination].length] = callback;
        } else {
            this.callbacks[destination] = [callback];
            this.getStompClient().then((client: any) => {
                try {
                    console.log("Subscribing to " + destination);
                    if (data) {
                        this.stompSubs[destination] = client.subscribe(destination, this.handleMessages.apply(this, [destination]), data);
                    } else {
                        this.stompSubs[destination] = client.subscribe(destination, this.handleMessages.apply(this, [destination]));
                    }
                } catch (e) {
                    console.log(e);
                    this.snotifyService.error("Something went wrong!", "");
                }
            });
        }

        return new WebSocketSubscription(this.callbacks[destination].length - 1, destination,
            Tennio.closure(this, this.unsubscribe, destination, this.callbacks[destination].length - 1));
    }

    send(destination: any, headers: any, body: any) {
        this.getStompClient().then((client: any) => {
            try {
                client.send(destination, headers, body);
            } catch (ex) {
                this.snotifyService.error("Something went wrong!", "");
            }
        });
    }
}

export class WebSocketSubscription {
    public id: number;
    public destination: string;
    public unsubscribe: () => void;

    constructor(id: number, destination: string, unsubscribe: () => void) {
        this.id = id;
        this.destination = destination;
        this.unsubscribe = unsubscribe;
    }
}