import {DashboardComponent} from "../section/dashboard/dashboard.component";
import {GameComponent} from "../section/game/game.component";
import {LoginComponent} from "../component/login/login.component";
import {FrontpageComponent} from "../section/frontpage/frontpage.component";
import {TrainingsComponent} from "../section/trainings/trainings.component";
import {TournamentComponent} from "../section/tournament/tournament.component";
import {TournamentDetailComponent} from "../section/tournament/tournament-details.component";
import {AdminPanelComponent} from "../section/admin/admin-panel.component";
import {AuthGuard} from "../component/login/auth-guard.service";
import {PlayerSelectedGuard} from "../component/login/player-selected-guard.service";
import {Routes, RouterModule} from "@angular/router";
import {EquipmentComponent} from "../section/equipment/equipment.component";
import {SettingsComponent} from "../section/settings/settings.component";
import {ConfirmationComponent} from "../component/confirmation/confirmation.component";
import {CancerTimeGuard} from "../component/login/CancerTimeGuard";

export const routes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard, CancerTimeGuard]
    }, {
        path: 'tournament',
        component: TournamentComponent,
        canActivate: [AuthGuard, PlayerSelectedGuard, CancerTimeGuard]
    },
    {
        path: 'tournament-detail/:id',
        component: TournamentDetailComponent,
        canActivate: [AuthGuard, PlayerSelectedGuard, CancerTimeGuard]
    },
    {
        path: 'game',
        component: GameComponent,
        canActivate: [AuthGuard, CancerTimeGuard, CancerTimeGuard]
    },
    {
        path: 'game/:id',
        component: GameComponent,
        canActivate: [AuthGuard, CancerTimeGuard],
    },
    {
        path: 'equipment',
        component: EquipmentComponent,
        canActivate: [AuthGuard, CancerTimeGuard]
    },
    {
        path: 'trainings',
        component: TrainingsComponent,
        canActivate: [AuthGuard, PlayerSelectedGuard, CancerTimeGuard]
    },
    {
        path: 'admin',
        component: AdminPanelComponent,
        canActivate: [AuthGuard, CancerTimeGuard]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: '',
        component: FrontpageComponent
    },
    {
        path: 'home',
        component: FrontpageComponent
    },
    {
        path: 'logout',
        component: LoginComponent
    },
    {
        path: 'settings',
        component: SettingsComponent
    },
    {
        path: 'confirm',
        component: ConfirmationComponent
    }
];

RouterModule.forRoot(routes);