import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {FooterComponent} from "./component/footer/footer.component";
import {HeaderComponent} from "./component/header/header.component";
import {LoginComponent} from "./component/login/login.component";
import {MatchMakingComponent} from "./component/matchmaking/matchmaking.component";
import {GameMenu} from "./component/menu/game-menu.component";
import {OnlinePlayersComponent} from "./component/onlineplayers/onlineplayers.component";
import {DatePickerComponent} from "./component/ui/date-picker.component";
import {SliderComponent} from "./component/ui/slider.component";
import {AdminTournamentComponent} from "./section/admin/admin-tournament.component";
import {AdminTrainingsComponent} from "./section/admin/admin-trainings.component";
import {DashboardComponent} from "./section/dashboard/dashboard.component";
import {FrontpageComponent} from "./section/frontpage/frontpage.component";
import {GameComponent} from "./section/game/game.component";
import {ShopComponent} from "./section/equipment/shop/shop.component";
import {TournamentComponent} from "./section/tournament/tournament.component";
import {TournamentDetailComponent} from "./section/tournament/tournament-details.component";
import {JoinTournamentComponent} from "./section/tournament/join-tournament.component";
import {TrainingsComponent} from "./section/trainings/trainings.component";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {StatComponent} from "./component/player/player-stats.component";
import {HttpModule} from "@angular/http";
import {routes} from "./config/app.routes";
import {AdminPanelComponent} from "./section/admin/admin-panel.component";
import {PaginatorComponent} from "./component/ui/paginator.component";
import {AuthGuard} from "./component/login/auth-guard.service";
import {PlayerSelectedGuard} from "./component/login/player-selected-guard.service";
import {LoginService} from "./component/login/login.service";
import {NotificationService} from "./component/menu/notification.service";
import {WebSocketService} from "./utility/websocket.service";
import {TournamentService} from "./section/tournament/tournament.service";
import {TrainingsService} from "./section/trainings/trainings.service";
import {GameService} from "./section/game/game.service";
import {OnlinePlayersService} from "./component/onlineplayers/onlineplayers.service";
import {LoginFormComponent} from "./component/login/login.form.component";
import {AdminItemsComponent} from "./section/admin/admin-items.component";
import {ShopService} from "./section/equipment/shop/shop.service";
import {EquipmentComponent} from "./section/equipment/equipment.component";
import {LoadOutComponent} from "./section/equipment/loadout/loadout.component";
import {DndModule} from "ng2-dnd";
import {RankingsComponent} from "./component/rankings/rankings.component";
import {StatPopup} from "./component/player/player-stats.popup";
import {SettingsComponent} from "./section/settings/settings.component";
import {SettingsService} from "./section/settings/settings.service";
import {ConfirmationComponent} from "./component/confirmation/confirmation.component";
import {TimeService} from "./utility/timeservice";
import {Ng2Bs3ModalModule} from "ng2-bs3-modal/ng2-bs3-modal";
import {CountryPickerComponent} from "./component/ui/countrypicker/countrypicker.component";
import {CreatePlayerComponent} from "./component/createplayer/createplayer.component";
import {NotificationComponent} from "./component/menu/notification.component";
import {AchievementsService} from "./section/achievements/achievements.service";
import {AdminAchievementsComponent} from "./section/admin/admin-achievements.component";
import {NewStatComponent} from "./component/player/new.stats.component";
import {GameHistoryComponent} from "./section/gamehistory/game-history.component";
import {GameHistoryService} from "./section/gamehistory/game-history.service";
import {RestService} from "./utility/restservice";
import {AnonymousRestService} from "./utility/anonymousrestservice";
import {AdminSimulationsComponent} from "./section/admin/admin-simulations.component";
import {Tennio} from "./utility/Tennio";
import {environment} from "../environments/environment";
import {CancerTimeGuard} from "./component/login/CancerTimeGuard";
import {SnotifyModule, SnotifyService} from "ng-snotify";

@NgModule({
    imports: [
        BrowserModule,
        RouterModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(routes),
        DndModule.forRoot(),
        Ng2Bs3ModalModule,
        SnotifyModule
    ],
    providers: [
        AnonymousRestService,
        RestService,
        AuthGuard,
        PlayerSelectedGuard,
        LoginService,
        NotificationService,
        WebSocketService,
        TournamentService,
        TrainingsService,
        GameService,
        OnlinePlayersService,
        ShopService,
        SettingsService,
        TimeService,
        AchievementsService,
        GameHistoryService,
        CancerTimeGuard,
        SnotifyService
    ],
    declarations: [
        AppComponent,
        FooterComponent,
        HeaderComponent,
        LoginComponent,
        MatchMakingComponent,
        GameMenu,
        OnlinePlayersComponent,
        DatePickerComponent,
        SliderComponent,
        AdminTournamentComponent,
        AdminItemsComponent,
        AdminTrainingsComponent,
        DashboardComponent,
        FrontpageComponent,
        GameComponent,
        ShopComponent,
        TournamentComponent,
        TournamentDetailComponent,
        JoinTournamentComponent,
        TrainingsComponent,
        StatComponent,
        AdminPanelComponent,
        PaginatorComponent,
        LoginFormComponent,
        EquipmentComponent,
        LoadOutComponent,
        RankingsComponent,
        StatPopup,
        SettingsComponent,
        ConfirmationComponent,
        CountryPickerComponent,
        CreatePlayerComponent,
        NotificationComponent,
        AdminAchievementsComponent,
        NewStatComponent,
        GameHistoryComponent,
        AdminSimulationsComponent
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
    constructor() {
        if (environment.production) {
            Tennio.killConsole();
        }
    }
}