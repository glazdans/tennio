import {Component} from "@angular/core";
import {LoginService} from "../../component/login/login.service";
import {ShopComponent} from "./shop/shop.component";
import {LoadOutComponent} from "./loadout/loadout.component";

@Component({
    selector: 'tennio-admin',
    templateUrl: "equipment.component.html"
})

export class EquipmentComponent {
    constructor() {
    }
}