import {Component, OnInit} from "@angular/core";
import {ShopService} from "./shop.service";
import {Item} from "../../../model/item";
import {LoginService} from "../../../component/login/login.service";
import {SnotifyService} from "ng-snotify";

declare var $: any; //JQuery

@Component({
    selector: 'tennio-shop',
    templateUrl: 'shop.component.html'
})

export class ShopComponent implements OnInit {
    shopItems: Item[];
    viewingItem: Item;

    constructor(private _shopService: ShopService, private _loginService: LoginService, private snotifyService: SnotifyService) {

    }

    ngOnInit() {
        this._shopService.getShopItems().subscribe((items: Item[]) => {
            this.shopItems = items;
        });
    }

    onItemClick(shopItem: Item) {
        window.event.stopPropagation();
    }

    onItemDetails(shopItem: Item) {
        window.event.stopPropagation();
        this.viewingItem = shopItem;
        $("#item-modal").modal();
    }

    onItemBuyClick(shopItem: Item) {
        if(this._loginService.user.money >= shopItem.price) {
            this._shopService.buyShopItem(shopItem).subscribe((newItem) => {
                if (newItem && newItem.code && newItem.code == "ERROR") {
                    this.snotifyService.error("Error occured", newItem.message);
                } else if (newItem) {
                    this._loginService.user.money -= shopItem.price;
                    this._loginService.user.items.push(newItem);
                    this._loginService.setUser();
                    this.snotifyService.success("Success", "Successfully bought " + newItem.itemType.name);
                }
            })
        } else {
            this.notEnoughMoney();
        }
    }

    notEnoughMoney() {
        this.snotifyService.error("Error", "Not enough money!");
    }
}