import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {RestService} from "../../../utility/restservice";
import {Item} from "../../../model/item";
import {environment} from "../../../../environments/environment";


@Injectable()
export class ShopService {
    constructor(private rest: RestService) {
    }
    private _shopUrl = environment.apiUrl + '/shop/';
    private _adminUrl = environment.apiUrl + '/admin/item/';

    getShopItems(): Observable<Item[]> {
        return this.rest.get(this._shopUrl + 'items');
    }

    buyShopItem(shopItem: Item): Observable<any> {
        return this.rest.get(this._shopUrl + 'items/buy/' + shopItem.id);
    }

    saveShopItem(shopItem: Item) : Observable<boolean> {
        return this.rest.post(this._adminUrl + 'save', shopItem);
    }
}