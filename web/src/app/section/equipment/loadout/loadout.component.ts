import {Component, OnInit, OnDestroy} from "@angular/core";
import {LoginService} from "../../../component/login/login.service";
import {LoadOutService} from "./loadout.service";
import {User} from "../../../model/user";
import {EquipmentType, ItemEntity, ItemType} from "../../../model/item";
import {Player, PlayerEquipment} from "../../../model/player";
import {Tennio} from "../../../utility/Tennio";
import {WebSocketService, WebSocketSubscription} from "../../../utility/websocket.service";
import {SnotifyService} from "ng-snotify";

@Component({
    selector: 'tennio-loadout',
    providers: [LoadOutService],
    templateUrl: 'loadout.component.html',
    styleUrls: ['loadout.component.scss']
})
export class LoadOutComponent implements OnInit, OnDestroy {
    user: User;
    player: Player;
    readonly equipmentSlots: string[] = Tennio.enumNames(EquipmentType);
    activePlayerLoadout: PlayerEquipment;
    private playerSub: WebSocketSubscription;

    constructor(private _loginService: LoginService,
                private _loadOutService: LoadOutService,
                private _webSocketService: WebSocketService,
                private snotifyService: SnotifyService) {
    }

    ngOnInit() {
        this._loginService.getUser().subscribe((user) => {
            this.user = user;
            this.player = user.activePlayer;
            this.activePlayerLoadout = this.getActiveLoadout();
        });

        this.playerSub = this._webSocketService.subscribe("/user/notifications/activeplayer", Tennio.closure(this, (player: any) => {
            this.player = this._loginService.findPlayerById(player.id);
            this.activePlayerLoadout = this.getActiveLoadout();
        }));
    }

    ngOnDestroy() {
        this.playerSub.unsubscribe();
    }

    onItemDroppedInLoadout(event: {dragData: ItemEntity, mouseEvent: DragEvent}) {
        this.equipItem(event.dragData);
    }

    onItemDroppedInInventory(event: {dragData: ItemEntity, mouseEvent: DragEvent}) {
        this.unequipItem(this.findItemInInventory(event.dragData.id));
    }

    equipItem(item: ItemEntity) {
        if(item.itemType.type != ItemType.CONSUMABLE) {
            this._loadOutService.equipItem(item.id).subscribe((success) => {
                if (success) {
                    this.onEquippedItem(item);
                } else {
                    this.snotifyService.error("Error occured!", "Failed to equip item");
                }
            });
        } else {
            this.activateConsumable(item);
        }
    }

    findItemInInventory(itemId: number) : ItemEntity {
        for(let item of this.user.items) {
            if(item.id == itemId){
                return item;
            }
        }
    }

    getActiveLoadout(player: Player = this.player) {
        for (let loadout of player.equipments) {
            if (loadout.active) {
                return loadout;
            }
        }
    }

    activateConsumable(item: ItemEntity) {
        // TODO
    }

    getClassForInventoryItem(item: ItemEntity) {
        let cls = 'shopItem';
        if(item.playerId) {
            if(item.playerId == this.player.id && item.equipmentId == this.activePlayerLoadout.id) {
                cls += ' equipped';
            } else {
                cls += ' otherLoadout';
            }
        }
        return cls;
    }

    onEquippedItem(item: ItemEntity) {
        let loadout = this.getActiveLoadout();

        // if some item is already in the slot, set its inUse to false
        if(loadout.equippedItems[item.itemType.equipmentType]) {
            let oldItem = this.findItemInInventory(loadout.equippedItems[item.itemType.equipmentType].id);
            oldItem.playerId = null;
            oldItem.equipmentId = null;
        }

        if(item.playerId) {
            this.removeItemFromEquipment(item);
        }

        item.playerId = this.player.id;
        item.equipmentId = this.activePlayerLoadout.id;
        loadout.equippedItems[item.itemType.equipmentType] = item;

        // save changes
        this._loginService.replacePlayer(this.player);
        this._loginService.setUser();
    }

    removeItemFromEquipment(item: ItemEntity) {
        let oldPlayer = this._loginService.findPlayerById(item.playerId);
        if(!oldPlayer) {
            this.snotifyService.error("Error occured!", "Couldn't find player!");
            return;
        }
        let oldPlayerLoadout = Tennio.getSomethingById<PlayerEquipment>(oldPlayer.equipments, item.equipmentId);

        for(let i in oldPlayerLoadout.equippedItems) {
            let equippedItem = oldPlayerLoadout.equippedItems[i];
            if(equippedItem.id == item.id) {
                delete oldPlayerLoadout.equippedItems[i];
                break;
            }
        }

        if(oldPlayer.id == this.player.id) {
            this.player = oldPlayer;
        }

        this._loginService.replacePlayer(oldPlayer);
    }

    unequipItem(item: ItemEntity) {
        this._loadOutService.unequipItem(item.id).subscribe((success) => {
            if(success) {
                this.removeItemFromEquipment(item);
                item.playerId = null;
                item.equipmentId = null;
            } else {
                this.snotifyService.error("Error occured!", "Failed to unequip item!");
            }
        });
    }
}