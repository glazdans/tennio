import {Injectable} from "@angular/core";
import "rxjs/Rx";
import {RestService} from "../../../utility/restservice";
import {environment} from "../../../../environments/environment";

@Injectable()
export class LoadOutService {
    private url = environment.apiUrl;

    constructor(private rest: RestService) {
    }

    public equipItem(itemId: number) {
        return this.rest.get(this.url + "/player/equipment/equip/" + itemId);
    }

    public unequipItem(itemId: number) {
        return this.rest.get(this.url + "/player/equipment/unequip/" + itemId);
    }
}