import {Injectable} from "@angular/core";
import {RestService} from "../../utility/restservice";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {User} from "../../model/user";
import {Response} from "../../utility/response";

@Injectable()
export class SettingsService {
    private _url:string = environment.apiUrl + '/user';

    constructor(private rest: RestService) {
    }

    public updateUserSettings(newSettings: any): Observable<Response<User>> {
        return this.rest.post(this._url + '/update', newSettings);
    }

    public requestResendConfirmationEmail(): Observable<Response<boolean>> {
        return this.rest.get(this._url + '/resend');
    }
}