import {Component, OnInit, ViewChild} from "@angular/core";
import {User} from "../../model/user";
import {LoginService} from "../../component/login/login.service";
import {NgForm} from "@angular/forms";
import {SettingsService} from "./settings.service";
import {Response, ResponseCode} from "../../utility/response";
import {Tennio} from "../../utility/Tennio";
import {CountryPickerComponent} from "../../component/ui/countrypicker/countrypicker.component";
import {SnotifyService} from "ng-snotify";

@Component({
    selector: 'tennio-user-settings',
    templateUrl: "settings.component.html"
})

export class SettingsComponent implements OnInit {
    user: User;
    resent: boolean = false;

    @ViewChild('cp')
        countryPicker: CountryPickerComponent;

    constructor(private loginService: LoginService, private settingsService: SettingsService, private snotifyService: SnotifyService) {
    }

    ngOnInit() {
        this.loginService.getUser().subscribe(user => this.user = user);
    }

    set errorMsg(txt: string){
        this.snotifyService.error("Error occured!", txt);
    }

    resend() {
        this.settingsService.requestResendConfirmationEmail().subscribe(data => {
            if(data.code.toString() == "OK"){
                this.resent = true;
            }
        });
    }

    canSubmit(f: NgForm) {
        return f.value.password && ((f.value.newPassword && f.value.rnewPassword) || f.value.firstName || f.value.lastName || f.value.email);
    }

    submit(f: NgForm) {
        if(f.value.newPassword || f.value.rnewPassword) {
            if(f.value.newPassword != f.value.rnewPassword){
                this.errorMsg = "Change password fields don't match!";
                return;
            }
        }

        f.value.countryCode = this.countryPicker.countryCode;

        this.settingsService.updateUserSettings(f.value).subscribe((data:Response<User>) => {
            if(Tennio.compareEnum(ResponseCode, data.code, ResponseCode.ERROR)){
                if(data.message){
                    this.errorMsg = data.message;
                }
            } else if(Tennio.compareEnum(ResponseCode, data.code, ResponseCode.OK)){
                this.snotifyService.success("Success", "Successfully saved changes!");
                this.loginService.setUser(data.object);
                this.user = data.object;
                f.reset();
            }
        });
    }
}