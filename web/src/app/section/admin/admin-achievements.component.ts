import {Component} from "@angular/core";
import {Tennio} from "../../utility/Tennio";
import {Achievement, AchievementType} from "../../model/achievement";
import {AchievementsService} from "../achievements/achievements.service";
import {SnotifyService} from "ng-snotify";

declare var $: any;

@Component({
    selector: 'tennio-admin-achievements',
    templateUrl: "admin-achievements.component.html"
})

export class AdminAchievementsComponent {
    editView: boolean = false;
    itType: any = Object.keys(AchievementType).slice(Object.keys(AchievementType).length / 2);
    selectedItem: Achievement;

    constructor(private _service: AchievementsService, private snotifyService: SnotifyService) {
    }

    onEditClick() {
        return Tennio.closure(this, (item:Achievement) => {
            this.editView = true;
            this.selectedItem = item;
        });
    }

    onNewClick() {
        this.selectedItem = new Achievement();
        this.editView = true;
    }

    onCloseEditClick() {
        this.editView = false;
    }

    onDeleteClick() {
        return Tennio.closure(this, (item: Achievement) => {
            const id = this.snotifyService.confirm('Really delete?', item.name, {
                buttons: [
                    {
                        text: 'Yes', action: () => {
                        this._service.deleteAchievement(item).subscribe(() => this.snotifyService.remove(id))
                    }, bold: false
                    },
                    {
                        text: 'No', action: () => {
                        this.snotifyService.remove(id)
                    }, bold: true
                    },
                ]
            });
        });
    }

    save() {
        this._service.saveAchievement(this.selectedItem).subscribe((data:any) => {
            if(data) {
                this.selectedItem = data;
                this.editView = false;
            }
        });
    }
}