import {Component, AfterViewInit} from "@angular/core";
import {RestService} from "../../utility/restservice";

@Component({
    selector: 'tennio-admin-sims',
    templateUrl: "admin-simulations.component.html"
})

export class AdminSimulationsComponent implements AfterViewInit {
    private _simEndpoint = "http://localhost:8081/run-simulations/";
    data: any;
    theads = ['time', 'redSets', 'blueSets', 'redBackHandStrokes', 'redForeHandStrokes', 'redFailedServes', 'redServes', 'redMisses', 'blueBackHandStrokes', 'blueForeHandStrokes', 'blueFailedServes', 'blueServes', 'blueMisses'];

    constructor(private _rest: RestService) {
    }

    ngAfterViewInit() {
        this._rest.get(this._simEndpoint + '15').subscribe((data) => {
           this.data = data;
        });
    }

    getValues(data: any): any[] {
        return Object.keys(data).map(k => data[k]);
    }

}