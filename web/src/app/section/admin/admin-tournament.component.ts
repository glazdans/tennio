import {Component} from "@angular/core";
import {Tournament, TournamentPhase} from "../tournament/tournament";
import {TournamentService} from "../tournament/tournament.service";
import {Tennio} from "../../utility/Tennio";

@Component({
    selector: 'tennio-admin-tournament',
    templateUrl: "admin-tournament.component.html"
})

export class AdminTournamentComponent {
    tournaments: Tournament[];
    selectedTournament: Tournament;
    minStartTime: Date;
    minEndTime: Date;

    constructor(private _tournamentService: TournamentService) {
    }

    ngOnInit() {
        this._tournamentService.getAllTournaments().subscribe(data => {
            this.tournaments = data;
        });
    }

    onSelect(tournament: Tournament) {
        this.selectedTournament = tournament;
        this.setDates();
    }

    setDates() {
        if (this.selectedTournament.id) {
            this.minStartTime = this.selectedTournament.startDate > Date.now() ? new Date(Date.now()) : new Date(this.selectedTournament.startDate);
        } else {
            this.minStartTime = new Date();
        }

        this.minEndTime = new Date(this.minStartTime.getTime() + 2.592e+8); // 3 days
    }

    newTournament() {
        this.selectedTournament = new Tournament();
        this.setDates();
    }

    save() {
        if (!this.selectedTournament.id) {
            this._tournamentService.saveTournament(this.selectedTournament).subscribe(data => {
                this.tournaments.push(data);
                this.selectedTournament = null;
            });
        } else {
            this._tournamentService.saveTournament(this.selectedTournament).subscribe(data => {
                this.selectedTournament = null;
            });
        }
    }

    delete() {
        if (!this.selectedTournament.id) {
            this.selectedTournament = null;
        } else {
            this._tournamentService.deleteTournament(this.selectedTournament).subscribe(data => {
                for (let index in this.tournaments) { // in operator returns index
                    let tournament = this.tournaments[index];
                    if (tournament.id === this.selectedTournament.id) {
                        this.tournaments.splice(+index, 1);
                        break;
                    }
                }
                this.selectedTournament = null;
            });
        }
    }

    start() {
        if (this.selectedTournament.id) {
            this._tournamentService.startTournament(this.selectedTournament).subscribe(data => {
                if (data) {
                    this.selectedTournament.tournamentPhase = TournamentPhase.START;
                }
            });
        }
    }

    end() {
        if (this.selectedTournament.id) {
            this._tournamentService.endTournament(this.selectedTournament).subscribe(data => {
                if (data) {
                    this.selectedTournament.tournamentPhase = TournamentPhase.END;
                }
            });
        }
    }

    isDisabled(index:any) {
        if (!this.selectedTournament || !this.selectedTournament.tournamentPhase) {
            return false;
        }
        return index == 0 ? (this.selectedTournament.tournamentPhase.toString() != "REGISTER") :
            (this.selectedTournament.tournamentPhase.toString() == "END");
    }

    canStartTournament() {
        return Tennio.compareEnum(TournamentPhase, this.selectedTournament.tournamentPhase, TournamentPhase.REGISTER);
    }

    canEndTournament() {
        return Tennio.compareEnum(TournamentPhase, this.selectedTournament.tournamentPhase, TournamentPhase.START);
    }
}