import {Component} from "@angular/core";
import {TrainingsService} from "../trainings/trainings.service";
import {Training} from "../trainings/training";
import {LoginService} from "../../component/login/login.service";

@Component({
    selector: 'tennio-admin-trainings',
    templateUrl: "admin-trainings.component.html"
})

export class AdminTrainingsComponent {
    trainings: Training[];
    selectedTraining: Training;

    constructor(private _loginService: LoginService, private _trainingService: TrainingsService) {
    }

    ngOnInit() {
        this._trainingService.getTrainings().subscribe(data => {
            this.trainings = data;
        });
    }

    onSelect(training: Training) {
        this.selectedTraining = training;
    }

    newTraining() {
        this.selectedTraining = new Training();
    }

    save() {
        if (!this.selectedTraining.id) {
            this._trainingService.saveTraining(this.selectedTraining).subscribe(data => {
                this.trainings.push(data);
                this.selectedTraining = null;
            });
        } else {
            this._trainingService.saveTraining(this.selectedTraining).subscribe(data => {
                this.selectedTraining = null;
            });
        }
    }

    delete() {
        if (!this.selectedTraining.id) {
            this.selectedTraining = null;
        } else {
            this._trainingService.deleteTraining(this.selectedTraining).subscribe(data => {
                for (let index in this.trainings) { // in operator returns index
                    let training = this.trainings[index];
                    if (training.id === data) {
                        this.trainings.splice(+index, 1);
                        break;
                    }
                }
                this.selectedTraining = null;
            });
        }
    }

}