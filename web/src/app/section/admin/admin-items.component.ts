import {Component} from "@angular/core";
import {Item, EquipmentType, ItemType} from "../../model/item";
import {ShopService} from "../equipment/shop/shop.service";
import {Stats} from "../../model/player";
import {Tennio} from "../../utility/Tennio";

declare var $: any;

@Component({
    selector: 'tennio-admin-items',
    templateUrl: "admin-items.component.html"
})

export class AdminItemsComponent {
    editView: boolean = false;
    selectedItem: Item; //todo
    eqType: any = Object.keys(EquipmentType).slice(Object.keys(EquipmentType).length / 2);
    itType: any = Object.keys(ItemType).slice(Object.keys(ItemType).length / 2);
    statsList: any = Object.keys(new Stats()).splice(0, Object.keys(new Stats()).length / 2);

    constructor(private _shopService: ShopService) {
    }

    ngOnInit() {

    }

    onEditClick() {
        return Tennio.closure(this, (item:Item) => {
            this.editView = true;
            this.selectedItem = item;
        });
    }

    onNewClick() {
        this.selectedItem = new Item();
        this.selectedItem.stats = new Stats();
        this.editView = true;
    }

    onCloseEditClick() {
        this.editView = false;
    }

    statsModal() {
        if(!this.selectedItem.stats) {
            this.selectedItem.stats = new Stats();
        }
        $("#stats-modal").modal();
    }

    save() {
        if(this.selectedItem.type.toString() == "EQUIPABLE") {
            this.selectedItem.equipmentType = +EquipmentType[this.selectedItem.equipmentType];
        }
        this.selectedItem.type = +ItemType[this.selectedItem.type];

        this._shopService.saveShopItem(this.selectedItem).subscribe((data:any) => {
            if(data) {
                this.selectedItem = data;
                this.editView = false;
            }
        });
    }
}