import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {TournamentService} from "./tournament.service";
import {LoginService} from "../../component/login/login.service";
import {Tournament, TournamentPhase} from "./tournament";
import {User} from "../../model/user";
import {Tennio} from "../../utility/Tennio";
import {TimeService} from "../../utility/timeservice";
import {Player} from "../../model/player";

@Component({
    selector: 'tennio-tournament',
    templateUrl: "tournament.component.html"
})

export class TournamentComponent implements OnInit {
    tournaments: Tournament[];
    selectedTournament: Tournament;
    timeService: TimeService;
    registrations: TournamentRegistration[] = [];

    user: User;

    constructor(private _tournamentService: TournamentService, private _loginService: LoginService, private _router: Router, timeService: TimeService) {
        this.timeService = timeService;
    }

    ngOnInit() {
        this._loginService.getUser().subscribe(user => this.user = user);
        this._tournamentService.getAllTournaments().subscribe(Tennio.closure(this,this.onTournamentDataReceived));
    }

    onTournamentDataReceived(data: Tournament[]) {
        this.tournaments = data;

        for (let tournament of this.tournaments) {
            for (let player of this.user.players) {
                let position = this.isPlayerInTournament(player, tournament);
                if (position) {
                    this.registrations.push(new TournamentRegistration(player, tournament, position));
                }
            }
        }
    }

    isPlayerInTournament(player: Player = this.user.activePlayer, tournament: Tournament = this.selectedTournament) {
        let isPlayerInTournament = 0;

        for (let playerPos of tournament.playerPositions) {
            if (playerPos.player.id === player.id) {
                isPlayerInTournament = playerPos.position;
                break;
            }
        }
        return isPlayerInTournament;
    }

    shouldShowPopup() {
        return this.selectedTournament.tournamentPhase.toString() != "END" && !this.isPlayerInTournament();
    }

    onSelect(tournament: Tournament) {
        this.selectedTournament = tournament;
        if (this.isPlayerInTournament() || this.selectedTournament.tournamentPhase.toString() == "END") {
            this._router.navigate(['/tournament-detail', tournament.id]);
        }
    }

    getTournamentPhaseText(tournamentPhase: TournamentPhase): string {
        if(Tennio.compareEnum(TournamentPhase, tournamentPhase, TournamentPhase.END)) {
            return "Finished";
        }
        if(Tennio.compareEnum(TournamentPhase, tournamentPhase, TournamentPhase.START)) {
            return "In progress";
        }
        if(Tennio.compareEnum(TournamentPhase, tournamentPhase, TournamentPhase.REGISTER)) {
            return "Registration";
        }
    }

    isEndedRegistration(registration: TournamentRegistration): boolean {
        return registration.tournament == null || Tennio.compareEnum(TournamentPhase, registration.tournament.tournamentPhase, TournamentPhase.END);
    }
}

class TournamentRegistration {
    constructor(public player: Player, public tournament:Tournament, public position: number){
    }
}