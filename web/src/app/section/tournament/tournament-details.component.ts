import {Component, OnInit, Input, OnDestroy, AfterViewInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {TournamentService} from "./tournament.service";
import {LoginService} from "../../component/login/login.service";
import {Tournament, PlayerPositions} from "./tournament";
import {User} from "../../model/user";
import {Player} from "../../model/player";
import {Tennio} from "../../utility/Tennio";
import {WebSocketService, WebSocketSubscription} from "../../utility/websocket.service";
import {SnotifyService} from "ng-snotify";

declare var $: any; //JQuery

@Component({
    selector: 'tennio-tournament-detail',
    templateUrl: "tournament-detail.component.html"
})

export class TournamentDetailComponent implements OnInit, OnDestroy, AfterViewInit {
    private sub: any;
    @Input()
    tournament: Tournament;

    challengeOpponents: Player[];

    selectedPlayer: Player;

    user: User;

    gettingOpponents: boolean = false;

    activePlayerSub: WebSocketSubscription;

    constructor(private _tournamentService: TournamentService, private route: ActivatedRoute,
                private _loginService: LoginService, private _ws: WebSocketService, private snotifyService: SnotifyService) {
    }

    ngOnInit(): any {
        this.sub = this.route.params.subscribe(params => {
            let id = +params['id'];
            this._tournamentService.getTournament(id).subscribe(data => {
                this.tournament = data;
                this.sortPlayers(this.tournament);
                this.onChangePlayer();
            });
        });
        this._loginService.getUser().subscribe(user => this.user = user);
    }

    ngAfterViewInit(): void {
        this.activePlayerSub = this._ws.subscribe('/user/notifications/activeplayer', Tennio.closure(this, this.onActivePlayer));
    }

    onActivePlayer(resp:any) {
        if (resp.success) {
            this.onChangePlayer();
        }
    }

    onChangePlayer(force: boolean = false) {
        this.challengeOpponents = [];
        if (this.tournament.tournamentPhase.toString() === "START" && (this.isPlayerInTournament() || force) && !this.gettingOpponents) {
            this.gettingOpponents = true;
            this._tournamentService.getTournamentOpponents(this.tournament.id).subscribe(data => {
                if(data instanceof Array) {
                    this.challengeOpponents = data;
                } else {
                    this.challengeOpponents = [];
                }

                this.gettingOpponents = false;
            });
        }
    }

    updateTournament() {
        return Tennio.closure(this, this.setTournament);
    }

    setTournament(tournament: Tournament) {
        this.tournament = tournament;
        this.sortPlayers(this.tournament);
        this.onChangePlayer(true);
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
        this.activePlayerSub.unsubscribe();
    }

    isPlayerInTournament(): boolean {
        let playerId = this.user.activePlayer.id;
        for (let player of this.tournament.playerPositions) {
            if (player.player.id === playerId) {
                return true;
            }
        }
        return false;
    }

    shouldShowPopup() {
        return this.tournament.tournamentPhase.toString() != "END" && !this.isPlayerInTournament();
    }

    challengeOpponent() {
        this.challengeOpponents = [];
        this._tournamentService
            .challengeOpponent(this.tournament.id, this.selectedPlayer.id, this._loginService.user.activePlayer.id)
            .subscribe(data => {
                if(data) {
                    this.challengeOpponents = [];
                    this.snotifyService.success("Successfully challenged " + this.selectedPlayer.name, "waiting for response...",
                        {
                            timeout: 30000,
                            closeOnClick: false,
                            pauseOnHover: false
                        });
                }
            });
        $("#player-modal").modal('hide');
    }

    onPlayerSelect(player: PlayerPositions) {
        this.selectedPlayer = player.player;
        $("#player-modal").modal();
    }

    canChallenge(player: Player): boolean {
        if(player == null) {
            return false;
        }
        if (this.challengeOpponents == undefined) {
            return false;
        }
        for (let opponent of this.challengeOpponents) {
            if (player.id === opponent.id) {
                return true;
            }
        }
        return false;
    }

    sortPlayers(tournaments: Tournament) {
        tournaments.playerPositions.sort(this.comparePositions);
    }

    comparePositions(a:any, b:any) {
        if (a.position < b.position)
            return -1;
        if (a.position > b.position)
            return 1;
        return 0;
    }
}