import {Component, Input, OnInit, OnChanges} from "@angular/core";
import {Router} from "@angular/router";
import {TournamentService} from "./tournament.service";
import {Tournament} from "./tournament";

declare var $: any; //JQuery

@Component({
    selector: 'tennio-join-tournament',
    templateUrl: "join-tournament.component.html"
})

export class JoinTournamentComponent implements OnInit, OnChanges {
    @Input()
    tournament: Tournament;
    @Input()
    show: boolean;
    @Input()
    update:any;

    showing: boolean = false;

    constructor(private _tournamentService: TournamentService, private _router: Router) {
    }

    ngOnInit() {
    }

    ngOnChanges() {
        if (this.show && !this.showing) {
            this.showing = true;
            $("#tournament-modal").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    }

    onDeclineJoin() {
        this.showing = false;
        this._router.navigate(['/tournament']);
    }

    onJoinTournament() {
        this._tournamentService.joinTournament(this.tournament.id).subscribe(response => {
            this._router.navigate(['/tournament-detail', this.tournament.id]);
            this.showing = false;
            if (this.update) {
                this.update(response);
            }
        });
    }
}