import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {Tournament} from "./tournament";
import {ServerInfo} from "../game/websocket/server-info";
import {Player} from "../../model/player";
import {RestService} from "../../utility/restservice";
import {environment} from "../../../environments/environment";

@Injectable()
export class TournamentService {
    constructor(private rest: RestService) {
    }
    private _tournamentUrl = environment.apiUrl + '/tournament/';
    private _adminTournamentUrl = environment.apiUrl + '/admin/tournament/';

    getAllTournaments(): Observable<Tournament[]> {
        return this.rest.get(this._tournamentUrl);
    }

    getTournament(id: number): Observable<Tournament> {
        return this.rest.get(this._tournamentUrl + id.toString());
    }

    saveTournament(tournament: Tournament): Observable<any> {
        return this.rest.post(this._adminTournamentUrl + "save", JSON.stringify(tournament));
    }

    deleteTournament(tournament: Tournament) {
        return this.rest.get(this._adminTournamentUrl + "delete/" + tournament.id);
    }

    startTournament(tournament: Tournament) {
        return this.rest.get(this._adminTournamentUrl + "start/" + tournament.id);
    }

    endTournament(tournament: Tournament) {
        return this.rest.get(this._adminTournamentUrl + "end/" + tournament.id);
    }

    joinTournament(tournamentId: number): Observable<any> {
        return this.rest.get(this._tournamentUrl + "add-player/" + tournamentId.toString());
    }

    getTournamentOpponents(tournamentId: number): Observable<Player[]> {
        return this.rest.get(this._tournamentUrl + "get-opponents/" + tournamentId.toString());
    }

    challengeOpponent(tournamentId: number, opponentId: number, challengerId: number): Observable<ServerInfo> {
        return this.rest.post(this._tournamentUrl + "challenge/" + tournamentId.toString()
            + "?challengerId=" + challengerId + "&opponentId=" + opponentId, {});
    }
}
