import {Player} from "../../model/player";

export class Tournament {
    id: number;
    name: string;
    players: Player[];

    playerPositions: PlayerPositions[];
    maxPlayers: number;

    tournamentPhase: TournamentPhase;
    startDate: number;
    endDate: number;
    prizePool: number;
    entryFee: number;
}

export enum TournamentPhase {
    REGISTER, START, END
}

export class PlayerPositions {
    player: Player;
    position: number;
}