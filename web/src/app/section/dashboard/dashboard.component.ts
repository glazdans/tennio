import {Component, OnInit} from "@angular/core";
import {LoginService} from "../../component/login/login.service";
import {AchievementsService} from "../achievements/achievements.service";
import {Achievement, AchievementProgress} from "../../model/achievement";
import {User} from "../../model/user";
import {isNullOrUndefined} from "util";
import {isNumeric} from "rxjs/util/isNumeric";

@Component({
    selector: 'my-dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    possibleAchievements: Achievement[] = [];
    playerAchievementProgress: AchievementProgress[] = [];
    user: User;

    constructor(public _loginService: LoginService, private _as: AchievementsService) {
    }

    getWinRate() {
        if (!this.user) {
            return 0;
        }
        if(isNullOrUndefined(this.user.wonGameCount) || isNullOrUndefined(this.user.lostGameCount) || !isNumeric(this.user.lostGameCount) || !isNumeric(this.user.wonGameCount)){
            return null;
        }
        if(!this.user || this.user.wonGameCount == 0) {
            return 0;
        }
        if(this.user.lostGameCount == 0 && this.user.wonGameCount != 0) {
            return 100;
        }
        return Math.round((+this.user.wonGameCount / (+this.user.wonGameCount + +this.user.lostGameCount)) * 100);
    }

    ngOnInit() {
        this._loginService.getUser().subscribe(user => {
            this.user = user;
            this.playerAchievementProgress = user.achievements || [];
            this._as.getPossibleAchievements().subscribe(ac => {
                if(!ac || !ac.length) {
                    return;
                }

                this.possibleAchievements = ac;
                let i = this.playerAchievementProgress.length;
                if(i < 5) { // pad list with some achievements if player has started less than 5
                    for(let ac of this.possibleAchievements) {
                        if(this._as.hasUserProgressed(user, ac)) {
                            continue; // skip those which user has already progressed in as they may be already in the list
                        }
                        if(i ++ >= 5) {
                            break;
                        }
                        this.playerAchievementProgress.push(AchievementProgress.fromAchievement(ac));
                    }
                }
            });
        });
    }
}
