import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {LoginService} from "../../component/login/login.service";
import {loginFormView} from "../../component/login/login.form.component";

@Component({
    selector: 'tennio-frontpage',
    templateUrl: "frontpage.component.html",
    styleUrls: ['frontpage.component.scss']
})

export class FrontpageComponent implements OnInit {
    view = loginFormView.SHORT_REGISTER;

    constructor(public router: Router, public loginService: LoginService) {
    }

    ngOnInit() {
        if (this.loginService.isLoggedIn() && this.router.url == '/') {
            this.router.navigate(['/dashboard']);
        }
    }
}