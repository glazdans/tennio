import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {Training} from "./training";
import {TrainingEvent} from "./training-event";
import {Response} from "../../utility/response";
import {RestService} from "../../utility/restservice";
import {environment} from "../../../environments/environment";

@Injectable()
export class TrainingsService {
    constructor(private rest: RestService) {
    }
    private url = environment.apiUrl + '/trainings';
    private adminUrl = environment.apiUrl+ '/admin/trainings';

    getTrainings(): Observable<Training[]> {
        return this.rest.get(this.url + "/list");
    }

    saveTraining(training: Training): Observable<Training> {
        return this.rest.post(this.adminUrl + "/", JSON.stringify(training));
    }

    deleteTraining(training: Training): Observable<number> {
        return this.rest.delete(this.adminUrl + "/" + training.id);
    }

    getActiveTrainings() {
        return Observable.forkJoin(
            this.rest.get(this.url + "/list"),
            this.rest.get(this.url + "/get")
        );
    }

    startTraining(id: number): Observable<Response<TrainingEvent>> {
        return this.rest.post(this.url + "/start/" + id, "");
    }

    finishTraining(id: number, playerId: number): Observable<Response<TrainingEvent>> {
        return this.rest.post(this.url + "/finish/" + id + "?playerId=" + playerId, "");
    }
}