import {Component, OnInit, OnDestroy} from "@angular/core";
import {Training, TrainingType} from "./training";
import {TrainingEvent} from "./training-event";
import {TrainingsService} from "./trainings.service";
import {LoginService} from "../../component/login/login.service";
import {User} from "../../model/user";
import {Player} from "../../model/player";
import {ResponseCode} from "../../utility/response";
import {WebSocketService, WebSocketSubscription} from "../../utility/websocket.service";
import {Tennio} from "../../utility/Tennio";
import {NotificationAttributeType} from "../../component/menu/notification";
import {SnotifyService} from "ng-snotify";

declare let $: any;

@Component({
    selector: 'tennio-trainings',
    templateUrl: "trainings.component.html",
    styleUrls: ["trainings.component.scss"]
})

export class TrainingsComponent implements OnInit, OnDestroy {
    freeTrainings: Training[] = [];
    paidTrainings: Training[] = [];
    notificationUpdateSub: WebSocketSubscription;
    lastTS: number;

    activeEvents: TrainingEvent[] = [];
    user: User;
    statsIncPlayer: Player;

    constructor(private trainingsService: TrainingsService, private loginService: LoginService,
                private wsService: WebSocketService, private snotifyService: SnotifyService) {
        this.lastTS = Date.now();
    }

    get mixedTrainings() {
        return this.freeTrainings.concat(this.paidTrainings);
    }

    updateAnimations() {
        requestAnimationFrame(() => {
            this.updateAnimations()
        });

        if (this.activeEvents.length) {
            this.activeEvents.forEach((event) => {
                event.percentage = this.getEventPercentage(event);
            });
        }
    }

    getEventPercentage(event: TrainingEvent): number {
        let currentTime = Date.now();
        let percentage = ((currentTime - +event.startDate) / (+event.completeDate - +event.startDate)) * 100;
        return percentage > 100 ? 100 : percentage;
    }

    ngOnInit() {
        this.loginService.getUser().subscribe(data => {
            this.user = data;
            if (this.user.activePlayer) {
                this.trainingsService.getActiveTrainings().subscribe(data => {
                    this.mapEvents(data[0], data[1]);
                    this.updateAnimations();
                });
            }
        });

        this.notificationUpdateSub = this.wsService.subscribe("/user/notifications/update", Tennio.closure(this, this.onUpdate));
    }

    ngOnDestroy() {
        this.notificationUpdateSub.unsubscribe();
    }

    onUpdate(response: any) {
        let playerId = response.notificationAttributes[NotificationAttributeType[NotificationAttributeType.PlayerId]];

        if (playerId) {
            let player: any = null;
            for (let p of this.user.players) {
                if (p.id == playerId) {
                    player = p;
                }
            }
            if (player !== null) {
                player.stats = JSON.parse(response.notificationAttributes[NotificationAttributeType[NotificationAttributeType.Stats]]);
                this.statsIncPlayer = player;
                this.loginService.replacePlayer(player);
            } else {
                console.error("no player id!");
            }
        }
    }

    mapEvents(trainings: Training[], events: TrainingEvent[]) {
        this.activeEvents = [];
        this.freeTrainings = [];
        this.paidTrainings = [];

        for (let training of trainings) {
            if (training.trainingType.toString() === TrainingType[TrainingType.FREE]) {
                this.freeTrainings.push(training);
            } else {
                this.paidTrainings.push(training);
            }
        }

        this.activeEvents = events;
        for (let activeEvent of this.activeEvents) {
            let training = this.getTrainingByIdAndType(activeEvent.training.id, activeEvent.training.trainingType);
            if (!training.activeEvents) {
                training.activeEvents = [];
            }
            training.activeEvents.push(activeEvent);
        }
    }

    getTrainingByIdAndType(id: number, type: TrainingType): Training {
        if (Tennio.compareEnum(TrainingType, type, TrainingType.FREE)) {
            return Tennio.getSomethingById<Training>(this.freeTrainings, id);
        } else if (Tennio.compareEnum(TrainingType, type, TrainingType.PAID)) {
            return Tennio.getSomethingById<Training>(this.paidTrainings, id);
        }
    }

    onTrainingClick(training: Training) {
        if (!this.hasMoneyForTraining(training)) {
            return;
        }

        this.trainingsService.startTraining(training.id).subscribe(data => {
            if (data.code.toString() == "ERROR") {
                this.snotifyService.error("Error occured", data.message);
            } else {
                this.activeTrainingEvent(data.object, training);
            }
        });
    }

    onFinishClick(event: TrainingEvent) {
        this.statsIncPlayer = Object.assign({}, this.loginService.findPlayerById(event.player.id));
        $("#stats-modal").modal();

        let oldEvents = this.getTrainingByIdAndType(event.training.id, event.training.trainingType).activeEvents;

        for (let i in oldEvents) {
            let oldEvent = oldEvents[i];
            if (event.id == oldEvent.id) {
                oldEvents.splice(+i, 1);
            }
        }

        setTimeout(Tennio.closure(this, this.finish, event), 500);
    }

    private finish(event: TrainingEvent) {
        this.trainingsService.finishTraining(event.id, event.player.id).subscribe(data => {
            if (data.code.toString() == ResponseCode[ResponseCode.OK]) {
                event.isActive = false;
                event.completeDate = null;
                this.activeEvents.splice(this.activeEvents.indexOf(event), 1);
            } else {
                this.snotifyService.error("Training event error occured!", "");
            }
        });
    }

    isEventDone(timeStamp: any): boolean {
        if(this.lastTS >= timeStamp){
            return true;
        }
        this.lastTS = Date.now();
        return this.lastTS >= timeStamp;
    }

    activeTrainingEvent(event: TrainingEvent, training: Training) {
        this.activeEvents.push(event);
        if (event.training.trainingType.toString() === TrainingType[TrainingType.PAID]) {
            this.loginService.getUser().subscribe(data => {
                data.money -= event.training.cost;
                this.loginService.setUser(data);
            });
        }

        if (!training.activeEvents) {
            training.activeEvents = [];
        }
        training.activeEvents.push(event);
    }

    public hasMoneyForTraining(training: Training): boolean {
        return this.user.money >= training.cost;
    }
}