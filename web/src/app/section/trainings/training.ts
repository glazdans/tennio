import {Stats} from "../../model/player";
import {TrainingEvent} from "./training-event";

export class Training {
    id: number;
    code: string;
    statIncrease: Stats;
    length: number;
    trainingType: TrainingType;
    cost: number;
    expDelta: number;
    activeEvents: TrainingEvent[] = [];

    constructor() {
        this.statIncrease = new Stats();
    }

}

export enum TrainingType{
    FREE, PAID
}