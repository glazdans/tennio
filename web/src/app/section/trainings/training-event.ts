import {Training} from "./training";
import {Player} from "../../model/player";

export class TrainingEvent {
    id: number;
    training: Training;

    startDate: string;
    completeDate: string;

    isActive: boolean;

    percentage: number;

    player: Player;
}