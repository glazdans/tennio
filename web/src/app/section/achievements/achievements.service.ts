import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {RestService} from "../../utility/restservice";
import {LoginService} from "../../component/login/login.service";
import {environment} from "../../../environments/environment";
import {Achievement, AchievementProgress} from "../../model/achievement";
import {User} from "../../model/user";

@Injectable()
export class AchievementsService {
    constructor(private loginService: LoginService, private rest: RestService) {
    }
    private _achievementsUrl = environment.apiUrl + '/achievement/';
    private _adminUrl = environment.apiUrl + '/admin/achievement/';

    getPossibleAchievements(): Observable<Achievement[]> {
        return this.rest.get(this._achievementsUrl + 'items');
    }

    saveAchievement(achievement: Achievement) : Observable<boolean> {
        return this.rest.post(this._adminUrl + 'save', achievement);
    }

    deleteAchievement(achievement: Achievement): Observable<void> {
        return this.rest.delete(this._adminUrl + achievement.id);
    }

    hasUserProgressed(user: User, achievement: Achievement): boolean {
        if(!user.achievements) {
            return false;
        }
        for(let ap of user.achievements) {
            if(ap.achievementType.id == achievement.id) {
                return true;
            }
        }
        return false;
    }
    onUpdate(progress:AchievementProgress) {
        if(this.hasUserProgressed(this.loginService.user, progress.achievementType)) {
            for(let i in this.loginService.user.achievements) {
                let ap = this.loginService.user.achievements[i];
                if(ap.achievementType.id == progress.achievementType.id) {
                    this.loginService.user.achievements.splice(+i, 1);
                    this.loginService.user.achievements.unshift(progress);
                    break;
                }
            }
        } else {
            if(!this.loginService.user.achievements) {
                this.loginService.user.achievements = [];
            }
            this.loginService.user.achievements.unshift(progress);
        }
        this.loginService.setUser();
    }
}