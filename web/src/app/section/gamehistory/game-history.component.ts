import {Component, OnInit, Input} from "@angular/core";
import {LoginService} from "../../component/login/login.service";
import {Tennio} from "../../utility/Tennio";
import {GameHistoryService} from "./game-history.service";
import {GameEvent, GameStatus, GameType} from "../game/websocket/server-info";
import {Player} from "../../model/player";
import {Score} from "../game/vector2";

declare let moment: any;

@Component({
    selector: 'tennio-game-history',
    templateUrl: 'game-history.component.html',
    styleUrls: ['game-history.component.scss']
})
export class GameHistoryComponent implements OnInit {
    @Input()
        short: boolean = true;
    gameHistory: GameEvent[];
    shortEvents: AdaptedEvent[];
    events: AdaptedEvent[];

    constructor(private _ghService: GameHistoryService, private _loginService: LoginService) {
    }

    getEvents(){
        return (this.short ? this.shortEvents : this.events);
    }

    ngOnInit() {
        this.events = [];
        this._ghService.getGameHistory().subscribe((data) => {
            this.gameHistory = data;

            for(let event of data) {
                if (Tennio.compareEnum(GameType, event.gameType, GameType.AI)) {
                    event.player2 = new Player();
                    event.player2.name = "BOT";
                    event.player2.id = -1;

                    if (event.losingPlayer == null) {
                        event.losingPlayer = event.player2;
                    }
                    if (event.winningPlayer == null) {
                        event.winningPlayer = event.player2;
                    }
                }

                let ae = new AdaptedEvent();
                if (Tennio.compareEnum(GameStatus, event.status, GameStatus.WAITING) || Tennio.compareEnum(GameStatus, event.status, GameStatus.STARTED) || !event.winningPlayer) {
                    continue;
                }
                if(this.isWinningPlayer(event)) {
                    ae.yourPlayer = event.winningPlayer;
                    if(event.player1.id == ae.yourPlayer.id){
                        ae.yourScore = event.player1Score;
                    } else {
                        ae.yourScore = event.player2Score;
                    }

                    ae.vsPlayer = event.losingPlayer;
                    if(event.player1.id == ae.vsPlayer.id){
                        ae.vsScore = event.player1Score;
                    } else {
                        ae.vsScore = event.player2Score;
                    }
                } else {
                    ae.vsPlayer = event.winningPlayer;
                    if(event.player1.id == ae.vsPlayer.id){
                        ae.vsScore = event.player1Score;
                    } else {
                        ae.vsScore = event.player2Score;
                    }

                    ae.yourPlayer = event.losingPlayer;
                    if(event.player1.id == ae.yourPlayer.id){
                        ae.yourScore = event.player1Score;
                    } else {
                        ae.yourScore = event.player2Score;
                    }
                }
                ae.winnerId = ae.vsScore.matches > ae.yourScore.matches ? ae.vsPlayer.id : ae.yourPlayer.id;
                ae.date = moment(event.gameStart).format('DD/MM/YYYY hh:mm:ss');
                if (Tennio.compareEnum(GameStatus, event.status, GameStatus.ENDED)) {
                    ae.status = ae.yourPlayer == event.losingPlayer ? "You lost" : "You won";
                } else if (Tennio.compareEnum(GameStatus, event.status, GameStatus.PLAYER1_SURRENDERED)) {
                    ae.status = event.player1.id == ae.yourPlayer.id ? "You surrendered" : "Opponent surrendered";
                } else if (Tennio.compareEnum(GameStatus, event.status, GameStatus.PLAYER2_SURRENDERED)) {
                    ae.status = event.player2.id == ae.yourPlayer.id ? "You surrendered" : "Opponent surrendered";
                }
                this.events.push(ae);
            }
            if(this.events.length > 5) {
                this.shortEvents = this.events.slice(0, 5);
            } else {
                this.shortEvents = this.events;
            }
        })
    }

    isWinningPlayer(ge: GameEvent) : boolean {
        return (Tennio.getSomethingById(this._loginService.user.players, ge.winningPlayer.id) != null);
    }
}

class AdaptedEvent {
    yourPlayer: Player;
    vsPlayer: Player;
    yourScore: Score;
    vsScore: Score;
    winnerId: number;
    date: string;
    status: string;
}