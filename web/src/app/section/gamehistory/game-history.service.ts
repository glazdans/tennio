import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {environment} from "../../../environments/environment";
import {GameEvent} from "../game/websocket/server-info";
import {RestService} from "../../utility/restservice";


@Injectable()
export class GameHistoryService {
    constructor(private rest: RestService) {
    }

    private _gameHistoryUrl = environment.apiUrl + '/gamehistory/recent';

    getGameHistory(): Observable<GameEvent[]> {
        return this.rest.get(this._gameHistoryUrl);
    }
}