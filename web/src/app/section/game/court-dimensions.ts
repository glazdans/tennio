export const colors: any = {
    tennioGreen: "#66ae30"
};

let dimensions: any = {
    length: 2 * 11.89,
    heightSingles: 8.23,
    heightDoubles: 10.97,
    serviceBoxLength: 6.4,
    totalHeight: 19.66,
    totalWidth: 36.58,
    netHeightAtPosts: 1.067,
    netHeightAtMiddle: 0.914,
    netLength: 10.97 + 2 * 0.914,
    scale: 20,
    playerSize: 0.8,
    lineWidth: 0.2,
    halfHeight: 0,
    halfWidth: 0,
    scaledHalfHeight: 0,
    scaledHalfWidth: 0,
    scaledHalfNetLength: 0,
    scaledNetHeightAtPosts: 0,
    scaledLineWidth: 0,
    sideViewLineBottomPad: 60,
    updateDimensions() {
        this.halfHeight = this.totalHeight / 2;
        this.halfWidth = this.totalWidth / 2;
        this.scaledHalfHeight = this.totalHeight / 2 * this.scale;
        this.scaledHalfWidth = this.totalWidth / 2 * this.scale;
        this.scaledHalfNetLength = this.netLength / 2 * this.scale;
        this.scaledNetHeightAtPosts = this.netHeightAtPosts * this.scale;
        this.scaledLineWidth = this.lineWidth * this.scale;
        this.sideViewLineBottomPad = this.scale * 1.9;
    }
};
dimensions.updateDimensions();

export const courtDimensions = dimensions;

class CourtBox {
    protected x: number;
    protected y: number;
    protected width: number;
    protected height: number;

    public scaledX: number;
    public scaledY: number;
    public scaledW: number;
    public scaledH: number;

    constructor(x: any, y: any, width: any, height: any) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.updateScale();
    }

    public updateScale() {
        this.scaledX = this.x * courtDimensions.scale;
        this.scaledY = this.y * courtDimensions.scale;
        this.scaledW = this.width * courtDimensions.scale;
        this.scaledH = this.height * courtDimensions.scale;
    }

    public draw(ctx: CanvasRenderingContext2D) {
        ctx.beginPath();
        ctx.strokeStyle = "white";
        ctx.lineWidth = courtDimensions.scaledLineWidth;

        ctx.rect(this.scaledX + (ctx.lineWidth / 2), this.scaledY + (ctx.lineWidth / 2), this.scaledW - ctx.lineWidth, this.scaledH - ctx.lineWidth);
        ctx.stroke();
    }

    public fill(ctx: CanvasRenderingContext2D, color: string) {
        ctx.fillStyle = color;
        ctx.fillRect(this.scaledX, this.scaledY, this.scaledW, this.scaledH);
    }

    public isBallInBox(iX: any, iY: any) {
        iX = (iX + courtDimensions.halfWidth) * courtDimensions.scale;
        iY = (iY + courtDimensions.halfHeight) * courtDimensions.scale;

        return (iX <= this.scaledX + this.scaledW && iX >= this.scaledX && iY >= this.scaledY && iY <= this.scaledY + this.scaledH);
    }
}

class TopServiceBox extends CourtBox {
    public updateScale() {
        this.scaledX = this.x * courtDimensions.scale + (courtDimensions.scaledLineWidth / 2);
        this.scaledY = this.y * courtDimensions.scale;
        this.scaledW = this.width * courtDimensions.scale - (courtDimensions.scaledLineWidth / 2);
        this.scaledH = this.height * courtDimensions.scale + (courtDimensions.scaledLineWidth / 2);
    }
}

class BottomServiceBox extends CourtBox {
    public updateScale() {
        this.scaledX = this.x * courtDimensions.scale + (courtDimensions.scaledLineWidth / 2);
        this.scaledY = this.y * courtDimensions.scale - (courtDimensions.scaledLineWidth / 2);
        this.scaledW = this.width * courtDimensions.scale - (courtDimensions.scaledLineWidth / 2);
        this.scaledH = this.height * courtDimensions.scale + (courtDimensions.scaledLineWidth / 2);
    }
}

export const courtBoxes = {
    singles: new CourtBox(
        courtDimensions.halfWidth - (courtDimensions.length / 2),
        courtDimensions.totalHeight / 2 - courtDimensions.heightSingles / 2,
        courtDimensions.length, courtDimensions.heightSingles
    ),
    doubles: new CourtBox(
        courtDimensions.halfWidth - (courtDimensions.length / 2),
        courtDimensions.halfHeight - courtDimensions.heightDoubles / 2,
        courtDimensions.length, courtDimensions.heightDoubles
    ),
    bottomServiceBox: new BottomServiceBox(
        courtDimensions.halfWidth - courtDimensions.serviceBoxLength,
        courtDimensions.halfHeight,
        2 * courtDimensions.serviceBoxLength, courtDimensions.heightSingles / 2
    ),
    topServiceBox: new TopServiceBox(
        courtDimensions.halfWidth - courtDimensions.serviceBoxLength,
        courtDimensions.halfHeight - courtDimensions.heightSingles / 2,
        2 * courtDimensions.serviceBoxLength, courtDimensions.heightSingles / 2
    ),
    draw: (ctx: any) => {
        courtBoxes.doubles.fill(ctx, "#426046");
        courtBoxes.singles.draw(ctx);
        courtBoxes.doubles.draw(ctx);
        courtBoxes.bottomServiceBox.draw(ctx);
        courtBoxes.topServiceBox.draw(ctx);
    },
    updateScale: () => {
        courtBoxes.doubles.updateScale();
        courtBoxes.singles.updateScale();
        courtBoxes.bottomServiceBox.updateScale();
        courtBoxes.topServiceBox.updateScale();
    }
};