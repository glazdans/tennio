export interface Vector2 {
    x: number;
    y: number;
    z: number;
}

export interface Score {
    points: number;
    games: number;
    sets: number;
    matches: number;
}

export class Snapshot {
    time: number;
    redPlayer: Vector2;
    bluePlayer: Vector2;
    ball: Vector2;
    ballWaiting: boolean;
    bouncePoint: BouncePoint;
    fitness: number;
    ballState: BallState; //0 - default 1 - superserve

    constructor() {
        this.clear();
    }

    clear() {
        this.redPlayer = {x: 0, y: 0, z: 0};
        this.bluePlayer = {x: 0, y: 0, z: 0};
        this.ball = {x: 0, y: 0, z: 0};
        this.bouncePoint = {x: 0, y: 0, z: 0, bounceIn: false};
        this.ballWaiting = true;
        this.fitness = 1000;
    }

    fromData(data: any): void {
        this.time = data.time;
        this.redPlayer.x = data.redX;
        this.redPlayer.y = data.redY;
        this.bluePlayer.x = data.blueX;
        this.bluePlayer.y = data.blueY;
        this.ball.x = data.ballX;
        this.ball.y = data.ballY;
        this.ball.z = data.ballZ;
        this.ballWaiting = data.ballWaiting;
        this.bouncePoint.x = data.bouncePointX;
        this.bouncePoint.y = data.bouncePointY;
        this.bouncePoint.bounceIn = data.bounceIn;
        this.fitness = data.fitness;
        this.ballState = data.ballState;
    }
}

export class BouncePoint implements Vector2 {
    x: number;
    y: number;
    z: number;
    bounceIn: boolean;
}

export enum BallState {
    Default, SuperServe
}

export enum GameEndCause {
    PlayerWon, PlayerSurrender
}

export enum PauseReason {
    NONE, SwitchingSides, PlayerRedPause, PlayerBluePause
}