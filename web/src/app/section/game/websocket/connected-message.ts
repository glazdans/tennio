import {WebSocketMessage} from "./websocket-message";
import {Player} from "./Player";
export class ConnectedMessage extends WebSocketMessage {
    public type:string = "CONNECTED";

    constructor(player: Player) {
        super();
        this.payload = player;
    }
}