export class WebSocketMessage {
    public type:string = "";
    public payload: any = {};
    public jwt: string;
}