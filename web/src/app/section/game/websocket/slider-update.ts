import {WebSocketMessage} from "./websocket-message";

export class SliderUpdateMessage extends WebSocketMessage {
    type = "SLIDER_UPDATE";

    constructor(update: SliderUpdate){
        super();
        this.payload = update;
    }
}

export class SliderUpdate {
    sliderName: SliderName;
    sliderValue: number;
}
/** @see com.tennio.gameserver.SliderUpdate.SliderName */
export enum SliderName{
    Serve, ShotAiming, Shooting, PositionAfterShot, SuperServe, SuperReaction, SuperSpeed, Jump
}

export let ActionButtons: ActionButton[] = [
    {
        sliderName: SliderName.SuperServe,
        cost: 100,
        caption: "Super Serve"
    },
    {
        sliderName: SliderName.SuperReaction,
        cost: 50,
        caption: "Super Reaction"
    },
    {
        sliderName: SliderName.SuperSpeed,
        cost: 100,
        caption: "Super Speed"
    },
    {
        sliderName: SliderName.Jump,
        cost: 50,
        caption: "Jump"
    }
];

export class ActionButton {
    public sliderName: SliderName;
    public cost: number;
    public caption: string;
}