import {Player} from "../../../model/player";
import {Score} from "../vector2";

export class ServerInfo {
    ip: string;
    port: string;
    gameId: number;
}

export class GameEvent {
    id: number;
    player1: Player;
    player2: Player;
    status: GameStatus;
    gameType: GameType;

    losingPlayer: Player;
    winningPlayer: Player;

    gameStart: number;

    player1Score: Score;
    player2Score: Score;
}

export enum GameStatus {
    WAITING, STARTED, ENDED, PLAYER1_SURRENDERED, PLAYER2_SURRENDERED
}

export enum GameType {
    Matchmaking, AI, Tournament
}