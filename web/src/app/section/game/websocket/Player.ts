import {Score} from "../vector2";
export class Player {
    public name: string = '';
    public id: number = 0;
    public score: Score = {points: 0, games: 0, sets: 0, matches: 0};
    public fitness: string = "100";
    public connected: boolean = false;
    public statistics: PlayerStatistics = null;

    fromPlayerInfo(data: any) {
        this.name = data.name;
        this.id = data.id;
        this.connected = data.connected;
    }
}

export class PlayerStatistics {
    backHandStrokes: number = 0;
    foreHandStrokes: number = 0;
    failedServes: number = 0;
    serves: number = 0;
    misses: number = 0;
    side: string;
}