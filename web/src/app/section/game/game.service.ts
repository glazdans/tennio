import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {ServerInfo} from "./websocket/server-info";
import {RestService} from "../../utility/restservice";
import {environment} from "../../../environments/environment";
import {SnotifyService} from "ng-snotify";

@Injectable()
export class GameService {
    constructor(private rest: RestService, private snotifyService: SnotifyService) {
    }
    private _gameUrl = environment.apiUrl + '/game/';

    startAIGame(): Observable<ServerInfo> {
        return this.rest.get(this._gameUrl + 'aigame/');
    }

    getGameInstance(gameId: number): Observable<any> {
        return this.rest.post(this._gameUrl + "get-game/" + gameId, "");
    }

    connectToWebSocketServer(serverInfo:any) {
        let socket = new WebSocket(serverInfo);
        socket.onerror = () => {
            this.snotifyService.error("Cannot connect to game server!", "");
        };
        return socket;
    }
}