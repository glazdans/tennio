import {Component, ViewChild, AfterViewInit, OnInit, OnDestroy} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Vector2, Snapshot, BallState, GameEndCause, PauseReason} from "./vector2";
import {GameService} from "./game.service";
import {LoginService} from "../../component/login/login.service";
import {courtDimensions, courtBoxes, colors} from "./court-dimensions";
import {SliderName, SliderUpdateMessage, SliderUpdate, ActionButtons} from "./websocket/slider-update";
import {Player} from "./websocket/Player";
import {ConnectedMessage} from "./websocket/connected-message";
import {Tennio} from "../../utility/Tennio";
import {WebSocketMessage} from "./websocket/websocket-message";
import {environment} from "../../../environments/environment";

declare let moment: any;
declare let $: any;

@Component({
    selector: 'tennio-game',
    templateUrl: "game.component.html",
    styleUrls: ["game.component.scss"]
})

export class GameComponent implements OnInit, AfterViewInit, OnDestroy {
    private sub: any;
    canvasHeight: number = 350;
    canvasHeightTopView: number = 100;
    canvasHeightScoreView: number = 200;
    canvasWidth: number = 750;
    private resized: boolean = false;

    private gameEndReason: string = '';

    context: CanvasRenderingContext2D;
    contextTopView: CanvasRenderingContext2D;

    playerRed: Player = new Player();
    playerBlue: Player = new Player();

    snapshotCount: number = 8;
    snapshots: Snapshot[] = [];
    currentTime: number = Date.now();
    totalTime: number = 0;
    gameEnded: boolean;

    @ViewChild("gameCanvas") gameCanvas: any;
    @ViewChild("scoreBoard") scoreCanvas: any;
    @ViewChild("sideView") sideViewCanvas: any;

    serverInfo: string;
    webSocket: WebSocket;
    sliderName = SliderName;
    actionButtons = ActionButtons;
    isRedPlayer: boolean = true;
    myPlayerId: number;
    sliderValues: SliderValues = new SliderValues();
    timeout: number = 0;
    pauseReason: PauseReason = PauseReason.NONE;
    contextScore: CanvasRenderingContext2D;
    fitness: number;
    private lastISnapshot: Snapshot = null;

    private tookTimeout: boolean = false;
    sideViewTogglable: boolean = true;
    slidersTogglable: boolean = true;

    constructor(private _gameService: GameService, private _route: ActivatedRoute, private _loginService: LoginService, private _router: Router) {
    }

    get timeoutText() {
        if (Tennio.compareEnum(PauseReason, this.pauseReason, PauseReason.SwitchingSides)) {
            return "Switching sides";
        }
        if (Tennio.compareEnum(PauseReason, this.pauseReason, PauseReason.PlayerBluePause)) {
            return this.isRedPlayer ? "Opponent" : "Your";
        }
        if (Tennio.compareEnum(PauseReason, this.pauseReason, PauseReason.PlayerRedPause)) {
            return !this.isRedPlayer ? "Opponent" : "Your";
        }
        return "";
    }

    ngOnInit() {
        this.sub = this._route.params.subscribe(params => {
            let gameId = +params["id"];
            if (gameId) {
                this._gameService.getGameInstance(gameId).subscribe(data => {
                    if (data.ip && data.port) {
                        this.serverInfo = data.ip + ":" + data.port;
                        this.connectToWebSocketServer(data.ip, data.port, gameId);
                    } else {
                        this._router.navigate(['/dashboard']);
                    }
                });
            } else {
                this._router.navigate(['/dashboard']);
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
        if (this.webSocket) {
            this.webSocket.close();
        }
    }

    getScore(yourScore: number, enemyScore: number): string {
        switch (yourScore) {
            case 0:
                return "0";
            case 1:
                return "15";
            case 2:
                return "30";
            case 3:
                return "40";
        }

        if (yourScore - enemyScore > 0) {
            return "AD"
        } else {
            return "40";
        }
    }

    toggleGamePause() {
        if (!this.timeout) {
            let pauseMsg: WebSocketMessage = new WebSocketMessage();
            pauseMsg.type = "PAUSE";
            this.send(pauseMsg);
            this.tookTimeout = true;
        }
    }

    connectToWebSocketServer(ip: string, port: string, gameId: number) {
        this.gameEnded = false;
        this.gameEndReason = '';
        this.sliderValues.initialized = false;
        let serverUrl: string;

        if (environment.production) {
            serverUrl = 'wss://' + ip + '/server/' + port;
        } else {
            serverUrl = 'ws://' + ip + ':' + port;
        }

        this.webSocket = this._gameService.connectToWebSocketServer(gameId ? (serverUrl + "?jwt=" + this._loginService.getJwt()) : serverUrl);
        this.webSocket.onmessage = Tennio.closure(this, this.onWebSocketMessage);
        this.webSocket.onclose = Tennio.closure(this, this.onWebSocketClose);
        this.updateCanvas();
    }

    toggleSurrenderModal() {
        $('#surrender-modal').modal();
    }

    surrender() {
        $('#surrender-modal').modal('hide');
        let msg = new WebSocketMessage();
        msg.type = "SURRENDER";
        msg.payload = true;
        this.send(msg);
    }

    onWebSocketMessage(event: any) {
        let data = JSON.parse(event.data);
        switch (data.type) {
            case "gameUpdate":
                this.handleGameUpdate(data);
                break;
            case "pointUpdate":
                this.playerRed.score = data.redPlayer;
                this.playerBlue.score = data.bluePlayer;
                this.updateScoreBoard();
                break;
            case "playerInfo":
                this.onPlayerInfo(data);
                break;
            case "sliderUpdate":
                this.onSliderUpdate(data);
                break;
            case "gameEnd":
                this.onGameEnd(data);
                break;
            default:
                console.log(data);
                break;
        }
    }

    onGameEnd(data) {
        if (Tennio.compareEnum(GameEndCause, data.payload, GameEndCause.PlayerSurrender)) {
            this.gameEndReason = data.causer == this.myPlayerId ? "You surrendered!" : "Opponent surrendered!";
        } else if (Tennio.compareEnum(GameEndCause, data.payload, GameEndCause.PlayerWon)) {
            this.gameEndReason = data.causer == this.myPlayerId ? "You won!" : "You lost!";
        }
    }

    onSliderUpdate(data) {
        for (let slider of data.payload) {
            switch (slider.sliderName) {
                case "Shooting" :
                    this.sliderValues.Shooting = slider.sliderValue;
                    break;
                case "PositionAfterShot":
                    this.sliderValues.PositionAfterShot = slider.sliderValue;
                    break;
                case "Serve":
                    this.sliderValues.Serve = slider.sliderValue;
                    break;
                case "ShotAiming":
                    this.sliderValues.ShotAiming = slider.sliderValue;
                    break;
            }
        }
        this.sliderValues.initialized = true;
    }

    onPlayerInfo(data) {
        this.playerRed.fromPlayerInfo(data.red);
        this.playerBlue.fromPlayerInfo(data.blue);
        this.updateScoreBoard();
        if (this._loginService.findPlayerById(this.playerBlue.id) != null) {
            if (!this.playerBlue.connected) {
                this.send(new ConnectedMessage(this.playerBlue));
                this.isRedPlayer = false;
            }
            this.myPlayerId = this.playerBlue.id;
        } else if (this._loginService.findPlayerById(this.playerRed.id) != null) {
            if (!this.playerRed.connected) {
                this.send(new ConnectedMessage(this.playerRed));
            }
            this.myPlayerId = this.playerRed.id;
        }
    }

    onWebSocketClose() {
        if (!this.gameEndReason) {
            this.gameEndReason = "Lost connection to server...";
        }
        this.hideSliders();
        this.gameEnded = true;
    }

    onSliderChange(event: any, type: SliderName) {
        let sliderUpdate: SliderUpdate = new SliderUpdate();
        sliderUpdate.sliderValue = event;
        sliderUpdate.sliderName = type;
        this.send(new SliderUpdateMessage(sliderUpdate));
    }

    onActionButton(type: SliderName) {
        let sliderUpdate: SliderUpdate = new SliderUpdate();
        sliderUpdate.sliderName = type;
        sliderUpdate.sliderValue = 0;
        this.send(new SliderUpdateMessage(sliderUpdate));
    }

    send(message: WebSocketMessage) {
        message.jwt = this._loginService.getJwt();
        this.webSocket.send(JSON.stringify(message));
    }

    handleGameUpdate(data: any) {
        if (this.gameEnded) {
            return;
        }
        if (this.snapshots.length > this.snapshotCount) {
            this.snapshots.shift();
        }

        let snapshot: Snapshot = new Snapshot();
        snapshot.fromData(data);

        this.timeout = data.timeout;
        this.fitness = data.fitness.toFixed(2);

        if (data.time != 0) {
            this.snapshots.push(snapshot);
            if (Tennio.compareEnum(PauseReason, PauseReason.NONE, this.pauseReason)) {
                this.updateScoreBoard();
            }
            if (!Tennio.compareEnum(PauseReason, PauseReason.NONE, this.pauseReason) && Tennio.compareEnum(PauseReason, PauseReason.NONE, data.pauseReason)) {
                this.tookTimeout = false;
            }
        }

        this.pauseReason = data.pauseReason;
    }

    getTimeoutLeft(): string {
        return moment(this.timeout).format("mm:ss");
    }

    ngAfterViewInit() {
        let canvas = this.gameCanvas.nativeElement;
        this.context = canvas.getContext("2d");
        this.context.transform(1, 0, 0, -1, 0, this.canvasHeight);

        let top = this.sideViewCanvas.nativeElement;
        this.contextTopView = top.getContext("2d");

        this.contextScore = this.scoreCanvas.nativeElement.getContext("2d");

        this.updateScoreBoard();
        this.updateCanvas();
    }

    createInterpolatedSnapshot(s1: Snapshot, s2: Snapshot, deltaTime: number): Snapshot {
        let snapshot: Snapshot = new Snapshot();
        snapshot.time = (s2.time - s1.time) * deltaTime + s1.time;
        snapshot.bluePlayer = this.interpolateVector(s1.bluePlayer, s2.bluePlayer, deltaTime);
        snapshot.redPlayer = this.interpolateVector(s1.redPlayer, s2.redPlayer, deltaTime);
        snapshot.ball = this.interpolateVector(s1.ball, s2.ball, deltaTime);
        snapshot.bouncePoint = s1.bouncePoint;
        snapshot.ballWaiting = s1.ballWaiting;
        snapshot.fitness = s1.fitness;
        snapshot.ballState = s1.ballState;
        return snapshot;
    }

    interpolateVector(v1: Vector2, v2: Vector2, delta: number): Vector2 {
        let interpolatedVector = {x: 0, y: 0, z: 0};
        interpolatedVector.x = (v2.x - v1.x) * delta + v1.x;
        interpolatedVector.y = (v2.y - v1.y) * delta + v1.y;
        interpolatedVector.z = (v2.z - v1.z) * delta + v1.z;
        return interpolatedVector;
    }

    findSnapshot(snapshots: Snapshot[], time: number): number {
        for (let i = 0; i < snapshots.length - 2; i++) {
            let s1 = snapshots[i];
            let s2 = snapshots[i + 1];
            if (s1.time <= time && s2.time >= time) {
                return i;
            }
        }
        return -1;
    }

    getRightPlayer(): Player {
        if (!this.snapshots.length) {
            return this.playerBlue;
        }
        return this.snapshots[this.snapshots.length - 1].redPlayer.x < 0 ? this.playerRed : this.playerBlue;
    }

    getLeftPlayer(): Player {
        if (!this.snapshots.length) {
            return this.playerRed;
        }
        return this.snapshots[this.snapshots.length - 1].redPlayer.x > 0 ? this.playerRed : this.playerBlue;
    }

    getPlayerTitle(player: Player): string {
        if (player.id == this.playerRed.id) {
            return !this.isRedPlayer ? "Your Player" : "Opponent Player";
        }
        if (player.id == this.playerBlue.id) {
            return this.isRedPlayer ? "Your Player" : "Opponent Player";
        }
    }

    toggleSideView() {
        if (this.sideViewTogglable) {
            this.sideViewTogglable = false;
            let button = $('#hide-sideview');
            let minimized = button.hasClass('minimized');
            let h = minimized ? courtDimensions.scale * 5 + courtDimensions.sideViewLineBottomPad : this.canvasHeightTopView;
            $(this.sideViewCanvas).height(this.canvasHeightTopView).animate({
                height: (minimized ? "+=" : "-=") + h.toString()
            }, {
                duration: 450,
                step: (curr) => {
                    this.canvasHeightTopView = h + curr;
                },
                complete: () => {
                    button.toggleClass('minimized');
                    this.sideViewTogglable = true;
                }
            });
        }
    }

    hideSliders() {
        if ($('#settings-cog').hasClass("toggled")) {
            this.toggleSliders();
        }
    }

    toggleSliders() {
        if (this.slidersTogglable) {
            this.slidersTogglable = false;
            let settingsBtn = $('#settings-cog');
            let sliders = $('#game-sliders');
            let opened = false;

            if (!settingsBtn.hasClass("toggled")) {
                settingsBtn.addClass("toggled");
                opened = true;
            }

            sliders.animate({height: 'toggle'}, 450, () => {
                if (settingsBtn.hasClass("toggled") && !opened) {
                    settingsBtn.removeClass("toggled");
                }
                this.slidersTogglable = true;
            });
        }
    }

    updateScoreBoard() {
        // let image = new Image();
        // image.src = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgNjk3LjUgNTkuNSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNjk3LjUgNTkuNTsiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+LnN0MHtmaWxsOiNGRkZGRkY7fTwvc3R5bGU+PHBhdGggaWQ9IlhNTElEXzE1XyIgZD0iTTY5Ny41LDAiLz48cG9seWdvbiBpZD0iWE1MSURfMV8iIHBvaW50cz0iNjk3LjUsMCA2NTkuNSw1OS41IDM0Ny44LDU5LjUgMzQ3LjgsMCAiLz48cGF0aCBpZD0iWE1MSURfMTdfIiBjbGFzcz0ic3QwIiBkPSJNMCwwIi8+PHBvbHlnb24gaWQ9IlhNTElEXzE2XyIgcG9pbnRzPSIwLDAgMzcuOSw1OS41IDM0OC44LDU5LjUgMzQ4LjgsMCAiLz48L3N2Zz4="
        let leftPlayer = this.getLeftPlayer();
        let rightPlayer = this.getRightPlayer();

        let ctx = this.contextScore;
        let scale = courtDimensions.scale / 1.2;

        ctx.beginPath();
        ctx.rect(0, 0, this.canvasWidth, this.canvasHeightScoreView);
        ctx.fillStyle = "black";
        ctx.fill();

        let headerFS = Math.round(scale * .6);
        let setFS = Math.round(scale * .9);
        let pointFS = Math.round(scale * 2.1);
        let playerFS = Math.round(scale * 1.15);
        let pointsH = Math.round(this.canvasHeightScoreView - pointFS / 2.3);
        let titleHeight = scale * 1.4;
        let botTxtHeight = titleHeight + setFS * 1.15;
        let playerNameH = titleHeight + playerFS;

        ctx.fillStyle = colors.tennioGreen;
        ctx.font = headerFS + "px Arial, Helvetica, sans-serif";

        let leftPlayerTxt = this.getPlayerTitle(leftPlayer);
        ctx.fillText(leftPlayerTxt, scale, titleHeight);

        let rightPlayerTxt = this.getPlayerTitle(rightPlayer);
        ctx.fillText(rightPlayerTxt, this.canvasWidth - ctx.measureText(rightPlayerTxt).width - scale, titleHeight);

        let lGamesX = this.canvasWidth / 2 - (this.canvasWidth * 3 / 12) - ctx.measureText("Games").width / 2;
        let lSetsX = this.canvasWidth / 2 - (this.canvasWidth * 3 / 20) - ctx.measureText("Sets").width / 2;
        let gameW = ctx.measureText("Games").width;
        let setsW = ctx.measureText("Sets").width;
        ctx.fillText("Games", lGamesX, titleHeight);
        ctx.fillText("Sets", lSetsX, titleHeight);

        let rGamesX = this.canvasWidth / 2 + (this.canvasWidth * 3 / 20) - ctx.measureText("Sets").width / 2;
        let rSetsX = this.canvasWidth / 2 + (this.canvasWidth * 3 / 12);
        ctx.fillText("Games", rGamesX, titleHeight);
        ctx.fillText("Sets", rSetsX, titleHeight);

        ctx.fillStyle = "white";
        let pointsTxt = "Match point";
        ctx.fillText(pointsTxt, this.canvasWidth / 2 - ctx.measureText(pointsTxt).width / 2, scale / 1.45);
        ctx.fillStyle = colors.tennioGreen;

        ctx.font = pointFS + "px Digital-Dream";
        let rightScore = this.getScore(rightPlayer.score.points, leftPlayer.score.points);
        let leftScore = this.getScore(leftPlayer.score.points, rightPlayer.score.points);
        ctx.fillText(rightScore, (this.canvasWidth / 2 - ctx.measureText(rightScore).width / 2 - pointFS / 1.1), pointsH);
        ctx.fillText(leftScore, (this.canvasWidth / 2 - ctx.measureText(leftScore).width / 2 + pointFS / 1.1), pointsH);

        ctx.fillStyle = "white";
        ctx.font = "bold " + playerFS + "px Arial";
        ctx.fillText(rightPlayer.name, scale, playerNameH);
        ctx.fillText(leftPlayer.name, this.canvasWidth - scale - ctx.measureText(leftPlayer.name).width, playerNameH);

        ctx.font = setFS + "px Digital-Dream";
        ctx.fillStyle = colors.tennioGreen;
        ctx.fillText(rightPlayer.score.sets.toString(), lSetsX + setsW / 2 - ctx.measureText(rightPlayer.score.sets.toString()).width / 2, botTxtHeight);
        ctx.fillText(rightPlayer.score.games.toString(), lGamesX + gameW / 2 - ctx.measureText(rightPlayer.score.games.toString()).width / 2, botTxtHeight);

        ctx.fillText(leftPlayer.score.sets.toString(), rSetsX + setsW / 2 - ctx.measureText(leftPlayer.score.sets.toString()).width / 2, botTxtHeight);
        ctx.fillText(leftPlayer.score.games.toString(), rGamesX + gameW / 2 - ctx.measureText(leftPlayer.score.games.toString()).width / 2, botTxtHeight);
    }

    updateScale(force = false) {
        if (force || this.gameCanvas.nativeElement.offsetWidth != this.canvasWidth || this.gameCanvas.nativeElement.offsetHeight != this.canvasHeight) {
            if (!this.resized) {
                this.canvasHeight = Math.round(this.gameCanvas.nativeElement.offsetHeight * 1.025) * 1.15;
                this.resized = true;
            } else {
                this.canvasHeight = this.gameCanvas.nativeElement.offsetHeight;
            }
            this.canvasWidth = this.gameCanvas.nativeElement.offsetWidth;
            this.canvasHeightTopView = courtDimensions.scale * 5 + courtDimensions.sideViewLineBottomPad;
            courtDimensions.scale = this.canvasWidth / courtDimensions.totalWidth;
            courtDimensions.updateDimensions();
            courtBoxes.updateScale();
            this.canvasHeightScoreView = courtDimensions.scale * 3;
        }
    }


    updateCanvas() {
        this.updateScale();

        if (!this.gameEnded) {
            requestAnimationFrame(() => {
                this.updateCanvas()
            });
        }

        let newTime = Date.now();
        let delta = (newTime - this.currentTime) / 1000;
        this.currentTime = newTime;
        this.totalTime += delta;

        let interpolatedSnapshot;
        if (this.snapshots.length == 1) {
            interpolatedSnapshot = this.snapshots[this.snapshots.length - 1];
        } else {
            if (this.snapshots.length < this.snapshotCount) {
                if (this.lastISnapshot) {
                    interpolatedSnapshot = this.lastISnapshot;
                } else {
                    return;
                }
            } else {
                let arrayIndex = this.findSnapshot(this.snapshots, this.totalTime);
                if (arrayIndex === -1) {
                    this.totalTime = this.snapshots[this.snapshotCount - 3].time;
                    arrayIndex = this.snapshotCount - 3;
                }
                let baseSnapshot: Snapshot = this.snapshots[arrayIndex];
                let nextSnapshot: Snapshot = this.snapshots[arrayIndex + 1];

                let dTime = (this.totalTime - baseSnapshot.time) / (nextSnapshot.time - baseSnapshot.time);
                interpolatedSnapshot = this.createInterpolatedSnapshot(baseSnapshot, nextSnapshot, dTime);
            }
        }
        this.lastISnapshot = interpolatedSnapshot;

        this.clearCanvas(this.context);
        courtBoxes.draw(this.context);

        if (interpolatedSnapshot.bouncePoint.x !== 0 && interpolatedSnapshot.bouncePoint.y !== 0) {
            this.drawBouncePoint(this.context, interpolatedSnapshot);
        }

        this.drawNet(this.context);
        this.drawPlayers(this.context, interpolatedSnapshot);

        if (!interpolatedSnapshot.ballWaiting) {
            this.context.beginPath();
            this.drawBall(this.context, false, interpolatedSnapshot.ball.x, interpolatedSnapshot.ball.y, (interpolatedSnapshot.ball.z + 1) / 5);
            this.context.fillStyle = interpolatedSnapshot.ballState == BallState.Default ? "black" : "#610B0B";
            this.context.fill();
        }

        this.drawSideView(interpolatedSnapshot);
        this.context.translate(0.5, 0.5);
        this.context.translate(-0.5, -0.5);

        if (this.gameEndReason) {
            this.overlay(this.gameEndReason);
        } else if (this.timeout && this.totalTime != 0) {
            this.overlay(this.timeoutText + " timeout: " + this.getTimeoutLeft());
        } else if (!(this.playerRed.connected && this.playerBlue.connected)) {
            this.overlay("Waiting for other player...")
        }
    }

    private overlay(txt) {
        this.context.fillStyle = "rgba(0,0,0,0.5)";
        this.context.fillRect(0, 0, this.canvasWidth, this.canvasHeight);

        this.context.fillStyle = "#426046";
        this.context.font = courtDimensions.scale * 2 + "px Arial, Helvetica, sans-serif";

        this.context.fillStyle = 'white';
        this.context.fillText(txt, courtDimensions.scaledHalfWidth - this.context.measureText(txt).width / 2, courtDimensions.scaledHalfHeight);
    }

    private drawBouncePoint(ctx: CanvasRenderingContext2D, interpolatedSnapshot: Snapshot) {
        ctx.beginPath();
        ctx.fillStyle = interpolatedSnapshot.bouncePoint.bounceIn ? "rgba(255,255,255,0.45)" : "red";
        this.drawCircle(ctx, interpolatedSnapshot.bouncePoint.x, interpolatedSnapshot.bouncePoint.y, 0.2);
        ctx.fill();
    }

    private clearCanvas(ctx: CanvasRenderingContext2D) {
        ctx.beginPath();
        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        ctx.fillStyle = "#b02c37";
        ctx.fillRect(0, 0, this.canvasWidth, this.canvasHeight);
        ctx.stroke();
    }

    private clearSideView(ctx: CanvasRenderingContext2D) {
        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeightTopView);
    }

    private drawPlayers(ctx: CanvasRenderingContext2D, interpolatedSnapshot: any) {
        ctx.beginPath();
        ctx.fillStyle = this.isRedPlayer ? "red" : "gray";
        this.drawCircle(ctx, interpolatedSnapshot.redPlayer.x, interpolatedSnapshot.redPlayer.y, 1);
        ctx.fill();

        ctx.beginPath();
        ctx.fillStyle = this.isRedPlayer ? "gray" : "red";
        this.drawCircle(ctx, interpolatedSnapshot.bluePlayer.x, interpolatedSnapshot.bluePlayer.y, 1);
        ctx.fill();
    }

    private drawCircle(ctx: CanvasRenderingContext2D, x: number, y: number, radius: number): void {
        x = (x + courtDimensions.halfWidth) * courtDimensions.scale;
        y = (y + courtDimensions.halfHeight) * courtDimensions.scale;
        radius = courtDimensions.playerSize * courtDimensions.scale * radius;
        ctx.arc(x, y, radius, 0, Math.PI * 2, true);
    }

    private drawBall(ctx: any, sideView: any, x: any, y: any, size: any) {
        size *= courtDimensions.scale;
        let halfSize = size / 2;
        x = (x + courtDimensions.halfWidth) * courtDimensions.scale - halfSize;
        y = (sideView ? 5 - y : y + courtDimensions.halfHeight) * courtDimensions.scale - halfSize;

        ctx.arc(x, y, size, 0, Math.PI * 2, true);
    }

    private drawNet(ctx: any) {
        ctx.beginPath();
        ctx.strokeStyle = "#c1c1c1";
        ctx.lineWidth = courtDimensions.scale / 10;
        ctx.moveTo(courtDimensions.scaledHalfWidth, courtDimensions.scaledHalfHeight - courtDimensions.scaledHalfNetLength);
        ctx.lineTo(courtDimensions.scaledHalfWidth, courtDimensions.scaledHalfHeight + courtDimensions.scaledHalfNetLength);
        ctx.stroke();

        let postRadius = courtDimensions.scale / 7.5;
        ctx.beginPath();
        ctx.fillStyle = "black";
        ctx.arc(courtDimensions.scaledHalfWidth, courtDimensions.scaledHalfHeight - courtDimensions.scaledHalfNetLength, postRadius, 0, Math.PI * 2, true);
        ctx.fill();

        ctx.beginPath();
        ctx.fillStyle = "black";
        ctx.arc(courtDimensions.scaledHalfWidth, courtDimensions.scaledHalfHeight + courtDimensions.scaledHalfNetLength, postRadius, 0, Math.PI * 2, true);
        ctx.fill();
    }

    private drawSideView(interpolatedSnapshot: Snapshot) {
        let ctx = this.contextTopView;

        this.clearSideView(ctx);
        ctx.lineWidth = Math.round(.35 * courtDimensions.scale);

        let botLineH = this.canvasHeightTopView - courtDimensions.sideViewLineBottomPad + ctx.lineWidth;
        let linePadding = Math.round(courtDimensions.scale * 1.1);

        // net
        ctx.beginPath();
        ctx.fillStyle = ctx.strokeStyle = "#7d7d7d";
        ctx.moveTo(courtDimensions.scaledHalfWidth, botLineH);
        ctx.lineTo(courtDimensions.scaledHalfWidth, botLineH - courtDimensions.scaledNetHeightAtPosts);
        ctx.stroke();

        ctx.beginPath();
        ctx.strokeStyle = "rgba(66, 96, 70, 0.8)";
        ctx.moveTo(linePadding, botLineH);
        ctx.lineTo(this.canvasWidth - linePadding, botLineH);
        ctx.stroke();

        ctx.beginPath();
        ctx.strokeStyle = "rgba(176, 44, 55, 0.8)";
        ctx.moveTo(linePadding, botLineH);
        ctx.lineTo(courtBoxes.doubles.scaledX - linePadding, botLineH);
        ctx.stroke();

        ctx.beginPath();
        ctx.strokeStyle = "rgba(176, 44, 55, 0.8)";
        ctx.moveTo(courtBoxes.doubles.scaledX + courtBoxes.doubles.scaledW, botLineH);
        ctx.lineTo(this.canvasWidth - linePadding, botLineH);
        ctx.stroke();

        if (!interpolatedSnapshot.ballWaiting) {
            ctx.beginPath();
            ctx.fillStyle = "black";
            let size = 0.25 * courtDimensions.scale;
            let halfSize = size / 2;
            let x = (interpolatedSnapshot.ball.x + courtDimensions.halfWidth) * courtDimensions.scale - halfSize;
            let y = this.canvasHeightTopView - interpolatedSnapshot.ball.z * courtDimensions.scale - halfSize - courtDimensions.sideViewLineBottomPad + Math.round(.35 * courtDimensions.scale) / 2;
            ctx.arc(x, y, size, 0, Math.PI * 2, true);
            ctx.fill();
        }
    }
}
class SliderValues {
    Serve: number = 0;
    ShotAiming: number = 0;
    Shooting: number = 0;
    PositionAfterShot: number = 0;
    initialized: boolean = false;
}