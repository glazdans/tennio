import {Component, AfterViewInit, OnInit} from "@angular/core";
import {Router, NavigationEnd, NavigationStart} from "@angular/router";
import "./rxjs-operators";
import {LoginService} from "./component/login/login.service";
import {SnotifyService} from "ng-snotify";

declare var $: any;

@Component({
    selector: 'my-app',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements AfterViewInit, OnInit {
    title = 'Tennio';
    clientHeight: number;
    wide: boolean = false;

    constructor(public loginService: LoginService, private router: Router, private snotifyService: SnotifyService) {
        this.clientHeight = window.innerHeight;
        router.events.subscribe((val) => {
            if(val instanceof NavigationStart || val instanceof NavigationEnd) {
                this.wide = val.url.toString().indexOf('/game') !== -1 || val.url.toString().indexOf('/admin') !== -1;
                $(document.body).css("padding", 0);
            }
        });
    }

    get containerClass() {
        return this.wide ? 'container container-wide' : 'container';
    }

    ngOnInit() {
        this.snotifyService.setConfig({
            timeout: 5000
        }, {
            newOnTop: true
        });
    }

    ngAfterViewInit() {
        if (!this.shouldDisplayMenu()) {
            $('#lower-menu').hide();
        }
    }

    shouldDisplayMenu() {
        return !((this.router.url == '/' && !this.loginService.isLoggedIn()) || this.router.url == 'home' || this.router.url == 'login');
    }
}