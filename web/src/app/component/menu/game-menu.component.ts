import {Component, OnInit, ViewChild} from "@angular/core";
import {LoginService} from "../login/login.service";
import {User} from "../../model/user";
import {WebSocketService} from "../../utility/websocket.service";
import {Tennio} from "../../utility/Tennio";
import {Notification} from "./notification";
import {NotificationService} from "./notification.service";
import {Router} from "@angular/router";
import {CreatePlayerComponent} from "../createplayer/createplayer.component";
import {TimeService} from "../../utility/timeservice";

@Component({
    selector: 'tennio-game-menu',
    templateUrl: 'game-menu.component.html',
    styleUrls: ['game-menu.component.scss']
})

export class GameMenu implements OnInit {
    @ViewChild('cPlayer')
        createPlayerModal: CreatePlayerComponent;

    isAdmin: boolean = false;
    user: User;
    newNotifications: Notification[] = [];
    isFlashing: boolean = false;
    flashState: boolean = false;
    initialTS: number;

    constructor(private loginService: LoginService,
                private ws: WebSocketService,
                private _notificationService: NotificationService,
                private router: Router,
                private timeService: TimeService) {
    }

    ngOnInit(): any {
        this.loginService.getUser().subscribe((user) => {
            this.user = user;
            if(user) {
                if (!this.user.players.length) {
                    this.createPlayerModal.showModal();
                }
            }
        });
        this.loginService.getIsAdmin().subscribe((data) => {
            this.isAdmin = data
        });
        this.ws.subscribe("/user/notifications/activeplayer", Tennio.closure(this, this.onActivePlayer));
        this.ws.subscribe("/user/notifications", Tennio.closure(this, this.onNotifications));
        this.initialTS = Date.now();

        this.loginService.registerOnLogoutCallback(Tennio.closure(this, () => {
            this.isAdmin = false;
        }));

        this.loginService.registerOnLoginCallback(Tennio.closure(this, () => {
            this.loginService.getIsAdmin().subscribe((data) => {
                this.isAdmin = data
            });
        }));

        this.newNotifications = this._notificationService.getNotifications();
    }

    reset() {
        this.loginService.reset().subscribe(() => window.location.reload());
    }

    logout() {
        this.loginService.logout();
    }

    setActivePlayer(id: number) {
        this.ws.send("/user/notifications/activeplayer", {}, id);
    }

    onActivePlayer(json: any) {
        if (json.success) {
            this.loginService.activatePlayer(json.id);
            this.user = this.loginService.user;
        }
    }

    startFlashing() {
        if (this.isFlashing === false) {
            this.isFlashing = true;
            this.flash(this);
        }
    }

    stopFlashing() {
        this.isFlashing = false;
        this.flashState = false;
    }

    onNotifications(messages : any) {
        if (messages.currentServerTime) {
            this.timeService.setServerTime(messages.currentServerTime);
        }
        if (messages instanceof Array && messages.length) {
            for (let message of messages) {
                this.addNotification(message);
            }
        } else {
            if (messages) {
                this.addNotification(messages);
            }
        }
    }

    addNotification(notification: Notification) {
        if (this.initialTS.toString() < notification.notificationEventStart) {
            this.newNotifications.push(notification);
            this.startFlashing();
        }
    }

    flash(component : any) {
        if (component.isFlashing === true) {
            setTimeout(() => {
                this.flash(component)
            }, 1000);
            component.flashState = !component.flashState;
        } else {
            component.flashState = false;
        }

    }

    getStyle() {
        if (this.flashState) {
            return "red";
        } else {
            return "#969696";
        }
    }

    getMenuItemClass(route: string) {
        return (this.router.url.toString().indexOf(route) !== -1 ? 'selected' : '');
    }

    clearAll() {
        this.newNotifications.splice(0, this.newNotifications.length);
    }

    onNotificationSelect = this._notificationService.onNotificationSelect;
}