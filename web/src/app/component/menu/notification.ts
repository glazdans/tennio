export class Notification {
    id: number;
    notificationType: NotificationType;
    additionalInfo: string;
    sticky: boolean;
    notificationEventStart: string;
    deleteNotification: boolean;
    notificationAttributes : any;
}

/** com.tennio.mainserver.domain.Notification.NotificationType */
export enum NotificationType{
    Game, TournamentGame, Tournament, Training, AttributeUpdate, Misc, GameResult, Duel, AchievementProgress,
    MatchmakingError, GameMigrated, GameCanceled, Popup, DuelOpponentFound, DuelOpponentDeclined, DuelExpired
}

/** com.tennio.mainserver.domain.Notification.NotificationAttributeType */
export enum NotificationAttributeType{
    TournamentId, GameId, TournamentName, OpponentName, TrainingName, Money, TrainingStats, Balls, TrainingEventId,
    ELO, PlayerId, Stats, AchievementProgressId, AchievementProgress, CurrentMatchMakingGame, DuelId, Expires
}