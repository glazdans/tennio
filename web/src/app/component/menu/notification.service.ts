import {Injectable} from "@angular/core";
import {LoginService} from "../login/login.service";
import {Notification, NotificationAttributeType, NotificationType} from "./notification";
import {WebSocketService} from "../../utility/websocket.service";
import {Tennio} from "../../utility/Tennio";
import {PlayerSeason, Player} from "../../model/player";
import {Router} from "@angular/router";
import {AchievementProgress} from "../../model/achievement";
import {AchievementsService} from "../../section/achievements/achievements.service";
import {SnotifyService} from "ng-snotify";

@Injectable()
export class NotificationService {
    private notifications: Notification[] = [];

    constructor(private _loginService: LoginService,
                private _webSocketService: WebSocketService,
                private _router: Router,
                private _achievementService: AchievementsService,
                private snotifyService: SnotifyService) {
        this._webSocketService.subscribe("/user/notifications/delete", Tennio.closure(this, this.onDeleteNotification));
        this._webSocketService.subscribe("/user/notifications/update", Tennio.closure(this, this.onUpdateAttributeNotification));
        this._webSocketService.subscribe("/notifications/update", Tennio.closure(this, this.onUpdateAttributeNotification));
        this._webSocketService.subscribe("/user/notifications", Tennio.closure(this, this.onNotifications));
        this._loginService.registerOnLogoutCallback(Tennio.closure(this, () => {
            this.notifications = [];
            localStorage.removeItem('notifications');
        }));
    }

    onNotifications(messages: any) {
        if (messages.notifications instanceof Array) {
            for (let message of messages.notifications) {
                this.onNotification(message);
            }
        } else if (messages instanceof Array) {
            for (let message of messages) {
                this.onNotification(message);
            }
        } else {
            this.onNotification(messages);
        }
        localStorage.setItem('notifications', JSON.stringify(this.notifications));
    }

    onNotification(message: Notification) {
        if (Tennio.N.checkType(message, NotificationType.GameMigrated)) {
            if (message.additionalInfo) {
                this.snotifyService.error("Error occured!", message.additionalInfo);
                return;
            }
        }
        if (message.deleteNotification === true) {
            this.deleteNotification(message.id);
        } else {
            for (let notification of this.notifications) {
                if (notification.id == message.id) {
                    return;
                }
            }
            this.notifications.push(message);
        }
    }

    onDeleteNotification(response: any) {
        if (response.success) {
            this.deleteNotification(response.id);
        }
    }

    deleteNotification(id: number) {
        for (let index in this.notifications) {
            let notification = this.notifications[index];
            if (notification.id === id) {
                this.notifications.splice(+index, 1);
                break;
            }
        }
    }

    onUpdateAttributeNotification(response: any) {
        this._loginService.getUser().subscribe(
            user => {
                for (let attributeType in response.notificationAttributes) {
                    if (!response.notificationAttributes.hasOwnProperty(attributeType)) continue;
                    let value = response.notificationAttributes[attributeType];
                    let playerId: number;

                    switch (+NotificationAttributeType[attributeType]) {
                        case NotificationAttributeType.Balls:
                            if (value != "+") {
                                user.balls = value;
                            } else {
                                if (user.balls < 5) {
                                    user.balls = (+user.balls + 1);
                                }
                            }
                            break;
                        case NotificationAttributeType.Money:
                            user.money = value;
                            break;
                        case NotificationAttributeType.ELO:
                            playerId = response.notificationAttributes[NotificationAttributeType[NotificationAttributeType.PlayerId]];
                            if (playerId) {
                                let player: Player = null;
                                for (let p of user.players) {
                                    if (p.id == playerId) {
                                        player = p;
                                    }
                                }
                                if (player !== null) {
                                    if (player.playerSeasonList instanceof Array && player.playerSeasonList.length) {
                                        player.playerSeasonList[player.playerSeasonList.length - 1].elo = value;
                                    } else {
                                        let newSeason = new PlayerSeason();
                                        newSeason.elo = value;
                                        player.playerSeasonList = [newSeason];
                                    }
                                    this._loginService.replacePlayer(player);
                                }
                            } else {
                                console.error("no player id!");
                            }
                            break;
                        case NotificationAttributeType.Stats:
                            playerId = response.notificationAttributes[NotificationAttributeType[NotificationAttributeType.PlayerId]];
                            if (playerId) {
                                let player: Player = null;
                                for (let p of user.players) {
                                    if (p.id == playerId) {
                                        player = p;
                                    }
                                }
                                if (player !== null) {
                                    player.stats = JSON.parse(response.notificationAttributes[NotificationAttributeType[NotificationAttributeType.Stats]]);
                                    this._loginService.replacePlayer(player);
                                }
                            } else {
                                console.error("no player id!");
                            }
                            break;
                        case NotificationAttributeType.AchievementProgressId:
                            if (!this._loginService.user.achievements) {
                                this._loginService.user.achievements = [];
                            }
                            let oldAP = Tennio.getSomethingById<AchievementProgress>(this._loginService.user.achievements, value);
                            let newProgress: AchievementProgress = JSON.parse(response.notificationAttributes[NotificationAttributeType[NotificationAttributeType.AchievementProgress]]);
                            if (!oldAP) {
                                this._loginService.user.achievements.push(newProgress);
                            } else {
                                this._loginService.replaceAchievement(newProgress);
                            }
                            this.onAchievementUpdate(newProgress);
                            break;
                    }

                }
                this._loginService.setUser(user);
            }
        );
    }

    onAchievementUpdate(progress: AchievementProgress) {
        this._achievementService.onUpdate(progress);
    }

    sendDeleteNotification(id: number) {
        this._webSocketService.send("/user/notifications/delete", {}, id);
    }

    getNotifications(): Notification[] {
        if (!this.notifications.length) {
            let notifications = JSON.parse(localStorage.getItem('notifications'));
            if (notifications instanceof Array) {
                this.notifications = notifications;
            }
        }
        return this.notifications;
    }

    onNotificationSelect(notification: Notification) {
        switch (+NotificationType[notification.notificationType]) {
            case NotificationType.Training:
                this._router.navigate(["/trainings"]);
                break;
            case NotificationType.Tournament:
                this._router.navigate([
                    '/tournament-detail',
                    notification.notificationAttributes[NotificationAttributeType[NotificationAttributeType.TournamentId]]
                ]);
                break;
            case NotificationType.GameResult:
                this._router.navigate([
                    '/game',
                    notification.notificationAttributes[NotificationAttributeType[NotificationAttributeType.GameId]]
                ]);
                break;
            case NotificationType.Game:
            case NotificationType.TournamentGame:
                if (Date.now() >= +notification.notificationEventStart) {
                    this._router.navigate([
                        '/game',
                        notification.notificationAttributes[NotificationAttributeType[NotificationAttributeType.GameId]]
                    ]);
                }
                break;
            default:
                console.log("Notification type not found for: ", notification);
                break;
        }
    }
}