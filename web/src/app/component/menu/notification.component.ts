import {Component, OnInit} from "@angular/core";
import {NotificationService} from "./notification.service";
import {Notification, NotificationType} from "./notification";
@Component({
    selector: 'tennio-notification',
    templateUrl: "notification.component.html"
})

export class NotificationComponent implements OnInit {
    constructor(private _notificationService: NotificationService) {
    }

    notifications: Notification[] = [];

    ngOnInit(): any {
        this.notifications = this._notificationService.getNotifications();
    }

    removeNotification(notification: Notification) {
        window.event.stopPropagation();
        this._notificationService.sendDeleteNotification(notification.id);
    }

    getNotificationColor(notification: Notification) {
        let coloredNotificationTypes = [NotificationType.Game, NotificationType.TournamentGame];
        let shouldColor = false;
        for (let type of coloredNotificationTypes) {
            if (type == +NotificationType[notification.notificationType]) {
                shouldColor = true;
            }
        }
        if (!shouldColor || !notification.notificationEventStart) {
            return "";
        } else {
            return new Date(Date.now()) >= new Date(+notification.notificationEventStart) ? "green" : "red";
        }
    }
    onNotificationSelect = this._notificationService.onNotificationSelect;
}