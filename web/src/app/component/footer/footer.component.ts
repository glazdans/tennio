import {Component, OnInit} from "@angular/core";
import {LoginService} from "../login/login.service";
import {User} from "../../model/user";

@Component({
    selector: 'tennio-footer',
    templateUrl: "footer.component.html",
    styleUrls: ['footer.component.scss'],
})

export class FooterComponent implements OnInit {
    user: User;
    ngOnInit(): void {
        this.loginService.getUser().subscribe(user => this.user = user);
    }
    constructor(private loginService:LoginService) {}
}