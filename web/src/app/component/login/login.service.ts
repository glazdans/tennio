import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import {Observable, ObservableInput} from "rxjs/Observable";
import "rxjs/Rx";
import {User} from "../../model/user";
import {Player} from "../../model/player";
import {Router} from "@angular/router";
import {AnonymousRestService} from "../../utility/anonymousrestservice";
import {Tennio} from "../../utility/Tennio";
import {Observer} from "rxjs";
import {NgForm} from "@angular/forms";
import {environment} from "../../../environments/environment";
import {Response} from "../../utility/response";
import {AchievementProgress} from "../../model/achievement";

@Injectable()
export class LoginService extends AnonymousRestService {
    constructor(protected http: Http, router: Router) {
        super(http, router);
    }

    private url = environment.apiUrl + '/login/';
    private playerUrl = environment.apiUrl + '/player/';
    private userUrl = environment.apiUrl + '/user/';
    public refreshUrl = environment.apiUrl + '/login/refresh';
    private onLogoutCallbacks: {(): void;} [] = [];
    private onLoginCallbacks: {(): void;} [] = [];

    jwt: string;
    redirectUrl: string;
    user: User = null;
    isAdmin: boolean = null;

    hasActivePlayer(): boolean {
        return this.user.activePlayer !== null;
    }

    hasPlayers(): boolean {
        return this.user.players && this.user.players.length != 0;
    }

    public refreshJWT() {
        if (this.isLoggedIn()) {
            this.get(this.refreshUrl).subscribe(jwtResponse => {
                if (!jwtResponse) {
                    this.logout();
                } else {
                    this.jwt = jwtResponse.token;
                    localStorage.setItem("jwt", jwtResponse.token);
                }
                return Observable.throw("401 Unauthorized asa");
            });
        }
    }

    public onError(err: any, caught: Observable<any>): ObservableInput<{}> {
        if (err.status === 401) {
            this.logout();
        }

        return Observable.empty();
    }

    authenticate(username: string, password: string): Observable<any> {
        let obs = this.post(this.url + "auth/", {"username": username.toLowerCase(), "password": password});
        obs.subscribe((data) => {
            this.onLogin();
        });
        return obs;
    }

    onLogin() {
        for (let callback of this.onLoginCallbacks) {
            callback();
        }
    }

    getIsAdmin(): Observable<boolean> {
        if (!this.getJwt()) {
            return new Observable<boolean>((observer: Observer<boolean>) => {
                observer.next(false);
            });
        }
        if (this.isAdmin === null) {
            let obs = this.get(this.url + "admin/");
            obs.subscribe((data: boolean) => {
                this.isAdmin = data;
            });
            return obs;
        }
        return new Observable<boolean>((observer: Observer<boolean>) => {
            observer.next(this.isAdmin);
        });
    }

    getUserInformation(): Observable<User> {
        return this.get(this.userUrl + "get/");
    }

    createPlayer(playerName: string, countryCode: string): Observable<Response<Player>> {
        return this.post(this.playerUrl + "create/", {name: playerName, countryCode: countryCode});
    }

    login(form: NgForm) {
        return new Promise((resolve, reject) => {
            this.authenticate(form.value.username, form.value.password)
                .subscribe(jwtResponse => {
                        this.jwt = jwtResponse.token;
                        localStorage.setItem("jwt", jwtResponse.token);

                        this.getUser().subscribe(user => {
                                this.setUser(user);
                                if (this.redirectUrl) {
                                    this.router.navigate([this.redirectUrl]);
                                } else {
                                    this.router.navigate(['/dashboard']);
                                }
                                resolve(true);
                            }, error => {
                            this.logout();
                                reject("User not found!");
                            }
                        );
                    },
                    error => reject("Wrong username/password!")
                );
        });
    }

    register(form: NgForm) {
        return new Promise((resolve, reject) => {
            this.postRegister(form.value).subscribe(data => {
                switch (data.code) {
                    case "OK":
                        resolve(true);
                        break;
                    case "FOUND":
                    case "ERROR":
                        reject(data.message ? data.message : "An error occured! Please try again later!");
                        break;
                }
            });
        });
    }

    postPasswordChange(newPass: string, mail: string, key: string): Observable<boolean> {
        return this.post(this.userUrl + "changepw", {
            "password": newPass,
            "email": mail,
            "key": key
        });
    }

    requestPasswordReset(form: any) {
        return new Promise((resolve, reject) => {
            this.postPasswordReset(form.value).subscribe(data => {
                resolve(data);
            })
        });
    }

    postPasswordReset(form: any): Observable<any> {
        return this.get(this.userUrl + "reset?email=" + form.email.toString().toLowerCase().trim());
    }

    postRegister(form: any): Observable<any> {
        let data: any = {
            "username": form.username.toLowerCase().trim(),
            "password": form.password.trim(),
            "firstName": form.firstName ? form.firstName.trim() : null,
            "lastName": form.firstName ? form.lastName.trim() : null,
            "email": form.email.toLowerCase().trim()
        };

        if (form.countryCode) {
            data.countryCode = form.countryCode;
        }

        return this.post(this.userUrl + "register/", data);
    }

    registerOnLogoutCallback(callback: {(): void;}) {
        this.onLogoutCallbacks.push(callback);
    }

    registerOnLoginCallback(callback: {(): void;}) {
        this.onLoginCallbacks.push(callback);
        if (this.isLoggedIn()) {
            callback();
        }
    }

    public logout() {
        for (let callback of this.onLogoutCallbacks) {
            callback();
        }
        localStorage.removeItem("jwt");
        localStorage.removeItem("user");
        this.jwt = null;
        this.user = null;
        this.router.navigate(['/']);
    }

    getJwt(): string {
        if (!this.jwt) {
            this.jwt = localStorage.getItem("jwt");
        }
        return this.jwt;
    }

    public isLoggedIn(): boolean {
        let jwt = this.getJwt();
        return jwt !== 'undefined' && jwt !== null && jwt.length > 0;
    }

    public replacePlayer(newPlayer: Player) {
        for (let i in this.user.players) {
            let player = this.user.players[i];
            if (player.id == newPlayer.id) {
                this.user.players[i] = newPlayer;
                break;
            }
        }
        if (this.user.activePlayer.id == newPlayer.id) {
            this.user.activePlayer = newPlayer;
        }
        this.setUser();
    }

    public replaceAchievement(newAchievement: AchievementProgress) {
        for (let i in this.user.achievements) {
            let achievement = this.user.achievements[i];
            if (achievement.id == newAchievement.id) {
                this.user.achievements[i] = newAchievement;
                break;
            }
        }
        this.setUser();
    }

    public findPlayerById(id: number) {
        for (let i in this.user.players) {
            let player = this.user.players[i];
            if (player.id == id) {
                return this.user.players[i];
            }
        }
        return null;
    }

    public getUser(): Observable<User> {
        let setUser = Tennio.closure(this, this.setUser);
        return Observable.create((observer: any) => {
            if (!this.user) {
                let userJson = localStorage.getItem("user");
                if (userJson) {
                    this.user = JSON.parse(userJson);
                    observer.next(this.user);
                    observer.complete();
                } else {
                    if (!this.getJwt()) {
                        observer.next(null);
                        observer.complete();
                        return;
                    }
                    this.getUserInformation().subscribe(data => {
                        if (data !== null) {
                            setUser(data);
                        }
                        console.log(this.user);
                        observer.next(data);
                        observer.complete();
                    });
                }
            } else {
                observer.next(this.user);
            }
        });
    }

    public reset(): Observable<User> {
        this.user = null;
        localStorage.removeItem("user");
        return this.getUser();
    }

    public setUser(user: User = null) {
        if (user != null) {
            this.user = user;
        }

        localStorage.setItem("user", JSON.stringify(this.user));
    }

    public addPlayer(player: Player) {
        if (!this.user.players.length) {
            this.user.activePlayer = player;
            this.user.players.push(this.user.activePlayer);
        } else {
            this.user.players.push(player);
        }
    }

    getJwtHeader(): Headers {
        if (!this.getJwt()) {
            console.log("JWT HEADER CALLED WITHOUT AUTHORIZATION")
        }
        let headers = new Headers();
        headers.append('Authorization', this.jwt);
        return headers;
    }

    protected getHeaders(options: any, post: boolean = false) {
        let o = options || {headers: new Headers()};

        if (this.isLoggedIn()) {
            o.headers.append("Authorization", this.getJwt());
        }

        if (post) {
            o.headers.append('Content-Type', 'application/json');
        }

        return o;
    }

    activatePlayer(id: number) {
        this.user.players.forEach((player: Player) => {
            if (player.id == id) {
                this.user.activePlayer = player;
            }
        });
        this.setUser();
    }
}