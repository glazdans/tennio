import {Component, OnInit, Input, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {LoginService} from "./login.service";
import {loginFormView, LoginFormComponent} from "./login.form.component";

declare var $: any;

@Component({
    selector: 'tennio-login',
    templateUrl: "login.component.html"
})

export class LoginComponent implements OnInit {
    @Input()
    btnClass: string = null;

    loginView: loginFormView = loginFormView.LOGIN;

    @ViewChild("form")
    loginComponent: LoginFormComponent;

    constructor(private loginService: LoginService, private router: Router) {
    }

    ngOnInit() {
        if(this.router.url.indexOf("logout") !== -1) {
           this.logout();
           return;
        }

        if (this.btnClass) {
            $('.' + this.btnClass).on('click', LoginComponent.openForm);
        } else {
            LoginComponent.openForm();
        }
    }

    static openForm() {
        $('#login-modal').modal();
        $('#reg-username').focus();
    }

    static destroyModalSkeleton() {
        $('#login-modal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }

    logout() {
        this.router.navigate(['/']);
        this.loginService.logout();
    }
}