import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {LoginService} from "./login.service";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private _loginService: LoginService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this._loginService.getJwt()) {
            return true;
        }

        // Store the attempted URL for redirecting
        this._loginService.redirectUrl = state.url;
        // Navigate to the login page
        this.router.navigate(['/']);
        return false;
    }
}