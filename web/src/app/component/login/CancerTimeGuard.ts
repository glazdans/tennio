import {Injectable} from "@angular/core";
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {TimeService} from "../../utility/timeservice";
import {SnotifyService} from "ng-snotify";

declare let $: any;

@Injectable()
export class CancerTimeGuard implements CanActivate {
    private static toasted: boolean = false;
    constructor(private timeService: TimeService, private snotifyService: SnotifyService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let canActivate = !this.timeService.isCancerTime;
        if(!canActivate && !CancerTimeGuard.toasted) {
            this.snotifyService.warning('Your computers clock is wrong!', 'Please update your computers clock to assure the best experience!', {
                timeout: 0,
                showProgressBar: false,
                closeOnClick: false,
                titleMaxLength: 200
            });
            CancerTimeGuard.toasted = true;
        }
        return true;
    }
}