import {Injectable} from "@angular/core";
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {LoginService} from "./login.service";
import {SnotifyService} from "ng-snotify";

@Injectable()
export class PlayerSelectedGuard implements CanActivate {
    constructor(private _loginService: LoginService, private snotifyService: SnotifyService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let canActivate = this._loginService.hasActivePlayer();
        if(!this._loginService.hasPlayers()) {
            this.snotifyService.error("Please create a player!", "");
        }
        if (!canActivate) {
            this.snotifyService.error("Please select a player!", "");
        }
        return canActivate;
    }
}