import {Component, Input, ViewChild} from "@angular/core";
import {LoginService} from "./login.service";
import {LoginComponent} from "./login.component";
import {Tennio} from "../../utility/Tennio";
import {NgForm} from "@angular/forms";
import {CountryPickerComponent} from "../ui/countrypicker/countrypicker.component";

declare var $: any;

@Component({
    selector: 'tennio-login-form',
    templateUrl: "login.form.component.html"
})

export class LoginFormComponent {
    @Input()
        view: loginFormView;
    @Input()
        showSwap: boolean = true;

    @ViewChild('cpp')
        countryPicker: CountryPickerComponent;

    showError: boolean = false;
    showSuccess: boolean = false;

    errorMsg: string = "An error occurred!";
    views: any = loginFormView;

    constructor(private loginService: LoginService) {
    }

    onError(reason: any) {
        this.errorMsg = reason;
        this.showError = true;
        this.showSuccess = false;
    }

    onSuccess(form: NgForm){
        form.reset();
        this.showError = false;
        this.showSuccess = true;
    }

    submit(form : NgForm) {
        if(this.countryPicker) {
            form.value.countryCode = this.countryPicker.countryCode;
        }
        switch(this.view) {
            case this.views.LOGIN:
                this.loginService.login(form).then(LoginComponent.destroyModalSkeleton, (reason) => this.onError(reason));
                break;
            case this.views.REGISTER:
                this.loginService.register(form).then(Tennio.closure(this, this.onSuccess, form), (reason) => this.onError(reason));
                break;
            case this.views.SHORT_REGISTER:
                this.loginService.register(form).then(Tennio.closure(this, this.onSuccess, form), (reason) => this.onError(reason));
                break;
            case this.views.RESET_PASSWORD:
                this.loginService.requestPasswordReset(form).then(Tennio.closure(this, this.onSuccess, form), (reason) => this.onError(reason));
                break;
        }
    }

    swapForms(openReset: boolean) {
        if(!openReset) {
            if(this.view != this.views.LOGIN) {
                this.view = this.views.LOGIN;
            } else if(this.view != this.views.REGISTER) {
                this.view = this.views.REGISTER;
            }
        } else {
            this.view = this.views.RESET_PASSWORD;
        }
        this.showSuccess = false;
        this.showError = false;
    }

    getSwapBtnTxt() {
        if(this.view != this.views.LOGIN) {
            return "Already registered?";
        } else if (this.view != this.views.REGISTER) {
            return "Not registered?";
        }
    }

    getSubmitBtnTxt(){
        if(this.view == this.views.LOGIN) {
            return 'Login';
        } else if(this.view == this.views.REGISTER || this.view == this.views.SHORT_REGISTER) {
            return 'Register';
        } else if(this.view == this.views.RESET_PASSWORD) {
            return 'Request new password';
        }
    }
}

export enum loginFormView {
    LOGIN, REGISTER, RESET_PASSWORD, SHORT_REGISTER
}