import {Component, OnInit} from "@angular/core";
import {LoginService} from "../login/login.service";
import {User} from "../../model/user";

@Component({
    selector: 'tennio-header',
    templateUrl: "header.component.html",
    styleUrls: ['header.component.scss']
})

export class HeaderComponent implements OnInit {
    user: User;

    constructor(public _loginService: LoginService) {
    }

    ngOnInit() {
        if (this._loginService.isLoggedIn()) {
            this._loginService.getUser().subscribe(user => this.user = user);
        }
    }
}