import {Component, Input, OnChanges, ViewChild, SimpleChanges} from "@angular/core";
import {Player, Stats} from "../../model/player";
import {StatComponent} from "./player-stats.component";

declare var $: any;

@Component({
    selector: 'tennio-stats-popup',
    template: `
<div class="modal fade" id="stats-modal-{{id}}" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center" *ngIf="player">
                <h4 class="modal-title">{{player?.name}}</h4>
            </div>

            <div class="modal-body">

            </div>
                <tennio-stats #swag></tennio-stats>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
`
})

export class StatPopup implements OnChanges{
    @Input() player: Player = null;
    @Input() itemStats: Stats = null;
    @ViewChild('swag') s: StatComponent;
    static lastId:number = 0;
    id: number;

    constructor() {
        this.id = StatPopup.lastId++;
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.s.player = this.player;
        this.s.itemStats = this.itemStats;
        this.s.stats = this.s.getStats();
    }

    public show() {
        $('#stats-modal-' + this.id).modal();
    }
}