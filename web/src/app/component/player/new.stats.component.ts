import {Component, Input} from "@angular/core";

import {StatComponent, Stat} from "./player-stats.component";
import {Player, Stats} from "../../model/player";

declare let $: any;

@Component({
    selector: 'tennio-new-stats',
    templateUrl: 'new.stats.component.html',
    styleUrls: ['new.stats.component.scss', 'player-stats.component.scss']
})

export class NewStatComponent extends StatComponent {
    @Input() player: Player = null;
    @Input() itemStats: Stats = null;

    constructor() {
         super();
    }

    getStats() {
        if (!this.player && !this.itemStats) {
            return;
        }

        let stats : any = [];
        let statCpy = this.player ? this.player.stats : this.itemStats;

        delete statCpy.mentalStrength;
        delete statCpy.mentalStrengthLVL;

        delete statCpy.fitness;
        delete statCpy.fitnessLVL;

        delete statCpy.technique;
        delete statCpy.techniqueLVL;

        delete statCpy.recovery;
        delete statCpy.reactionLVL;

        for (let i in statCpy) {
            let lvlIdx = i.indexOf('LVL');
            let stat;

            if(i == 'id'){
                continue;
            }

            if(this.player) {
                if (lvlIdx == -1) {
                    continue;
                }

                stat = i.substring(0, lvlIdx);
                if (!stat) {
                    continue;
                }
            } else {
                if (lvlIdx == -1) {
                    stat = i;
                } else {
                    continue;
                }
            }
            if(statCpy[stat] >= StatComponent.maxEXP[1] && statCpy[i] == 0){
                statCpy[i] = StatComponent.getStatLVL(statCpy[stat]);
            }
            stats.push(new Stat(statCpy[stat], statCpy[i], stat));
        }
        return stats;
    }
}