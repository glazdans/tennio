import {Component, OnInit, Input, OnChanges} from "@angular/core";
import {Player, Stats} from "../../model/player";
import {Tennio} from "../../utility/Tennio";

declare let $: any;

@Component({
    selector: 'tennio-stats',
    templateUrl: 'player-stats.component.html',
    styleUrls: ['player-stats.component.scss'],
    inputs: ['player']
})

export class StatComponent implements OnInit, OnChanges {
    @Input() player: Player = null;
    @Input() itemStats: Stats = null;

    public stats: Stat[] = [];

    protected static BASE_EXP = 111;
    protected static SCALE = 1.15;
    protected static MAX_LEVEL = 30;
    protected static maxEXP: number[];

    getStatPercentage(stat: Stat): number {
        if(this.player) {
            let gEXP = stat.EXP - StatComponent.maxEXP[stat.LVL - 1];
            let dEXP = StatComponent.maxEXP[stat.LVL] - StatComponent.maxEXP[stat.LVL - 1];

            return (gEXP / dEXP) * 100;
        } else {
            return stat.LVL / StatComponent.MAX_LEVEL * 100;
        }
    }

    private static calcXPRanges() {
        let exp = new Array(StatComponent.MAX_LEVEL + 1);
        exp[0] = 0;

        for (let i = 1; i <= StatComponent.MAX_LEVEL; i++) {
            exp[i] = (exp[i - 1] + Math.ceil(Math.pow((StatComponent.BASE_EXP * i), StatComponent.SCALE)));
        }
        localStorage.setItem('maxEXP', JSON.stringify(exp));
        return exp;
    }

    ngOnInit() {
        if(this.player && !StatComponent.maxEXP) {
            StatComponent.maxEXP = JSON.parse(localStorage.getItem('maxEXP'));
        }
        if (!StatComponent.maxEXP) {
            StatComponent.maxEXP = StatComponent.calcXPRanges();
        }
        this.stats = this.getStats();
    }

    ngOnChanges(changes : any): void {
        if(!StatComponent.maxEXP){
            return;
        }
        let newStats = this.getStats();

        if (this.player && changes.player && changes.player.currentValue && changes.player.previousValue && changes.player.currentValue.id == changes.player.previousValue.id) {
            for (let i in this.stats) {
                let oldStat = this.stats[i];
                let newStat = newStats[i];

                if (oldStat.EXP != newStat.EXP) {
                    newStat.change = true;
                    let statElem = $("#" + newStat.name + '-bar');
                    let newPerc = this.getStatPercentage(newStat);
                    let oldPerc = this.getStatPercentage(oldStat);

                    statElem.css('background-color', 'purple');
                    if (oldPerc >= newPerc) {
                        statElem.animate({
                            width: '100%'
                        }, 400).delay(450).css('width', 0).delay(50).animate({
                            width: newPerc + '%'
                        }, 400);
                    } else {
                        statElem.animate({
                            width: newPerc + '%',
                        }, 400);
                    }
                }
            }
            setTimeout(Tennio.closure(this, (newStats : any) => {
                this.stats = newStats
            }, newStats), 1e3);
        } else {
            this.stats = newStats;
        }
    }

    getStats(): Stat[] {
        if (!this.player && !this.itemStats) {
            return;
        }

        let stats : any = [];
        let statCpy = this.player ? this.player.stats : this.itemStats;

        for (let i in statCpy) {
            let lvlIdx = i.indexOf('LVL');
            let stat;

            if(i == 'id'){
                continue;
            }

            if(this.player) {
                if (lvlIdx == -1) {
                    continue;
                }

                stat = i.substring(0, lvlIdx);
                if (!stat) {
                    continue;
                }
            } else {
                if (lvlIdx == -1) {
                    stat = i;
                } else {
                    continue;
                }
            }
            if(statCpy[stat] >= StatComponent.maxEXP[1] && statCpy[i] == 0){
                statCpy[i] = StatComponent.getStatLVL(statCpy[stat]);
            }
            stats.push(new Stat(statCpy[stat], statCpy[i], stat));
        }
        return stats;
    }

    public static getStatLVL(exp: number) {
        if(exp < StatComponent.maxEXP[1]) {
            return 1;
        }
        for(let i = 0; i <= StatComponent.MAX_LEVEL; i++) {
            if(exp > StatComponent.maxEXP[i] && exp <= StatComponent.maxEXP[i + 1]) {
                return i + 1;
            }
        }
    }
}

export class Stat {
    EXP: number;
    LVL: number;
    name: string;
    change: boolean = false;

    constructor(EXP : any, LVL : any, name : any) {
        this.EXP = EXP;
        this.LVL = LVL;
        this.name = name;
    }
}