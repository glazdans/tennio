import {Injectable, OnDestroy} from "@angular/core";
import {Tennio} from "../../utility/Tennio";
import {Notification, NotificationType, NotificationAttributeType} from "../menu/notification";
import {WebSocketService, WebSocketSubscription} from "../../utility/websocket.service";
import {LoginService} from "../login/login.service";
import {Router, NavigationStart, NavigationEnd} from "@angular/router";
import {SnotifyService} from "ng-snotify";

declare var $ : any;
declare var moment : any;

@Injectable()
export class MatchmakingService implements OnDestroy {
    private mmGameSub: WebSocketSubscription;
    private notificationSub: WebSocketSubscription;
    private mmSub: WebSocketSubscription;
    private gameSub: WebSocketSubscription;

    gameId: number;
    state: MMState = MMState.None;
    isGameOpen: boolean = false;
    private _matchupPrincipal: MatchMakingDecisionPrincipal = null;

    // vars for upcoming games
    timeLeftUntilGame: string = '00:00:00';
    gameStartTime: Date;
    waitingForGame: boolean = false; // don't remove

    // vars for matchmaking games
    timeInQueue: string = '00:00';
    queueStartTime: Date;
    totalPlayersInQueue: number = 1;

    matchupTimeLeft: string = '00:15';

    constructor(private loginService: LoginService,
                private router: Router,
                private ws: WebSocketService,
                private snotifyService: SnotifyService) {
        loginService.registerOnLogoutCallback(Tennio.closure(this, this.leaveQueue));
        this.mmGameSub = this.ws.subscribe("/user/matchmaking/game", Tennio.closure(this, this.onGameNotifications));
        this.gameSub = this.ws.subscribe("/user/matchup", Tennio.closure(this, this.onGameNotifications));
        this.notificationSub = this.ws.subscribe("/user/notifications", Tennio.closure(this, this.onNotifications));
        this.router.events.subscribe((event) => {
            if(event instanceof NavigationStart || event instanceof NavigationEnd) {
                this.isGameOpen = event.url.toString().indexOf("game/" + this.gameId) !== -1;
            }
        });
    }

    public onDecide(decision: boolean) {
        this._matchupPrincipal.decision = decision;
        this.ws.send("/app/match/decide", {}, JSON.stringify(this._matchupPrincipal));
        if (!decision) {
            this._matchupPrincipal = null;
            this.state = MMState.None;
            $('#game-decision-modal').modal('hide');
        }
    }

    private onNotifications(messages: any) {
        if(messages.inMMQueue) {
            this.joinQueue();
        }
        if (typeof messages.game !== "undefined") {
            if(messages.game !== null) {
                this.onGame(messages.game.startTime, messages.game.id);
            }
        } else {
            this.filterGameEndAndMatchmakingErrors(messages);
        }
    }

    private onGameNotifications(notification: Notification) {
        if (Tennio.N.checkType(notification, NotificationType.DuelExpired)) {
            $('#game-decision-modal').modal('hide');
            if (this._matchupPrincipal && Tennio.N.findOneAttr(notification, NotificationAttributeType.DuelId) == this._matchupPrincipal.matchupId) {
                this.state = MMState.None;
                $('#game-expired').modal();
            }
        } else if (Tennio.N.checkType(notification, NotificationType.DuelOpponentDeclined)) {
            $('#game-decision-modal').modal('hide');
            if (this.gameId.toString().indexOf("tournament") == -1) {
                this.state = MMState.InQueue;
            }
        } else if (Tennio.N.checkType(notification, NotificationType.DuelOpponentFound)) {
            let duelId = Tennio.N.findOneAttr<string>(notification, NotificationAttributeType.DuelId);
            let playerId = Tennio.N.findOneAttr<number>(notification, NotificationAttributeType.PlayerId);
            let expiryTime = Tennio.N.findOneAttr<number>(notification, NotificationAttributeType.Expires);
            if (duelId && playerId) {
                this._matchupPrincipal = new MatchMakingDecisionPrincipal(playerId, duelId, expiryTime);
                this.updateMatchupTimeLeft();
                $('#game-decision-modal').modal();
                this.state = MMState.WaitingForAccept;
            } else {
                this.snotifyService.error("Error occured!", "");
            }
        } else if (Tennio.N.checkType(notification, NotificationType.Duel)) {
            this.setGameId(notification.notificationAttributes[NotificationAttributeType[NotificationAttributeType.GameId]]);
            $('#game-decision-modal').modal('hide');
        } else if (Tennio.N.checkType(notification, NotificationType.TournamentGame)
            || Tennio.N.checkType(notification, NotificationType.Game)) {
            $('#game-decision-modal').modal('hide');
            this.onGame(+notification.notificationEventStart,
                notification.notificationAttributes[NotificationAttributeType[NotificationAttributeType.GameId]]);
        }
    }

    private updateMatchupTimeLeft() {
        if (this._matchupPrincipal) {
            let now = Date.now();
            this.matchupTimeLeft = moment.utc(moment(+this._matchupPrincipal.expiryTime).diff(moment(now))).format("mm:ss");
            if (now < this._matchupPrincipal.expiryTime) {
                setTimeout(Tennio.closure(this, this.updateMatchupTimeLeft), 999);
            } else {
                this.matchupTimeLeft = "00:00";
            }
        }
    }

    get matchupPrincipal(): MatchMakingDecisionPrincipal {
        return this._matchupPrincipal;
    }

    private onGame(startTime : any, gameID : any) {
        this.state = MMState.WaitingForGame;
        this.gameStartTime = new Date(startTime);
        setTimeout(Tennio.closure(this, this.updateTimeInQueue), 1e3);
        let timeout = startTime - Date.now();
        timeout = timeout >= 0 ? timeout : 0;
        setTimeout(Tennio.closure(this, (gameId : any) => {
            this.setGameId(gameId);
        }, gameID), timeout);
    }

    public setGameId(gameId: number){
        this.gameId = gameId;
        this.state = MMState.GameReady;
        this.isGameOpen = this.router.url.toString().indexOf("game/" + this.gameId) !== -1;
        if(!this.isGameOpen) {
            $('#game-ready-modal').modal();
            this.snotifyService.clear();
        }
    }

    ngOnDestroy() {
        if(this.isInQueue()) {
            this.leaveQueue().then(Tennio.closure(this, () => {
                this.mmGameSub.unsubscribe();
                if (this.notificationSub) {
                    this.notificationSub.unsubscribe();
                }
            })).catch(() => {
                console.log("failed to leave queue");
            });
        }
        if (this.gameSub) {
            this.gameSub.unsubscribe();
        }
    }

    public joinQueue(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                this.loginService.getUser()
                    .subscribe(user => {
                        this.ws.send("/app/match/join", {}, {});
                        resolve(true);
                    });
            } catch (e) {
                resolve(false);
            }
        }).then(Tennio.closure(this, this.onJoinQueue));
    }

    private onJoinQueue(resp : any) {
        this.timeInQueue = "00:00";
        if (resp) {
            this.state = MMState.InQueue;
            this.queueStartTime = new Date();
            setTimeout(Tennio.closure(this, this.updateTimeInQueue), 1e3);
        }
        this.mmSub = this.ws.subscribe("/matchmaking/active", (count) => {
            this.totalPlayersInQueue = +count;
        });
    }

    private filterGameEndAndMatchmakingErrors(notification: Notification) {
        if (Tennio.N.checkType(notification, NotificationType.GameResult)) {
            Tennio.N.onFindAttr(notification, NotificationAttributeType.GameId, (gameId) => {
                if(gameId == this.gameId) {
                    this.gameId = null;
                    this.state = MMState.None;
                    return false;
                }
                return true;
            });
        } else if (Tennio.N.checkType(notification, NotificationType.MatchmakingError)) {
            Tennio.N.onFindAttr(notification, NotificationAttributeType.CurrentMatchMakingGame, () => {
                this.gameId = null;
                this.state = MMState.None;
                if(notification.additionalInfo) {
                    this.snotifyService.error("Error occured!", notification.additionalInfo);
                }
                return false;
            });
        }
    }

    private updateTimeInQueue() {
        if (this.isInQueue()) {
            let diff = moment.utc(moment(Date.now()).diff(moment(this.queueStartTime)));
            this.timeInQueue = diff.format('x') >= 36e5 ? diff.format("HH:mm:ss") : diff.format("mm:ss");
        } else if (this.isWaitingForGame()) {
            let diff = moment.utc(moment(this.gameStartTime).diff(Date.now()));
            this.timeLeftUntilGame = diff.format('x') >= 36e5 ? diff.format("HH:mm:ss") : diff.format("mm:ss");
        }

        if(!this.isGameReady() && !this.isNoneState()) {
            setTimeout(Tennio.closure(this, this.updateTimeInQueue), 1e3);
        }
    }

    public leaveQueue(): Promise<boolean> {
        if(this.isInQueue()) {
            return new Promise((resolve, reject) => {
                try {
                    this.loginService.getUser()
                        .subscribe(user => {
                            this.ws.send("/app/match/leave", {playerId: user.activePlayer}, {});
                            resolve(true);
                        });
                } catch (e) {
                    resolve(false);
                }
            }).then(Tennio.closure(this, this.onLeaveQueue));
        }
    }

    private onLeaveQueue(resp : any) {
        if (resp) {
            this.mmSub.unsubscribe();
            this.state = MMState.None;
        }
    }

    public isInQueue() {
        return this.state == MMState.InQueue;
    }

    public isWaitingForGame() {
        return this.state  == MMState.WaitingForGame;
    }

    public isGameReady() {
        return this.state  == MMState.GameReady;
    }

    public isNoneState() {
        return this.state  == MMState.None;
    }

    onBotGame(gameId: number) {
        this.gameId = gameId;
        this.state = MMState.GameReady;
    }
}

export enum MMState {
    None, InQueue, WaitingForGame, GameReady, WaitingForAccept
}

class MatchMakingDecisionPrincipal {
    constructor(playerId: number, matchupId: string, expiryTime: number) {
        this.playerId = playerId;
        this.matchupId = matchupId;
        this.expiryTime = expiryTime;
    }

    playerId: number;
    matchupId: string;
    expiryTime: number;
    decision: Boolean = null;
}