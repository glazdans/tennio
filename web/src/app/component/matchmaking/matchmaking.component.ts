import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {LoginService} from "../login/login.service";
import {MatchmakingService} from "./matchmaking.service";
import {GameService} from "../../section/game/game.service";
import {ServerInfo} from "../../section/game/websocket/server-info";

declare var $: any;

@Component({
    selector: 'tennio-matchmaking',
    templateUrl: "matchmaking.component.html",
    providers: [
        MatchmakingService
    ]
})

export class MatchMakingComponent implements OnInit {
    inQueue: boolean = false;
    playerId: number;

    constructor(private _router: Router, private loginService: LoginService,
                public _mmService: MatchmakingService, private _gameService: GameService) {
    }

    ngOnInit() {
        this.loginService.getUser().subscribe(user => {
            this.playerId = user.activePlayer.id;
        });
    }

    openGameSelectionModal() {
        $('#game-selection-modal').modal();
    }

    hideGameSelectionModal() {
        $('#game-selection-modal').modal('hide');
    }

    joinQueue() {
        this._mmService.joinQueue();
    }

    playVSAI() {
        this._gameService.startAIGame().subscribe((serverInfo: ServerInfo) => {
            if (serverInfo) {
                this._mmService.onBotGame(serverInfo.gameId);
                this._router.navigate(['/game', serverInfo.gameId]);
            }
        });
    }

    leaveQueue() {
        this._mmService.leaveQueue();
    }

    modalNavigateToGame() {
        $('#game-ready-modal').modal('hide');
        this.navigateToGame();
    }

    navigateToGame() {
        let link = ['/game', this._mmService.gameId];
        this.inQueue = false;
        this._router.navigate(link);
    }

    acceptGame() {
        this._mmService.onDecide(true);
    }

    declineGame() {
        this._mmService.onDecide(false);
    }
}