import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Http} from "@angular/http";
import {environment} from "../../../environments/environment";
import {ResponseCode} from "../../utility/response";
import {Tennio} from "../../utility/Tennio";
import {NgForm} from "@angular/forms";
import {LoginService} from "../login/login.service";

@Component({
    selector: 'tennio-confirmation',
    templateUrl: "confirmation.component.html",
    styleUrls: ['confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {
    success: any = null;
    error: string = null;
    changePW: boolean = false;
    mail: string;
    key: string;

    constructor(private route: ActivatedRoute, private http: Http, private loginService: LoginService) {
    }

    ngOnInit(): void {
        this.route.queryParams.subscribe(params => {
            let mail = params['email'];
            let key = params['key'];
            let changepw = params['changepw'];
            if (mail && key) {
                this.http.get(environment.apiUrl + "/user/confirm?email=" + mail + "&key=" + key).subscribe((data) => {
                    let resp: any = data.json();
                    if (Tennio.compareEnum(ResponseCode, resp.code, ResponseCode.ERROR)) {
                        this.error = resp.message;
                        this.success = false;
                    } else {
                        if(!changepw) {
                            this.success = true;
                        } else {
                            this.changePW = true;
                            this.mail = mail;
                            this.key = key;
                        }
                    }
                });
            } else {
                this.success = false;
                this.error = "Invalid confirmation link!";
            }
            if(this.success && this.loginService.user != null) {
                this.loginService.reset();
            }
        })
    }

    canSubmit(form: NgForm): boolean {
        return form.value.newPassword && form.value.newPassword.length >= 6 && form.value.newPassword == form.value.rnewPassword;
    }

    submit(form: NgForm): void {
        this.loginService.postPasswordChange(form.value.newPassword, this.mail, this.key).subscribe((data:boolean) => {
            this.success = data;
            if(!data) {
                this.error = "Something went wrong!";
            }
        });
    }
}