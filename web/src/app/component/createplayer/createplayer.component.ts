import {Component, Input, ViewChild} from "@angular/core";
import {LoginService} from "../login/login.service";
import {WebSocketService} from "../../utility/websocket.service";
import {ResponseCode} from "../../utility/response";
import {Tennio} from "../../utility/Tennio";
import {ModalComponent} from "ng2-bs3-modal/components/modal";
import {CountryPickerComponent} from "../ui/countrypicker/countrypicker.component";
import {SnotifyService} from "ng-snotify";

@Component({
    selector: 'create-player',
    templateUrl: 'createplayer.component.html',
    styleUrls: ['createplayer.component.scss']
})
export class CreatePlayerComponent {
    @Input()
        modal: boolean = false;

    @ViewChild('cpModal')
        cpModal: ModalComponent;

    @ViewChild('cp')
        cp: CountryPickerComponent;

    playerName: string;

    constructor(private _loginService: LoginService, private ws: WebSocketService, private snotifyService: SnotifyService) {
    }

    showModal() {
        if(this.cpModal) {
            this.cpModal.open("small");
        }
    }

    hideModal() {
        this.cpModal.dismiss();
    }

    createPlayer() {
        this._loginService.createPlayer(this.playerName, this.cp.countryCode).subscribe(
            data => {
                if (!Tennio.compareEnum(ResponseCode, data.code, ResponseCode.OK)) {
                    this.snotifyService.error("Error occured!", data.message);
                }
                this._loginService.addPlayer(data.object);
                this.ws.send("/user/notifications/activeplayer", {}, data.object.id);
                this._loginService.activatePlayer(data.object.id);
                this.cpModal.dismiss();
            }
        )
    }
}
