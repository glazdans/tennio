import {Component, Input, Output, EventEmitter, OnChanges, ViewChild} from "@angular/core";
import "rxjs/Rx";
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'slider',
    styleUrls: ['slider.component.scss'],
    template: `<input #slider type="range" 
                [attr.min]="min" [attr.max]="max" 
                [attr.value]="value" [attr.step]="step" 
                (change)="onChange($event)" (input)="onChange($event)"/>`,
})
export class SliderComponent {
    @Input()
    min: number = 1;
    @Input()
    max: number = 100;
    @Input()
    value: number = 50;
    @Input()
    step: number = 1;

    @Output() valueChanged = new EventEmitter();

    private observable: Observable<number>;

    private tempObs: any;

    ngOnInit() {
        this.observable = Observable.create((observer: any) => {
            this.tempObs = observer;
        });

        this.observable.debounceTime(400)
            .subscribe((event) => this.valueChanged.emit(event));
    }

    onChange(event: any) {
        this.tempObs.next(event.target.value);
    }

}