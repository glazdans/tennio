import {Component, Output, Input} from "@angular/core";
import "rxjs/Rx";
import {countries, Country} from "../../../../assets/countries";
import {LoginService} from "../../login/login.service";

@Component({
    selector: 'country-picker',
    templateUrl: 'countrypicker.component.html'
})

export class CountryPickerComponent {
    countries: Country[];
    @Input()
    @Output()
        countryCode: string = null;

    @Output()
        countryName: string = null;

    @Input()
        label: string = "Country";

    constructor(loginService: LoginService) {
        this.countries = countries;
        if(!this.countryCode && loginService.user && loginService.user.country) {
            this.countryCode = loginService.user.country.code;
            this.countryName = loginService.user.country.name;
        } else {
            this.countryCode = 'lv'; // hail potat
            this.countryName = 'Latvia';
        }
    }

    onChange(countryCode: string, countryName: string){
        this.countryCode = countryCode;
        this.countryName = countryName;
    }
}