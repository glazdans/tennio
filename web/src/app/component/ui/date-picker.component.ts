import {Component, OnInit, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import "rxjs/Rx";
import {Observable} from "rxjs/Observable";
// More documentation
//https://eonasdan.github.io/bootstrap-datetimepicker/
declare let $: any;

@Component({
    selector: 'date-picker',
    template: `
            <div class="form-group">
                <div class='input-group date' [attr.id]="id" >
                    <input [disabled]="disabled" required="true" type='text' class="form-control" (change)="onChange($event)" (input)="onChange($event)"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>`
})

export class DatePickerComponent implements OnInit, AfterViewInit {
    @Input()
    id : any;
    @Input()
    date : any;
    @Input()
    minDate: Date;
    @Input()
    disabled: boolean = false;

    @Output() dateChange = new EventEmitter();

    observable: Observable<number>;

    tempObs : any;

    ngOnInit() {
        this.observable = Observable.create((observer : any) => {
            this.tempObs = observer;
        });

        this.observable
            .subscribe((event) => this.dateChange.emit(event));

        if (this.date % 1 === 0) {
            this.date = new Date(this.date);
        }
    }

    // Don't put in OnInit, because it sets id after
    ngAfterViewInit() {
        $(() => {
            let datePicker = $('#' + this.id);
            datePicker.datetimepicker({
                minDate: this.minDate ? this.minDate : new Date(0)
            });
            if (this.date) {
                datePicker.data("DateTimePicker").date(this.date);
            }
            datePicker.on("dp.change", (e : any) => {
                this.date = new Date(e.date).getTime();
                this.tempObs.next(this.date);
            })
        });
    }

    // Manual typing handler
    onChange(event: any) {
        let date = $('#' + this.id).data("DateTimePicker").date();
        if (date) {
            this.date = date.valueOf();
            this.tempObs.next(this.date);
        }
    }
}