import {Component, Input, OnInit, ViewChild} from "@angular/core";
import "rxjs/Rx";
import {Http, Response} from "@angular/http";
import {LoginService} from "../login/login.service";
import {Tennio} from "../../utility/Tennio";
import {environment} from "../../../environments/environment";
import {Player, Stats} from "../../model/player";
import {StatPopup} from "../player/player-stats.popup";

declare let $: any;

@Component({
    selector: 'paginator',
    templateUrl: 'paginator.component.html'
})

export class PaginatorComponent implements OnInit {
    @Input()
    endpoint: string;
    @Input()
    child: boolean = false;
    @Input()
    title: string = '';
    @Input()
    editCB: () => void = null;
    @Input()
    deleteCB: () => void = null;

    @ViewChild('popupPaginator') popupPaginator: PaginatorComponent;
    @ViewChild('statPopup') statPopup: StatPopup;

    protected _url = environment.apiUrl;
    elements: any[] = [];
    visiblePages: number = 5;
    page: number = null;
    currentElements: any[] = [];
    size: number = 10;
    totalPages: number = 1;
    lastPage: boolean = false;
    firstPage: boolean = false;
    stats: Stats;

    player: Player = null;

    constructor(protected http: Http, protected loginService: LoginService) {
    }

    ngOnInit() {
        if (!this.child) {
            this.loadPage(0);
        }
    }

    protected getVisiblePages(): number[] {
        let end = this.page + this.visiblePages;
        if (this.totalPages < end) {
            end = this.totalPages;
        }
        return this.range(Math.floor(this.page / this.visiblePages) + 1, end);
    }

    protected range(from: number, to: number): number[] {
        let range : any = [];
        for (let i = from; i <= to; i++) {
            range.push(i);
        }
        return range;
    }

    protected loadPage(page : any) {
        if(page == this.page || page < 0 || page >= this.totalPages){
            return;
        }

        this.page = page;
        if(page in this.elements) {
            this.currentElements = this.elements[page];
        } else {
            let url = this._url + this.endpoint + "?page=" + this.page + "&size=" + this.size;
            this.http.get(url, {
                headers: this.loginService.getJwtHeader()
            }).subscribe(Tennio.closure(this, this.onLoadPage));
        }
    }

    protected getKeys() {
        return this.currentElements.length ? Object.keys(this.currentElements[0]) : [];
    }

    protected getValues(element : any) {
        let arr : any = [];
        for (let value in element) {
            if (!element.hasOwnProperty(value)) {
                continue;
            }

            if (element[value] != null && typeof element[value] == 'object' && !(element[value] instanceof Array) || (element[value] instanceof Array && element[value].length)) {
                arr.push(new SubView(element, value));
            } else {
                arr.push(element[value]);
            }
        }
        return arr;
    }

    isSubView(value: any): boolean {
        return value instanceof SubView;
    }

    protected openSubView(subView: SubView) {
        if(subView.key == 'stats') {
            if(subView.element.name) {
                this.player = subView.element;
            } else {
                this.player = <Player>{stats: subView.element.stats};
            }
            this.statPopup.show();
            return;
        }
        let els = subView.element[subView.key];
        if (!(els instanceof Array)) {
            els = [els];
        }
        if (!this.child) {
            this.popupPaginator.currentElements = els;
            let modal = $('#paginator-modal-' + this.title);
            modal.modal();
        } else {
            this.currentElements = els;
        }
    }

    protected onLoadPage(page: Response) {
        let response = page.json();
        this.currentElements = response.content;
        this.elements[response.number] = response.content;
        this.totalPages = response.totalPages;
        this.lastPage = response.last;
        this.firstPage = response.first;
    }
}

class SubView {
    public element: any;
    public key: string;

    constructor(element : any, key : any) {
        this.element = element;
        this.key = key;
    }

    toString() {
        return 'View';
    }
}