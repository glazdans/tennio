import {Injectable} from "@angular/core";
import "rxjs/Rx";

import {FrontPageStats} from "./frontpagestats";
import {Observable} from "rxjs";
import {AnonymousRestService} from "../../utility/anonymousrestservice";
import {environment} from "../../../environments/environment";

@Injectable()
export class OnlinePlayersService {
    constructor(private rest: AnonymousRestService) {
    }
    private url = environment.apiUrl + '/stats/frontpage';

    getStats(): Observable<FrontPageStats>{
        return this.rest.get(this.url);
    }
}
