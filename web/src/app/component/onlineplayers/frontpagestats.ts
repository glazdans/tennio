export class FrontPageStats {
    public registeredPlayers: number;
    public currentlyOnline: number;
    public playersByCountry: CountryPlayerCount[];
    public shortPlayersByCountry: CountryPlayerCount[];
}

export class CountryPlayerCount {
    public country_code: string;
    public cnt: number;
}