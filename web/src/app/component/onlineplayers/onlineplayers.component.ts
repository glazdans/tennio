import {Component, OnInit} from "@angular/core";
import {OnlinePlayersService} from "./onlineplayers.service";
import {FrontPageStats} from "./frontpagestats";
import {countries} from "../../../assets/countries";

@Component({
    selector: 'tennio-onlineplayers',
    templateUrl: "onlineplayers.component.html",
    styleUrls: ['onlineplayers.component.scss']
})

export class OnlinePlayersComponent implements OnInit {
    stats: FrontPageStats;

    constructor(private service: OnlinePlayersService) {
        this.stats = new FrontPageStats();
        this.stats.registeredPlayers = 0;
        this.stats.currentlyOnline = 0;
    }

    ngOnInit () {
        this.service.getStats().subscribe((data) => {
            this.stats = data;
            this.stats.shortPlayersByCountry = data.playersByCountry.slice(0, 10);
            console.log(data);
        })
    }

    getCountryName(code:string): string {
        for(let country of countries) {
            if(country.code == code) {
                return country.name;
            }
        }
        return '';
    }
}