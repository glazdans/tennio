import {Component, Input} from "@angular/core";
import "rxjs/Rx";
import {PaginatorComponent} from "../ui/paginator.component";
import {Response, Http} from "@angular/http";
import {LoginService} from "../login/login.service";

declare let $: any;

@Component({
    selector: 'tennio-rankings',
    templateUrl: '../ui/paginator.component.html'
})

export class RankingsComponent extends PaginatorComponent {
    endpoint: string = '/stats/rankings';
    @Input()
    title: string = "Top players";
    size: number = 5;

    constructor(protected http: Http, protected loginService: LoginService) {
        super(http, loginService);
    }

    protected onLoadPage(page: Response) : void {
        let response:any = page.json();
        for(let i in response.ranks) {
            let o:any = response.ranks[i];
            response.ranks[i]['#'] = +i + 1;
            response.ranks[i].points = response.ranks[i].elo;
            response.ranks[i].playerName = o.player.name;
            response.ranks[i].username = o.username;
            response.ranks[i].won = o.won;
            response.ranks[i].lost = o.lost;
            response.ranks[i].winRate = this.getWinRate(o.won, o.lost) + '%';
            delete response.ranks[i].player;
            delete response.ranks[i].elo;
            delete response.ranks[i].username;
        }

        this.currentElements = response.ranks;
        this.elements[response.number] = response.content;
        this.totalPages = response.totalPages;
        this.lastPage = response.last;
        this.firstPage = response.first;
        this.child = true;
    }

    getWinRate(w, l) {
        if (l == 0 && w != 0) {
            return 100;
        } else if (w == 0 && l != 0) {
            return 0;
        }
        return Math.round((+w / (+w + +l)) * 100);
    }
}