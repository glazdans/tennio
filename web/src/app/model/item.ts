import {Stats} from "./player";

export class Item {
    public id: number;
    public stats: Stats;
    public name: string;
    public price: number;
    public type: ItemType;
    public equipmentType: EquipmentType;
    public consumable: boolean;
    public usages: number;

    public constructor() {
        this.stats = new Stats();
    }
}

export class ItemEntity {
    public id: number;
    public itemType: Item;
    public playerId: number;
    public equipmentId: number;
    public timesUsed: number;
}

export enum ItemType {
    EQUIPABLE, CONSUMABLE
}

export enum EquipmentType {
    HEAD, TORSO, ARMS, HANDS, LEGS, FEET
}