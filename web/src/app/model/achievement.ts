export class Achievement {
    id: number;
    name: string;
    description: string;
    reward: string;
    type: AchievementType;
    requirement: number;
}

export enum AchievementType {
    GamesWon, GamesLost, PointsAchieved, WinningSpree, LosingSpree, TournamentsWon, CompetitiveGamesWon, GamesInTwentyFourHours, ZeroSetGamesLost
}

export class AchievementProgress {
    id: number;
    achievementType: Achievement;
    progress: number = 0;


    public static fromAchievement(achievement: Achievement): AchievementProgress {
        let ap = new AchievementProgress();
        ap.achievementType = achievement;
        return ap;
    }
}