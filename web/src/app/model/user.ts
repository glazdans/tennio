import {Player} from "./player";
import {ItemEntity} from "./item";
import {Country} from "../../assets/countries";
import {AchievementProgress} from "./achievement";
export class User {
    public username: string;
    public firstname: string;
    public lastname: string;
    public money: number;
    public balls: number;
    public activePlayer: Player;
    public players: Player[];
    public enabled: boolean;
    public email: boolean;
    public id: number;
    public country: Country;
    public items: ItemEntity[];
    public achievements: AchievementProgress[];
    public wonGameCount: number;
    public lostGameCount: number;
}