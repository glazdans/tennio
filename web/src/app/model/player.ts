import {ItemEntity} from "./item";
export class Player {
    public id: number;
    public name: string;
    public stats: Stats;
    public playerSeasonList: PlayerSeason[];
    public equipments: PlayerEquipment[];
}

export class PlayerSeason {
    public id: number;
    public elo: number;
}

export class PlayerEquipment {
    public id: number;
    public equippedItems: { [key:string]:ItemEntity; };
    public active: boolean;

    constructor() {
        this.equippedItems = {};
    }
}

export class Stats {
    speed = 0;
    backhand = 0;
    forehand = 0;
    server = 0;
    precision = 0;
    armStrength = 0;
    technique = 0;
    mentalStrength = 0;
    fitness = 0;
    reaction = 0;
    recovery = 0;

    speedLVL = 0;
    backhandLVL = 0;
    forehandLVL = 0;
    serverLVL = 0;
    precisionLVL = 0;
    armStrengthLVL = 0;
    techniqueLVL = 0;
    mentalStrengthLVL = 0;
    fitnessLVL = 0;
    reactionLVL = 0;
    recoveryLVL = 0;
}