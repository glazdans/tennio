package com.tennio.gameserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.util.Objects;

@SpringBootApplication
@EnableScheduling
public class Application {

    public static void main(String[] args) {
        System.setProperty("server.port", "8081");
        System.setProperty("server.tomcat.compression", "on");
        System.setProperty("server.tomcat.compressableMimeTypes", "application/json,application/xml,text/html,text/xml,text/plain");
        ApplicationContext ctx = SpringApplication.run(Application.class, args);

        // check if ip provided in application.properties points to this server
        try {
            boolean prod = Boolean.parseBoolean(ctx.getEnvironment().getProperty("tennio.production"));
            if(!prod) {
                System.setProperty("tennio.gameServer.address", "127.0.0.1");
                return;
            }

            String serverAddress = ctx.getEnvironment().getProperty("tennio.gameServer.address");
            InetAddress address = InetAddress.getByName(serverAddress);

            // don't validate if address is local or loopback
            if(address.isAnyLocalAddress() || address.isLoopbackAddress()) {
                return;
            }

            URL url = new URL("http://checkip.amazonaws.com/");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

            if (!Objects.equals(address.getHostAddress(), br.readLine())) {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("Please provide a valid server address!!!");
            SpringApplication.exit(ctx);
        }
    }
}