package com.tennio.gameserver.websockets;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tennio.gameserver.GameThread;
import com.tennio.gameserver.JwtTokenUtil;
import com.tennio.gameserver.Player;
import com.tennio.gameserver.SliderUpdate;
import com.tennio.gameserver.exceptions.NoMoreTimeoutsLeftException;
import com.tennio.gameserver.exceptions.PlayerNotFoundException;
import com.tennio.gameserver.exceptions.TimeoutAlreadyStartedException;
import com.tennio.gameserver.game.iodata.GameUpdate;
import com.tennio.gameserver.game.iodata.PointUpdate;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SimpleServer extends WebSocketServer {

    private GameThread gameThread;

    private ObjectMapper mapper;

    private JwtTokenUtil jwtTokenUtil;

    private Map<String, Player> playersByResourceDesc = new HashMap<>();

    public SimpleServer(InetSocketAddress address, GameThread gameThread, JwtTokenUtil jwtTokenUtil) {
        super(address);
        this.gameThread = gameThread;
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        try {
            String jwt = conn.getResourceDescriptor().replace("/?jwt=", "");
            String username = jwtTokenUtil.getUsernameFromToken(jwt);
            Player sender = gameThread.getPlayerByUsername(username);
            if (sender == null) {
                throw new Exception("Can't identify sender!");
            } else {
                playersByResourceDesc.put(conn.getResourceDescriptor(), sender);
                System.out.println("NEW CONNECTION TO: " + conn.getRemoteSocketAddress());
                sendPlayers();
                sendSliders(conn, sender);
                sendGameUpdate(gameThread.generateGameUpdate(gameThread.getEngine(), true));
                sendPointUpdate(gameThread.getEngine().createPointUpdate());
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.connections().remove(conn);
            conn.closeConnection(1, "yes");
        }
    }

    public void onGameEnd(GameEndMessage message) throws JsonProcessingException {
        String json = mapper.writeValueAsString(message);
        synchronized (connections()) {
            for (WebSocket connection : connections()) {
                connection.send(json);
            }
        }
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        System.out.println("closed " + conn.getRemoteSocketAddress() + " with exit code " + code + " additional info: " + reason);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        try {
            WebSocketMessage msg = this.mapper.readValue(message, WebSocketMessage.class);
            String username = jwtTokenUtil.getUsernameFromToken(msg.jwt);
            Player sender;

            try {
                sender = gameThread.getPlayerByUsername(username);
            } catch (PlayerNotFoundException e) {
                System.out.printf("couldnt identify player");
                conn.close();
                return;
            }

            switch (msg.type) {
                case "SLIDER_UPDATE":
                    gameThread.onSliderUpdate(this.mapper.readValue(message, SliderUpdateMessage.class).payload, sender);
                    break;
                case "CONNECTED":
                    if (!sender.isConnected()) {
                        sender.setConnected(true);
                        sendPlayers();
                    }
                    break;
                case "PAUSE":
                    try {
                        gameThread.attemptGamePause(sender);
                    } catch (NoMoreTimeoutsLeftException e) {
                        sendErrorMsg(conn, "You have no timeouts left!");
                    } catch (TimeoutAlreadyStartedException e) {
                        sendErrorMsg(conn, "Game already in timeout!");
                    }
                    break;
                case "SURRENDER":
                    gameThread.surrender(sender);
                    break;
                default:
                    System.out.println(message);
                    break;
            }
        } catch (IOException e) {
            System.out.println("Failed to parse string! => " + message);
            e.printStackTrace();
        }
    }

    @Override
    public void onMessage(WebSocket conn, ByteBuffer message) {
        super.onMessage(conn, message);
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        if (conn != null && ex != null) {
            System.err.println("an error occured on connection " + conn.getRemoteSocketAddress() + ":" + ex);
        }
        ex.printStackTrace();
    }

    private void sendErrorMsg(WebSocket senderConnection, String msg) throws JsonProcessingException {
        WebSocketMessage message = new ErrorMessage();
        message.payload = msg;
        senderConnection.send(this.mapper.writeValueAsString(message));
    }

    private void sendSliders(WebSocket conn, Player sender) throws JsonProcessingException {
        SlidersUpdateMessage sliderUpdateMessage = new SlidersUpdateMessage();
        sliderUpdateMessage.type = "sliderUpdate";
        try {
            com.tennio.gameserver.game.player.Player player = gameThread.getGamePlayerById(sender.getId());
            if (player != null) {
                sliderUpdateMessage.payload = SliderUpdate.fromPlayer(player);
                conn.send(this.mapper.writeValueAsString(sliderUpdateMessage));
            }
        } catch (PlayerNotFoundException e) {
            System.out.println("Player not found");
        }
    }

    private void sendPlayers() {
        String json;
        json = "{\"type\":\"playerInfo\",\"red\":" + gameThread.getPlayerRed().toJSON()
                + ",\"blue\":" + gameThread.getPlayerBlue().toJSON() + "}";
        System.out.println("Json: " + json);

        synchronized (connections()) {
            for (WebSocket connection : connections()) {
                connection.send(json);
            }
        }
    }

    public void sendGameUpdate(GameUpdate gameUpdate) {
        synchronized (connections()) {
            for (WebSocket connection : connections()) {
                Player p = playersByResourceDesc.get(connection.getResourceDescriptor());
                if (Objects.equals(gameThread.getPlayerBlue().getUsername(), p.getUsername())) {
                    gameUpdate.fitness = gameThread.getEngine().playerBlue.attributes.fitness;
                } else if (Objects.equals(gameThread.getPlayerRed().getUsername(), p.getUsername())) {
                    gameUpdate.fitness = gameThread.getEngine().playerRed.attributes.fitness;
                } else {
                    return;
                }
                connection.send(gameUpdate.toJson());
            }
        }
    }

    public void sendPointUpdate(PointUpdate update) {
        String json = "";
        try {
            json = mapper.writeValueAsString(update);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        synchronized (connections()) {
            for (WebSocket connection : connections()) {
                connection.send(json);
            }
        }
    }

}
