package com.tennio.gameserver.websockets;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayerMessage extends WebSocketMessage<com.tennio.gameserver.Player> {
}
