package com.tennio.gameserver.websockets;

public class WebSocketMessage<T> {
    public String type = "";
    public T payload;
    public String jwt;
}
