package com.tennio.gameserver.websockets;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tennio.common.requests.GameEndEvent;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GameEndMessage extends WebSocketMessage<GameEndEvent.GameEndCause> {
    public long causer;

    public GameEndMessage() {
        this.type = "gameEnd";
    }
}
