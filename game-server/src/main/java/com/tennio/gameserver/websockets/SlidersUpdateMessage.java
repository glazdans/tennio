package com.tennio.gameserver.websockets;

import com.tennio.gameserver.SliderUpdate;

import java.util.List;

public class SlidersUpdateMessage extends WebSocketMessage<List<SliderUpdate>> {
    public String type = "sliderUpdate";
}
