package com.tennio.gameserver.game.gamemodes;

import com.tennio.gameserver.GameThread;
import com.tennio.gameserver.Player;
import com.tennio.gameserver.game.player.PlayerAttributes;

public class AIGame extends BasicGame {
    public AIGame() {
    }

    @Override
    public void beforeInitPlayers(GameThread gameThread) {
        Player bot = new Player();
        bot.setConnected(true);
        bot.setName("BOT");

        if (gameThread.getPlayerRed() != null && gameThread.getPlayerBlue() == null) {
            bot.setStats(new PlayerAttributes(gameThread.getPlayerRed().getStats()));
            gameThread.setPlayerBlue(bot);
            return;
        }

        if (gameThread.getPlayerBlue() != null && gameThread.getPlayerRed() == null) {
            bot.setStats(new PlayerAttributes(gameThread.getPlayerBlue().getStats()));
            gameThread.setPlayerRed(bot);
        }
    }
}
