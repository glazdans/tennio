package com.tennio.gameserver.game.mechanics;

import com.tennio.gameserver.game.Engine;

public class VeganMechanic extends GameMechanic {
    private static VeganMechanic instance = new VeganMechanic();

    private VeganMechanic() {
    }

    public static VeganMechanic getInstance() {
        return instance;
    }

    @Override
    public void onEngineUpdate(Engine engine) {
        engine.playerBlue.attributes.fitness = 1000;
        engine.playerRed.attributes.fitness = 1000;
    }
}
