package com.tennio.gameserver.game.mechanics;

import com.tennio.gameserver.game.Engine;
import com.tennio.gameserver.game.player.Player;

public class IrregularScoreLimitGame extends GameMechanic {
    private int minGames;
    private int minSets;
    private boolean twoAdv;

    public IrregularScoreLimitGame(int minGames, int minSets, boolean twoAdv) {
        this.minGames = minGames;
        this.minSets = minSets;
        this.twoAdv = twoAdv;
    }

    @Override
    public void onAfterGame(Engine engine, Player winner) {
        if(winner.score.games >= minGames && (!twoAdv || Math.abs(engine.playerRed.score.games - engine.playerBlue.score.games) >= 2)) {
            engine.onSetEnd(winner);
        }
    }

    @Override
    public void onAfterSet(Engine engine, Player winner) {
        if (winner.score.sets >= minSets) {
            engine.onMatchEnd(winner);
        }
    }
}