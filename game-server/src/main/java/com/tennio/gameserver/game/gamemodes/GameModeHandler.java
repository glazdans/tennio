package com.tennio.gameserver.game.gamemodes;

import com.tennio.common.GameType;
import com.tennio.gameserver.exceptions.UnimplementedGameTypeException;

public final class GameModeHandler {
    private static BasicGame basicGame = new BasicGame();
    private static AIGame aiGame = new AIGame();
    private static QuickFireGame quickFireGame = new QuickFireGame();

    public static BasicGame getGameModeByGameType(GameType gameType) throws UnimplementedGameTypeException {
        if (gameType == GameType.Matchmaking || gameType == GameType.Tournament) {
            return basicGame;
        }

        if (gameType == GameType.AI) {
            return aiGame;
        }

        if(gameType == GameType.TournamentQuickFire) {
            return quickFireGame;
        }

        throw new UnimplementedGameTypeException();
    }
}
