package com.tennio.gameserver.game.iodata;

import com.tennio.gameserver.game.player.Score;

public class PointUpdate {
    public final String type = "pointUpdate";
    public Score redPlayer;
    public Score bluePlayer;
}
