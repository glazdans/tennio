package com.tennio.gameserver.game;

import com.tennio.gameserver.game.gamemodes.BasicGame;
import com.tennio.gameserver.game.helpers.Constants;
import com.tennio.gameserver.game.helpers.CourtDimensions;
import com.tennio.gameserver.game.helpers.TimePoint3D;
import com.tennio.gameserver.game.helpers.Vector3D;
import com.tennio.gameserver.game.player.Player;

import java.util.ArrayList;
import java.util.List;

public class Ball<T extends BasicGame> {
    // characteristics of the ball
    // All data used has been converted to SI.

    // Ball weight in kg
    private double weight = (56.0 + 59.4) / 2 / 1000; // ball must weigh between 56.0 and 59.4 grams

    // Ball radius in m
    private double radius = (6.541 + 6.858) / 2 / 2 / 100; // balls official diameter must be between 65.41-68.58 mm equals to 0.0335 m

    private double diameter = radius * 2;

    // The balls cross section Area in square meters.
    private double crossSectionArea = Math.PI * Math.pow((6.541 + 6.858) / 2 / 2 / 100, 2);

    // 3d point with engine time.
    public double time;

    // whether its served ball, used to determine whether ball are in bound
    public boolean serve = true;

    // Ball placement
    public enum Placement {
        serveTop, serveBottom, baseLine, dropShot, passingShot, attackingBaseLine
    }
    
    public enum PointState {
        ballInGame, returnerScores, returnerError, failedServe
    }

    public Placement placement;
    //  whether ball hits the net
    public boolean net = false;

    //  spin in rotation per minute (-3000 to 3000)
    public double spin = 0;

    public enum SpinTypes {topSpin, none, backspin}

    public SpinTypes spinType = SpinTypes.none;

    /// <summary>  court type</summary>
    public enum courtTypes {
        clay, grass, hard
    }

    public static courtTypes courtType = courtTypes.clay;

    /// <summary>  short ball</summary>
    public boolean shortBall = false;
    public boolean nextBallIsShort = false;
    // 0-ball in game, 1- returner scores,2- returner make error
    public PointState pointState = PointState.ballInGame;

    public Vector3D ballPosition = new Vector3D();

    public Vector3D direction = new Vector3D();

    // consist of all balls trajectory points
    public List<TimePoint3D> trajectoryPoints = new ArrayList<>();

    // consist of all balls trajectory points
    public List<BouncePoint> bouncePoints = new ArrayList<>();

    //
    public double bouncePointTime = 0;
    // determines whether ball is moving or waiting to be moved
    public boolean waiting = true;
    public int bounceCount = 0;
    public int updateLevel = 0;
    public boolean secondServe = false;
    private Player lastToucher = null;
    public BallState ballState = BallState.Default;
    private T gameMode;
    private boolean testBall = true;

    public Ball(T gameMode, boolean testBall) {
        this.gameMode = gameMode;
        this.testBall = testBall;
    }

    public boolean hitNet() {
        return Math.abs(this.ballPosition.Z) < CourtDimensions.netHeightArMiddle + (CourtDimensions.netHeightAtPosts - CourtDimensions.netHeightArMiddle)
                * (this.ballPosition.Y / CourtDimensions.widthDoubles) + radius;
    }

    // Balls trajectory is calculated in 2d and then dimension x is divided in 2 dimensions x and y
    public Vector3D acceleration(Vector3D velocity) {
        Vector3D acceleration = new Vector3D();
        double accelerationXY, k, velocityXY;
        velocityXY = Math.sqrt(Math.pow(velocity.X, 2) + Math.pow(velocity.Y, 2));

        k = Constants.airDensity * crossSectionArea / (2 * weight); // TODO is this a constant? Yes!
        accelerationXY = -k * velocity.length() * (dragCoefficient() * velocityXY + liftCoefficient() * velocity.Z);

        // acceleration
        if (velocityXY != 0) {
            acceleration.X = (accelerationXY / velocityXY) * velocity.X;
            acceleration.Y = (accelerationXY / velocityXY) * velocity.Y;
        } else {
            acceleration.X = 0;
            acceleration.Y = 0;
        }

        acceleration.Z = k * velocity.length() * (liftCoefficient() * velocityXY - dragCoefficient() * velocity.Z) - Constants.g;
        return acceleration;
    }

    //calculating lift coefficient
    private double liftCoefficient() {
        double Cl;
        switch (this.spinType) {
            case topSpin:
                Cl = -1 / (2 + (this.direction.length() / Math.abs(this.spin)));
                break;
            case backspin:
                Cl = 1 / (2 + (this.direction.length() / Math.abs(this.spin)));
                break;
            case none:
            default:
                Cl = 0;
                break;
        }
        return Cl;
    }

    //calculating drag coefficient
    private double dragCoefficient() {
        return 0.55 + 1 / Math.pow(22.5 + 4.2 * (this.direction.length() / Math.abs(this.spin)), 0.4);
    }

    //calculating rebound angle
    private Vector3D reboundDirection() {
        Vector3D reboundDirection = new Vector3D();
        double angleK = 0, reboundAngle, reboundK = 0.75;

        if (Math.abs(this.ballPosition.X) < 0.5 * CourtDimensions.serviceBoxLength && !this.serve) {
            reboundK = 0.375;
        }

        // Assigns angleK for clay court depend of type and spin type/intensity
        if (courtType == courtTypes.clay) {
            switch (this.spinType) {
                case none:
                    angleK = 1.428;
                    break;

                case topSpin:
                    if (this.spin > 2500 * Constants.revPerMinToRevPerSec) {
                        angleK = 1;
                    } else {
                        angleK = 1.36;
                    }
                    break;
                case backspin:
                    if (this.spin > 2500 * Constants.revPerMinToRevPerSec) {
                        angleK = 1.502;
                    } else {
                        angleK = 1.594;
                    }
                    break;
            }
        }

        // Assigns angleK for grass court depend of type and spin type/intensity
        if (courtType == courtTypes.grass) {
            switch (this.spinType) {
                case none:
                    angleK = 1.172;
                    break;

                case topSpin:
                    if (this.spin > 2500 * Constants.revPerMinToRevPerSec) {
                        angleK = 0.72;
                    } else {
                        angleK = 1.042;
                    }
                    break;
                case backspin:
                    if (this.spin > 2500 * Constants.revPerMinToRevPerSec) {
                        angleK = 1.118;
                    } else {
                        angleK = 1.140;
                    }
                    break;
            }
        }

        // Assigns angleK for hard court depend of type and spin type/intensity
        if (courtType == courtTypes.hard) {
            switch (this.spinType) {
                case none:
                    angleK = 1.37;
                    break;

                case topSpin:
                    if (this.spin > 2500 * Constants.revPerMinToRevPerSec) {
                        angleK = 1;
                    } else {
                        angleK = 1.2;
                    }
                    break;
                case backspin:
                    if (this.spin > 2500 * Constants.revPerMinToRevPerSec) {
                        angleK = 1.442;
                    } else {
                        angleK = 1.65;
                    }
                    break;
            }
        }

        reboundAngle = Math.asin(Math.abs(this.direction.Z) / this.direction.length()) * angleK;

        reboundDirection.X = Math.signum(direction.X) * (this.direction.length() * reboundK * Math.cos(reboundAngle));
        reboundDirection.Y = Math.signum(direction.Y) * (this.direction.length() * reboundK * Math.cos(reboundAngle));
        reboundDirection.Z = direction.length() * reboundK * Math.sin(reboundAngle);

        if (this.direction.Y != 0) {
            reboundDirection.X = reboundDirection.X / (Math.sqrt(1.0 / Math.pow(direction.X / direction.Y, 2) + 1));
        }
        if (this.direction.X != 0) {
            reboundDirection.Y = reboundDirection.Y / (Math.sqrt(Math.pow(direction.X / direction.Y, 2) + 1));
        }

        return reboundDirection;
    }

    //newShot ball
    void resetDirection() {
        this.bounceCount = 0;
        this.direction.X = 0;
        this.direction.Y = 0;
        this.direction.Z = 0;
        this.trajectoryPoints.clear();
        this.bouncePoints.clear();
        this.net = false;
    }

    void reset() {
        this.resetDirection();
        this.resetPosition();
    }

    void resetPosition() {
        this.spin = 0;
        this.spinType = SpinTypes.none;
        this.ballPosition.X = 0;
        this.ballPosition.Y = 0;
        this.ballPosition.Z = 0;
        this.waiting = true;
    }

    //updates the ball. Update level 0 is for game ball. Update level 1 adds trajectory point list
    public void update() {
        if (!waiting) {
            double lastX = this.ballPosition.X;

            //Ball movement

            this.direction.add(acceleration(this.direction).scl(gameMode.getEngineUpdateInterval()));
            this.ballPosition.add(new Vector3D(this.direction).scl(gameMode.getEngineUpdateInterval()));
            //if ball position Z is bellow zero change it to 0
            if (this.ballPosition.Z < 0) {
                this.ballPosition.Z = 0;
            }

            //ball hit the net. If updateLevel=1 ignoring net
            if ((Math.signum(this.ballPosition.X) != Math.signum(lastX) || this.ballPosition.X == 0) && this.updateLevel != 1) {
                this.net = this.hitNet();
                if (this.net) {
                    this.ballPosition.X *= -0.1;
                    this.direction.X *= -0.1;
                    this.direction.Y *= 0.05;
                    this.direction.Z *= 0.1;
                    this.pointState = PointState.returnerError;
                }
            }

            //bounce points
            if (this.direction.Z < 0 && this.ballPosition.Z <= radius) {
                boolean in = true;
                this.bounceCount++;
                this.ballPosition.Z = radius;
                if (this.bounceCount == 1) {
                    this.bouncePointTime = 1.5;
                }

                //gets point state if ball bounces twice
                if (this.bounceCount == 2) {
                    if (!this.net) {
                        this.pointState = PointState.returnerScores;
                    } else {
                        if (this.placement == Placement.serveBottom || this.placement == Placement.serveTop) {
                            this.pointState = PointState.failedServe;
                        } else {
                            this.pointState = PointState.returnerError;
                        }
                    }
                }
                //gets point state if ball is out of bound
                if (this.serve && this.bounceCount == 1) {
                    if (this.placement == Placement.serveTop && Math.abs(this.ballPosition.X) > CourtDimensions.serviceBoxLength &&
                            (this.ballPosition.Y > 0 || this.ballPosition.Y < -CourtDimensions.widthSingles / 2 + diameter)) {
                        this.pointState = PointState.failedServe;
                        in = false;
                    }
                    if (this.placement == Placement.serveBottom && Math.abs(this.ballPosition.X) > CourtDimensions.serviceBoxLength &&
                            (this.ballPosition.Y < 0 || this.ballPosition.Y > CourtDimensions.widthSingles / 2 - diameter)) {
                        this.pointState = PointState.failedServe;
                        in = false;
                    }
                } else if ((Math.abs(this.ballPosition.X) > CourtDimensions.length / 2 + diameter ||
                        Math.abs(this.ballPosition.Y) > CourtDimensions.widthSingles / 2 + diameter) && bounceCount == 1) {
                    this.pointState = PointState.returnerError;
                    in = false;
                }

                //If ball moves slower then 2 kph stops the ball
                if (this.direction.length() < 0.55) {
                    this.direction.X = 0;
                    this.direction.Y = 0;
                    this.direction.Z = 0;
                    this.waiting = true;
                }
                this.direction = reboundDirection();

                // determines whether ball is short or not
                if (Math.abs(this.ballPosition.X) < lastToucher.playerStrategies.getShotPositioningPercentage() * (CourtDimensions.length / 2 - CourtDimensions.serviceBoxLength) + CourtDimensions.serviceBoxLength && !this.serve && this.bounceCount < 2) {
                    this.shortBall = true;
                }
                this.bouncePoints.add(new BouncePoint(this.ballPosition, in));
            }

            // Ball trajectory points added to the list
            if (this.updateLevel == 1) {
                this.time += gameMode.getEngineUpdateInterval();
                this.trajectoryPoints.add(new TimePoint3D(new Vector3D(this.ballPosition), this.time, this.bounceCount));
            }

        }

    }

    public Player getLastToucher() {
        return lastToucher;
    }

    public void setLastToucher(Player lastToucher) {
        this.lastToucher = lastToucher;
    }

    public enum BallState {
        Default, SuperServe
    }

    public class BouncePoint extends Vector3D {
        public boolean bounceIn;

        public BouncePoint(Vector3D ballPosition, boolean in) {
            super(ballPosition);
            this.bounceIn = in;
        }
    }
}
