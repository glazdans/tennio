package com.tennio.gameserver.game.gamemodes;

import com.tennio.gameserver.GameThread;
import com.tennio.gameserver.game.helpers.Constants;
import com.tennio.gameserver.game.mechanics.BallChameleonMechanic;
import com.tennio.gameserver.game.mechanics.GameMechanic;
import com.tennio.gameserver.game.mechanics.SideSwitchingMechanic;
import com.tennio.gameserver.game.mechanics.TieBreakerMechanic;

import java.util.ArrayList;
import java.util.List;

public class BasicGame {
    public BasicGame() {
    }

    private List<GameMechanic> gameMechanics = new ArrayList<GameMechanic>() {{
        add(BallChameleonMechanic.getInstance());
        add(SideSwitchingMechanic.getInstance());
        add(TieBreakerMechanic.getInstance());
    }};

    // Players Size
    private double playerSize = 0.8;

    // maximal theoretical serve speed
    private double maxTheoreticalServeSpeed = 200 * Constants.kphToMps;

    // minimal theoretical serve speed
    private double minTheoreticalServeSpeed = 130 * Constants.kphToMps;

    // maximal theoretical shot speed
    private double maxTheoreticalShotSpeed = 200 * Constants.kphToMps;

    // minimal theoretical shot speed
    private double minTheoreticalShotSpeed = 60 * Constants.kphToMps;

    // reaction time in sec (200 mili. sec.)
    private double reactionTime = 0.200;

    // Shot angle error (degrees) 
    private double angleError = 1.5;

    private double intervalBetweenPoints = 0.8;

    // Determines the amount of a value that should be randomized
    // FORMULA:(maxSpeed-minSpeed) *((1-maxspeedRandom)+maxspeedRandom)
    private double maxSpeedRandom = 0.25;

    // Determines influence to shots maxSpeed. All togeteher should be 100%.
    private double maxSpeedDefaultPercentage = 0.1;

    private double maxSpeedRandomPercentage = 0.4;

    private double maxSpeedForehandBackhandPercentage = 0.3;

    private double maxSpeedArmStrengthPercentage = 0.2;

    // TARGETPOINT
    private double serveAndVolleyPercentage = 0.1;

    private double engineUpdateInterval = 0.01;

    private double threadUpdateInterval = 0.01;

    public List<GameMechanic> getGameMechanics() {
        return gameMechanics;
    }

    public boolean isGameReady(GameThread gameThread) {
        return gameThread.areBothPlayersConnected();
    }

    public void beforeInitPlayers(GameThread gameThread) {
    }

    public double getPlayerSize() {
        return playerSize;
    }

    public double getMaxTheoreticalServeSpeed() {
        return maxTheoreticalServeSpeed;
    }

    public double getMinTheoreticalServeSpeed() {
        return minTheoreticalServeSpeed;
    }

    public double getMaxTheoreticalShotSpeed() {
        return maxTheoreticalShotSpeed;
    }

    public double getMinTheoreticalShotSpeed() {
        return minTheoreticalShotSpeed;
    }

    public double getReactionTime() {
        return reactionTime;
    }

    public double getAngleError() {
        return angleError;
    }

    public double getIntervalBetweenPoints() {
        return intervalBetweenPoints;
    }

    public double getMaxSpeedRandom() {
        return maxSpeedRandom;
    }

    public double getMaxSpeedDefaultPercentage() {
        return maxSpeedDefaultPercentage;
    }

    public double getMaxSpeedRandomPercentage() {
        return maxSpeedRandomPercentage;
    }

    public double getMaxSpeedForehandBackhandPercentage() {
        return maxSpeedForehandBackhandPercentage;
    }

    public double getMaxSpeedArmStrengthPercentage() {
        return maxSpeedArmStrengthPercentage;
    }

    public double getServeAndVolleyPercentage() {
        return serveAndVolleyPercentage;
    }

    public double getEngineUpdateInterval() {
        return engineUpdateInterval;
    }

    public double getThreadUpdateInterval() {
        return threadUpdateInterval;
    }
}
