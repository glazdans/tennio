package com.tennio.gameserver.game.helpers;

public class TimePoint3D {
    public Vector3D point;
    public double time;
    public int bounceCount;

    public TimePoint3D(Vector3D point, double time, int bounceCount) {
        this.point = point;
        this.time = time;
        this.bounceCount = bounceCount;

    }

    public TimePoint3D(TimePoint3D timePoint3D) {
        this.point = new Vector3D(timePoint3D.point);
        this.time = timePoint3D.time;
        this.bounceCount = timePoint3D.bounceCount;
    }
}
