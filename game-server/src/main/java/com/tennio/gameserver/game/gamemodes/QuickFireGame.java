package com.tennio.gameserver.game.gamemodes;

import com.tennio.gameserver.game.mechanics.*;

import java.util.ArrayList;
import java.util.List;

public class QuickFireGame extends BasicGame {
    private List<GameMechanic> gameMechanics = new ArrayList<GameMechanic>() {{
        add(BallChameleonMechanic.getInstance());
        add(QuickFireSideSwitchingMechanic.getInstance());
        add(TieBreakerMechanic.getInstance());
        add(new IrregularScoreLimitGame(3, 1, false));
    }};

    public QuickFireGame() {
    }

    @Override
    public List<GameMechanic> getGameMechanics() {
        return gameMechanics;
    }

    public void setGameMechanics(List<GameMechanic> gameMechanics) {
        this.gameMechanics = gameMechanics;
    }
}
