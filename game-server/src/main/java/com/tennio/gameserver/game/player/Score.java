package com.tennio.gameserver.game.player;

public class Score extends com.tennio.common.requests.Score {
    public Score() {
    }

    public void addSet() {
        sets += 1;
        games = 0;
        points = 0;
        games = 0;
        points = 0;
    }

    public void addPoint() {
        points += 1;
    }

    public void addGame() {
        games += 1;
    }

    public void addMatch() {
        matches += 1;
    }
}
