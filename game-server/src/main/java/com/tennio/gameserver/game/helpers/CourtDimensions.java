package com.tennio.gameserver.game.helpers;

public class CourtDimensions {
    // characteristics of the tennis court
    // All data used has been converted to SI (meters).
    public static double length = 2 * 11.89;
    public static double widthSingles = 8.23;
    public static double widthDoubles = 10.97;
    public static double serviceBoxLength = 6.4;
    public static double totalWidth = 18.29;
    public static double totalLength = 36.58;
    public static double netHeightAtPosts = 1.067;
    public static double netHeightArMiddle = 0.914;
    public static double netLength = widthDoubles + 2 * 0.914;
}
