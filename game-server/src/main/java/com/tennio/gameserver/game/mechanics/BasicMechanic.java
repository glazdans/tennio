package com.tennio.gameserver.game.mechanics;

import com.tennio.gameserver.game.Engine;
import com.tennio.gameserver.game.player.Player;

public interface BasicMechanic {
    void onInitPlayers(Engine engine);
    void onEngineUpdate(Engine engine);
    void onBeforeShot(Engine engine, Player shooter);
    void onAfterShot(Engine engine, Player shooter);
    void onAfterPoint(Engine engine, Player winner);
    void onAfterGame(Engine engine, Player winner);
    void onAfterSet(Engine engine, Player winner);
    void onAfterMatch(Engine engine, Player winner);
    void onBeforeServe(Engine engine, Player server);
    boolean enabled();
}
