package com.tennio.gameserver.game.helpers;

public class ShotParameters {
    public Vector3D direction;
    public double speed;

    public ShotParameters() {
        direction = new Vector3D();
    }

    public ShotParameters(Vector3D direction, double speed) {
        this.direction = direction;
        this.speed = speed;
    }
}
