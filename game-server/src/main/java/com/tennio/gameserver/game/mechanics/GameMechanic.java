package com.tennio.gameserver.game.mechanics;

import com.tennio.gameserver.game.Engine;
import com.tennio.gameserver.game.player.Player;

public abstract class GameMechanic implements BasicMechanic {
    @Override
    public void onInitPlayers(Engine engine) {

    }

    @Override
    public void onEngineUpdate(Engine engine) {

    }

    @Override
    public void onAfterShot(Engine engine, Player shooter) {

    }

    @Override
    public void onAfterPoint(Engine engine, Player winner) {

    }

    @Override
    public void onAfterGame(Engine engine, Player winner) {

    }

    @Override
    public void onAfterSet(Engine engine, Player winner) {

    }

    @Override
    public void onAfterMatch(Engine engine, Player winner) {

    }

    @Override
    public void onBeforeShot(Engine engine, Player shooter) {

    }

    @Override
    public void onBeforeServe(Engine engine, Player server) {

    }

    @Override
    public boolean enabled() {
        return false;
    }
}
