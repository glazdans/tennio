package com.tennio.gameserver.game.gamemodes;

import com.tennio.gameserver.game.mechanics.GameMechanic;

import java.util.ArrayList;
import java.util.List;

public class SliderTestGame extends BackgroundGame {
    private List<GameMechanic> gameMechanics = new ArrayList<GameMechanic>() {{

    }};

    @Override
    public List<GameMechanic> getGameMechanics() {
        return gameMechanics;
    }
}