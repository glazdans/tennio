package com.tennio.gameserver.game.gamemodes;

import com.tennio.gameserver.GameThread;
import com.tennio.gameserver.game.mechanics.GameMechanic;

import java.util.ArrayList;
import java.util.List;

public class BackgroundGame extends BasicGame {
    private List<GameMechanic> gameMechanics = new ArrayList<GameMechanic>() {{
    }};

    @Override
    public List<GameMechanic> getGameMechanics() {
        return gameMechanics;
    }

    public double getEngineUpdateInterval() {
        return super.getEngineUpdateInterval();
    }

    public double getThreadUpdateInterval() {
        return 0.000001;
    }

    @Override
    public boolean isGameReady(GameThread gameThread) {
        return true;
    }
}
