package com.tennio.gameserver.game.mechanics;

import com.tennio.gameserver.game.Engine;
import com.tennio.gameserver.game.player.Player;

public class QuickFireSideSwitchingMechanic extends SideSwitchingMechanic {
    private static QuickFireSideSwitchingMechanic instance = new QuickFireSideSwitchingMechanic();

    private QuickFireSideSwitchingMechanic() {
        super();
    }

    public static QuickFireSideSwitchingMechanic getInstance() {
        return instance;
    }

    @Override
    public void onAfterGame(Engine engine, Player winner) {
        switchSides(engine);
    }
}
