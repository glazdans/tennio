package com.tennio.gameserver.game.mechanics;

import com.tennio.gameserver.game.Ball;
import com.tennio.gameserver.game.Engine;
import com.tennio.gameserver.game.player.Player;

import java.util.Arrays;

public class BallChameleonMechanic extends GameMechanic {
    private static BallChameleonMechanic instance = new BallChameleonMechanic();

    private BallChameleonMechanic() {
    }

    public static BallChameleonMechanic getInstance() {
        return instance;
    }

    @Override
    public void onBeforeServe(Engine engine, Player server) {
        if(server.playerStrategies.isSuperServe() && server.state == Player.states.serving) {
            engine.ball.ballState = Ball.BallState.SuperServe;
        } else {
            engine.ball.ballState = Ball.BallState.Default;
        }
    }

    @Override
    public void onBeforeShot(Engine engine, Player server) {
        onBeforeServe(engine, server);
    }

    @Override
    public void onEngineUpdate(Engine engine) {
        if(engine.ball.direction.X == 0 && engine.ball.direction.Y == 0) {
            Arrays.asList(engine.playerBlue, engine.playerRed).forEach((player) -> {
                if (player.playerStrategies.isSuperServe() && player.state == Player.states.serving) {
                    engine.ball.ballState = Ball.BallState.SuperServe;
                    return;
                }
            });
        }
    }
}
