package com.tennio.gameserver.game.player;

import com.tennio.gameserver.game.helpers.Vector3D;

public class MovementParameters {
    public Vector3D direction;
    public double speed;
    public Vector3D shotPosition;
    public double acceleration;
    public boolean startingMove;
    public boolean delayedAction;
    public double delayTime;

    public MovementParameters() {
        direction = new Vector3D();
        shotPosition = new Vector3D();
    }

    public MovementParameters(Vector3D direction, double speed, Vector3D shotPosition, double acceleration, Vector3D newDirection, Vector3D newShotPosition, boolean startingMove, boolean delayedAction, double delayTime) {
        this.direction = direction;
        this.speed = speed;
        this.shotPosition = shotPosition;
        this.acceleration = acceleration;
        this.startingMove = startingMove;
        this.delayedAction = delayedAction;
        this.delayTime = delayTime;
    }

}
