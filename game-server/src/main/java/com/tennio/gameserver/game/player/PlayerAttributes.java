package com.tennio.gameserver.game.player;

import com.tennio.common.Settings;

public class PlayerAttributes {
    public double speed = 0.0;
    public double backhand = 0.0;
    public double forehand = 0.0;
    public double serve = 0.0;
    public double precision = 0.0;
    public double armStrength = 0.0;
    public double recovery = 0.0;
    public double fitness = 1000;

    public int speedLVL = 10;
    public int backhandLVL = 10;
    public int forehandLVL = 10;
    public int serverLVL = 10;
    public int precisionLVL = 10;
    public int armStrengthLVL = 10;
    public int fitnessLVL = 30;
    public int recoveryLVL = 10;

    public PlayerAttributes() {

    }

    public PlayerAttributes(PlayerAttributes pa) {
        speedLVL = pa.speedLVL;
        backhandLVL = pa.backhandLVL;
        forehandLVL = pa.forehandLVL;
        serverLVL = pa.serverLVL;
        precisionLVL = pa.precisionLVL;
        armStrengthLVL = pa.armStrengthLVL;
        fitnessLVL = pa.fitnessLVL;
        speed = pa.speed;
        backhand = pa.backhand;
        forehand = pa.forehand;
        serve = pa.serve;
        precision = pa.precision;
        armStrength = pa.armStrength;
        fitness = pa.fitness;
        recovery = pa.recovery;
    }

    public void setStats(){
        speed = 0.35 + speedLVL / Settings.MAX_STAT_LEVEL * .015;
        backhand = 0.35 + backhandLVL / Settings.MAX_STAT_LEVEL * .015;
        forehand = 0.35 + forehandLVL / Settings.MAX_STAT_LEVEL * .015;
        serve = 0.35 + serverLVL / Settings.MAX_STAT_LEVEL * .015;
        precision = 0.35 + precisionLVL / Settings.MAX_STAT_LEVEL * .015;
        armStrength = 0.35 + armStrengthLVL / Settings.MAX_STAT_LEVEL * .015;
        recovery = 0.45 + recoveryLVL / Settings.MAX_STAT_LEVEL - recoveryLVL * 0.012;
        fitness = 1000;
    }

    public static PlayerAttributes base(int base) {
        PlayerAttributes attributes = new PlayerAttributes();
        attributes.speedLVL = base;
        attributes.backhandLVL = base;
        attributes.forehandLVL = base;
        attributes.serverLVL = base;
        attributes.precisionLVL = base;
        attributes.armStrengthLVL = base;
        attributes.fitnessLVL = base;
        attributes.recoveryLVL = base;
        return attributes;
    }
}
