package com.tennio.gameserver.game.player;

import com.tennio.gameserver.game.gamemodes.BasicGame;
import com.tennio.gameserver.game.helpers.Vector3D;

public class Player<T extends BasicGame> {
    public MovementParameters movementParameters = new MovementParameters();
    public PlayerAttributes attributes = new PlayerAttributes();
    public PlayerStrategies playerStrategies = new PlayerStrategies();
    public Vector3D position = new Vector3D(0, 0, 0);
    public boolean changeShotPosition = false;
    public Score score = new Score();

    public sides servingSide;
    public boolean waiting;
    public boolean serving;
    public states state;
    public zones zone = zones.baseline;
    private shotType currentShotType;
    public ShotPenalty shotPenalty;

    // acceleration =maxSpeed/accelerationRate, acceleration rate calculated for Usain Bolt is 85 units
    public static double accelerationRate = 100;
    public double maxSpeed = 36 * 1000 / 3600;
    public double reactionTime = 0.2;
    // aggression rate. Must be between 0 and 1.
    public double height = 2;
    //redPlayer=1 bluePlayer=2
    public int id;

    private boolean recoveringFromJump = false;
    private T gameMode;

    public Player(T gameMode) {
        this.gameMode = gameMode;
    }

    public void updateDelay() {
        if (this.movementParameters.delayedAction && this.movementParameters.delayTime > 0) {
            this.movementParameters.delayTime -= gameMode.getEngineUpdateInterval();
        }
        // instead of 0 used Engine.update interval diveded by 10, to avoid small calculation errors
        if (this.movementParameters.delayTime < gameMode.getEngineUpdateInterval() / 10) {
            this.movementParameters.delayTime = 0;
        }
    }

    // changing player characteristics when  retrieving serve or shot (delayed action)
    public void delayedAction() {
        if (this.movementParameters.delayedAction && this.movementParameters.delayTime == 0) {
            if (this.state == states.preparingForServe && this.serving) {
                this.state = states.tossingBall;
            } else {
                this.waiting = false;
                this.movementParameters.startingMove = true;
                this.changeShotPosition = true;
                this.state = states.returningShot;
            }
            this.movementParameters.delayedAction = false;
        }
    }

    //set delayed action
    public void setDelayedAction() {
        this.movementParameters.delayedAction = true;
        this.movementParameters.delayTime = this.playerStrategies.isSuperReaction() ? 0 : this.reactionTime;
        this.playerStrategies.setSuperReaction(false);
        if (this.state == states.preparingForServe && this.serving) {
            this.movementParameters.delayTime = gameMode.getIntervalBetweenPoints();
        }
    }

    public void setAction() {
        this.waiting = false;
        this.state = Player.states.movingAfterShot;
        this.movementParameters.startingMove = true;
        this.changeShotPosition = true;
    }

    //TODO change this! change what????
    public Vector3D getDirection() {
        Vector3D tmp = new Vector3D();
        Vector3D tmp1 = new Vector3D();
        tmp.set(this.movementParameters.shotPosition);
        tmp.Z = 0;
        tmp1.set(this.position);
        tmp1.Z = 0;


        return tmp.sub(tmp1);
    }

    public double breakingDistance() {
        return (accelerationRate * (accelerationRate - 1) * this.maxSpeed * gameMode.getEngineUpdateInterval()) / (2 * accelerationRate);
    }

    private double breakingDistance(int j) {
        return (j * (j - 1) * this.maxSpeed * gameMode.getEngineUpdateInterval()) / (2 * accelerationRate);
    }

    public void onPointEnd() {
        this.zone = zones.baseline;
        movementParameters.delayedAction = false;
        waiting = false;
        state = Player.states.preparingForServe;
        movementParameters.startingMove = true;
    }

    public void onGameEnd() {
        score.points = 0;
        serving = !serving;
    }

    public void onSetEnd() {
        score.games = 0;
    }

    public void onMatchEnd() {
        state = Player.states.matchEnded;
        waiting = false;

        movementParameters.startingMove = true;
    }

    public void init(PlayerAttributes playerAttributes, double posX) {
        position.X = posX;
        position.Y = 0;
        height = 2;
        if (playerAttributes == null) {
            attributes = new PlayerAttributes();
        } else {
            playerAttributes.setStats();
            attributes = playerAttributes;
        }
        waiting = false;
        movementParameters.speed = 0;
        movementParameters.delayedAction = false;
        movementParameters.startingMove = true;
        state = states.preparingForServe;
        score.points = 0;
        score.games = 0;
        score.sets = 0;
        score.matches = 0;
        zone = zones.baseline;
        playerStrategies.setSuperServe(false);
        playerStrategies.setSuperSpeed(false);
        playerStrategies.setSuperReaction(false);
        this.shotPenalty = new ShotPenalty(playerStrategies);
    }

    //updating players
    public void update() {
        if (!this.waiting) {
            //calculating direction have to delete this
            this.movementParameters.direction = getDirection();
            double accelerationRate = this.playerStrategies.isSuperSpeed() ? Player.accelerationRate / 2 : Player.accelerationRate;
            if(this.playerStrategies.isJump()){
                if(this.playerStrategies.isSuperSpeed()){
                    accelerationRate /= 1.33f;
                } else {
                    accelerationRate /= 2.66f;
                }
            }

            //changing speed
            if (this.movementParameters.startingMove) {
                if(recoveringFromJump && !playerStrategies.isJump()) {
                    accelerationRate *= 2.66f;
                }
                if (this.movementParameters.speed < this.maxSpeed) {
                    this.movementParameters.speed += this.maxSpeed / accelerationRate;
                    if (!recoveringFromJump && this.movementParameters.direction.length() <= breakingDistance((int) (accelerationRate - (this.maxSpeed - this.movementParameters.speed) / (this.maxSpeed / accelerationRate)))) {
                        this.movementParameters.startingMove = false;
                    }
                    if (this.movementParameters.speed >= this.maxSpeed) {
                        this.movementParameters.speed = this.maxSpeed;
                        this.movementParameters.startingMove = false;
                    }
                }
            } else {
                if (this.movementParameters.speed >= 0 && this.movementParameters.direction.length() <= breakingDistance()) {
                    this.movementParameters.speed -= this.maxSpeed / accelerationRate;
                    if (this.movementParameters.speed < 0) {
                        this.movementParameters.speed = 0;
                        this.movementParameters.startingMove = true;
                    }
                }

            }

            if (this.movementParameters.direction.length() < this.movementParameters.speed * gameMode.getEngineUpdateInterval()) {
                this.position.X = this.movementParameters.shotPosition.X;
                this.position.Y = this.movementParameters.shotPosition.Y;
                this.waiting = true;
                this.movementParameters.speed = 0;
                this.playerStrategies.setSuperSpeed(false);
                this.recoveringFromJump = false;
                if(this.playerStrategies.isJump()) {
                    this.recoveringFromJump = true;
                    this.playerStrategies.setJump(false);
                }
            } else {
                this.movementParameters.direction.normalize();

                //move
                if (Math.abs(this.position.X + this.movementParameters.direction.X * this.movementParameters.speed * gameMode.getEngineUpdateInterval()) >
                        gameMode.getPlayerSize() / 2) {
                    this.position.X += this.movementParameters.direction.X * this.movementParameters.speed * gameMode.getEngineUpdateInterval();
                }
                this.position.Y += this.movementParameters.direction.Y * this.movementParameters.speed * gameMode.getEngineUpdateInterval();
            }

            // updating players energy (stat fitness)
            this.attributes.fitness -= (this.movementParameters.speed * gameMode.getEngineUpdateInterval() * (shotPenalty.tiredness * 2.5) * .35);
            if (this.attributes.fitness < 0) {
                this.attributes.fitness = 0;
            }
        }

    }

    public ShotPenalty getShotPenalty() {
        return shotPenalty;
    }

    public shotType getCurrentShotType() {
        return currentShotType;
    }

    public void setCurrentShotType(shotType currentShotType) {
        this.currentShotType = currentShotType;
        if(currentShotType != null) {
            this.calculateStrategyPenalties();
        }
    }

    public void calculateStrategyPenalties() {
        /* "Serving" Slider (Aggressive - Precise) */
        long serveStrategy = playerStrategies.getServing();
        /* "Aim" Slider (Net - Baseline) */
        long aimStrategy = playerStrategies.getShotAiming();
        /* "Shooting" Slider (Aggressive - Precise) */
        long shootingStrategy = playerStrategies.getShooting();
        /* "Positioning after shot" Slider (Attacking - Defensive) */
        long shotPositioningStrategy = playerStrategies.getPositioningAfterShot();

        if(this.shotPenalty == null) {
            this.shotPenalty = new ShotPenalty(playerStrategies);
        } else {
            this.shotPenalty.reset();
        }

        if(aimStrategy < 0) {
            shotPenalty.errorMultiplier *= (1 + (Math.abs(aimStrategy) * .1));
        } else if(aimStrategy > 0) {
            shotPenalty.errorMultiplier *= (1 - (aimStrategy * .02));
        }

        if (currentShotType == Player.shotType.serve) {
            if(serveStrategy < 0) {
                // Aggressive serving
                shotPenalty.minSpeedMultiplier *= (1 + (Math.abs(serveStrategy) * .06));
                shotPenalty.maxSpeedMultiplier *= (1 + (Math.abs(serveStrategy) * .06));
                shotPenalty.errorMultiplier *= (1 + (Math.abs(serveStrategy) * .05));
            } else if(serveStrategy > 0) {
                // Precise serving
                shotPenalty.minSpeedMultiplier *= (1 - (Math.abs(serveStrategy) * .05));
                shotPenalty.maxSpeedMultiplier *= (1 - (Math.abs(serveStrategy) * .05));
                shotPenalty.errorMultiplier *= (1 - (Math.abs(serveStrategy) * .035));
            }
        } else {
            if(shotPositioningStrategy < 0) {
                // Closer to net = greater error rate
                shotPenalty.errorMultiplier *= (1 + (Math.abs(shotPositioningStrategy) * .09));
            } else if(shotPositioningStrategy > 0) {
                // Further from net = lesser error rate
                shotPenalty.errorMultiplier *= (1 - (shotPositioningStrategy * .05));
            }

            if(shootingStrategy < 0) {
                // Aggressive shooting = higher max speed and error
                shotPenalty.maxSpeedMultiplier *= (1 + (Math.abs(shootingStrategy) * .07));
                shotPenalty.errorMultiplier *= (1 + (Math.abs(shootingStrategy) * .07));
            } else if(shootingStrategy > 0) {
                // Precise shooting = lower max speed and error
                shotPenalty.maxSpeedMultiplier *= (1 - (shootingStrategy * .07));
                shotPenalty.errorMultiplier *= (1 - (shootingStrategy * .05));
            }

            if(aimStrategy != 0) {
                // When aim is not 0, apply 5% lower speed for each increment
                shotPenalty.minSpeedMultiplier *= (1 - (Math.abs(aimStrategy) * 0.05));
                shotPenalty.maxSpeedMultiplier *= (1 - (Math.abs(aimStrategy) * 0.05));
            }
        }

        shotPenalty.aggressiveness = currentShotType == shotType.serve ? rAdjust(serveStrategy) : rAdjust(shootingStrategy) / 2;

        if(shotPenalty.shotStrategy != this.playerStrategies) {
            double errorSnow = shotPenalty.errorMultiplier / 500;
            double tirednessSnow = shotPenalty.aggressiveness * .00035; // we meet again, mr bond

            if(shotPenalty.errorMultiplier > 1) {
                shotPenalty.setErrorSnowBall(shotPenalty.getErrorSnowBall() + errorSnow);
            } else if(shotPenalty.errorMultiplier != 1) {
                shotPenalty.setErrorSnowBall(shotPenalty.getErrorSnowBall() - (errorSnow / 1.5));
            }

            if(tirednessSnow != 0 && shotPenalty.aggressiveness > .5) {
                shotPenalty.setTirednessSnowBall(shotPenalty.getTirednessSnowBall() + tirednessSnow);
            } else if(shotPenalty.tiredness > .5) {
                shotPenalty.setTirednessSnowBall(shotPenalty.getTirednessSnowBall() - tirednessSnow);
            }
        }
        
        shotPenalty.setShotStrategy(this.playerStrategies);
    }

    private double rAdjust(long sliderVal) {
        if(sliderVal < -1) {
            return 1;
        } else if(sliderVal < 0) {
            return 0.75;
        } else if(sliderVal < 1) {
            return 0.5;
        } else if(sliderVal < 2) {
            return 0.25;
        }
        return 0;
    }

    private double adjust(long sliderVal) {
        return 1 - rAdjust(sliderVal);
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", serving=" + serving +
                ", zone=" + zone +
                '}';
    }

    public enum states {preparingForServe, serving, returningShot, movingAfterShot, tossingBall, matchEnded}

    public enum sides {top, bottom}

    public enum zones {baseline, net, noMansLand, behindBaseline, outside}

    public enum shotType {
        serve, backHand, foreHand
    }

    public class ShotPenalty {
        double maxSpeedMultiplier = 1;
        double minSpeedMultiplier = 1;
        double errorMultiplier = 1;
        private double errorSnowBall = 1;
        private PlayerStrategies shotStrategy;
        double tiredness = 0.5;
        private double tirednessSnowBall = 1;
        double aggressiveness = 0.5;

        ShotPenalty(PlayerStrategies playerStrategies) {
            this.shotStrategy = new PlayerStrategies(playerStrategies);
        }

        void reset() {
            maxSpeedMultiplier = 1;
            minSpeedMultiplier = 1;
            errorMultiplier = 1;
        }

        public void setErrorSnowBall(double errorSnowBall) {
            if(errorSnowBall < 1 || errorSnowBall < this.errorSnowBall && errorMultiplier > 1) {
                this.errorMultiplier -= errorSnowBall / 25;
            } else if(errorSnowBall > this.errorSnowBall) {
                this.errorMultiplier *= errorSnowBall;
            }
            this.errorSnowBall = errorSnowBall < 1 ? 1 : errorSnowBall;
        }

        public double getErrorSnowBall() {
            return errorSnowBall;
        }

        public void setTirednessSnowBall(double tirednessSnowBall) {
            if(tirednessSnowBall < 1 || tirednessSnowBall < this.tirednessSnowBall && tiredness > .5) {
                this.tiredness -= errorSnowBall / 25;
            } else if(tirednessSnowBall > this.tirednessSnowBall) {
                this.tiredness *= tirednessSnowBall;
            }
            this.tirednessSnowBall = tirednessSnowBall < .5 ? .5 : tirednessSnowBall;
        }

        public double getTirednessSnowBall() {
            return errorSnowBall;
        }

        public void setShotStrategy(PlayerStrategies shotStrategy) {
            this.shotStrategy = new PlayerStrategies(shotStrategy);
        }

        public PlayerStrategies getShotStrategy() {
            return shotStrategy;
        }

        @Override
        public String toString() {
            return "ShotPenalty{" +
                    "maxSpeedMultiplier=" + maxSpeedMultiplier +
                    ", minSpeedMultiplier=" + minSpeedMultiplier +
                    ", errorMultiplier=" + errorMultiplier +
                    ", errorSnowBall=" + errorSnowBall +
                    ", shotStrategy=" + shotStrategy +
                    ", tiredness=" + tiredness +
                    ", tirednessSnowBall=" + tirednessSnowBall +
                    ", aggressiveness=" + aggressiveness +
                    '}';
        }

        public double getMaxSpeedMultiplier() {
            return maxSpeedMultiplier;
        }

        public double getMinSpeedMultiplier() {
            return minSpeedMultiplier;
        }

        public double getErrorMultiplier() {
            return errorMultiplier;
        }

        public double getTiredness() {
            return tiredness;
        }

        public double getAggressiveness() {
            return aggressiveness;
        }
    }
}
