package com.tennio.gameserver.game;

import com.tennio.gameserver.SliderUpdate;
import com.tennio.gameserver.game.gamemodes.BackgroundGame;
import com.tennio.gameserver.game.gamemodes.BasicGame;
import com.tennio.gameserver.game.helpers.*;
import com.tennio.gameserver.game.iodata.PointUpdate;
import com.tennio.gameserver.game.player.Player;
import com.tennio.gameserver.websockets.SimpleServer;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static com.tennio.gameserver.game.player.Player.zones.*;

public class Engine<T extends BasicGame> {
    public double time;
    public boolean matchEnded = false;

    private SimpleServer webSocketServer;
    private static Random rand;

    public Ball<T> ball;
    public Player<T> playerRed;
    public Player<T> playerBlue;
    private List<Player> bothPlayers;
    private T gameMode;
    /// Target point
    private Vector3D tPoint = new Vector3D(0, 0, 0);
    private PauseReason pauseReason = PauseReason.NONE;

    public Engine(SimpleServer server, T gameMode) {
        this.webSocketServer = server;
        if (Engine.rand == null) {
            Engine.rand = new SecureRandom();
        }
        this.gameMode = gameMode;
        this.playerRed = new Player<>(gameMode);
        this.playerBlue = new Player<>(gameMode);
        this.bothPlayers = Arrays.asList(this.playerBlue, this.playerRed);
        this.ball = new Ball<>(gameMode, false);
    }

    public void update() {
        // time update
        time += gameMode.getEngineUpdateInterval();
        recoverFitnessTick(false);

        gameMode.getGameMechanics().forEach((mechanic) -> mechanic.onEngineUpdate(this));

        // bounce point timer
        if (ball.bouncePointTime >= 0) {
            ball.bouncePointTime -= gameMode.getEngineUpdateInterval();
        }

        // delay timer
        bothPlayers.forEach(Player::updateDelay);

        // check if point hasn't ended
        if (ball.pointState != Ball.PointState.ballInGame) {
            ball.bounceCount = 3;

            if (ball.pointState == Ball.PointState.failedServe) {
                if (ball.secondServe) {
                    if (playerRed.serving) {
                        addPoint(playerBlue);
                    } else {
                        addPoint(playerRed);
                    }
                } else {
                    ball.secondServe = true;
                    ball.pointState = Ball.PointState.ballInGame;

                    bothPlayers.forEach((player) -> {
                        player.state = Player.states.preparingForServe;
                        player.movementParameters.startingMove = true;
                    });
                    setShotPositions();
                }
            } else if (playerRed.state == Player.states.returningShot) {
                addPoint(ball.pointState == Ball.PointState.returnerScores ? playerBlue : playerRed);
            } else {
                addPoint(ball.pointState == Ball.PointState.returnerScores ? playerRed : playerBlue);
            }
        }

        // ends match
        if (playerRed.waiting && playerBlue.waiting && playerRed.state == Player.states.matchEnded && playerBlue.state ==
                Player.states.matchEnded && !playerBlue.movementParameters.delayedAction && !playerRed.movementParameters.delayedAction) {
            matchEnded = true;
        }

        // sets delayed action - tossing ball.
        if (playerRed.waiting && playerBlue.waiting && playerRed.state == Player.states.preparingForServe &&
                playerBlue.state == Player.states.preparingForServe &&
                !playerBlue.movementParameters.delayedAction && !playerRed.movementParameters.delayedAction) {
            bothPlayers.forEach((player) -> {
                if (player.serving) {
                    player.setDelayedAction();
                }
            });
        }

        // tossing ball
        if (playerBlue.state == Player.states.tossingBall || playerRed.state == Player.states.tossingBall) {
            tossBall();
        }

        // Player serving
        if (ball.direction.Z < 0 && ((playerBlue.state == Player.states.serving && ball.ballPosition.Z < 1.5 * playerBlue.height)
                || (playerRed.state == Player.states.serving && ball.ballPosition.Z < 1.5 * playerRed.height))) {
            bothPlayers.forEach((player) -> {
                if (player.state == Player.states.serving) {
                    shoot(player);
                    player.setAction();
                    opponentPlayer(player).setDelayedAction();
                }
            });
        }

        // changing player state when returning shot
        bothPlayers.forEach((player) -> {
            if (player.state == Player.states.returningShot && distanceXY(ball.ballPosition, player.position) <= 1.1 * (3 * gameMode.getPlayerSize() / 2)) {
                ball.bouncePoints.clear();
                ball.serve = false;

                shoot(player);
                ball.waiting = false;
                player.setAction();

                opponentPlayer(player).setDelayedAction();
            }
        });

        // Delayed action Return serve
        bothPlayers.forEach(Player::delayedAction);

        // changing shot position after action
        bothPlayers.forEach((player) -> {
            if (player.changeShotPosition) {
                player.movementParameters.shotPosition = getShotPosition(player);
                player.changeShotPosition = false;
            }
        });
    }

    public void recoverFitnessTick(boolean resting) {
        bothPlayers.forEach((player) -> {
            if (player.attributes.fitness < 1000) {
                player.attributes.fitness += player.attributes.recovery * gameMode.getEngineUpdateInterval() * (resting ? 5.5 : 1);
                if (player.attributes.fitness > 1000) {
                    player.attributes.fitness = 1000;
                }
            }
        });
    }

    // Gets shot target point. Function gets area and use getTargetPoint to find point from this area
    private Vector3D targetPoint(Player player) {
        Vector3D precision = new Vector3D(0, 0, 0);
        Vector3D target = new Vector3D();

        if (player.playerStrategies.getPositioningAfterShot() == -2) {
            player.zone = Player.zones.net;
        } else if (player.playerStrategies.getPositioningAfterShot() == -1) {
            player.zone = Player.zones.noMansLand;
        } else if (player.playerStrategies.getPositioningAfterShot() == 0) {
            player.zone = Player.zones.baseline;
        } else if (player.playerStrategies.getPositioningAfterShot() == 1) {
            player.zone = Player.zones.behindBaseline;
        } else if (player.playerStrategies.getPositioningAfterShot() == 2) {
            player.zone = Player.zones.outside;
        }

        // determines shot placement type
        if (player.getCurrentShotType() == Player.shotType.serve) {
            if (player.servingSide == Player.sides.top) {
                ball.placement = Ball.Placement.serveTop;
            } else {
                ball.placement = Ball.Placement.serveBottom;
            }
        } else {
            switch (opponentPlayer(player).zone) {
                case net:
                    //TODO add lob
                    if (getRandom() < 0.5) {
                        ball.placement = Ball.Placement.passingShot;
                    } else {
                        ball.placement = Ball.Placement.attackingBaseLine;
                    }
                    break;
                case noMansLand:
                    ball.placement = Ball.Placement.passingShot;
                    break;
                case outside:
                case behindBaseline:
                case baseline:
                    if (ball.shortBall) {
                        ball.placement = Ball.Placement.dropShot;
                    } else {
                        if (player.playerStrategies.getShooting() == -2 && getRandom() > 0.35 || player.playerStrategies.getShooting() == -1 && getRandom() > 0.5) {
                            ball.placement = Ball.Placement.attackingBaseLine;
                        } else {
                            ball.placement = Ball.Placement.baseLine;
                        }
                    }
                    break;
            }
        }

        // Set target point and precision values depend on placement
        switch (ball.placement) {
            case serveBottom:
                if (getRandom() < gameMode.getServeAndVolleyPercentage() + player.playerStrategies.getServePercentage()) {
                    player.zone = net;
                }

                precision.X = (0.7 * (1 - 0.4 * player.attributes.precision)) * (1 - player.playerStrategies.getServePercentage());
                precision.Y = (0.7 * (1 - 0.4 * player.attributes.precision)) * (1 - player.playerStrategies.getServePercentage());
                target.X = 0.9 * CourtDimensions.serviceBoxLength;

                if ((getRandom() - 0.5) > 0) {
                    target.Y = -0.9 * CourtDimensions.widthSingles / 2;
                } else {
                    target.Y = -(1 - 0.9) * CourtDimensions.widthSingles / 2;
                }

                break;
            case serveTop:
                //in 15% cases player serve and volley
                if (getRandom() < 0.15) {
                    player.zone = net;
                }

                precision.X = (0.7 * (1 - 0.4 * player.attributes.precision)) * (1 - player.playerStrategies.getServePercentage());
                precision.Y = (0.7 * (1 - 0.4 * player.attributes.precision)) * (1 - player.playerStrategies.getServePercentage());
                target.X = 0.9 * CourtDimensions.serviceBoxLength;

                if ((getRandom() - 0.5) > 0) {
                    target.Y = 0.9 * CourtDimensions.widthSingles / 2;
                } else {
                    target.Y = (1 - 0.9) * CourtDimensions.widthSingles / 2;
                }
                break;

            case baseLine:
                // Aiming at 1/2 of court between service line and service box
                precision.X = 2 * (1 - 0.4 * player.attributes.precision);
                precision.Y = 1.2 * (1 - 0.4 * player.attributes.precision);
                target.X = 0.5 * (CourtDimensions.length / 2 - CourtDimensions.serviceBoxLength) + CourtDimensions.serviceBoxLength;
                target.Y = 0.55 * Math.signum(getRandom() - 0.5) * CourtDimensions.widthSingles / 2;
                //set spin
                ball.spinType = Ball.SpinTypes.topSpin;
                ball.spin = 3200 * Constants.revPerMinToRevPerSec * getRandom();
                break;
            case attackingBaseLine:
                // Aiming at corners
                //TODO balance shot
                precision.X = 0.7 * (1 - 0.4 * player.attributes.precision);
                precision.Y = 0.7 * (1 - 0.4 * player.attributes.precision);
                target.X = 0.9 * (CourtDimensions.length / 2 - CourtDimensions.serviceBoxLength) + CourtDimensions.serviceBoxLength;
                target.Y = 0.9 * Math.signum(getRandom() - 0.5) * CourtDimensions.widthSingles / 2;
                //set spin
                ball.spinType = Ball.SpinTypes.topSpin;
                ball.spin = 3200 * Constants.revPerMinToRevPerSec * getRandom();
                break;
            case passingShot:
                precision.X = 1.2 * (1 - 0.4 * player.attributes.precision);
                precision.Y = 1.2 * (1 - 0.4 * player.attributes.precision);
                target.X = 0.9 * CourtDimensions.length / 2;
                target.Y = 0.9 * Math.signum(getRandom() - 0.5) * CourtDimensions.widthSingles / 2;
                //set spin
                ball.spinType = Ball.SpinTypes.topSpin;
                ball.spin = 3200 * Constants.revPerMinToRevPerSec * getRandom();

                break;
            case dropShot:
                //set movement zone
                player.zone = noMansLand;

                precision.X = 1.2 * (1 - 0.4 * player.attributes.precision);
                precision.Y = 1.2 * (1 - 0.4 * player.attributes.precision);
                target.X = 0.35 * CourtDimensions.serviceBoxLength;
                target.Y = 0.9 * Math.signum(getRandom() - 0.5) * CourtDimensions.widthSingles / 2;
                //set spin
                ball.spinType = Ball.SpinTypes.backspin;
                ball.spin = 2300 * Constants.revPerMinToRevPerSec + 1000 * Constants.revPerMinToRevPerSec * getRandom();
                break;
        }

        //calculates target point
        target.X += precision.X * (2 * getRandom() - 1);
        target.Y += precision.Y * (2 * getRandom() - 1);

        target.X *= -Math.signum(player.position.X);

        tPoint.X = target.X;
        tPoint.Y = target.Y;
        return target;
    }

    public boolean isPausable() {
        return playerBlue.state == Player.states.tossingBall || playerRed.state == Player.states.tossingBall;
    }

    //Find fastest possible shot speed and direction z
    private ShotParameters calculateShotParameters(Player player, Vector3D targetPoint, double minSpeed, double maxSpeed, double minAngle, double maxAngle, double error) {
        ShotParameters param = new ShotParameters();
        Vector3D direction = new Vector3D();
        Ball<T> testBall = new Ball<>(gameMode, true);
        testBall.setLastToucher(player);
        Vector3D point = new Vector3D(0, 0, 0);
        double currentAngle = maxAngle;
        double currentMinAngle, currentMaxAngle;
        double currentZ;
        double lengthError = 1000;
        boolean found = false;

        ball.shortBall = false;
        direction.set(targetPoint).sub(ball.ballPosition);
        direction.Z = 0;

        testBall.serve = ball.serve;
        param.direction.set(direction);
        param.direction.Z = 110;
        param.speed = 0;

        testBall.waiting = false;
        testBall.spinType = ball.spinType;
        testBall.spin = ball.spin;
        targetPoint.Z = 0;
        // calculating l min angle (can be found better min value if include y in calculations)
        // have to think about adding this 9.8*(Math.Abs(ball.ballPosition.X)/maxSpeed)
        minAngle = Math.atan((CourtDimensions.netHeightArMiddle - ball.ballPosition.Z) / Math.abs(ball.ballPosition.X));
        minAngle *= (180 / Math.PI);

        // If ball is "close" to the net or target point is "close", best natural angle is just above the net. To avoid 50% error rate adjusting it.
        if (Math.abs(targetPoint.X) < CourtDimensions.serviceBoxLength + 1 || Math.abs(ball.ballPosition.X) < CourtDimensions.length / 2 - 2 && !ball.serve) {
            error *= Math.signum(0.8 - getRandom()) * getRandom();
        } else {
            error *= Math.signum(0.5 - getRandom()) * getRandom();
        }

        //adjusting speed and angles for drop shot
        if (ball.placement == Ball.Placement.dropShot) {
            maxSpeed = 100 * Constants.kphToMps;
            minAngle = 5;
            maxAngle = 45;
            error *= 0.25;
        }

        //reducing angle error for serve
        if (ball.placement == Ball.Placement.serveTop || ball.placement == Ball.Placement.serveBottom) {
            error *= 0.25;
        }

        if (player.playerStrategies.isSuperServe() && player.state == Player.states.serving) {
            player.playerStrategies.setSuperServe(false);
            error *= 0.1;
            maxSpeed *= (1.1 + player.attributes.serve);
        }

        // adding 15% of speed for attacking shots
        if (ball.placement == Ball.Placement.attackingBaseLine) {
            maxSpeed *= 1.15;
        }

        double i;
        double j = maxSpeed;

        while (j >= minSpeed && !found) {
            j -= 0.2;
            currentMinAngle = minAngle;
            currentMaxAngle = maxAngle;

            while (Math.abs(currentMaxAngle - currentMinAngle) > 0.1 && !found) {
                i = (currentMaxAngle + currentMinAngle) / 2;
                testBall.resetDirection();
                //  testBall.waiting = false;
                testBall.ballPosition.set(ball.ballPosition);

                testBall.direction.X = direction.X;
                testBall.direction.Y = direction.Y;
                testBall.direction.Z = Math.tan(i * Math.PI / 180) * direction.length();

                currentZ = testBall.direction.Z;

                testBall.direction.normalize();
                testBall.direction.scl(j);

                while (testBall.bounceCount == 0 && !testBall.net && Math.abs(testBall.ballPosition.X) < CourtDimensions.totalLength / 2 && Math.abs(testBall.ballPosition.Y) < CourtDimensions.totalWidth / 2) {
                    testBall.update();
                }

                point.set(testBall.ballPosition);
                point.Z = 0;

                Vector3D tmp = new Vector3D();

                double length = tmp.set(targetPoint).sub(point).length();
                //bestZ
                if (length < lengthError && !testBall.net && Math.signum(point.X) != Math.signum(ball.ballPosition.X)) {
                    lengthError = length;
                    param.direction.Z = currentZ;
                    currentAngle = i;
                    param.speed = j;
                }

                //!!! Have to consider to change error
                if (length < 0.25 && !testBall.net && Math.signum(point.X) != Math.signum(ball.ballPosition.X)) {
                    found = true;
                    param.direction.Z = currentZ;
                    currentAngle = i;
                    param.speed = j;
                }

                // changing angles
                if (distanceXY(targetPoint, ball.ballPosition) < distanceXY(ball.ballPosition, point)) {
                    currentMaxAngle = i;
                } else {
                    currentMinAngle = i;
                }
            }
        }

        //
        // bestZ found, adding trajectory points to testBalls list
        //

        // preparing ball
        testBall.reset();
        testBall.waiting = false;
        testBall.updateLevel = 1;
        testBall.serve = ball.serve;
        testBall.time = time;
        testBall.ballPosition.set(ball.ballPosition);
        testBall.direction.set(direction);

        testBall.direction.Z = Math.tan((currentAngle + error) * Math.PI / 180) * direction.length();
        testBall.spin = ball.spin;
        testBall.spinType = ball.spinType;

        param.direction.Z = testBall.direction.Z;

        testBall.direction.normalize();
        testBall.direction.scl(param.speed);

        while (testBall.bounceCount < 2) {
            testBall.update();
        }

        // bestZ found, adding trajectory points to testBalls list and asigns object ball value short ball
        ball.nextBallIsShort = testBall.shortBall;

        for (TimePoint3D p : testBall.trajectoryPoints) {
            ball.trajectoryPoints.add(new TimePoint3D(p));
        }
        return param;
    }

    private double calculateShotError(Player player) {
        double error = gameMode.getAngleError();

        error *= player.getShotPenalty().getErrorMultiplier();

        if (player.zone == net) {
            error *= 1.5;
        }

        if (player.state == Player.states.serving) {
            error *= 0.25 * (0.6 + 0.2 * player.attributes.serve + 0.2 * player.attributes.precision);
        }

        if (player.state == Player.states.returningShot) {
            if (player.getCurrentShotType() == Player.shotType.foreHand) {
                error *= (0.6 + 0.2 * player.attributes.forehand + 0.2 * player.attributes.precision);
            } else {
                error *= (0.6 + 0.2 * player.attributes.backhand + 0.2 * player.attributes.precision);
            }
        }

        return error;
    }

    private void shoot(Player player) {
        gameMode.getGameMechanics().forEach((mechanic) -> mechanic.onBeforeShot(this, player));
        ShotParameters shotParameters = new ShotParameters();
        player.setCurrentShotType(Player.shotType.serve);
        //clear trajectory points and bounce points
        ball.trajectoryPoints.clear();
        ball.bounceCount = 0;

        // Player is serving
        if (player.state == Player.states.serving) {
            shotParameters = calculateShotParameters(player, targetPoint(player), minSpeed(player), maxSpeed(player), -30, 30, calculateShotError(player));
        }

        // Player is returning shot
        if (player.state == Player.states.returningShot) {
            // I and III quadrant
            if ((ball.ballPosition.X > 0 && ball.ballPosition.Y < 0) || (ball.ballPosition.X < 0 && ball.ballPosition.Y > 0)) {
                if (Math.abs(player.position.Y) < Math.abs(ball.ballPosition.Y)) {
                    player.setCurrentShotType(Player.shotType.foreHand);
                } else {
                    player.setCurrentShotType(Player.shotType.backHand);
                }
            }
            //II and IV quadrant
            if ((ball.ballPosition.X < 0 && ball.ballPosition.Y < 0) || (ball.ballPosition.X > 0 && ball.ballPosition.Y > 0)) {
                if (Math.abs(player.position.Y) < Math.abs(ball.ballPosition.Y)) {
                    player.setCurrentShotType(Player.shotType.backHand);
                } else {
                    player.setCurrentShotType(Player.shotType.foreHand);
                }
            }

            shotParameters = calculateShotParameters(player, targetPoint(player), gameMode.getMinTheoreticalShotSpeed(), maxSpeed(player), -30, 30, calculateShotError(player));
        }
        // shoot the ball with parameter shotSpeed
        shotParameters.direction.normalize();
        ball.setLastToucher(player);
        ball.direction = new Vector3D(shotParameters.direction).scl(shotParameters.speed);

        gameMode.getGameMechanics().forEach((mechanic) -> mechanic.onAfterShot(this, player));
    }

    private Vector3D getShotPosition(Player player) {
        Vector3D position = new Vector3D(0, 0, 0);
        switch (player.state) {
            case matchEnded:
                if (player.position.X < 0) {
                    position.X = -gameMode.getPlayerSize() / 2;
                } else {
                    position.X = gameMode.getPlayerSize() / 2;
                }

                //Getting target position for server
                position.Y = CourtDimensions.widthSingles / 2 - gameMode.getPlayerSize() / 2;
                break;
            case preparingForServe:
                if (player.position.X < 0) {
                    position.X = -CourtDimensions.length / 2 - gameMode.getPlayerSize() / 2;
                } else {
                    position.X = CourtDimensions.length / 2 + gameMode.getPlayerSize() / 2;
                }

                double serveX = CourtDimensions.length / 2 + gameMode.getPlayerSize() + gameMode.getPlayerSize() / 2;
                double serveY = gameMode.getPlayerSize() / 2;
                //Getting target position for server
                if (player.servingSide == Player.sides.top && player.serving) {
                    position.Y = serveY - getRandom() * 2.5 - 1.25;
                    position.X = player.position.X > 0 ? serveX : -serveX;
                } else if (player.servingSide == Player.sides.bottom && player.serving) {
                    position.Y = -serveY + getRandom() * 2.5 + 1.25;
                    position.X = player.position.X > 0 ? serveX : -serveX;
                }

                //getting target position for serve retriever
                if (player.servingSide == Player.sides.top && !player.serving) {
                    position.Y = -CourtDimensions.widthSingles / 4 - gameMode.getPlayerSize() - getRandom() * 2;
                } else if (player.servingSide == Player.sides.bottom && !player.serving) {
                    position.Y = +CourtDimensions.widthSingles / 4 + gameMode.getPlayerSize() + getRandom() * 2;
                }
                break;

            case movingAfterShot:
                switch (player.zone) {
                    case net:
                        position.X = Math.signum(player.position.X) * (CourtDimensions.serviceBoxLength - CourtDimensions.serviceBoxLength / 1.75);
                        break;
                    case noMansLand:
                        position.X = Math.signum(player.position.X) * ((CourtDimensions.serviceBoxLength) + 0.5 * (CourtDimensions.length / 2 - CourtDimensions.serviceBoxLength));
                        break;
                    case baseline:
                        position.X = Math.signum(player.position.X) * (CourtDimensions.length / 2);
                        break;
                    case behindBaseline:
                        position.X = Math.signum(player.position.X) * (CourtDimensions.length / 2 + 2.5);
                        break;
                    case outside:
                        position.X = Math.signum(player.position.X) * (CourtDimensions.length / 2 + 5);
                        break;
                }

                if (player.zone != behindBaseline && player.zone != outside && player.zone != baseline) {
                    if (Math.abs(ball.direction.Y) <= CourtDimensions.widthSingles) {
                        position.Y = ball.direction.Y;
                    }
                } else {
                    position.Y = ball.direction.Y / 3;
                }
                break;

            case returningShot:
                double difficulty, easiestShot = 1000;
                Vector3D closestPoint = new Vector3D(0, 0, 0);
                double closestPointDistance = 100;
                boolean inReach;

                for (TimePoint3D trajectoryPoint : ball.trajectoryPoints) {
                    if (Math.signum(player.position.X) == Math.signum(trajectoryPoint.point.X) && Math.abs(player.position.X) > gameMode.getPlayerSize() / 2 &&
                            Math.abs(trajectoryPoint.point.X) < CourtDimensions.totalLength / 2 - 3 * gameMode.getPlayerSize() / 2 + 1) {
                        inReach = distanceXY(trajectoryPoint.point, player.position) - 3 * gameMode.getPlayerSize() / 2 <= (trajectoryPoint.time - time - Player.accelerationRate * gameMode.getEngineUpdateInterval()) * player.maxSpeed + player.breakingDistance();

                        if (ball.nextBallIsShort || player.zone == net || player.zone == noMansLand) {
                            difficulty = Math.abs(trajectoryPoint.point.X);
                        } else {
                            difficulty = Math.abs(player.height - trajectoryPoint.point.Z) + (2 - trajectoryPoint.bounceCount);
                        }

                        if (inReach && easiestShot > difficulty) {
                            easiestShot = difficulty;
                            position.X = player.position.X + (trajectoryPoint.point.X - player.position.X) * (distanceXY(player.position, trajectoryPoint.point) - 3 * gameMode.getPlayerSize() / 2) / distanceXY(player.position, trajectoryPoint.point);
                            position.Y = player.position.Y + (trajectoryPoint.point.Y - player.position.Y) * (distanceXY(player.position, trajectoryPoint.point) - 3 * gameMode.getPlayerSize() / 2) / distanceXY(player.position, trajectoryPoint.point);
                        }

                        // Finds nearest point if there are no target points in reach
                        if (position.X == 0 && position.Y == 0 && distanceXY(trajectoryPoint.point, player.position) < closestPointDistance) {
                            closestPointDistance = distanceXY(trajectoryPoint.point, player.position);
                            closestPoint.X = trajectoryPoint.point.X;
                            closestPoint.Y = trajectoryPoint.point.Y;
                        }
                    }


                }
                // If there are no reachable points, players target point is closest possible point
                if (Math.abs(position.X) < 0.01 && Math.abs(position.Y) < 0.01) {
                    position.X = closestPoint.X;
                    position.Y = closestPoint.Y;
                }
                break;

        }
        ball.nextBallIsShort = false;
        return position;
    }

    private void tossBall() {
        ball.reset();
        ball.serve = true;
        ball.waiting = false;

        bothPlayers.forEach((player -> {
            if (player.serving) {
                gameMode.getGameMechanics().forEach((mechanic) -> mechanic.onBeforeServe(this, player));
                player.state = Player.states.serving;
                player.waiting = false;
                ball.ballPosition.X = player.position.X;
                ball.ballPosition.Y = player.position.Y;
                ball.ballPosition.Z = 0.75 * player.height - ((getRandom() > 0.5 ? -1 : 1) * (getRandom()));
            }
        }));

        ball.direction.Z = 10 - ((getRandom() > 0.5 ? -1 : 1) * (getRandom() * 2));
    }

    public void changePlayerStrategies(SliderUpdate sliderUpdate, Player player) {
        if (SliderUpdate.actionButtons.contains(sliderUpdate.getSliderName())) {
            if (player.attributes.fitness >= SliderUpdate.actionCosts.get(sliderUpdate.getSliderName())) {
                player.attributes.fitness -= SliderUpdate.actionCosts.get(sliderUpdate.getSliderName());
            } else {
                return; //not enough fitness for action
            }
        }
        switch (sliderUpdate.getSliderName()) {
            case Serve:
                player.playerStrategies.setServing(sliderUpdate.getSliderValue());
                break;
            case ShotAiming:
                player.playerStrategies.setShotAiming(sliderUpdate.getSliderValue());
                break;
            case Shooting:
                player.playerStrategies.setShooting(sliderUpdate.getSliderValue());
                break;
            case PositionAfterShot:
                player.playerStrategies.setPositioningAfterShot(sliderUpdate.getSliderValue());
                break;
            case SuperServe:
                if(!player.playerStrategies.isSuperServe()){
                player.playerStrategies.setSuperServe(true);}
                break;
            case SuperSpeed:
                if(!player.playerStrategies.isSuperSpeed()) {
                    player.playerStrategies.setSuperSpeed(true);
                }
                break;
            case SuperReaction:
                if(!player.playerStrategies.isSuperReaction()) {
                    player.playerStrategies.setSuperReaction(true);
                }
                break;
            case Jump:
                if(!player.playerStrategies.isJump()) {
                    player.playerStrategies.setJump(true);
                }
                break;
        }
        player.calculateStrategyPenalties();
    }

    public double getAbsPlayerStartingPosition() {
        return gameMode.getPlayerSize() / 2 + 0.5;
    }

    public void initPlayers(com.tennio.gameserver.Player redPlayer, com.tennio.gameserver.Player bluePlayer) {
        double positionX = getAbsPlayerStartingPosition();
        playerRed.init(redPlayer.getStats(), positionX);
        playerBlue.init(bluePlayer.getStats(), -positionX);

        playerRed.id = 1;
        playerBlue.id = 2;

        if (this.gameMode instanceof BackgroundGame) {
            playerBlue.serving = getRandom() > 0.5;
            playerRed.serving = !playerBlue.serving;
            playerRed.servingSide = playerBlue.serving ? getAdvantageSide(playerRed) : getDeuceSide(playerRed);
            playerBlue.servingSide = getOppositeSite(playerRed.servingSide);
        } else {
            playerRed.servingSide = getDeuceSide(playerRed);
            playerBlue.servingSide = getOppositeSite(playerRed.servingSide);
            playerBlue.serving = false;
            playerRed.serving = true;
        }

        playerRed.movementParameters.direction = playerRed.getDirection();

        playerRed.maxSpeed *= (0.6 + 0.4 * playerRed.attributes.speed);
        playerRed.reactionTime = gameMode.getReactionTime(); // TODO better reaction time calculations

        playerBlue.maxSpeed *= (0.6 + 0.4 * playerBlue.attributes.speed);
        playerBlue.reactionTime = gameMode.getReactionTime();

        playerRed.movementParameters.shotPosition = getShotPosition(playerRed);
        playerBlue.movementParameters.shotPosition = getShotPosition(playerBlue);

        ball.setLastToucher(playerRed);
        gameMode.getGameMechanics().forEach((mechanic) -> mechanic.onInitPlayers(this));
    }

    private double minSpeed(Player player) {
        double minSpeed = gameMode.getMinTheoreticalServeSpeed();
        minSpeed *= player.getShotPenalty().getMinSpeedMultiplier();
        return minSpeed;
    }

    //Find max shot speed depending on players height and shot type
    private double maxSpeed(Player player) {
        double minSpeed = gameMode.getMinTheoreticalServeSpeed();
        double maxSpeed = gameMode.getMaxTheoreticalServeSpeed();

        minSpeed *= player.getShotPenalty().getMinSpeedMultiplier();
        maxSpeed *= player.getShotPenalty().getMaxSpeedMultiplier();

        if (player.getCurrentShotType() == Player.shotType.serve) {
            maxSpeed = (minSpeed + (maxSpeed * player.height / 2.2 - minSpeed) * player.attributes.serve *
                    ((1 - gameMode.getMaxSpeedRandom()) + gameMode.getMaxSpeedRandom() * getRandom()));
        } else {
            double attribute;

            if (player.getCurrentShotType() == Player.shotType.foreHand) {
                attribute = player.attributes.forehand;
            } else if (player.getCurrentShotType() == Player.shotType.backHand) {
                attribute = player.attributes.backhand;
            } else {
                return maxSpeed;
            }

            maxSpeed = gameMode.getMinTheoreticalShotSpeed() + (gameMode.getMaxTheoreticalShotSpeed() - gameMode.getMinTheoreticalShotSpeed()) *
                    (gameMode.getMaxSpeedDefaultPercentage() + gameMode.getMaxSpeedRandomPercentage() * getRandom() +
                            gameMode.getMaxSpeedForehandBackhandPercentage() * attribute +
                            gameMode.getMaxSpeedArmStrengthPercentage() * player.attributes.armStrength);

        }

        return maxSpeed;
    }

    //adds points and updates players
    private void addPoint(Player player) {
        Player opponent = opponentPlayer(player);
        ball.secondServe = false;

        playerRed.onPointEnd();
        playerBlue.onPointEnd();
        player.score.addPoint();

        // game ended
        if (Math.abs(playerRed.score.points - playerBlue.score.points) >= 2 &&
                (playerRed.score.points >= 4 || playerBlue.score.points >= 4)) {
            opponent.servingSide = getDeuceSide(opponent);
            player.servingSide = getOppositeSite(opponent.servingSide);
            this.onGameEnd(player);
        } else {
            player.servingSide = isDeuceSide() ? getDeuceSide(player) : getAdvantageSide(player);
            opponent.servingSide = getOppositeSite(player.servingSide);
        }

        setShotPositions();

        if (!(gameMode instanceof BackgroundGame)) {
            webSocketServer.sendPointUpdate(createPointUpdate());
        }

        ball.pointState = Ball.PointState.ballInGame;
        gameMode.getGameMechanics().forEach((mechanic) -> mechanic.onAfterPoint(this, player));
    }

    public PointUpdate createPointUpdate() {
        PointUpdate pointUpdate = new PointUpdate();
        pointUpdate.redPlayer = playerRed.score;
        pointUpdate.bluePlayer = playerBlue.score;
        return pointUpdate;
    }

    private void onGameEnd(Player winner) {
        playerRed.onGameEnd();
        playerBlue.onGameEnd();
        winner.score.addGame();

        gameMode.getGameMechanics().forEach((mechanic) -> mechanic.onAfterGame(this, winner));
        // set ended
        if (winner.score.games >= 6 && Math.abs(playerRed.score.games - playerBlue.score.games) >= 2) {
            onSetEnd(winner);
        }
    }

    public void onSetEnd(Player winner) {
        playerRed.onSetEnd();
        playerBlue.onSetEnd();
        winner.score.addSet();

        gameMode.getGameMechanics().forEach((mechanic) -> mechanic.onAfterSet(this, winner));
        if (winner.score.sets >= 2) { // best of 3
            this.onMatchEnd(winner);
        }
    }

    public void onMatchEnd(Player winner) {
        playerRed.onMatchEnd();
        playerBlue.onMatchEnd();
        winner.score.addMatch();
        setShotPositions();
        gameMode.getGameMechanics().forEach((mechanic) -> mechanic.onAfterMatch(this, winner));
    }

    private double distanceXY(Vector3D firstPoint, Vector3D secondPoint) {
        Vector3D tmp1 = new Vector3D(firstPoint);
        Vector3D tmp2 = new Vector3D(secondPoint);
        tmp1.Z = 0;
        tmp2.Z = 0;
        return tmp1.sub(tmp2).length();
    }

    private double getRandom() {
        return rand.nextDouble();
    }

    public void setShotPositions() {
        playerBlue.movementParameters.shotPosition = getShotPosition(playerBlue);
        playerRed.movementParameters.shotPosition = getShotPosition(playerRed);
    }

    public Player opponentPlayer(Player player) {
        if (player.id == 1) {
            return playerBlue;
        } else {
            return playerRed;
        }
    }

    private Player.sides getDeuceSide(Player player) {
        return player.position.X < 0 ? Player.sides.bottom : Player.sides.top;
    }

    private Player.sides getAdvantageSide(Player player) {
        return player.position.X > 0 ? Player.sides.bottom : Player.sides.top;
    }

    private Player.sides getOppositeSite(Player.sides side) {
        return side == Player.sides.bottom ? Player.sides.top : Player.sides.bottom;
    }

    private boolean isDeuceSide() {
        return Objects.equals(playerBlue.score.points, playerRed.score.points) || (playerRed.score.points + playerBlue.score.points) % 2 == 0;
    }

    public PauseReason getPauseReason() {
        return pauseReason;
    }

    public void setPauseReason(PauseReason pauseReason) {
        this.pauseReason = pauseReason;
    }

    public enum PauseReason {
        NONE, SwitchingSides, PlayerRedPause, PlayerBluePause
    }
}
