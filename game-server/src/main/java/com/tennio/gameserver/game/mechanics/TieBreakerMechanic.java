package com.tennio.gameserver.game.mechanics;

import com.tennio.gameserver.game.Engine;
import com.tennio.gameserver.game.player.Player;

public class TieBreakerMechanic extends GameMechanic {
    private static TieBreakerMechanic instance = new TieBreakerMechanic();

    private TieBreakerMechanic() {
    }

    public static TieBreakerMechanic getInstance() {
        return instance;
    }

    @Override
    public void onAfterPoint(Engine engine, Player winner) {
        if (engine.playerRed.score.games == 6 && engine.playerBlue.score.games == 6) {
            int totalPts = engine.playerBlue.score.points + engine.playerRed.score.points;
            // first serve goes to the player who was in line to serve before
            // second serve goes to other player and now every player gets two serves each
            if (totalPts == 1 || totalPts % 3 == 0) {
                changeServer(engine);
            }
            // every six points switch sides
            if (totalPts % 6 == 0) {
                SideSwitchingMechanic.getInstance().switchSides(engine);
            }
        }
    }

    @Override
    public void onAfterGame(Engine engine, Player winner) {
        // the player who wins the tie break game also wins the set
        if (winner.score.games == 7 && engine.opponentPlayer(winner).score.games == 6) {
            engine.onSetEnd(winner);
        }
    }

    private void changeServer(Engine engine) {
        if (engine.playerRed.serving) {
            engine.playerBlue.serving = true;
            engine.playerRed.serving = false;
        } else {
            engine.playerBlue.serving = false;
            engine.playerRed.serving = true;
        }
    }
}
