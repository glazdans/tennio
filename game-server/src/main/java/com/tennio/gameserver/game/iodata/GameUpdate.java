package com.tennio.gameserver.game.iodata;

import com.tennio.gameserver.game.Ball;
import com.tennio.gameserver.game.Engine;

public class GameUpdate {
    public final String type = "gameUpdate";
    public double time;
    public boolean ballWaiting;
    public double redX;
    public double redY;
    public double blueX;
    public double blueY;
    public double ballX;
    public double ballY;
    public double ballZ;
    public Ball.BallState ballState;
    public boolean clearBouncePoints;
    public double bouncePointX;
    public double bouncePointY;
    public boolean bounceIn;
    public double fitness;
    public Engine.PauseReason pauseReason;
    public Long timeout = null;

    public String toJson() {
        return "{\n" +
                "  \"type\": \"" + type + "\",\n" +
                "  \"time\": " + time + ",\n" +
                "  \"ballWaiting\": " + ballWaiting + ",\n" +
                "  \"redX\": " + redX + ",\n" +
                "  \"redY\": " + redY + ",\n" +
                "  \"blueX\": " + blueX + ",\n" +
                "  \"blueY\":" + blueY + ",\n" +
                "  \"ballX\": " + ballX + ",\n" +
                "  \"ballY\": " + ballY + ",\n" +
                "  \"ballZ\": " + ballZ + ",\n" +
                "  \"clearBouncePoints\": " + clearBouncePoints + ",\n" +
                "  \"bouncePointX\": " + bouncePointX + ",\n" +
                "  \"bouncePointY\": " + bouncePointY + ",\n" +
                "  \"fitness\": " + fitness + ",\n" +
                "  \"bounceIn\": " + bounceIn + ",\n" +
                "  \"timeout\": " + timeout + ",\n" +
                "  \"pauseReason\": " + pauseReason.ordinal() + ",\n" +
                "  \"ballState\": " + (ballState != null ? ballState.ordinal() : "null") + "\n" +
                "}";
    }
}
