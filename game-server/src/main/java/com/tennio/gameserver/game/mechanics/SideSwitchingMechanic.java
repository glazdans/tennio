package com.tennio.gameserver.game.mechanics;

import com.tennio.gameserver.game.Engine;
import com.tennio.gameserver.game.helpers.Vector3D;
import com.tennio.gameserver.game.player.MovementParameters;
import com.tennio.gameserver.game.player.Player;

public class SideSwitchingMechanic extends GameMechanic {
    private static SideSwitchingMechanic instance = new SideSwitchingMechanic();

    protected SideSwitchingMechanic() {
    }

    public static SideSwitchingMechanic getInstance() {
        return instance;
    }

    @Override
    public void onAfterGame(Engine engine, Player winner) {
        long currentGames = winner.score.games + engine.opponentPlayer(winner).score.games;
        if (currentGames % 2 != 0) {
            switchSides(engine);
        }
    }

    public void switchSides(Engine engine) {
        engine.setPauseReason(Engine.PauseReason.SwitchingSides);

        MovementParameters tmp = engine.playerBlue.movementParameters;
        engine.playerBlue.movementParameters = engine.playerRed.movementParameters;
        engine.playerRed.movementParameters = tmp;

        Vector3D tmpP = engine.playerBlue.position;
        engine.playerBlue.position = engine.playerRed.position;
        engine.playerRed.position = tmpP;
    }
}
