package com.tennio.gameserver.game.helpers;

public final class Constants {
    ///  Air density in kg/m3
    public static double airDensity = 1.21;

    // gravity in m/s2
    public static double g = 9.80665;

    // converting km/h to m/s
    public static double kphToMps = 0.27777778;

    // from revolutions per min to m/s
    public static double revPerMinToRevPerSec = (2 * Math.PI * (6.541 + 6.858) / 2 / 2 / 100 / 60);
}
