package com.tennio.gameserver.game.helpers;

public class Vector3D {
    /**
     * the x-component of this vector
     **/
    public double X;
    /**
     * the y-component of this vector
     **/
    public double Y;
    /**
     * the z-component of this vector
     **/
    public double Z;

    public Vector3D() {
    }

    public Vector3D(Vector3D vector3D) {
        this(vector3D.X, vector3D.Y, vector3D.Z);
    }

    public Vector3D(double x, double y, double z) {
        X = x;
        Y = y;
        Z = z;
    }

    public Vector3D normalize() {
        final double len2 = this.len2();
        if (len2 == 0f || len2 == 1f) return this;
        return this.scl(1f / (float) Math.sqrt(len2));
    }

    public double length() {
        return (float) Math.sqrt(X * X + Y * Y + Z * Z);
    }

    public double len2() {
        return X * X + Y * Y + Z * Z;
    }

    public Vector3D scl(double scalar) {
        return this.set(this.X * scalar, this.Y * scalar, this.Z * scalar);
    }

    public Vector3D set(Vector3D vector) {
        return this.set(vector.X, vector.Y, vector.Z);
    }

    public Vector3D set(double x, double y, double z) {
        this.X = x;
        this.Y = y;
        this.Z = z;
        return this;
    }

    public Vector3D add(final Vector3D vector) {
        return this.add(vector.X, vector.Y, vector.Z);
    }

    public Vector3D add(double x, double y, double z) {
        return this.set(this.X + x, this.Y + y, this.Z + z);
    }

    public Vector3D sub(final Vector3D a_vec) {
        return this.sub(a_vec.X, a_vec.Y, a_vec.Z);
    }

    public double lenDiff(final Vector3D a_vec) {
        double x = X - a_vec.X, y  = Y - a_vec.Y, z = Z - a_vec.Z;
        return x * x  + y * y + z * z;
    }

    public Vector3D sub(double x, double y, double z) {
        return this.set(this.X - x, this.Y - y, this.Z - z);
    }

}
