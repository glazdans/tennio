package com.tennio.gameserver.game.player;

import com.tennio.gameserver.SliderUpdate;
import com.tennio.gameserver.game.Engine;

/**
 * @see SliderUpdate - I/O data wrapper for this class
 * @see Engine#changePlayerStrategies - logic implementation
 */
public class PlayerStrategies {
    /** Sliders */
    private long serving;
    private long shotAiming;
    private long shooting;
    private long positioningAfterShot;

    /** Action Buttons
     * @see SliderUpdate#actionCosts - button press costs
     * @see SliderUpdate#actionButtons - button list
     */
    private boolean superServe;
    private boolean superReaction;
    private boolean superSpeed;
    private boolean jump;

    public PlayerStrategies() {

    }

    public PlayerStrategies(PlayerStrategies other) {
        this.serving = other.serving;
        this.shotAiming = other.shotAiming;
        this.shooting = other.shooting;
        this.positioningAfterShot = other.positioningAfterShot;
        this.superServe = other.superServe;
        this.superReaction = other.superReaction;
        this.superSpeed = other.superSpeed;
        this.jump = other.jump;
    }

    public long getServing() {
        return serving;
    }

    // between -0.15 to 0.15
    public double getServePercentage() {
        return serving * 0.15 / 100;
    }

    // between -0.1 to 0.4
    public double getShotPositioningPercentage() {
        return -0.1 + 0.5 * ((-shotAiming + 100) / 200);
    }

    public void setServing(long serving) {
        this.serving = serving;
    }

    public long getShotAiming() {
        return shotAiming;
    }

    public void setShotAiming(long shotAiming) {
        this.shotAiming = shotAiming;
    }

    public long getShooting() {
        return shooting;
    }

    public void setShooting(long shooting) {
        this.shooting = shooting;
    }

    public long getPositioningAfterShot() {
        return positioningAfterShot;
    }

    public void setPositioningAfterShot(long positioningAfterShot) {
        this.positioningAfterShot = positioningAfterShot;
    }

    public boolean isSuperServe() {
        return superServe;
    }

    public void setSuperServe(boolean superServe) {
        this.superServe = superServe;
    }

    public boolean isSuperReaction() {
        return superReaction;
    }

    public void setSuperReaction(boolean superReaction) {
        this.superReaction = superReaction;
    }

    public boolean isSuperSpeed() {
        return superSpeed;
    }

    public void setSuperSpeed(boolean superSpeed) {
        this.superSpeed = superSpeed;
    }

    public boolean isJump() {
        return jump;
    }

    public void setJump(boolean jump) {
        this.jump = jump;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlayerStrategies that = (PlayerStrategies) o;

        if (serving != that.serving) return false;
        if (shotAiming != that.shotAiming) return false;
        if (shooting != that.shooting) return false;
        return positioningAfterShot == that.positioningAfterShot;
    }

    @Override
    public int hashCode() {
        int result = (int) (serving ^ (serving >>> 32));
        result = 31 * result + (int) (shotAiming ^ (shotAiming >>> 32));
        result = 31 * result + (int) (shooting ^ (shooting >>> 32));
        result = 31 * result + (int) (positioningAfterShot ^ (positioningAfterShot >>> 32));
        return result;
    }
}
