package com.tennio.gameserver;

import com.tennio.gameserver.game.player.Player;
import com.tennio.gameserver.game.player.PlayerStrategies;

import java.util.*;

/**
 * @implNote Any changes should be mirrored in typescript (slider-update.ts)
 */
public class SliderUpdate {
    public static List<SliderUpdate> fromPlayer(Player player) {
        PlayerStrategies ps = player.playerStrategies;
        return new ArrayList<SliderUpdate>() {{
            add(new SliderUpdate(SliderName.Serve, ps.getServing()));
            add(new SliderUpdate(SliderName.PositionAfterShot, ps.getPositioningAfterShot()));
            add(new SliderUpdate(SliderName.Shooting, ps.getShooting()));
            add(new SliderUpdate(SliderName.ShotAiming, ps.getShotAiming()));
        }};
    }

    public SliderUpdate() {

    }

    public SliderUpdate(SliderName sliderName, Long sliderValue) {
        this.sliderName = sliderName;
        this.sliderValue = sliderValue;
    }

    @Override
    public String toString() {
        return "\"sliderName\":" + sliderName +
                ", \"sliderValue\":" + sliderValue + "}";
    }

    public enum SliderName {
        Serve, ShotAiming, Shooting, PositionAfterShot, SuperServe, SuperReaction, SuperSpeed, Jump
    }

    public final static List<SliderName> actionButtons = Arrays.asList(
            SliderName.SuperServe,
            SliderName.SuperReaction,
            SliderName.Jump,
            SliderName.SuperSpeed
    );

    public final static Map<SliderName, Integer> actionCosts = new HashMap<SliderName, Integer>() {{
        put(SliderName.SuperServe, 100);
        put(SliderName.SuperReaction, 50);
        put(SliderName.SuperSpeed, 100);
        put(SliderName.Jump, 50);
    }};

    private SliderName sliderName;
    private Long sliderValue;

    public SliderName getSliderName() {
        return sliderName;
    }

    public void setSliderName(SliderName sliderName) {
        this.sliderName = sliderName;
    }

    public Long getSliderValue() {
        return sliderValue;
    }

    public void setSliderValue(Long sliderValue) {
        this.sliderValue = sliderValue;
    }
}
