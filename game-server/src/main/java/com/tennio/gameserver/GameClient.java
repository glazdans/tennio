package com.tennio.gameserver;

import com.tennio.common.requests.security.JwtAuthenticationRequest;
import com.tennio.common.requests.security.JwtAuthenticationResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class GameClient {
    private RestTemplate restTemplate;
    private HttpEntity<Object> entity; // For authentication headers only

    @Value("${tennio.server.main.ip}")
    private String uri = "localhost:8080/";

    @Value("${tennio.gameServer.username}")
    private String username;

    @Value("${tennio.gameServer.password}")
    private String password;

    @Value("${tennio.gameServer.address}")
    private String serverIp;

    private List<GameEndEvent> unsentGameEndEvents = new ArrayList<>();

    @PostConstruct
    public void init() {
        restTemplate = new RestTemplate();
        login();
    }

    private void login() {
        try {
            JwtAuthenticationRequest request = new JwtAuthenticationRequest(username, password);
            String jwtToken = restTemplate.postForObject(uri + "/login/auth/", request, JwtAuthenticationResponse.class).getToken();
            createHttpEntity(jwtToken);
        } catch (RestClientException exception) {
            entity = null;
            System.out.println("Failed to login: " + uri);
        }
    }

    private void createHttpEntity(String jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken);
        headers.set("Address", serverIp);

        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<>(null, headers);
    }

    public void endGame(GameEndEvent gameEndEvent) {
        try {
            restTemplate.postForObject(uri + "/game/end-game", gameEndEvent, GameEndEvent.class);
        } catch (ResourceAccessException e) {
            this.unsentGameEndEvents.add(gameEndEvent);
        }
    }

    private void sendUnsentGameEndEvents() {
        this.unsentGameEndEvents.forEach(gameEndEvent -> {
            try {
                restTemplate.postForObject(uri + "/game/end-game", gameEndEvent, GameEndEvent.class);
                this.unsentGameEndEvents.remove(gameEndEvent);
            } catch (ResourceAccessException e) {
                // TODO : add some way to notify admin when server is down and cannot finish games and game might already be canceled!!
            }
        });
    }

    @Scheduled(initialDelay = 60 * 60 * 1000, fixedDelay = 60 * 60 * 1000)
    public void refreshJWT() {
        try {
            ResponseEntity<JwtAuthenticationResponse> response = restTemplate.exchange(uri + "/login/refresh",
                    HttpMethod.GET, this.entity, JwtAuthenticationResponse.class);
            String jwtToken = response.getBody().getToken();
            createHttpEntity(jwtToken);
            sendUnsentGameEndEvents();
        } catch (RestClientException e) {
            e.printStackTrace();
            this.login();
        }

    }

    @Scheduled(initialDelay = 0, fixedDelay = 5 * 1000)
    public void addToMainServer() {
        if (entity == null) {
            login();            // If login doesn't work entity will still be null
        }

        if (entity != null) {
            try {
                ResponseEntity<String> response = restTemplate.exchange(uri + "/server/", HttpMethod.PUT, this.entity, String.class);
                if(!response.getBody().equals("true")) {
                    entity = null;
                    System.out.println("Failed to add to main server!!!");
                }
            } catch (Exception e) {
                e.printStackTrace();
                entity = null;
            }
        }
    }

    public String getServerIp() {
        return serverIp;
    }
}
