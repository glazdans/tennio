package com.tennio.gameserver;

import com.tennio.common.GameType;
import com.tennio.common.requests.ServerInfoRequest;
import com.tennio.gameserver.exceptions.UnimplementedGameTypeException;
import com.tennio.gameserver.game.gamemodes.GameModeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GameServerService {
    // List of ports, max servers,
    private List<GameThread<?>> gameThreads;

    private Map<Long, GameThread> gameThreadCache;

    public GameServerService() {
        gameThreads = new ArrayList<>();
    }

    @Autowired
    private GameClient gameClient;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${tennio.production}")
    private boolean production;

    @PostConstruct
    public void init() {
        gameThreadCache = new HashMap<>();
    }

    public ServerInfoRequest getInstance(Long gameId, GameCreationRequest request) throws UnimplementedGameTypeException {
        GameThread gameThread = gameThreadCache.get(gameId);
        if (gameThread == null) {
            gameThread = new GameThread<>(gameClient.getServerIp(), jwtTokenUtil, production, GameModeHandler.getGameModeByGameType(request.getGameType()));
            gameThread.setGameServerService(this);
            gameThread.setGameId(gameId);
            System.out.println(request);

            if (request != null) {
                gameThread.setPlayerRed(request.getPlayerRed());
                if (request.getGameType() != GameType.AI) {
                    gameThread.setPlayerBlue(request.getPlayerBlue());
                }
            }
            gameThread.setName("GameThread game:" + gameId);
            gameThread.start();
            while (gameThread.server == null) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            gameThreadCache.put(gameId, gameThread);
        }

        return gameThread.getServerInfo();
    }

    public GameClient getGameClient() {
        return gameClient;
    }

    public Map<Long, GameThread> getGameThreadCache() {
        return gameThreadCache;
    }
}
