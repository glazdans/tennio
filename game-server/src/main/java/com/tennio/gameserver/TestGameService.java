package com.tennio.gameserver;

import com.tennio.common.Settings;
import com.tennio.gameserver.game.gamemodes.BackgroundGame;
import com.tennio.gameserver.game.player.PlayerAttributes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TestGameService {
    // List of ports, max servers,
    private List<GameThread<?>> gameThreads;

    @Value("${tennio.production}")
    private boolean production;

    private final GameClient gameClient;

    private final JwtTokenUtil jwtTokenUtil;

    private int maxThreads = 75;

    @Autowired
    public TestGameService(GameClient gameClient, JwtTokenUtil jwtTokenUtil) {
        gameThreads = new ArrayList<>();
        this.gameClient = gameClient;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    public Map<Integer, Map<Integer, GameEndResults>> runForAllStatLevels(int cnt) throws Exception {
        List<String> csv = new ArrayList<>();
        csv.add(",1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30");
        Map<Integer, Map<Integer, GameEndResults>> resultsMap = new HashMap<>();

        for(Integer j = 1; j <= Settings.MAX_STAT_LEVEL; j++) {
            String jString = j.toString();
            System.out.println("started: " + j.toString());
            Map<Integer, GameEndResults> jMap = new HashMap<>();
            for (Integer i = 1; i <= Settings.MAX_STAT_LEVEL; i++) {
                if(i >= j) {
                    jMap.put(i, createGameThreads(cnt, PlayerAttributes.base(j), PlayerAttributes.base(i)));
                    jString += ",\"" + String.format("%.2f", jMap.get(i).redWinPerc) + "\"";
                } else {
                    jString += ',';
                }
            }
            resultsMap.put(j, jMap);
            csv.add(jString);
            System.out.println("ended: " + j.toString());
        }

        for (String s :
                csv) {
            System.out.println(s);
        }

        return resultsMap;
    }

    public GameEndResults createGameThreads(int cnt) throws Exception {
        return createGameThreads(cnt, PlayerAttributes.base(30), PlayerAttributes.base(30));
    }

    public GameEndResults createGameThreads(int cnt, PlayerAttributes redAttrs, PlayerAttributes blueAttrs) throws Exception {
        List<GameThread> gameThreads = new ArrayList<>();
        GameEndResults gameEndResults = new GameEndResults();

        int ranThreads = 0;
        int redWins = 0;
        long startTime = System.nanoTime();
        BackgroundGame gameMode = new BackgroundGame();

        while(ranThreads < cnt) {
            int threadsThisRun = cnt - ranThreads;
            threadsThisRun = threadsThisRun > maxThreads ? maxThreads : threadsThisRun;

            for (int i = 0; i < threadsThisRun; i++) {
                GameThread gameThread = new GameThread<>(gameClient.getServerIp(), this.jwtTokenUtil, production, gameMode);
                gameThread.setPlayerRed(new Player("RED", 1, new PlayerAttributes(redAttrs)));
                gameThread.setPlayerBlue(new Player("BLUE", 0, new PlayerAttributes(blueAttrs)));
                gameThread.setName("GameThread" + i);
                gameThread.start();
                gameThreads.add(gameThread);
                ranThreads++;
            }

            for (GameThread gameThread :
                    gameThreads) {
                try {
                    gameThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            for (GameThread gameThread : gameThreads) {
                GameEndResult endResult = new GameEndResult();
                endResult.time = Math.round(gameThread.endTime * (0.01 / gameMode.getThreadUpdateInterval()));
                endResult.redSets = gameThread.getEngine().playerRed.score.sets;
                endResult.blueSets = gameThread.getEngine().playerBlue.score.sets;
                gameEndResults.results.add(endResult);
                if (endResult.redSets > endResult.blueSets) {
                    redWins++;
                }
            }
            gameThreads.clear();
        }

        gameEndResults.time = System.nanoTime() - startTime;

        gameEndResults.redWinPerc = redWins * 1.0 / ranThreads * 100;
        gameEndResults.blueWinPerc = 100.0 - gameEndResults.redWinPerc;

        return gameEndResults;
    }

    class GameEndResults {
        private List<GameEndResult> results = new ArrayList<>();
        public Double redWinPerc;
        public double blueWinPerc;
        public long time;
    }

    class GameEndResult {
        public long time;

        public int redSets;
        public int blueSets;
    }
}
