package com.tennio.gameserver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tennio.common.requests.GameEndEvent.GameEndCause;
import com.tennio.common.requests.Score;
import com.tennio.common.requests.ServerInfoRequest;
import com.tennio.gameserver.exceptions.NoMoreTimeoutsLeftException;
import com.tennio.gameserver.exceptions.PlayerNotFoundException;
import com.tennio.gameserver.exceptions.TimeoutAlreadyStartedException;
import com.tennio.gameserver.game.Ball;
import com.tennio.gameserver.game.Engine;
import com.tennio.gameserver.game.gamemodes.BackgroundGame;
import com.tennio.gameserver.game.gamemodes.BasicGame;
import com.tennio.gameserver.game.helpers.Vector3D;
import com.tennio.gameserver.game.iodata.GameUpdate;
import com.tennio.gameserver.websockets.GameEndMessage;
import com.tennio.gameserver.websockets.SimpleServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Objects;

public class GameThread<T extends BasicGame> extends Thread {
    private GameServerService gameServerService;
    private Long gameId;
    private Player playerRed = null;
    private Player playerBlue = null;
    private boolean isRunning;
    SimpleServer server;
    private String thisServerIp;

    public Engine<T> getEngine() {
        return engine;
    }

    private Engine<T> engine;
    private long currentTime = System.nanoTime();
    private double accumulator;
    private int updateSendRate = 5;
    private int currentTick;
    private static int staticPort = 8887;
    private JwtTokenUtil jwtTokenUtil;
    private T gameMode;
    private boolean production;
    public long endTime = 0;

    private Long timeout = 0L;
    private Long currentPauseLength = 0L;
    private Engine.PauseReason pauseReason = Engine.PauseReason.NONE;
    private GameEndCause gameEndCause = null;

    GameThread(String thisServerIp, JwtTokenUtil jwtTokenUtil, boolean production, T gameMode) {
        this.thisServerIp = thisServerIp;
        this.jwtTokenUtil = jwtTokenUtil;
        this.production = production;
        this.gameMode = gameMode;
    }

    public void initPause(Long ms, Engine.PauseReason pauseReason) {
        this.timeout = ms;
        this.currentPauseLength = ms;
        this.pauseReason = pauseReason;
    }

    @Override
    public void run() {
        init();

        long gameStartTime = System.currentTimeMillis();
        currentTime = System.nanoTime();

        while (isRunning) {
            interceptPause();
            update(false);

            if (engine.matchEnded) {
                isRunning = false;
            }
        }

        endTime = System.currentTimeMillis() - gameStartTime;
        if (engine.matchEnded && this.gameEndCause == null) {
            this.gameEndCause = GameEndCause.PlayerWon;
            sendGameResult(GameEndCause.PlayerWon, engine.playerBlue.score.sets > engine.playerRed.score.sets ? playerBlue.getId() : playerRed.getId());
        }

        if (server != null) {
            try {
                server.stop();
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void init() {
        int port = staticPort;

        if (!(gameMode instanceof BackgroundGame)) {
            InetSocketAddress socketAddress = new InetSocketAddress(port);
            server = new SimpleServer(socketAddress, this, jwtTokenUtil);
            staticPort++;
            Thread serverThread = new Thread(server);
            serverThread.setName("ServerThread#" + gameId);
            serverThread.start();
        }

        engine = new Engine<>(server, gameMode);
        gameMode.beforeInitPlayers(this);
        engine.initPlayers(this.playerRed, this.playerBlue);

        while (!gameMode.isGameReady(this)) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        isRunning = true;
    }

    private void interceptPause() {
        Engine.PauseReason enginePauseReason = engine.getPauseReason();
        if (engine.isPausable() && (timeout != 0L || enginePauseReason != Engine.PauseReason.NONE)) {
            long elapsedTime = 0;

            if (enginePauseReason == Engine.PauseReason.SwitchingSides) {
                this.initPause(5000L, Engine.PauseReason.SwitchingSides);
                engine.setPauseReason(Engine.PauseReason.NONE);
            } else if (pauseReason == Engine.PauseReason.PlayerRedPause || pauseReason == Engine.PauseReason.PlayerBluePause) {
                Player timeoutPlayer = pauseReason == Engine.PauseReason.PlayerRedPause ? this.playerRed : this.playerBlue;
                if (timeoutPlayer.getTimeoutsLeft() >= 1) {
                    timeoutPlayer.setTimeoutsLeft(timeoutPlayer.getTimeoutsLeft() - 1);
                } else {
                    resetAfterPause();
                    return;
                }
            }

            long authStartTime = System.currentTimeMillis();
            while (elapsedTime < currentPauseLength && this.gameEndCause == null) {
                try {
                    update(true);
                    elapsedTime = System.currentTimeMillis() - authStartTime;
                    timeout = currentPauseLength - elapsedTime;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            resetAfterPause();
        }
    }

    private void resetAfterPause() {
        this.timeout = 0L;
        this.pauseReason = Engine.PauseReason.NONE;
    }

    private void update(boolean paused) {
        long newTime = System.nanoTime();
        long frameTimeInNanos = newTime - currentTime;
        currentTime = newTime;
        double frameTime = frameTimeInNanos / (1000000000.0);

        accumulator += frameTime;
        while (accumulator >= gameMode.getThreadUpdateInterval()) {
            currentTick++;

            if(!paused) {
                engine.update();
                engine.ball.update();
                engine.playerRed.update();
                engine.playerBlue.update();
            } else {
                engine.recoverFitnessTick(true);
            }

            if (currentTick % updateSendRate == 0 && !(gameMode instanceof BackgroundGame)) {
                server.sendGameUpdate(generateGameUpdate(engine, false));
            }

            accumulator -= gameMode.getThreadUpdateInterval();
        }

        try {
            Thread.sleep(1);
        } catch (Exception e) {
        }
    }

    public void sendGameResult(GameEndCause gameEndCause, Long causerId) {
        if (gameServerService != null) {
            gameServerService.getGameThreadCache().remove(gameId);

            GameEndMessage message = new GameEndMessage();
            message.causer = causerId;
            message.payload = gameEndCause;

            try {
                server.onGameEnd(message);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            HashMap<Long, Score> scores = new HashMap<Long, Score>() {{
                put(playerBlue.getId(), engine.playerBlue.score);
                put(playerRed.getId(), engine.playerRed.score);
            }};

            GameEndEvent gameEndEvent = new GameEndEvent(gameId, scores, engine.time, gameEndCause, causerId);
            gameServerService.getGameClient().endGame(gameEndEvent);
        }
    }

    public boolean areBothPlayersConnected() {
        return this.playerBlue.isConnected() && this.playerRed.isConnected();
    }

    public Player getPlayerByUsername(String username) throws PlayerNotFoundException {
        if (Objects.equals(this.playerBlue.getUsername(), username)) {
            return this.playerBlue;
        }

        if (Objects.equals(this.playerRed.getUsername(), username)) {
            return this.playerRed;
        }

        throw new PlayerNotFoundException();
    }

    public com.tennio.gameserver.game.player.Player<T> getGamePlayerById(Long id) throws PlayerNotFoundException {
        if (Objects.equals(this.playerBlue.getId(), id)) {
            return this.engine.playerBlue;
        }

        if (Objects.equals(this.playerRed.getId(), id)) {
            return this.engine.playerRed;
        }

        throw new PlayerNotFoundException();
    }

    public void onSliderUpdate(SliderUpdate sliderUpdate, Player sender) {
        if (sender.getId() == this.playerRed.getId()) {
            engine.changePlayerStrategies(sliderUpdate, engine.playerRed);
        } else if (sender.getId() == this.playerBlue.getId()) {
            engine.changePlayerStrategies(sliderUpdate, engine.playerBlue);
        } else {
            System.out.println("Player id not found");
        }
        System.out.println(sliderUpdate);
    }

    public ServerInfoRequest getServerInfo() {
        ServerInfoRequest serverInfoRequest = new ServerInfoRequest();
        serverInfoRequest.setIp(this.thisServerIp); // localhost
        serverInfoRequest.setPort(Integer.toString(server.getAddress().getPort()));
        return serverInfoRequest;
    }

    public GameUpdate generateGameUpdate(Engine engine, boolean ignorePause) {
        GameUpdate gameUpdate = new GameUpdate();

        gameUpdate.pauseReason = this.pauseReason;
        gameUpdate.timeout = Objects.equals(timeout, currentPauseLength) ? 0 : timeout;

        if (ignorePause || pauseReason.equals(Engine.PauseReason.NONE) || Objects.equals(timeout, currentPauseLength)) {
            gameUpdate.time = engine.time;
            gameUpdate.ballState = engine.ball.ballState;
            gameUpdate.ballWaiting = engine.ball.waiting;
            gameUpdate.redX = engine.playerRed.position.X;
            gameUpdate.redY = engine.playerRed.position.Y;
            gameUpdate.blueX = engine.playerBlue.position.X;
            gameUpdate.blueY = engine.playerBlue.position.Y;
            gameUpdate.ballX = engine.ball.ballPosition.X;
            gameUpdate.ballY = engine.ball.ballPosition.Y;
            gameUpdate.ballZ = engine.ball.ballPosition.Z;

            if (!engine.ball.bouncePoints.isEmpty() && engine.ball.bouncePointTime > 0) {
                gameUpdate.bouncePointX = ((Vector3D) engine.ball.bouncePoints.get(0)).X;
                gameUpdate.bouncePointY = ((Vector3D) engine.ball.bouncePoints.get(0)).Y;
                gameUpdate.bounceIn = ((Ball.BouncePoint) engine.ball.bouncePoints.get(0)).bounceIn;
            }
        }

        return gameUpdate;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setIsRunning(boolean isActive) {
        this.isRunning = isActive;
    }

    public GameServerService getGameServerService() {
        return gameServerService;
    }

    public void setGameServerService(GameServerService gameServerService) {
        this.gameServerService = gameServerService;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public void setPlayerRed(Player playerRed) {
        this.playerRed = playerRed;
    }

    public void setPlayerBlue(Player playerBlue) {
        this.playerBlue = playerBlue;
    }

    public Player getPlayerRed() {
        return playerRed;
    }

    public Player getPlayerBlue() {
        return playerBlue;
    }

    @Override
    public String toString() {
        return "GameThread{" +
                "gameServerService=" + gameServerService +
                ", gameId=" + gameId +
                ", playerRed=" + playerRed +
                ", playerBlue=" + playerBlue +
                ", isRunning=" + isRunning +
                ", server=" + server +
                ", thisServerIp='" + thisServerIp + '\'' +
                ", engine=" + engine +
                ", currentTime=" + currentTime +
                ", accumulator=" + accumulator +
                ", updateSendRate=" + updateSendRate +
                ", currentTick=" + currentTick +
                ", jwtTokenUtil=" + jwtTokenUtil +
                ", gameMode=" + gameMode +
                ", production=" + production +
                '}';
    }

    public boolean attemptGamePause(Player sender) throws TimeoutAlreadyStartedException, NoMoreTimeoutsLeftException {
        if (this.pauseReason != Engine.PauseReason.NONE) {
            throw new TimeoutAlreadyStartedException();
        }
        if (sender.getTimeoutsLeft() > 0) {
            this.initPause(Player.TIMEOUT_LENGTH, (sender.getId() == this.getPlayerRed().getId() ? Engine.PauseReason.PlayerRedPause : Engine.PauseReason.PlayerBluePause));
            server.sendGameUpdate(generateGameUpdate(engine, false));
            return true;
        }
        throw new NoMoreTimeoutsLeftException();
    }

    public long getTimeout() {
        return timeout;
    }

    public void surrender(Player sender) {
        engine.playerRed.state = com.tennio.gameserver.game.player.Player.states.matchEnded;
        engine.playerBlue.state = com.tennio.gameserver.game.player.Player.states.matchEnded;

        this.gameEndCause = GameEndCause.PlayerSurrender;
        sendGameResult(GameEndCause.PlayerSurrender, sender.getId());
        this.isRunning = false;
    }
}
