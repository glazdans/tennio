package com.tennio.gameserver;

import com.tennio.gameserver.game.player.PlayerAttributes;

public class Player extends com.tennio.common.requests.Player {
    private PlayerAttributes stats;
    private boolean connected = false;
    private long timeoutsLeft = 3;
    public static final long TIMEOUT_LENGTH = 60000;

    public Player(String name, long id, PlayerAttributes stats) {
        super(name, id);
        this.stats = stats;
    }

    public Player() {
        super();
    }

    public PlayerAttributes getStats() {
        return stats;
    }

    public void setStats(PlayerAttributes stats) {
        this.stats = stats;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String toJSON() {
        return "{" +
                "\"name\":\"" + name + '"' +
                ", \"id\":" + id +
                ", \"connected\":" + (connected ? "true" : "false") +
                '}';
    }

    public long getTimeoutsLeft() {
        return timeoutsLeft;
    }

    public void setTimeoutsLeft(long timeoutsLeft) {
        this.timeoutsLeft = timeoutsLeft;
    }
}
