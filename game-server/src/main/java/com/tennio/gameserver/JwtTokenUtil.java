package com.tennio.gameserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class JwtTokenUtil extends com.tennio.common.security.JwtTokenUtil {
    @Autowired
    void setSecret(@Value("${jwt.secret}") String secret) {
        this.secret = secret;
    }
}
