package com.tennio.gameserver;


import com.tennio.common.requests.Score;

import java.util.HashMap;

public class GameEndEvent extends com.tennio.common.requests.GameEndEvent {
    public GameEndEvent(Long gameId, HashMap<Long, Score> scores, Double time, GameEndCause gameEndCause, Long gameEndCauserId) {
        this.gameId = gameId;
        this.scores = scores;
        this.time = time;
        this.gameEndCause = gameEndCause;
        this.gameEndCauserId = gameEndCauserId;
    }
}
