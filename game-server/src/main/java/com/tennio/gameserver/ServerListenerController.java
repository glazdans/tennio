package com.tennio.gameserver;

import com.tennio.common.requests.ServerInfoRequest;
import com.tennio.gameserver.exceptions.UnimplementedGameTypeException;
import com.tennio.gameserver.game.gamemodes.BasicGame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

// TODO origin add only from main server
@CrossOrigin()
@RestController
public class ServerListenerController {
    @Autowired
    private GameServerService gameServerService;

    @Autowired
    private GameClient gameClient;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

//    @Autowired
//    private TestGameService testGameService;

    @Value("${tennio.production")
    private String production;

    @RequestMapping(path = "/create-server/", method = RequestMethod.GET)
    public ServerInfoRequest index() throws Exception {
        GameThread gameThread = new GameThread<>(gameClient.getServerIp(), this.jwtTokenUtil, Boolean.parseBoolean(production), new BasicGame());
        gameThread.setName("GameThread");
        gameThread.start();
        while (gameThread.server == null) {
            Thread.sleep(1);
        }
        ServerInfoRequest serverInfoRequest = gameThread.getServerInfo();
        System.out.println(serverInfoRequest);
        return serverInfoRequest;
    }

//    /**
//     * @param cnt - count of simulations to be run
//     * @return TestGameService.GameEndResults
//     * @implNote REMOVE WHEN GOING LIVE!
//     */
//    @RequestMapping(path = "/run-simulations/{cnt}", method = RequestMethod.GET)
//    public TestGameService.GameEndResults simulate(@PathVariable("cnt") int cnt) throws Exception {
//        return testGameService.createGameThreads(cnt);
//    }
//
//    /**
//     * @param cnt - count of simulations to be run
//     * @return List<TestGameService.GameEndResults>
//     * @implNote REMOVE WHEN GOING LIVE!
//     */
//    @RequestMapping(path = "/run-all-simulations/{cnt}", method = RequestMethod.GET)
//    public Map<Integer, Map<Integer, TestGameService.GameEndResults>> simulateAll(@PathVariable("cnt") int cnt) throws Exception {
//        return testGameService.runForAllStatLevels(cnt);
//    }

    @RequestMapping(path = "/get-instance/{gameId}", method = RequestMethod.GET)
    public ServerInfoRequest getInstance(@PathVariable("gameId") Long gameId) throws UnimplementedGameTypeException {
        return gameServerService.getInstance(gameId, null);
    }

    @RequestMapping(path = "/create-instance/{gameId}", method = RequestMethod.POST)
    public ServerInfoRequest createInstance(@PathVariable("gameId") Long gameId, @RequestBody GameCreationRequest request) throws UnimplementedGameTypeException {
        return gameServerService.getInstance(gameId, request);
    }
}
