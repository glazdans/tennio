# Tennio
Online tennis simulation game with character development features. 

## Dependencies (Required programms):
    Node.js
    Maven

## Starting node.js (client side):
    with command prompt navigate to "main-server/src/main/web"
    `npm install`
    `ng serve -dev -aot` (if it fails - install '@angular/cli' npm package globally)

## Servers:
    GameServer: for the game simulation;
    Main-server: for Non game simulation logic (not needed for game simulation)
