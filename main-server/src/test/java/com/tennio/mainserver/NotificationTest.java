package com.tennio.mainserver;

import com.tennio.mainserver.domain.Notification;
import com.tennio.mainserver.repository.NotificationRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.security.repository.UserRepository;
import org.aspectj.weaver.ast.Not;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by PC on 2016.08.31..
 */
public class NotificationTest extends WebAppConfigTest {

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void MapTest(){
        Notification notification = new Notification();
        Map<Notification.NotificationAttributeType,String> attributes = new HashMap<>();
        attributes.put(Notification.NotificationAttributeType.TrainingName,"TEST");
        notification.setNotificationAttributes(attributes);
        notification.setSticky(false);
        notification.setAdditionalInfo("TEST");
        notification.setNotificationType(Notification.NotificationType.Game);
        notification.setNotificationEventStart(new Date());
        User user = new User();
        user.setEnabled(true);
        user.setUsername("LOHS");
        user.setPassword("hurr");
        user.setFirstname("erwqe");
        user.setLastname("dwadsdw");
        user.setMoney(1000L);
        user.setBalls((short) 5);
        user.setLastPasswordResetDate(new Date());
        this.userRepository.save(user);
        notification.setUser(user);
        notification = this.notificationRepository.save(notification);
        new Boolean("");
    }
}
