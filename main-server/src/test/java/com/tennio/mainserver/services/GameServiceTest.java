package com.tennio.mainserver.services;


import com.tennio.common.GameType;
import com.tennio.common.requests.GameEndEvent;
import com.tennio.common.requests.Score;
import com.tennio.common.requests.ServerInfoRequest;
import com.tennio.mainserver.WebAppConfigTest;
import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.domain.Season;
import com.tennio.mainserver.domain.requests.UserRequest;
import com.tennio.mainserver.repository.GameEventRepository;
import com.tennio.mainserver.repository.PlayerRepository;
import com.tennio.mainserver.repository.SeasonRepository;
import com.tennio.mainserver.rest.Client;
import com.tennio.mainserver.service.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class GameServiceTest extends WebAppConfigTest{

    @Autowired
    private GameService gameService;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private UserService userService;

    @Autowired
    private GameEventRepository gameEventRepository;

    @Autowired
    private SeasonService seasonService;

    @Autowired
    private SeasonRepository seasonRepository;

    private ServerService serverService;
    private Client client;

    private ServerService oldServerService;
    private Client oldClient;

    @Before
    public void setUp(){
        gameEventRepository.deleteAll();
        playerRepository.deleteAll();

        UserRequest userRequest = new UserRequest();

        userRequest.setEmail("tenniotest1@inbox.lv"); // pw = lol123
        userRequest.setUsername("Gatis");
        userRequest.setPassword("lol123");
        userService.registerNewUser(userRequest);

        userRequest.setEmail("tenniotest2@inbox.lv"); // pw = lol123
        userRequest.setUsername("Georgs");
        userService.registerNewUser(userRequest);

        userRequest.setEmail("tenniotest3@inbox.lv"); // pw = lol123
        userRequest.setUsername("Pauls");
        userService.registerNewUser(userRequest);

        playerService.createPlayer("Gatis", "Gatis", "lv");
        playerService.createPlayer("Pauls", "Pauls", "lv");
        playerService.createPlayer("Georgs", "Georgs", "lv");

        Season activeSeason = seasonRepository.findByEndDateIsNull();
        if(activeSeason == null) {
            seasonService.startNewSeason();
        }

        serverService = Mockito.mock(ServerService.class);
        client = Mockito.mock(Client.class);
        oldServerService = (ServerService)ReflectionTestUtils.getField(gameService,"serverService");
        oldClient = (Client)ReflectionTestUtils.getField(gameService,"client");
        ReflectionTestUtils.setField(gameService, "serverService", serverService);
        ReflectionTestUtils.setField(gameService,"client",client);
        NotificationService notificationService = Mockito.mock(NotificationService.class);
        ReflectionTestUtils.setField(gameService,"notificationService",notificationService);
    }

    @After
    public void cleanup(){
        Mockito.reset(client, serverService);
        ReflectionTestUtils.setField(gameService, "serverService", oldServerService);
        ReflectionTestUtils.setField(gameService,"client",oldClient);
    }

    @Test
    public void addPlayerTest(){
        GameEvent gameEvent = gameService.createEvent(GameType.Matchmaking);
        List<Player> playerList = (List<Player>)playerRepository.findAll();
        gameEvent = gameService.addPlayer(gameEvent.getId(),playerList.get(0));
        assertEquals("Player1 should be the first player",playerList.get(0),gameEvent.getPlayer1());
        gameEvent = gameService.addPlayer(gameEvent.getId(), playerList.get(0));
        assertEquals("Player1 should be the first player",playerList.get(0),gameEvent.getPlayer1());
        assertNotEquals("Player2 should not be set if player1 == newPlayer",playerList.get(0),gameEvent.getPlayer2());
        gameEvent = gameService.addPlayer(gameEvent.getId(),playerList.get(1));
        assertEquals("Player1 should be the first player",playerList.get(0),gameEvent.getPlayer1());
        assertEquals("Player2 should be the second player",playerList.get(1),gameEvent.getPlayer2());
        gameEvent = gameService.addPlayer(gameEvent.getId(), playerList.get(2));
        assertEquals("Player1 should not change after adding different player",playerList.get(0),gameEvent.getPlayer1());
        assertEquals("Player2 should not change after adding different player",playerList.get(1),gameEvent.getPlayer2());
    }

    @Test
    public void startGameTest(){
        GameEvent gameEvent = gameService.createEvent(GameType.Matchmaking);
        List<Player> playerList = (List<Player>)playerRepository.findAll();
        ServerInfoRequest serverInfoRequest = gameService.initializeGame(gameEvent.getId(), false);
        assertNull("Should be null if there arent 2 players", serverInfoRequest);
        gameEvent = gameService.addPlayer(gameEvent.getId(),playerList.get(0));
        serverInfoRequest = gameService.initializeGame(gameEvent.getId(), false);
        assertNull("Should be null if there arent 2 players", serverInfoRequest);
        gameEvent = gameService.addPlayer(gameEvent.getId(),playerList.get(1));
        Mocks.mockClient(client
                ,gameEvent, Mocks.mockGameServerOnGameEvent(serverService,gameEvent));
        serverInfoRequest = gameService.initializeGame(gameEvent.getId(), false);
        gameEvent = gameEventRepository.findOne(gameEvent.getId());
        assertEquals("GameStatus did not change to started",gameEvent.getStatus(), GameEvent.GameStatus.STARTED);
    }

    @Test
    public void endGameTest(){
        GameEvent gameEvent = gameService.createEvent(GameType.Matchmaking);
        List<Player> playerList = (List<Player>)playerRepository.findAll();
        Player player1 = playerList.get(0);
        Player player2 = playerList.get(1);

        gameEvent = gameService.addPlayer(gameEvent.getId(),player1);
        gameEvent = gameService.addPlayer(gameEvent.getId(),player2);
        Mocks.mockClient(client
                , gameEvent, Mocks.mockGameServerOnGameEvent(serverService, gameEvent));
        gameService.initializeGame(gameEvent.getId(), false);
        GameEndEvent gameEndEvent = new GameEndEvent();
        gameEndEvent.setGameId(gameEvent.getId());

        HashMap<Long, Score> scores = new HashMap<Long, Score>() {{
            put(player1.getId(), new Score(0,0,2));
            put(player2.getId(), new Score(0, 0, 1));
        }};

        gameEndEvent.setScores(scores);
        gameEndEvent.setTime(0.1);
        gameService.endGame(gameEndEvent);
        gameEvent = gameEventRepository.findOne(gameEvent.getId());
        assertEquals("Game status should be ended after a game",gameEvent.getStatus(), GameEvent.GameStatus.ENDED);
        assertEquals("Score should be the same as end event",gameEvent.getPlayer1Score().getMatches(),
                gameEndEvent.getScores().get(player1.getId()).matches);
        assertEquals("Score should be the same as end event",gameEvent.getPlayer2Score().getMatches(),
                gameEndEvent.getScores().get(player2.getId()).matches);
    }


}
