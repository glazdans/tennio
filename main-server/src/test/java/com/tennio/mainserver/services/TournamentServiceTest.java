package com.tennio.mainserver.services;

import com.tennio.common.requests.GameEndEvent;
import com.tennio.common.requests.Score;
import com.tennio.mainserver.WebAppConfigTest;
import com.tennio.mainserver.domain.*;
import com.tennio.mainserver.domain.requests.UserRequest;
import com.tennio.mainserver.repository.GameEventRepository;
import com.tennio.mainserver.repository.PlayerRepository;
import com.tennio.mainserver.repository.SeasonRepository;
import com.tennio.mainserver.repository.TournamentRepository;
import com.tennio.mainserver.service.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TournamentServiceTest extends WebAppConfigTest{

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private GameEventRepository gameEventRepository;

    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private GameService gameService;

    @Autowired
    private UserService userService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private SeasonService seasonService;

    @Autowired
    private SeasonRepository seasonRepository;

    @Before
    public void setup(){
        tournamentRepository.deleteAll();
        gameEventRepository.deleteAll();
        playerRepository.deleteAll();

        UserRequest userRequest = new UserRequest();

        userRequest.setEmail("tenniotest1@inbox.lv"); // pw = lol123
        userRequest.setUsername("Gatis");
        userRequest.setPassword("lol123");
        userService.registerNewUser(userRequest);

        userRequest.setEmail("tenniotest2@inbox.lv"); // pw = lol123
        userRequest.setUsername("Georgs");
        userService.registerNewUser(userRequest);

        userRequest.setEmail("tenniotest3@inbox.lv"); // pw = lol123
        userRequest.setUsername("Pauls");
        userService.registerNewUser(userRequest);

        userRequest.setEmail("tenniotest4@inbox.lv");
        userRequest.setUsername("Jekabs");
        userService.registerNewUser(userRequest);

        userRequest.setEmail("tenniotest5@inbox.lv");
        userRequest.setUsername("Rihards");
        userService.registerNewUser(userRequest);

        playerService.createPlayer("Gatis", "Gatis", "lv");
        playerService.createPlayer("Pauls", "Pauls", "lv");
        playerService.createPlayer("Georgs", "Georgs", "lv");
        playerService.createPlayer("Jekabs", "Jekabs", "lv");
        playerService.createPlayer("Rihards", "Rihards", "lv");

        Season activeSeason = seasonRepository.findByEndDateIsNull();
        if(activeSeason == null) {
            seasonService.startNewSeason();
        }
    }

    @Test
    public void createTournamentTest(){
        String tournamentName = "Wimbladin Open";
        Long maxPlayers = 17L;
        int prizePool = 1000;
        Tournament tournament = tournamentService.createTournamentInstance(tournamentName, maxPlayers, prizePool, new Date(), new Date(), Tournament.TournamentType.NORMAL, 0);
        assertEquals("Name should be equal",tournamentName,tournament.getName());
        assertEquals("Max players should be equal",maxPlayers,tournament.getMaxPlayers());
        assertEquals("New tournamnet should be in register phase",
                tournament.getTournamentPhase(), Tournament.TournamentPhase.REGISTER);
        assertEquals("Tournament prize pool should be equal",
                tournament.getPrizePool(), prizePool);
    }

    @Test
    public void addPlayerTest(){
        String tournamentName = "Wimbladin Open";
        Long maxPlayers = 4L;
        int prizePool = 1000;
        List<Player> playerList = (List<Player>)playerRepository.findAll();
        Tournament tournament = tournamentService.createTournamentInstance(tournamentName, maxPlayers, prizePool, new Date(), new Date(), Tournament.TournamentType.NORMAL, 0);
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(0));
        assertEquals("After adding a person size should be 1",tournament.getPlayers().size(), 1);
        assertEquals(tournament.getPlayers().toArray()[0],playerList.get(0));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(0));
        assertEquals("Should not add a player more than once",tournament.getPlayers().size(),1);
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(1));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(2));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(3));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(4));
        assertEquals("Don't add if the tournament is full",tournament.getPlayers().size(),4);
        assertEquals(tournament.getPlayers().size(),tournament.getPlayerPositions().keySet().size());
    }

    @Test
    public void tournamentStartTest(){
        String tournamentName = "Wimbladin Open";
        Long maxPlayers = 5L;
        int prizePool = 1000;
        List<Player> playerList = (List<Player>)playerRepository.findAll();
        Tournament tournament = tournamentService.createTournamentInstance(tournamentName, maxPlayers, prizePool, new Date(), new Date(), Tournament.TournamentType.NORMAL, 0);
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(0));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(1));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(2));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(3));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(4));
        tournament = tournamentService.startTournament(tournament.getId());
        assertEquals("Tournament phase should be started",
                tournament.getTournamentPhase(), Tournament.TournamentPhase.START);
        assertEquals("Each player should be given unique positions",
                new HashSet<>(tournament.getPlayerPositions().values()).size(),5);
    }

    @Test
    public void getTournamentOpponentsAndGameTest(){
        String tournamentName = "Wimbladin Open";
        Long maxPlayers = 5L;
        int prizePool = 1000;
        List<Player> playerList = playerRepository.findAll();
        Tournament tournament = tournamentService.createTournamentInstance(tournamentName, maxPlayers,prizePool, new Date(), new Date(), Tournament.TournamentType.NORMAL, 0);
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(0));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(1));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(2));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(3));
        tournament = tournamentService.addPlayerToTournament(tournament.getId(),playerList.get(4));

        tournament = tournamentService.startTournament(tournament.getId());

        Player lastPlace = tournament.getPlayerPositions().entrySet().stream()
                .filter(entry -> entry.getValue() == 5L).findAny().get().getKey();
        Player middlePlace = tournament.getPlayerPositions().entrySet().stream()
                .filter(entry -> entry.getValue() == 3L).findAny().get().getKey();
        Player firstPlace = tournament.getPlayerPositions().entrySet().stream()
                .filter(entry -> entry.getValue() == 1L).findAny().get().getKey();

        List<Player> players = tournamentService.getTournamentOpponents(tournament.getId(),lastPlace);

        //assertTrue("Should be in challenge list",players.contains(middlePlace)); TODO
        assertTrue("Should not be in challenge list",!players.contains(firstPlace));
        assertTrue("Can't challenge yourself",!players.contains(lastPlace));

        //ADD game
        // TODO TEST POSITION CHECK WHEN DONE
        GameEvent gameEvent =
                tournamentService.addTournamentGame(tournament.getId(), lastPlace, middlePlace, new Date());
        assertEquals(gameEvent.getPlayer1(),lastPlace);
        assertEquals(gameEvent.getPlayer2(),middlePlace);
        assertTrue(gameEventRepository.findActiveGamesByPlayerAndTournament(lastPlace, tournament).contains(gameEvent));

        GameEndEvent gameEndEvent = new GameEndEvent();

        HashMap<Long, Score> scores = new HashMap<Long, Score>() {{
            put(gameEvent.getPlayer2().getId(), new Score(0,0,2));
            put(gameEvent.getPlayer1().getId(), new Score(0, 0, 1));
        }};

        gameEndEvent.setScores(scores);

        gameEndEvent.setGameId(gameEvent.getId());
        ServerService serverService = (ServerService)ReflectionTestUtils.getField(gameService, "serverService");
        ReflectionTestUtils.setField(gameService,"serverService",mock(ServerService.class));
        // Calls on tournament game finish
        gameService.endGame(gameEndEvent);
        tournament = tournamentRepository.findOne(tournament.getId());
        assertTrue("Should be last place after loss vs last place",
                tournament.getPlayerPositions().get(middlePlace) == 5L);
        assertTrue("Should be last place after loss vs last place",
                tournament.getPlayerPositions().get(lastPlace) == 3L);

        ReflectionTestUtils.setField(gameService, "serverService", serverService);
    }
}
