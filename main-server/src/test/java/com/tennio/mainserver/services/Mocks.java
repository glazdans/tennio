package com.tennio.mainserver.services;

import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.GameServer;
import com.tennio.common.requests.ServerInfoRequest;
import com.tennio.mainserver.domain.requests.GameCreationRequest;
import com.tennio.mainserver.rest.Client;
import com.tennio.mainserver.service.ServerService;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

import static org.mockito.Mockito.when;

public class Mocks {
    public static GameServer mockGameServerOnGameEvent(ServerService serverService,GameEvent gameEvent){
        GameServer gameServer = new GameServer();
        gameServer.setIp("tennio.com");
        gameServer.setLastResponse(new Date());
        gameServer.setServerStatus(GameServer.ServerStatus.AVAILABLE);
        gameServer.setCurrentGames(new HashSet<>(Arrays.asList(gameEvent)));
        when(serverService.getGameServer(gameEvent)).thenReturn(gameServer);
        return gameServer;
    }

    public static void mockClient(Client client,GameEvent gameEvent, GameServer gameServer){
        GameCreationRequest request = new GameCreationRequest();
        request.setPlayerBlue(gameEvent.getPlayer1());
        request.setPlayerRed(gameEvent.getPlayer2());

        ServerInfoRequest event = new ServerInfoRequest();
        event.setIp("tennio.com");
        event.setPort("8081");
        when(client.getGameServer(gameEvent.getId(), gameServer.getIp(), request)).thenReturn(event);
    }
}
