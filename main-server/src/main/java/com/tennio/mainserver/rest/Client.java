package com.tennio.mainserver.rest;

import com.tennio.common.requests.ServerInfoRequest;
import com.tennio.mainserver.domain.requests.GameCreationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

@Component
public class Client {
    private RestTemplate restTemplate;
    private String schema;

    @PostConstruct
    public void init(){
        restTemplate = new RestTemplate();
    }

    @Autowired
    public Client(@Value("${tennio.production") String production) {
        schema = Boolean.parseBoolean(production) ? "https://" : "http://";
    }

    public ServerInfoRequest getGameServer(Long gameId, String ip){
        return restTemplate.getForObject(schema + ip + ":8081/get-instance/" + gameId ,ServerInfoRequest.class);
    }

    public ServerInfoRequest getGameServer(Long gameId, String ip, GameCreationRequest gameCreationRequest){
        return restTemplate.postForObject(schema + ip + ":8081/create-instance/" + gameId, gameCreationRequest, ServerInfoRequest.class);
    }
}
