package com.tennio.mainserver.service;

import com.tennio.mainserver.TennioException;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.domain.PlayerSeason;
import com.tennio.mainserver.domain.Season;
import com.tennio.mainserver.repository.PlayerSeasonRepository;
import com.tennio.mainserver.repository.SeasonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class PlayerSeasonService {

    @Autowired
    private PlayerSeasonRepository playerSeasonRepository;

    @Autowired
    private SeasonRepository seasonRepository;

    @Autowired
    private NotificationService notificationService;

    public PlayerSeason getOrCreatePlayerSeason(Player player) {
        Season activeSeason = seasonRepository.findByEndDateIsNull();
        if (activeSeason == null) {
            throw new TennioException("No active season, can't calculate ELO");
        }
        PlayerSeason playerSeason = playerSeasonRepository.findBySeasonAndPlayer(activeSeason, player);
        if (playerSeason == null) {
            playerSeason = new PlayerSeason();
            player.getId();
            playerSeason.setPlayer(player);
            playerSeason.setSeason(activeSeason);
            playerSeason.setElo(1500L);
            playerSeasonRepository.save(playerSeason);
        }
        return playerSeason;
    }

    public void addElo(Player winner, int add) {
        PlayerSeason winnerPlayerSeason = this.getOrCreatePlayerSeason(winner);
        winnerPlayerSeason.setElo(winnerPlayerSeason.getElo() + add);
        playerSeasonRepository.save(winnerPlayerSeason);
        notificationService.sendEloUpdate(Arrays.asList(winner));
    }

    public void updateElo(Player winner, Player loser) {
        PlayerSeason winnerPlayerSeason = this.getOrCreatePlayerSeason(winner);
        PlayerSeason loserPlayerSeason = this.getOrCreatePlayerSeason(loser);
        Long eloDifference = getEloDifference(winnerPlayerSeason.getElo(), loserPlayerSeason.getElo());

        winnerPlayerSeason.setElo(winnerPlayerSeason.getElo() + eloDifference);
        loserPlayerSeason.setElo(loserPlayerSeason.getElo() - eloDifference);
        playerSeasonRepository.save(winnerPlayerSeason);
        playerSeasonRepository.save(loserPlayerSeason);

        notificationService.sendEloUpdate(Arrays.asList(winner, loser));
    }

    private static final long MAX_ELO_GAIN = 15;
    private static final long MAX_ELO_DIFFERENCE = 100;
    private static final long MIN_ELO_DIFFERENCE = 10;

    public Long getEloDifference(Long winnerElo, Long loserElo) {
        long difference = winnerElo - loserElo;

        long kFactor = 32;
        long avgElo = (winnerElo + loserElo) / 2;

        if (avgElo >= 2100 && avgElo <= 2400) {
            kFactor = 24;
        }

        if (avgElo > 2400) {
            kFactor = 16;
        }

        difference = difference > MAX_ELO_DIFFERENCE ? MAX_ELO_DIFFERENCE : difference;
        difference = difference < MAX_ELO_DIFFERENCE ? MIN_ELO_DIFFERENCE : difference;

        double expectedScoreWinner = 1 / (1 + Math.pow(10, difference / 400));
        long eloDiff = (long) Math.ceil(kFactor * (1 - expectedScoreWinner));

        return eloDiff > MAX_ELO_GAIN ? MAX_ELO_GAIN : eloDiff;
    }
}
