package com.tennio.mainserver.service;

import com.tennio.mainserver.domain.requests.UserRequest;
import com.tennio.mainserver.domain.requests.UserUpdateRequest;
import com.tennio.mainserver.security.model.Authority;
import com.tennio.mainserver.security.model.AuthorityName;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.security.repository.AuthorityRepository;
import com.tennio.mainserver.security.repository.UserRepository;
import com.tennio.mainserver.service.exception.AlreadyExistsException;
import com.tennio.mainserver.service.exception.GenericErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

@Service
public class UserService extends TService<User> {
    @Autowired
    protected UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private EmailConfirmationService confirmationService;

    @Autowired
    private CountryService countryService;

    private static Authority userAuthority;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.repository = userRepository;
    }

    @PostConstruct
    public void init() {
        userAuthority = authorityRepository.findByName(AuthorityName.ROLE_USER);
    }

    public boolean registerNewUser(UserRequest userRequest) throws AlreadyExistsException {
        if (userRepository.countByUsername(userRequest.getUsername()) > 0) {
            throw new AlreadyExistsException("Specified user name is already taken!");
        }
        if (userRepository.countByEmail(userRequest.getEmail()) > 0 || confirmationService.doesValidationForEmailExist(userRequest.getEmail())) {
            throw new AlreadyExistsException("Specified email is already taken!");
        }
        User user = new User();
        user.setEnabled(true);

        user.setFirstname(userRequest.getFirstName());
        user.setLastname(userRequest.getLastName());
        user.setUsername(userRequest.getUsername());
        user.setCountry(countryService.getCountryByCode(userRequest.getCountryCode()));

        user.setPassword(this.passwordEncoder.encode(userRequest.getPassword()));
        user.setAuthorities(Arrays.asList(userAuthority));

        user.setMoney(1000L);

        user.setLastPasswordResetDate(new Date());

        repository.save(user);
        confirmationService.sendNewEmailAddressConfirmationRequest(user, userRequest.getEmail());
        return true;
    }

    @CachePut(cacheNames = {"usersByUsername"}, key = "#result.getUsername()")
    public User updateUser(User user, UserUpdateRequest userUpdateRequest) throws GenericErrorException {
        if (!BCrypt.checkpw(userUpdateRequest.getPassword(), user.getPassword())) {
            throw new GenericErrorException("Wrong password!");
        }

        if (!Objects.equals(userUpdateRequest.getNewPassword(), "")) {
            if (!Objects.equals(userUpdateRequest.getRnewPassword(), userUpdateRequest.getNewPassword())) {
                throw new GenericErrorException("Passwords don't match!");
            } else {
                user.setPassword(this.passwordEncoder.encode(userUpdateRequest.getNewPassword()));
            }
        }

        if (!Objects.equals(userUpdateRequest.getEmail(), "") && !Objects.equals(userUpdateRequest.getEmail(), user.getEmail())) {
            if (userRepository.countByEmail(userUpdateRequest.getEmail()) > 0) {
                throw new AlreadyExistsException("Specified email is already taken!");
            } else {
                confirmationService.sendChangedEmailAddressConfirmationRequest(user, userUpdateRequest.getEmail());
            }
        }

        if (!Objects.equals(userUpdateRequest.getFirstName(), "") && !Objects.equals(userUpdateRequest.getFirstName(), user.getFirstname())) {
            user.setFirstname(userUpdateRequest.getFirstName());
        }

        if (!Objects.equals(userUpdateRequest.getLastName(), "") && !Objects.equals(userUpdateRequest.getLastName(), user.getLastname())) {
            user.setLastname(userUpdateRequest.getLastName());
        }

        if (!Objects.equals(userUpdateRequest.getCountryCode(), "") && !Objects.equals(userUpdateRequest.getCountryCode(), user.getCountry().getCode())) {
            user.setCountry(countryService.getCountryByCode(userUpdateRequest.getCountryCode()));
        }

        try {
            userRepository.save(user);
        } catch (ConstraintViolationException e) {
            e.printStackTrace();
            System.out.println(e.getConstraintViolations());
            throw new GenericErrorException("Something went wrong!");
        }

        return user;
    }

    @CachePut(cacheNames = {"usersByEmail"}, key = "#result.getEmail()")
    public User handlePasswordChangeRequest(String email, String key, String newPassword) {
        if (!confirmationService.validateAndDeleteConfirmation(email, key)) {
            return null;
        }

        User user = userRepository.findByEmail(email);
        if (user == null) {
            return null;
        }

        user.setLastPasswordResetDate(new Date());
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);
        return user;
    }

    public void handlePasswordResetRequest(String email) {
        if (email.length() < 1) {
            return;
        }
        User user = userRepository.findByEmail(email);
        if (user == null) {
            return;
        }
        confirmationService.sendPasswordResetConfirmationRequest(user);
    }
}
