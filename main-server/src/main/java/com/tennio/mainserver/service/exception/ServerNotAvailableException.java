package com.tennio.mainserver.service.exception;

import com.tennio.mainserver.TennioException;

public class ServerNotAvailableException extends TennioException {
    public ServerNotAvailableException(String message) {
        super(message);
    }

    public ServerNotAvailableException() {
    }
}
