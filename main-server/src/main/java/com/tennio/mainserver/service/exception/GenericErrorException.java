package com.tennio.mainserver.service.exception;

import com.tennio.mainserver.TennioException;

public class GenericErrorException extends TennioException {

    public GenericErrorException(String message) {
        super(message);
    }

    public GenericErrorException() {
    }
}
