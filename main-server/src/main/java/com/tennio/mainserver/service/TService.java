package com.tennio.mainserver.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract class TService<T> {
    protected JpaRepository<T, Long> repository;

    public Page<T> findPaginated(int page, int size) {
        return repository.findAll(new PageRequest(page,size));
    }
}
