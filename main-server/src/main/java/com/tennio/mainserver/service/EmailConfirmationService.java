package com.tennio.mainserver.service;

import com.google.common.collect.Lists;
import com.tennio.mainserver.domain.EmailConfirmation;
import com.tennio.mainserver.repository.EmailConfirmationRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.security.repository.UserRepository;
import com.tennio.mainserver.service.exception.GenericErrorException;
import it.ozimov.springboot.mail.model.defaultimpl.DefaultEmail;
import it.ozimov.springboot.mail.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.Date;
import java.util.List;

@Service
public class EmailConfirmationService {
    @Autowired
    private EmailConfirmationRepository confirmationRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserRepository userRepository;

    private String generateConfirmationString() {
        return java.util.UUID.randomUUID().toString();
    }

    /* for newly registered emails */
    public void sendNewEmailAddressConfirmationRequest(User user, String newEmail) {
        EmailConfirmation confirmation = createAndSave(EmailConfirmation.ConfirmationType.NEW_EMAIL, newEmail, user);
        DefaultEmail email;

        try {
             email = DefaultEmail.builder()
                    .from(new InternetAddress("noreply@tennio.com"))
                    .to(Lists.newArrayList(new InternetAddress(newEmail)))
                    .subject("Tennio registration confirmation")
                    .body("You have successfully registered on tennio.com, to confirm your email address please open this link: " +
                            "https://tennio.com/confirm?email=" + newEmail + "&key=" + confirmation.getConfirmationString())
                    .encoding("UTF-8").build();
        } catch (AddressException e) {
            e.printStackTrace();
            return;
        }

        emailService.send(email);
    }

    /* used when changing email */
    public void sendChangedEmailAddressConfirmationRequest(User user, String newEmail) {
        EmailConfirmation confirmation = createAndSave(EmailConfirmation.ConfirmationType.CHANGED_EMAIL, newEmail, user);
        DefaultEmail email;

        try {
            email = DefaultEmail.builder()
                    .from(new InternetAddress("noreply@tennio.com"))
                    .to(Lists.newArrayList(new InternetAddress(newEmail)))
                    .subject("Tennio email change confirmation")
                    .body("You have requested to change your email address to " + newEmail + " at tennio.com to confirm please open this link: " +
                            "https://tennio.com/confirm?email=" + newEmail + "&key=" + confirmation.getConfirmationString())
                    .encoding("UTF-8").build();
        } catch (AddressException e) {
            e.printStackTrace();
            return;
        }

        emailService.send(email);
    }

    public void sendPasswordResetConfirmationRequest(User user){
        EmailConfirmation confirmation = createAndSave(EmailConfirmation.ConfirmationType.PASSWORD_RESET, user.getEmail(), user);
        DefaultEmail email;

        try {
            email = DefaultEmail.builder()
                    .from(new InternetAddress("noreply@tennio.com"))
                    .to(Lists.newArrayList(new InternetAddress(user.getEmail())))
                    .subject("Tennio password reset confirmation")
                    .body("You have requested to reset your password at tennio.com for username: " + user.getUsername() + " to change your password open this link: " +
                            "https://tennio.com/confirm?email=" + user.getEmail() + "&key=" + confirmation.getConfirmationString() + "&changepw=true")
                    .encoding("UTF-8").build();
        } catch (AddressException e) {
            e.printStackTrace();
            return;
        }

        emailService.send(email);
    }

    private EmailConfirmation createAndSave(EmailConfirmation.ConfirmationType type, String email, User user){
        EmailConfirmation confirmation = new EmailConfirmation();
        confirmation.setType(type);
        confirmation.setEmail(email);
        confirmation.setUser(user);
        confirmation.setConfirmationString(generateConfirmationString());
        confirmation.setWhenSent(new Date());

        confirmationRepository.save(confirmation);
        return confirmation;
    }

    public boolean handleResendRequest(User user) throws GenericErrorException {
        List<EmailConfirmation> confirmations = confirmationRepository.findByUser(user);
        if(confirmations.size() == 0) {
            throw new GenericErrorException("An error occured!");
        }

        EmailConfirmation confirmation = confirmations.get(confirmations.size() - 1);
        switch(confirmation.getType()) {
            case CHANGED_EMAIL:
                this.sendChangedEmailAddressConfirmationRequest(user, confirmation.getEmail());
                break;
            case NEW_EMAIL:
                this.sendNewEmailAddressConfirmationRequest(user, confirmation.getEmail());
                break;
        }
        confirmationRepository.delete(confirmations);
        return true;
    }

    public boolean handleConfirmationAttempt(String email, String confirmationString) throws GenericErrorException {
        EmailConfirmation confirmation = confirmationRepository.findByEmailAndConfirmationString(email, confirmationString);
        if(confirmation == null) {
            throw new GenericErrorException("Invalid confirmation link!");
        }

        // if email was sent more than 24hrs ago = GTFO
        if(new Date().getTime() - confirmation.getWhenSent().getTime() >= 864e5) {
            confirmationRepository.delete(confirmation);
            throw new GenericErrorException("Confirmation link has expired!");
        }

        User user = confirmation.getUser();

        if(confirmation.getType() == EmailConfirmation.ConfirmationType.PASSWORD_RESET){
            return true;
        } else {
            user.setEmail(email);
            userRepository.save(user);
            confirmationRepository.delete(confirmation);
            return true;
        }
    }

    public boolean doesValidationForEmailExist(String email) {
        return confirmationRepository.countByEmail(email) > 0;
    }

    public boolean validateAndDeleteConfirmation(String email, String confirmationString) {
        EmailConfirmation confirmation = confirmationRepository.findByEmailAndConfirmationString(email, confirmationString);
        if(confirmation != null) {
            confirmationRepository.delete(confirmation);
            return true;
        } else {
            return false;
        }
    }
}
