package com.tennio.mainserver.service;

import com.tennio.common.GameType;
import com.tennio.common.requests.GameEndEvent;
import com.tennio.common.requests.Score;
import com.tennio.common.requests.ServerInfoRequest;
import com.tennio.mainserver.TennioException;
import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.GameServer;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.domain.requests.GameCreationRequest;
import com.tennio.mainserver.repository.GameEventRepository;
import com.tennio.mainserver.rest.Client;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.service.exception.GenericErrorException;
import com.tennio.mainserver.service.exception.ServerNotAvailableException;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

@Service
public class GameService {
    @Autowired
    private Client client;

    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private ServerService serverService;

    @Autowired
    private PlayerSeasonService playerSeasonService;

    @Autowired
    private StatLevelService statLevelService;

    @Autowired
    private AchievementProgressService achievementProgressService;

    @Autowired
    private ActivityService activityService;

    @PostConstruct
    public void init() {
    }

    public void deleteEvent(GameEvent event) {
        serverService.removeGameEvent(event);
    }

    public GameEvent createMatchmakingGame(Player player1, Player player2) {
        GameEvent gameEvent = createEvent(GameType.Matchmaking);
        addPlayer(gameEvent.getId(), player1);
        return addPlayer(gameEvent.getId(), player2);
    }

    public GameEvent createEvent(GameType gameType) {
        GameEvent gameEvent = new GameEvent();
        gameEvent.setStatus(GameEvent.GameStatus.WAITING);
        gameEvent.setGameType(gameType);
        gameEvent.setGameStart(new Date());
        activityService.getGameEventRepository().save(gameEvent);
        return gameEvent;
    }

    public List<GameEvent> getAllEvents() {
        return activityService.getGameEventRepository().findByStatusNot(GameEvent.GameStatus.ENDED);
    }

    public List<GameEvent> getPlayerActiveGames(Player player) {
        return activityService.getGameEventRepository().findActiveGamesByPlayer(player);
    }

    public GameEvent addPlayer(Long eventId, Player player) {
        GameEvent gameEvent = activityService.getGameEventRepository().findOne(eventId);
        Hibernate.initialize(gameEvent);
        if (gameEvent.getPlayer1() == null) {
            gameEvent.setPlayer1(player);
            activityService.getGameEventRepository().save(gameEvent);
            return gameEvent;
        } else if (gameEvent.getPlayer2() == null && !player.equals(gameEvent.getPlayer1())) {
            gameEvent.setPlayer2(player);
            activityService.getGameEventRepository().save(gameEvent);
            return gameEvent;
        }
        gameEvent.getPlayer1().getId();
        if (gameEvent.getPlayer2() != null) {
            gameEvent.getPlayer2().getId();
        }
        return gameEvent;
    }

    public ServerInfoRequest newAIGame(User user) {
        GameEvent event = createEvent(GameType.AI);
        event.setPlayer1(user.getActivePlayer());
        GameEventRepository gameEventRepository = activityService.getGameEventRepository();
        gameEventRepository.save(event);
        return this.initializeGame(event.getId(), true);
    }

    public ServerInfoRequest initializeGame(Long gameId, boolean singlePlayer) throws GenericErrorException, RestClientException, ServerNotAvailableException {
        GameEvent gameEvent = activityService.getGameEventRepository().findOne(gameId);

        if (gameEvent == null) {
            throw new GenericErrorException("Game with that id does not exist!");
        }

        if (GameEvent.GameStatus.STARTED.equals(gameEvent.getStatus())) {
            GameServer gameServer = serverService.getGameServer(gameEvent);
            return client.getGameServer(gameId, gameServer.getIp());
        }

        if (!singlePlayer && gameEvent.getPlayer1() != null) {
            if (gameEvent.getPlayer1() == null || gameEvent.getPlayer2() == null) {
                throw new GenericErrorException("Not enough registered players!");
            }
        }
        if (GameEvent.GameStatus.ENDED.equals(gameEvent.getStatus())) {
            throw new GenericErrorException("Game has already ended!");
        }

        User player1User = gameEvent.getPlayer1().getUser();

        User player2User = null;
        if (!singlePlayer) {
            player2User = gameEvent.getPlayer2().getUser();
        }

        if (gameEvent.getGameStart() != null && gameEvent.getGameStart().after(new Date())) {
            throw new GenericErrorException("Game is not ready yet!");
        }
        if (player1User.getBalls() < 1) {
            throw new GenericErrorException(player1User.getUsername() + " doesn't have enough balls!");
        }

        gameEvent.setStatus(GameEvent.GameStatus.STARTED);
        activityService.getGameEventRepository().save(gameEvent);

        GameCreationRequest request = new GameCreationRequest();

        GameServer gameServer;

        try {
            gameServer = serverService.getGameServer(gameEvent);
        } catch (ServerNotAvailableException e) {
            this.deleteEvent(gameEvent);
            throw new ServerNotAvailableException("Server not available");
        }

        request.setPlayerRed(new com.tennio.mainserver.domain.requests.Player(statLevelService.setStatLVLs(gameEvent.getPlayer1()), player1User.getUsername()));
        if (player2User != null) {
            request.setPlayerBlue(new com.tennio.mainserver.domain.requests.Player(statLevelService.setStatLVLs(gameEvent.getPlayer2()), player2User.getUsername()));
        }

        request.setGameType(gameEvent.getGameType());
        ServerInfoRequest serverInfoRequest;
        try {
            serverInfoRequest = client.getGameServer(gameId, gameServer.getIp(), request);
        } catch (ResourceAccessException e) {
            serverService.removeGameEvent(gameEvent);
            throw new ServerNotAvailableException("Server not available");
        }

        serverInfoRequest.setGameId(gameId);
        activityService.decrementBalls(player1User);

        return serverInfoRequest;
    }

    public ServerInfoRequest initializeGame(GameEvent gameEvent, GameServer gameServer) {
        User player1User = gameEvent.getPlayer1().getUser();
        User player2User = gameEvent.getPlayer2().getUser();

        gameEvent.setStatus(GameEvent.GameStatus.STARTED);
        activityService.getGameEventRepository().save(gameEvent);

        activityService.decrementBalls(player1User);

        GameCreationRequest request = new GameCreationRequest();

        request.setPlayerRed(new com.tennio.mainserver.domain.requests.Player(statLevelService.setStatLVLs(gameEvent.getPlayer1()), player1User.getUsername()));
        request.setPlayerBlue(new com.tennio.mainserver.domain.requests.Player(statLevelService.setStatLVLs(gameEvent.getPlayer2()), player2User.getUsername()));

        return client.getGameServer(gameEvent.getId(), gameServer.getIp(), request);
    }

    public void endGame(GameEndEvent event) {
        GameEvent gameEvent = activityService.getGameEventRepository().findOne(event.getGameId());

        if (event.getGameEndCause().equals(GameEndEvent.GameEndCause.PlayerSurrender)) {
            GameEvent.Score loserScore = new GameEvent.Score(0, 0, 0);
            GameEvent.Score winnerScore = new GameEvent.Score(0, 0, 1);

            if (gameEvent.getPlayer1().getId().equals(event.getGameEndCauserId())) {
                gameEvent.setStatus(GameEvent.GameStatus.PLAYER1_SURRENDERED);
                gameEvent.setPlayer1Score(loserScore);
                gameEvent.setPlayer2Score(winnerScore);
            } else if (gameEvent.getPlayer2().getId().equals(event.getGameEndCauserId())) {
                gameEvent.setStatus(GameEvent.GameStatus.PLAYER2_SURRENDERED);
                gameEvent.setPlayer1Score(winnerScore);
                gameEvent.setPlayer2Score(loserScore);
            } else {
                throw new TennioException("Invalid GameEndEvent state");
            }

            activityService.getNotificationService().onGameSurrender(gameEvent);
            activityService.getGameEventRepository().save(gameEvent);
        }

        if (event.getGameEndCause().equals(GameEndEvent.GameEndCause.PlayerWon)) {
            gameEvent.setPlayer1Score(commonScoreToEntity(event.getScores().get(gameEvent.getPlayer1().getId())));
            if (!gameEvent.getGameType().equals(GameType.AI)) {
                gameEvent.setPlayer2Score(commonScoreToEntity(event.getScores().get(gameEvent.getPlayer2().getId())));
            } else {
                event.getScores().remove(gameEvent.getPlayer1().getId());
                gameEvent.setPlayer2Score(commonScoreToEntity(event.getScores().entrySet().iterator().next().getValue()));
            }
            gameEvent.setStatus(GameEvent.GameStatus.ENDED);
            activityService.getGameEventRepository().save(gameEvent);

            if (gameEvent.getGameType() != GameType.AI) {
                if (gameEvent.getPlayer1Score().getMatches() > gameEvent.getPlayer2Score().getMatches()) {
                    activityService.addMoney(gameEvent.getPlayer1(), gameEvent.getPlayer2(), 100L, 10L);
                    playerSeasonService.updateElo(gameEvent.getPlayer1(), gameEvent.getPlayer2());
                    achievementProgressService.onGameEnd(gameEvent);
                } else {
                    activityService.addMoney(gameEvent.getPlayer1(), gameEvent.getPlayer2(), 100L, 10L);
                    achievementProgressService.onGameEnd(gameEvent);
                    playerSeasonService.updateElo(gameEvent.getPlayer2(), gameEvent.getPlayer1());
                }
                if (gameEvent.getGameType() != GameType.Tournament) {
                    activityService.getNotificationService().onGameComplete(gameEvent);
                }
            } else {
                activityService.getNotificationService().onAIGameComplete(gameEvent);
            }
        }

        if (gameEvent.getGameType() == GameType.Tournament || gameEvent.getGameType() == GameType.TournamentQuickFire) {
            tournamentService.onTournamentGameFinish(gameEvent.getId());
        }

        serverService.removeGameEventFromServer(gameEvent);
    }

    public GameEvent.Score commonScoreToEntity(Score score) {
        GameEvent.Score newScore = new GameEvent.Score();
        newScore.setGames(score.games);
        newScore.setMatches(score.matches);
        newScore.setSets(score.sets);

        return newScore;
    }

    public GameEvent createTournamentGame(Player player, Player opponentPlayer, Long tournamentId) {
        return this.tournamentService.addTournamentGame(tournamentId, player, opponentPlayer, new Date());
    }
}
