package com.tennio.mainserver.service;

import com.tennio.mainserver.TennioException;
import com.tennio.mainserver.domain.Season;
import com.tennio.mainserver.repository.SeasonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SeasonService {
    @Autowired
    private SeasonRepository seasonRepository;

    public Season startNewSeason() {
            Season season = new Season();
            season.setStartDate(new Date());
            seasonRepository.save(season);
            return season;
    }

    /**
     * This method shouldn't be used without starting also a new season
     *
     * @param seasonId
     */
    private void endSeason(Long seasonId) {
        Season season = seasonRepository.findOne(seasonId);
        if (season.getEndDate() == null) {
            season.setEndDate(new Date());
            seasonRepository.save(season);
        } else {
            throw new TennioException("Season is already ended");
        }
    }

    @Scheduled(cron = "0 0 0 1 */3 ?")
    @CacheEvict(cacheNames = {"activeSeason"}, allEntries = true)
    public void seasonSchedule() {
        Season activeSeason = seasonRepository.findByEndDateIsNull();
        if (activeSeason == null) {
            this.startNewSeason();
        } else {
            this.endSeason(activeSeason.getId());
            this.startNewSeason();
        }
    }
}

