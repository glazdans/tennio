package com.tennio.mainserver.service;

import com.tennio.mainserver.TennioException;
import com.tennio.mainserver.domain.*;
import com.tennio.mainserver.repository.PlayerRepository;
import com.tennio.mainserver.repository.TrainingEventsRepository;
import com.tennio.mainserver.repository.TrainingRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.service.exception.GenericErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class TrainingsService {
    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private TrainingEventsRepository trainingEventsRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private SchedulerService schedulerService;

    public static final String TRAININGS_KEY = "trainings";

    @CacheEvict(cacheNames = {"allTrainings"}, allEntries = true)
    @CachePut(cacheNames = {"trainingsById"}, key = "#training.getId()")
    public Training createTraining(Training training){
        if (trainingRepository.countByCode(training.getCode()) > 0) {
            throw new TennioException("Duplicate training code found");
        }
        trainingRepository.save(training);
        return training;
    }

    @CachePut(cacheNames = {"trainingsById"}, key = "#training.getId()")
    public Training updateTraining(Training training){
        if(training.getId() == null){
            throw new TennioException("Training id is null");
        }
        trainingRepository.save(training);
        return training;
    }

    @CacheEvict(cacheNames = {"trainingsById"}, key = "#id")
    public void deleteTraining(Long id){
        if(id == null){
            throw new TennioException("Training id is null");
        }
        trainingRepository.delete(id);
    }

    public TrainingEvent findEvent(Long id){
        return trainingEventsRepository.findOne(id);
    }

    @Cacheable(cacheNames = {"allTrainings"}, key = "#root.target.TRAININGS_KEY")
    public List<Training> getAllTrainings(){
        Iterable<Training> trainings = trainingRepository.findAll();
        List<Training> trainingList = new ArrayList<>();
        for(Training t : trainings){
            trainingList.add(t);
        }
        return trainingList;
    }

    public TrainingEvent newTrainingEvent(Long trainingId, String username) throws GenericErrorException {
        User user = activityService.getUserRepository().findByUsername(username);
        Player player = user.getActivePlayer();
        Training training = trainingRepository.findOne(trainingId);

        List<TrainingEvent> userEvents = getActiveEventsForActivePlayer(username);

        if(userEvents.size() != 0){
            throw new GenericErrorException("Active player already in training!");
        }

        if(Training.TrainingType.PAID.equals(training.getTrainingType()) ){
            if(user.getMoney() < training.getCost()){
                throw new GenericErrorException("Not enough money!");
            } else {
                activityService.addMoney(user, -training.getCost());
            }
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.SECOND,training.getLength().intValue());

        TrainingEvent event = new TrainingEvent(cal.getTime(),training,player);
        trainingEventsRepository.save(event);

        schedulerService.addSchedule(ScheduleEvent.EventType.TrainingEnd, event.getId(), event.getCompleteDate());

        return event;
    }

    public TrainingFinishEvent finishTraining(Long eventId, String username) throws GenericErrorException {
        User user = activityService.getUserRepository().findByUsername(username);

        TrainingEvent event = trainingEventsRepository.findOne(eventId);
        Player player = null;
        if (event != null) {
            player = user.getPlayerById(event.getPlayer().getId());
        }
        if (player == null) {
            throw new GenericErrorException("Couldn't find training event!");
        }
        player.getStats().addStats(addDelta(event));
        playerRepository.save(player);

        activityService.getNotificationService().deleteTrainingNotification(event);
        activityService.getNotificationService().sendStatUpdate(player);
        trainingEventsRepository.delete(event);

        return new TrainingFinishEvent(event.getTraining().getStatIncrease());
    }

    private Stats addDelta(TrainingEvent event){
        Stats increase = event.getTraining().getStatIncrease();
        float p = ThreadLocalRandom.current().nextInt(0, (int)event.getTraining().getExpDelta() + 1) / 100;
        increase.addFraction(p);
        return increase;
    }

    public List<TrainingEvent> getActiveEvents(String username){
        return trainingEventsRepository.findByPlayers(activityService.getUserRepository().findByUsername(username).getPlayers());
    }

    public List<TrainingEvent> getActiveEventsForActivePlayer(String username){
        return trainingEventsRepository.findByPlayers(Arrays.asList(activityService.getUserRepository().findByUsername(username).getActivePlayer()));
    }

    public void onTrainingComplete(Long trainingEventId){
        TrainingEvent trainingEvent = trainingEventsRepository.findOne(trainingEventId);
        if(trainingEvent == null){
            return;
        }

        activityService.getNotificationService().onTrainingComplete(trainingEvent);
    }
}
