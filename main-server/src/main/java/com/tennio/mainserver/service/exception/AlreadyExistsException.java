package com.tennio.mainserver.service.exception;

import com.tennio.mainserver.TennioException;

public class AlreadyExistsException extends TennioException {

    public AlreadyExistsException(String message) {
        super(message);
    }

    public AlreadyExistsException() {
    }
}
