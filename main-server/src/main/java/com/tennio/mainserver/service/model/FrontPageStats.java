package com.tennio.mainserver.service.model;

import com.tennio.mainserver.security.repository.UserRepository;

import java.io.Serializable;
import java.util.List;

public class FrontPageStats implements Serializable {
    public long registeredPlayers;
    public long currentlyOnline;
    //         Country, Player
    public List<UserRepository.CountryUserCount> playersByCountry;
}
