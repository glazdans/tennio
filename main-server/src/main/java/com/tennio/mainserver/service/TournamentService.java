package com.tennio.mainserver.service;

import com.tennio.common.GameType;
import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.domain.ScheduleEvent;
import com.tennio.mainserver.domain.Tournament;
import com.tennio.mainserver.repository.TournamentRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.service.exception.GenericErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Service
public class TournamentService {

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private GameService gameService;

    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private PlayerSeasonService playerSeasonService;

    @Autowired
    private MatchupService matchupService;

    @Autowired
    private AchievementProgressService achievementProgressService;

    private static final Long PLAYER_INTERVAL = 3L;

    private static final int CONCURRENT_QUICKFIRE_TOURNAMENT_COUNT = 1;

    @Value("${tennio.production}")
    private boolean production;
    public static final String TOURNAMENT_KEY = "tournaments";

    @Cacheable(cacheNames = {"allTournaments"}, key = "#root.target.TOURNAMENT_KEY")
    public List<Tournament> getAllTournaments() {
        List<Tournament> tournaments = tournamentRepository.findNormal();
        List<Tournament> latestQFTournaments = tournamentRepository.findActiveQFTournaments();
        if (latestQFTournaments != null) {
            tournaments.addAll(0, latestQFTournaments);
        }
        return latestQFTournaments;
    }

    @Cacheable(cacheNames = {"tournamentsById"}, key = "#id")
    public Tournament getTournament(Long id) {
        return tournamentRepository.findOne(id);
    }

    @CacheEvict(cacheNames = {"allTournaments"}, allEntries = true)
    @CachePut(cacheNames = {"tournamentsById"}, key = "#result.getId()")
    public Tournament createTournamentInstance(String name, Long maxPlayers, int prizePool, Date startDate, Date endDate, Tournament.TournamentType type, long entryFee) {
        Tournament tournament = new Tournament();
        tournament.setTournamentPhase(Tournament.TournamentPhase.REGISTER);
        if (maxPlayers != null && maxPlayers > 0L) {
            tournament.setMaxPlayers(maxPlayers);
        } else {
            tournament.setMaxPlayers(16L);
        }
        tournament.setName(name);
        tournament.setPrizePool(prizePool);
        tournament.setStartDate(startDate);
        tournament.setEndDate(endDate);
        tournament.setTournamentType(type);
        tournament.setEntryFee(entryFee);
        tournamentRepository.save(tournament);

        schedulerService.addSchedule(ScheduleEvent.EventType.TournamentStart, tournament.getId(), startDate);

        return tournament;
    }

    @CacheEvict(cacheNames = {"allTournaments"}, allEntries = true)
    @CachePut(cacheNames = {"tournamentsById"}, key = "#id")
    public Tournament updateTournament(Long id, String name, Long maxPlayers, int prizePool, Date startDate, Date endDate, long entryFee) {
        Tournament tournament = tournamentRepository.findOne(id);

        if (maxPlayers != null && maxPlayers > 0L) {
            tournament.setMaxPlayers(maxPlayers);
        } else {
            tournament.setMaxPlayers(16L);
        }

        tournament.setName(name);
        tournament.setPrizePool(prizePool);
        tournament.setEntryFee(entryFee);

        if (tournament.getStartDate().getTime() != startDate.getTime() && tournament.getTournamentPhase() == Tournament.TournamentPhase.REGISTER) {
            schedulerService.reSchedule(ScheduleEvent.EventType.TournamentStart, id, startDate);
            tournament.setStartDate(startDate);
        }

        if (tournament.getEndDate().getTime() != endDate.getTime() && tournament.getTournamentPhase() == Tournament.TournamentPhase.START) {
            schedulerService.reSchedule(ScheduleEvent.EventType.TournamentEnd, id, endDate);
        }

        tournament.setEndDate(endDate);

        tournamentRepository.save(tournament);

        return tournament;
    }

    @CachePut(cacheNames = {"tournamentsById"}, key = "#tournamentId")
    @CacheEvict(cacheNames = {"allTournaments"}, allEntries = true)
    public Tournament addPlayerToTournament(Long tournamentId, Player player) throws GenericErrorException {
        Tournament tournament = tournamentRepository.findOne(tournamentId);
        Set<Player> players = tournament.getPlayers();
        User user = player.getUser();

        if (tournament.getMaxPlayers() <= players.size()) {
            throw new GenericErrorException("Tournament is full!");
        }

        if (players.contains(player) || tournament.getPlayerPositions().keySet().contains(player)) {
            throw new GenericErrorException("Player is already in this tournament!");
        }

        if (tournament.getEntryFee() != 0 && user.getMoney() < tournament.getEntryFee()) {
            throw new GenericErrorException("Not enough money to enter tournament, needed: " + tournament.getEntryFee());
        }

        players.add(player);
        tournament.getPlayerPositions().put(player, (long) tournament.getPlayers().size());
        if (tournament.getEntryFee() != 0) {
            user.setMoney(user.getMoney() - tournament.getEntryFee());
            activityService.getUserRepository().save(user);
            activityService.getNotificationService().sendMoneyUpdate(Arrays.asList(user));
        }

        tournament.setPlayers(players);
        if (tournament.getTournamentType().equals(Tournament.TournamentType.QUICKFIRE)) {
            tournament.setPrizePool(tournament.getPrizePool() + Math.round(tournament.getEntryFee() / 2));
        }
        tournamentRepository.save(tournament);
        return tournament;
    }

    @CachePut(cacheNames = {"tournamentsById"}, key = "#tournamentId")
    @CacheEvict(cacheNames = {"allTournaments"}, allEntries = true)
    public Tournament startTournament(Long tournamentId) {
        Tournament tournament = tournamentRepository.findOne(tournamentId);
        tournament.setTournamentPhase(Tournament.TournamentPhase.START);
        List<Player> players = new ArrayList<>();
        players.addAll(tournament.getPlayers());

        Long playerCount = (long) players.size();
        List<Long> range = LongStream.rangeClosed(1L, playerCount)
                .boxed().collect(Collectors.toList());
        long seed = System.nanoTime();
        Collections.shuffle(range, new Random(seed));

        Map<Player, Long> playerPositions = tournament.getPlayerPositions();

        for (int i = 0; i < range.size(); i++) {
            playerPositions.put(players.get(i), range.get(i));
        }
        tournament.setPlayerPositions(playerPositions);
        tournamentRepository.save(tournament);
        activityService.getNotificationService().onTournamentStart(tournament);

        schedulerService.addSchedule(ScheduleEvent.EventType.TournamentEnd, tournamentId, tournament.getEndDate());

        return tournament;
    }

    public List<Player> getTournamentOpponents(Long tournamentId, Player player) throws GenericErrorException {
        Tournament tournament = tournamentRepository.findOne(tournamentId);
        Set<Player> players = tournament.getPlayers();
        if (!players.contains(player)) {
            throw new GenericErrorException("Player has not applied for this tournament!");
        }
        List<GameEvent> games = activityService.getGameEventRepository().findActiveGamesByPlayerAndTournament(player, tournament);
        if (!games.isEmpty()) {
            throw new GenericErrorException("Player is already in a game!");
        }
        Map<Player, Long> playerPositions = tournament.getPlayerPositions();
        List<Tournament.PlayerPosition> positions = players.stream()
                .filter(player1 -> !player1.getId().equals(player.getId()))
                .map(p -> new Tournament.PlayerPosition(p, playerPositions.get(p))).collect(Collectors.toList());

        List<Player> opponents;

        Tournament.TournamentType type = tournament.getTournamentType();

        if (type.equals(Tournament.TournamentType.NORMAL)) {
            Long playerPosition = playerPositions.get(player);
            opponents = positions.stream()
                    .filter(position -> position.getPosition() >= playerPosition - PLAYER_INTERVAL && position.getPosition() < playerPosition)
                    .filter(position -> activityService.getGameEventRepository().findActiveGamesByPlayerAndTournament(position.getPlayer(), tournament).isEmpty())
                    .map(Tournament.PlayerPosition::getPlayer).collect(Collectors.toList());
        } else if (type.equals(Tournament.TournamentType.QUICKFIRE)) {
            opponents = positions.stream().map(Tournament.PlayerPosition::getPlayer).collect(Collectors.toList());
        } else {
            throw new IllegalArgumentException("Unknown tournament type: " + type.toString());
        }

        if (!opponents.isEmpty()) {
            Long userId = player.getUser().getId();
            opponents = opponents.stream()
                    .filter((Player opp) -> !Objects.equals(opp.getUser().getId(), userId))
                    .collect(Collectors.toList());
        }

        return opponents;
    }

    @Transactional
    @Caching(evict = {
            @CacheEvict(cacheNames = {"tournamentsById"}, key = "#tournamentId"),
            @CacheEvict(cacheNames = {"allTournaments"}, allEntries = true)}
    )
    public GameEvent addTournamentGame(Long tournamentId, Player player, Player opponent, Date gameStart) throws GenericErrorException {
        Tournament tournament = tournamentRepository.findOne(tournamentId);
        if (tournament == null) {
            throw new GenericErrorException("Tournament not found!");
        }
        List<GameEvent> gameEvents = activityService.getGameEventRepository().findActiveGamesByPlayers(Arrays.asList(player, opponent));
        if (!gameEvents.isEmpty()) {
            throw new GenericErrorException("Player is already in a game!");
        }

        Tournament.TournamentType tournamentType = tournament.getTournamentType();
        GameType gameType;
        if (tournamentType.equals(Tournament.TournamentType.NORMAL)) {
            gameType = GameType.Tournament;
        } else if (tournamentType.equals(Tournament.TournamentType.QUICKFIRE)) {
            gameType = GameType.TournamentQuickFire;
        } else {
            throw new IllegalArgumentException("Could not resolve valid tournament game type from tournament type: " + tournamentType.toString());
        }

        // TODO CHECK FOR POSITIONS
        GameEvent gameEvent = gameService.createEvent(gameType);

        gameEvent = gameService.addPlayer(gameEvent.getId(), player);
        gameEvent = gameService.addPlayer(gameEvent.getId(), opponent);
        // TODO make this a bit cleaner -> collapse save on tournamentSave
        gameEvent.setGameStart(gameStart);
        activityService.getGameEventRepository().save(gameEvent);

        Set<GameEvent> tournamentGames = tournament.getTournamentGames();
        tournamentGames.add(gameEvent);
        tournament.setTournamentGames(tournamentGames);
        tournamentRepository.save(tournament);
        return gameEvent;
    }

    @CachePut(cacheNames = {"tournamentsById"}, key = "#result.getId()")
    @CacheEvict(cacheNames = {"allTournaments"}, allEntries = true)
    public Tournament onTournamentGameFinish(Long gameId) {
        GameEvent gameEvent = activityService.getGameEventRepository().findOne(gameId);
        Tournament tournament = tournamentRepository.findByTournamentGames(gameEvent);
        if (tournament != null) {
            if (gameEvent.getPlayer1Score().getMatches() >= gameEvent.getPlayer2Score().getMatches()) {
                Long position1 = tournament.getPlayerPositions().get(gameEvent.getPlayer1());
                Long position2 = tournament.getPlayerPositions().get(gameEvent.getPlayer2());
                if (position1 > position2) {
                    tournament.getPlayerPositions().put(gameEvent.getPlayer1(), position2);
                    tournament.getPlayerPositions().put(gameEvent.getPlayer2(), position1);
                }
            } else {
                Long position1 = tournament.getPlayerPositions().get(gameEvent.getPlayer2());
                Long position2 = tournament.getPlayerPositions().get(gameEvent.getPlayer1());
                if (position1 > position2) {
                    tournament.getPlayerPositions().put(gameEvent.getPlayer2(), position2);
                    tournament.getPlayerPositions().put(gameEvent.getPlayer1(), position1);
                }
            }
            tournamentRepository.save(tournament);
        }
        return tournament;
    }

    private static final int MAX_BENEFICIARIES = 5;

    public void giveWinnersMoney(Tournament tournament) {
        int prizePool = tournament.getPrizePool();
        Map<Player, Long> positions = tournament.getPlayerPositions();

        if (prizePool <= 0 || positions.isEmpty()) {
            return;
        }

        int profitablePositions = positions.size() > MAX_BENEFICIARIES ? MAX_BENEFICIARIES : positions.size();

        User[] winnersByPosition = new User[positions.size() + 1];

        positions.forEach((player, position) -> {
            User playerUser = player.getUser();
            if (position <= profitablePositions) {
                winnersByPosition[position.intValue()] = playerUser;
            }
        });

        double prizes[] = new double[profitablePositions + 1];

        double lastPrize = prizePool / profitablePositions / profitablePositions;

        double firstPrize = 2 * prizePool / profitablePositions - lastPrize;

        double ratio = (firstPrize - lastPrize) / (profitablePositions - 1);

        prizes[profitablePositions] = lastPrize;

        for (int i = profitablePositions - 1; i >= 1; i--) {
            prizes[i] = prizes[i + 1] + ratio;
        }

        Set<User> uniqueUsers = new HashSet<>();

        for (int i = 1; i <= profitablePositions; i++) {
            User user = winnersByPosition[i];
            user.setMoney(user.getMoney() + (int) prizes[i]);
            uniqueUsers.add(user);
        }

        activityService.getUserRepository().save(uniqueUsers);
        activityService.getNotificationService().sendMoneyUpdate(new ArrayList<>(uniqueUsers));
    }

    private void giveWinnersElo(Tournament tournament) {
        Map<Player, Long> positions = tournament.getPlayerPositions();
        Map<Long, Player> qualityMap = new HashMap<>();

        for (Player i : positions.keySet())
            qualityMap.put(positions.get(i), i);

        int profitablePositions = positions.size() > MAX_BENEFICIARIES ? MAX_BENEFICIARIES : positions.size();

        int baseElo;
        Tournament.TournamentType tournamentType = tournament.getTournamentType();

        if (tournamentType.equals(Tournament.TournamentType.NORMAL)) {
            baseElo = 30;
        } else if (tournamentType.equals(Tournament.TournamentType.QUICKFIRE)) {
            baseElo = 20;
        } else {
            throw new IllegalArgumentException("Could not resolve elo for tournament type: " + tournamentType.toString());
        }

        for (int i = 1; i <= profitablePositions; i++) {
            playerSeasonService.addElo(qualityMap.get((long) i), ((int) Math.ceil(baseElo / i)));
        }
    }

    @CacheEvict(cacheNames = {"allTournaments"}, allEntries = true)
    public void newQFEvents(boolean startNow) {
        long qfTournamentCount = tournamentRepository.findQFTournamentCount();
        List<Tournament> activeQFTournaments = tournamentRepository.findActiveQFTournaments();
        int create = TournamentService.CONCURRENT_QUICKFIRE_TOURNAMENT_COUNT - activeQFTournaments.size();
        for (int i = 1; i <= create; i++) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MINUTE, startNow ? 0 : 30);
            Date start = cal.getTime();

            cal.add(Calendar.HOUR, 1);
            Date end = cal.getTime();
            this.createTournamentInstance("Quickfire #" + (qfTournamentCount + i), 50L, 150, start, end, Tournament.TournamentType.QUICKFIRE, 150);
        }
    }

    @PostConstruct
    private void checkQFTournaments() {
        List<Tournament> qfTournaments = tournamentRepository.findQFTournaments();
        List<Tournament> finishedQFTournaments = tournamentRepository.findEndedQFTournaments();

        if (qfTournaments.size() == finishedQFTournaments.size()) {
            this.newQFEvents(true);
        }
    }

    @CachePut(cacheNames = {"tournamentsById"}, key = "#id")
    @CacheEvict(cacheNames = {"allTournaments"}, allEntries = true)
    public Tournament endTournament(Long id) {
        Tournament tournament = tournamentRepository.findOne(id);

        if (tournament.getTournamentType().equals(Tournament.TournamentType.QUICKFIRE) && tournament.getPlayers().size() < 2) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.HOUR, 1);
            Date end = calendar.getTime();
            schedulerService.addSchedule(ScheduleEvent.EventType.TournamentEnd, tournament.getId(), end);
            tournament.setEndDate(end);
            tournamentRepository.save(tournament);
            return tournament;
        }

        tournament.setTournamentPhase(Tournament.TournamentPhase.END);
        tournamentRepository.save(tournament);

        tournament.getPlayerPositions().forEach((player, position) -> {
                    activityService.getNotificationService().onTournamentComplete(player.getUser(), player, position, tournament);
                    if (position == 1L) {
                        achievementProgressService.onTournamentWin(player);
                    }
                }
        );

        this.giveWinnersMoney(tournament);
        this.giveWinnersElo(tournament);

        if (tournament.getTournamentType().equals(Tournament.TournamentType.QUICKFIRE)) {
            if (tournamentRepository.findActiveQFTournaments().size() == 0) {
                this.newQFEvents(false);
            }
        }

        return tournament;
    }

    @Caching(evict = {
            @CacheEvict(cacheNames = {"allTournaments"}, allEntries = true),
            @CacheEvict(cacheNames = {"tournamentsById"}, key = "#tournamentId")
    })
    public void deleteTournament(Long tournamentId) {
        tournamentRepository.delete(tournamentId);
        schedulerService.removeSchedule(ScheduleEvent.EventType.TournamentStart, tournamentId);
        schedulerService.removeSchedule(ScheduleEvent.EventType.TournamentEnd, tournamentId);
    }

    @Caching(evict = {
            @CacheEvict(cacheNames = {"allTournaments"}, allEntries = true),
            @CacheEvict(cacheNames = {"tournamentsById"}, key = "#tournamentId")
    })
    public boolean challenge(Player player, Player opponentPlayer, Long tournamentId) {
        matchupService.newMatchup("tournament#" + tournamentId, Arrays.asList(player, opponentPlayer), GameType.Tournament);
        return true;
    }
}
