package com.tennio.mainserver.service;

import com.tennio.mainserver.domain.Item;
import com.tennio.mainserver.domain.ItemEntity;
import com.tennio.mainserver.repository.ItemEntityRepository;
import com.tennio.mainserver.repository.ItemRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.security.repository.UserRepository;
import com.tennio.mainserver.service.exception.GenericErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShopService extends TService<Item> {
    private List<Item> itemsForSale = null;

    @Autowired
    private ItemEntityRepository itemEntityRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    public ShopService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
        this.repository = itemRepository;
        this.itemsForSale = itemRepository.findByForSaleTrue();
    }

    public List<Item> getItemsForSale() {
        return itemsForSale;
    }

    public ItemEntity getNewItemEntity(Item item){
        ItemEntity entity = new ItemEntity(item);
        itemEntityRepository.save(entity);
        return entity;
    }

    public ItemEntity buyItem(String username, Long itemId) {
        User user = userRepository.findByUsername(username);
        Item item = repository.findOne(itemId);
        return buyItem(user, item);
    }

    public ItemEntity buyItem(User user, Item item) {
        if(hasEnoughMoneyForItem(user, item)) {
            ItemEntity itemEntity = getNewItemEntity(item);
            user.setMoney(user.getMoney() - item.getPrice());
            user.getItems().add(itemEntity);
            userRepository.save(user);
            return itemEntity;
        } else {
            throw new GenericErrorException("Not enough money!");
        }
    }

    public boolean hasEnoughMoneyForItem(User user, Item item) {
        return (user.getMoney() >= item.getPrice());
    }

    public boolean setItemForSale(Long itemId, boolean forSale) {
        return setItemForSale(repository.findOne(itemId), forSale);
    }

    public boolean setItemForSale(Item item, boolean forSale) {
        if(item.isForSale() == forSale) {
            return true;
        }

        item.setForSale(forSale);
        repository.save(item);

        return forSale ? itemsForSale.add(item) : itemsForSale.remove(item);
    }

    public Item createNewItem(Item item) {
        repository.save(item);
        onItemUpdateCreate(item);
        return item;
    }

    private void onItemUpdateCreate(Item item) {
        this.itemsForSale = itemRepository.findByForSaleTrue();
    }

    public Item updateItem(Item item) {
        Item originalItem = repository.findOne(item.getId());

        originalItem.setForSale(item.isForSale());
        originalItem.setPrice(item.getPrice());
        originalItem.setName(item.getName());
        originalItem.setEquipmentType(item.getEquipmentType());
        originalItem.setType(item.getType());
        originalItem.setUsages(item.getUsages());
        originalItem.getStats().clone(item.getStats());

        repository.save(item);
        onItemUpdateCreate(item);
        return item;
    }
}
