package com.tennio.mainserver.service;

import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.GameServer;
import com.tennio.mainserver.repository.ServerRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.service.exception.NotFoundException;
import com.tennio.mainserver.service.exception.ServerNotAvailableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ServerService {
    @Autowired
    private ServerRepository serverRepository;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private GameService gameService;

    public void addServer(String uri){
        GameServer gameServer = serverRepository.findByIp(uri);
        if(gameServer == null) {
            gameServer = new GameServer();
            gameServer.setIp(uri);
        }
        gameServer.setServerStatus(GameServer.ServerStatus.AVAILABLE);
        gameServer.setLastResponse(new Date());
        serverRepository.save(gameServer);
    }

    public GameServer getGameServer(GameEvent game) throws ServerNotAvailableException{
        GameServer gameServer = serverRepository.findByCurrentGames(game);
        if(gameServer == null){
            gameServer = serverRepository.findServerWithLowestGames();
            if(gameServer == null){
                throw new ServerNotAvailableException();
            }
        }
        Set<GameEvent> currentGames = gameServer.getCurrentGames();
        currentGames.add(game);
        gameServer.setCurrentGames(currentGames);
        serverRepository.save(gameServer);
        return gameServer;
    }

    public GameServer serverForGame(Long gameId){
        GameEvent gameEvent = activityService.getGameEventRepository().findOne(gameId);
        return serverRepository.findByCurrentGames(gameEvent);
    }

    public void removeGameEvent(GameEvent gameEvent) {
        try {
            removeGameEventFromServer(gameEvent);
        } catch (NotFoundException e){
            // :)
        }
        activityService.getGameEventRepository().delete(gameEvent);
    }

    public void removeGameEventFromServer(GameEvent gameEvent){
        GameServer gameServer = serverRepository.findByCurrentGames(gameEvent);
        if(gameServer == null) {
            throw new NotFoundException("Couldn't find server by GameEvent!");
        }
        gameServer.getCurrentGames().remove(gameEvent);
        serverRepository.save(gameServer);
    }

    @Scheduled(fixedRate = 30 * 1000)
    public void testServers() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, -30);
        List<GameServer> deadServers = serverRepository.findUnavailableWithGames();
        deadServers.addAll(serverRepository.findAvailableByLastResponseLessThan(calendar.getTime()));
        this.handleDeadServers(deadServers);
    }

    private void handleDeadServers(List<GameServer> deadServers) {
        deadServers.forEach(server -> server.setServerStatus(GameServer.ServerStatus.UNAVAILABLE));
        serverRepository.save(deadServers);

        deadServers.forEach(this::vacateServer);
    }

    private void vacateServer(GameServer deadServer) {
        Set<GameEvent> deadServerGames = deadServer.getCurrentGames();
        GameServer replacementServer = serverRepository.findServerWithLowestGames();

        if(replacementServer == null) {
            for (GameEvent serverLessGame : deadServerGames) {
                User challenger = serverLessGame.getPlayer1().getUser();

                // refund ball
                activityService.refundBall(challenger);

                // send notifications about game being canceled ;)
                activityService.getNotificationService().onGameComplete(serverLessGame);

                deadServer.getCurrentGames().remove(serverLessGame);
                serverRepository.save(deadServer);
                activityService.getGameEventRepository().delete(serverLessGame);
            }
        } else {
            replacementServer.getCurrentGames().addAll(deadServerGames);
            for (GameEvent serverLessGame : deadServer.getCurrentGames()) {
                gameService.initializeGame(serverLessGame, replacementServer);
                activityService.getNotificationService().onGameMigrate(serverLessGame);
            }

            deadServer.getCurrentGames().clear();
            serverRepository.save(Arrays.asList(deadServer, replacementServer));
        }
    }
}
