package com.tennio.mainserver.service;

import com.tennio.mainserver.domain.ScheduleEvent;
import com.tennio.mainserver.repository.ScheduleEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

@Service
public class SchedulerService {

    @Autowired
    private ScheduleEventRepository scheduleEventRepository;

    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private GameService gameService;

    @Autowired
    private TrainingsService trainingsService;

    public void addSchedule(ScheduleEvent.EventType eventType, Long eventId, Date date) {
        ScheduleEvent scheduleEvent = new ScheduleEvent();
        scheduleEvent.setEventType(eventType);
        scheduleEvent.setEventId(eventId);
        scheduleEvent.setDate(date);
        scheduleEventRepository.save(scheduleEvent);
    }

    public boolean reSchedule(ScheduleEvent.EventType eventType, Long eventId, Date date) {
        ScheduleEvent event = scheduleEventRepository.findByEventIdInAndEventTypeIn(eventId, eventType);
        if (event != null) {
            event.setDate(date);
            scheduleEventRepository.save(event);
            return true;
        }
        return false;
    }

    public void removeSchedule(ScheduleEvent.EventType eventType, Long eventId) {
        scheduleEventRepository.deleteByEventIdInAndEventTypeIn(eventId, eventType);
    }

    @Scheduled(initialDelay = 0, fixedRate = 60 * 1000)
    public void dueEvents() {
        Date currentDate = new Date();
        Date future = new Date(System.currentTimeMillis() + 60 * 1000); // Events 1 min in the future
        Iterable<ScheduleEvent> scheduleEvents = scheduleEventRepository.findByDateBefore(future);
        scheduleEvents.forEach(event -> {
            if (event.getDate().after(currentDate)) {
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handleEvent(event);
                    }
                }, event.getDate());
            } else {
                handleEvent(event);
            }
        });
    }

    private void handleEvent(ScheduleEvent event) {
        scheduleEventRepository.delete(event);
        if (event.getEventType().equals(ScheduleEvent.EventType.GameStart)) {
            gameService.initializeGame(event.getEventId(), false);
        } else if (event.getEventType().equals(ScheduleEvent.EventType.TournamentStart)) {
            tournamentService.startTournament(event.getEventId());
        } else if (event.getEventType().equals(ScheduleEvent.EventType.TournamentEnd)) {
            tournamentService.endTournament(event.getEventId());
        } else if (event.getEventType().equals(ScheduleEvent.EventType.TrainingEnd)) {
            trainingsService.onTrainingComplete(event.getEventId());
        }
    }
}
