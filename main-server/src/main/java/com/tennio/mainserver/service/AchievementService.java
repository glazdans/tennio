package com.tennio.mainserver.service;

import com.tennio.mainserver.domain.Achievement;
import com.tennio.mainserver.repository.AchievementProgressRepository;
import com.tennio.mainserver.repository.AchievementRepository;
import com.tennio.mainserver.service.exception.AlreadyExistsException;
import com.tennio.mainserver.service.exception.GenericErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AchievementService extends TService<Achievement> {
    private AchievementRepository achievementRepository;
    private List<Achievement> achievementCache;
    private AchievementProgressRepository achievementProgressRepository;

    @Autowired
    public AchievementService(AchievementRepository achievementRepository, AchievementProgressRepository achievementProgressRepository) {
        this.achievementRepository = achievementRepository;
        this.achievementCache = achievementRepository.findAll();
        this.repository = achievementRepository;
        this.achievementProgressRepository = achievementProgressRepository;
    }

    private Achievement getAchievement(Long id) {
        if (id == null) {
            throw new GenericErrorException("No id supplied!");
        }
        Achievement achievement = achievementRepository.findOne(id);
        if (achievement == null) {
            throw new GenericErrorException("Achievement with that id was not found!");
        }
        return achievement;
    }

    @Transactional
    public void deleteAchievement(Long id) {
        Achievement achievement = getAchievement(id);
        this.achievementCache.remove(achievement);
        this.achievementProgressRepository.deleteAllByAchievementType(achievement);
        this.achievementRepository.delete(id);
    }

    public Achievement updateAchievement(Achievement request) throws GenericErrorException {
        checkFields(request);
        Achievement achievement = getAchievement(request.getId());

        this.achievementCache.remove(achievement);

        achievement.setName(request.getName());
        achievement.setDescription(request.getDescription());
        achievement.setRequirement(request.getRequirement());
        achievement.setReward(request.getReward());
        achievement.setType(request.getType());

        achievementRepository.save(request);
        this.achievementCache.add(request);

        return request;
    }

    public Achievement createAchievement(Achievement request) throws GenericErrorException, AlreadyExistsException {
        checkFields(request);
        if(achievementRepository.findByName(request.getName()) != null) {
            throw new AlreadyExistsException("Achievement with the same name already exists!");
        }

        achievementRepository.save(request);
        this.achievementCache.add(request);

        return request;
    }

    public List<Achievement> getAllAchievements() {
        return achievementCache;
    }

    public void checkFields(Achievement request) throws GenericErrorException {
        if(request.getName() == null || request.getDescription() == null || request.getRequirement() == null || request.getReward() == null || request.getType() == null){
            throw new GenericErrorException("One or more attributes are null!");
        }
    }
}
