package com.tennio.mainserver.service;

import com.tennio.mainserver.domain.Country;
import com.tennio.mainserver.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CountryService {
    private Map<String, Country> countriesByCode;

    @Autowired
    public CountryService(CountryRepository countryRepository) {
        List<Country> allCountries = countryRepository.findAll();

        this.countriesByCode = allCountries.stream().collect(
            HashMap<String, Country>::new,
            (m, c) -> m.put(c.getCode(), c),
            (m, u) -> { }
        );
    }

    public Country getCountryByCode(String code) {
        return this.countriesByCode.get(code);
    }
}
