package com.tennio.mainserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class BallFillerService {
    @Autowired
    private ActivityService activityService;

    @Scheduled(cron="0 0/30 * * * *")
    @Transactional()
    public void addBalls(){
        activityService.incrementBalls();
    }
}
