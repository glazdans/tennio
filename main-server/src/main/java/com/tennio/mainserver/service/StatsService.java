package com.tennio.mainserver.service;

import com.tennio.mainserver.domain.PlayerSeason;
import com.tennio.mainserver.domain.Season;
import com.tennio.mainserver.repository.PlayerSeasonRepository;
import com.tennio.mainserver.repository.SeasonRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.security.repository.UserRepository;
import com.tennio.mainserver.service.model.FrontPageStats;
import com.tennio.mainserver.service.model.Rank;
import com.tennio.mainserver.service.model.Ranks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class StatsService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PlayerSeasonRepository playerSeasonRepository;

    @Autowired
    private SeasonRepository seasonRepository;
    public static final String STATS_KEY = "stats";

    private Set<String> connectedUsers;

    private FrontPageStats lastFrontpageStats;

    public StatsService() {
        connectedUsers = new HashSet<String>();
    }

    public FrontPageStats getFrontPageStats() {
        if (this.lastFrontpageStats != null) {
            return this.lastFrontpageStats;
        } else {
            FrontPageStats stats = new FrontPageStats();

            stats.registeredPlayers = userRepository.count();
            stats.currentlyOnline = this.connectedUsers.size();

            stats.playersByCountry = userRepository.getCountriesByCount();

            this.lastFrontpageStats = stats;
            return this.lastFrontpageStats;
        }
    }

    void setConnectedCount() {
        if (this.lastFrontpageStats != null) {
            this.lastFrontpageStats.currentlyOnline = this.connectedUsers.size();
        }
    }

    public void onConnect(String username) {
        this.connectedUsers.add(username);
        this.setConnectedCount();
    }

    public void onDisconnect(String username) {
        this.connectedUsers.remove(username);
        this.setConnectedCount();
    }

    @Cacheable(cacheNames = "playerRanks", key = "{#page, #size}")
    public Ranks getPlayerRankingPage(int page, int size) {
        Season activeSeason = seasonRepository.findByEndDateIsNull();
        Page<PlayerSeason> seasons = playerSeasonRepository.getBySeason(activeSeason, new PageRequest(page, size, Sort.Direction.DESC, "elo"));
        List<PlayerSeason> seasonsList = seasons.getContent();

        List<Rank> rankList = new ArrayList<>();
        for (PlayerSeason playerSeason :
                seasonsList) {
            try {
                User user = playerSeason.getPlayer().getUser();
                rankList.add(new Rank(playerSeason.getElo(), playerSeason.getPlayer(), user));
            } catch (NullPointerException e) {
                // fuck off (zombie players)
            }
        }
        return new Ranks(rankList, seasons.getTotalPages(), seasons.isLast(), seasons.isFirst(), seasons.getNumber());
    }

    @Scheduled(fixedDelay = 1200000)
    @CacheEvict(allEntries = true, cacheNames = {"playerRanks"})
    public void clearCache() {
        this.lastFrontpageStats = null;
    }
}
