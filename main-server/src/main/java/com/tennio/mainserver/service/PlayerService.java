package com.tennio.mainserver.service;

import com.tennio.mainserver.domain.ItemEntity;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.domain.PlayerEquipment;
import com.tennio.mainserver.domain.Stats;
import com.tennio.mainserver.repository.ItemEntityRepository;
import com.tennio.mainserver.repository.PlayerEquipmentRepository;
import com.tennio.mainserver.repository.PlayerRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.security.repository.UserRepository;
import com.tennio.mainserver.service.exception.AlreadyExistsException;
import com.tennio.mainserver.service.exception.GenericErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class PlayerService extends TService<Player> {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ItemEntityRepository itemEntityRepository;

    @Autowired
    private PlayerEquipmentRepository playerEquipmentRepository;

    @Autowired
    private CountryService countryService;

    @Autowired
    public void setPlayerRepository(PlayerRepository playerRepository) {
        this.repository = playerRepository;
    }

    public boolean equipItem(String userName, long itemEntityId) throws GenericErrorException {
        User user;
        try {
            user = userRepository.findByUsername(userName);
        } catch (Exception e) {
            throw new GenericErrorException("Couldn't retrieve user '" + userName + "'");
        }

        ItemEntity itemEntity;
        try {
            itemEntity = itemEntityRepository.findOne(itemEntityId);
        } catch (Exception e) {
            throw new GenericErrorException("Couldn't retrieve item with id: " + itemEntityId);
        }
        return equipItem(user.getActivePlayer(), itemEntity) != null;
    }

    @Caching(put = {@CachePut(cacheNames = {"usersByUsername"}, key = "#result.username"), @CachePut(cacheNames = {"usersByPlayer"}, key = "#player.id")})
    private User equipItem(Player player, ItemEntity itemEntity) throws GenericErrorException {
        User user = player.getUser();
        if (!user.getItems().contains(itemEntity)) {
            throw new GenericErrorException("Selected item does not exist in your inventory!");
        }

        PlayerEquipment equipment = getActivePlayerEquipment(player);

        // if slot already has an item in it, reset playerId and equipmentId
        try {
            ItemEntity currentlyEquipped = equipment.getEquippedItems().get(itemEntity.getItemType().getEquipmentType());
            currentlyEquipped.setPlayerId(null);
            currentlyEquipped.setEquipmentId(null);
        } catch (NullPointerException e) {
            // do nothing
        }

        // if any other player has equipped the same item, un-equip it for them
        if (itemEntity.getPlayerId() != null) {
            this.unequipItem(user, itemEntity);
        }

        itemEntity.setPlayerId(player.getId());
        itemEntity.setEquipmentId(equipment.getId());
        equipment.equipItem(itemEntity);

        repository.save(player);
        return user;
    }

    public boolean unequipItem(String userName, long itemEntityId) throws GenericErrorException {
        User user;
        try {
            user = userRepository.findByUsername(userName);
        } catch (Exception e) {
            throw new GenericErrorException("Couldn't retrieve user '" + userName + "'");
        }

        ItemEntity itemEntity;
        try {
            itemEntity = itemEntityRepository.findOne(itemEntityId);
        } catch (Exception e) {
            throw new GenericErrorException("Couldn't retrieve item with id: " + itemEntityId);
        }
        return unequipItem(user, itemEntity) != null;
    }

    @CachePut(value = "usersByUsername", key = "#result.getUsername()", unless = "#result == null")
    public User unequipItem(User user, ItemEntity itemEntity) throws GenericErrorException {
        if (user.getPlayerById(itemEntity.getPlayerId()) == null) {
            throw new GenericErrorException("Player not found!");
        }

        PlayerEquipment oldPlayerEquipment = playerEquipmentRepository.findOne(itemEntity.getEquipmentId());
        oldPlayerEquipment.getEquippedItems().remove(itemEntity.getItemType().getEquipmentType());

        itemEntity.setEquipmentId(null);
        itemEntity.setPlayerId(null);

        playerEquipmentRepository.save(oldPlayerEquipment);
        return user;
    }

    public PlayerEquipment getActivePlayerEquipment(Player player) {
        for (PlayerEquipment equipment : player.getEquipments()) {
            if (equipment.isActive()) {
                return equipment;
            }
        }

        //no equipments found, temporary code for old players
        PlayerEquipment playerEquipment = new PlayerEquipment();
        playerEquipment.setActive(true);
        playerEquipment.setPlayer(player);
        playerEquipmentRepository.save(playerEquipment);

        return player.getEquipments().get(0);
    }

    public List<Player> getPlayers(String username) throws GenericErrorException {
        User user = userRepository.findByUsername(username);
        if(user == null) {
            throw new GenericErrorException("User not found!");
        }
        List<Player> players = user.getPlayers();
        players.forEach((player) -> player.getStats().getId()); // Initializes hibernate object, maybe there is a better way (on REST Mapper)
        return players;
    }

    @Caching(evict = {@CacheEvict(cacheNames = {"usersByUsername"}, key = "#username"), @CacheEvict(cacheNames = {"usersByPlayer"}, key = "#result.id")})
    public Player createPlayer(String username, String name, String countryCode) throws GenericErrorException, AlreadyExistsException {
        User user = userRepository.findByUsername(username);

        if(user == null) {
            throw new GenericErrorException("User not found!");
        }

        Optional<Player> aPlayer = user.getPlayers().stream().filter(p -> Objects.equals(p.getName(), name)).findFirst();
        if(aPlayer.isPresent()) {
            throw new AlreadyExistsException("A player with that name already exists!");
        }

        Player player = new Player();
        player.setName(name);
        player.setCountry(countryService.getCountryByCode(countryCode));

        Stats stats = new Stats(0L);
        player.setStats(stats);
        repository.save(player);
        user.getPlayers().add(player);

        if (user.getActivePlayer() == null) {
            user.setActivePlayerId(player.getId());
        }

        PlayerEquipment playerEquipment = new PlayerEquipment();
        playerEquipment.setActive(true);
        playerEquipment.setPlayer(player);
        playerEquipmentRepository.save(playerEquipment);

        userRepository.save(user);
        return player;
    }
}
