package com.tennio.mainserver.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tennio.common.GameType;
import com.tennio.mainserver.domain.*;
import com.tennio.mainserver.repository.NotificationRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class NotificationService {

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PlayerSeasonService playerSeasonService;

    public List<Notification> getNotifications(User user) {
        return notificationRepository.findByTennioUser(user);
    }

    public boolean deleteUserNotification(Long id, User user) {
        Notification notification = notificationRepository.findOne(id);
        if (notification.getUser().equals(user) && !notification.getSticky()) {
            notificationRepository.delete(notification);
            return true;
        }
        return false;
    }

    public void deleteStickyNotification(Long id) {
        notificationRepository.delete(id);
    }

    @Transactional
    public void saveNotificationAndNotify(Notification notification) {
        if (notification.getNotificationEventStart() == null) {
            notification.setNotificationEventStart(new Date());
        }
        notificationRepository.save(notification);
        messagingTemplate.convertAndSendToUser(notification.getUser().getUsername(), "/notifications", notification);
    }

    public void sendBallsCountUpdate(User user) {
        Notification notification = new Notification.NotificationBuilder()
                .setUser(user)
                .setNotificationType(Notification.NotificationType.AttributeUpdate)
                .putAttribute(Notification.NotificationAttributeType.Balls, user.getBalls().toString())
                .build();

        messagingTemplate.convertAndSendToUser(
                user.getUsername(), "/notifications/update", notification
        );
    }

    public void sendBallsCountUpdate(Player player) {
        this.sendBallsCountUpdate(userRepository.findByPlayer(player));
    }

    public void sendMMGameNotification(Player player, Date time, GameEvent gameEvent) {
        User user = userRepository.findByPlayer(player);
        Notification notification = new Notification.NotificationBuilder()
                .setUser(user)
                .setEventStart(time)
                .setNotificationType(Notification.NotificationType.Duel)
                .putAttribute(Notification.NotificationAttributeType.GameId, gameEvent.getId().toString())
                .setSticky(true)
                .build();

        this.sendGameNotification(notification);
    }

    public void sendGameNotification(Notification notification) {
        messagingTemplate.convertAndSendToUser(notification.getUser().getUsername(), "/matchmaking/game", notification);
    }

    public void sendBallsIncrement() {
        Notification notification = new Notification.NotificationBuilder()
                .setNotificationType(Notification.NotificationType.AttributeUpdate)
                .putAttribute(Notification.NotificationAttributeType.Balls, "+")
                .build();

        messagingTemplate.convertAndSend("/notifications/update", notification);
    }

    public void sendMoneyUpdate(List<User> users) {
        users.forEach((User user) ->
                messagingTemplate.convertAndSendToUser(user.getUsername(), "/notifications/update",
                        new Notification.NotificationBuilder()
                                .setUser(user)
                                .setNotificationType(Notification.NotificationType.AttributeUpdate)
                                .putAttribute(Notification.NotificationAttributeType.Money, user.getMoney().toString())
                                .build()
                )
        );
    }

    public void sendEloUpdate(List<Player> players) {
        players.forEach((Player player) -> {
                    User user = player.getUser();
                    PlayerSeason season = playerSeasonService.getOrCreatePlayerSeason(player);
                    messagingTemplate.convertAndSendToUser(user.getUsername(), "/notifications/update",
                            new Notification.NotificationBuilder()
                                    .setUser(user)
                                    .setNotificationType(Notification.NotificationType.AttributeUpdate)
                                    .putAttribute(Notification.NotificationAttributeType.PlayerId, player.getId().toString())
                                    .putAttribute(Notification.NotificationAttributeType.ELO, season.getElo().toString())
                                    .build()
                    );
                }
        );
    }

    public void onAIGameComplete(GameEvent gameEvent) {
        saveNotificationAndNotify(
                new Notification.NotificationBuilder()
                        .setNotificationType(Notification.NotificationType.GameResult)
                        .setSticky(false)
                        .setUser(gameEvent.getPlayer1().getUser())
                        .setAdditionalInfo(gameEvent.getWinningPlayer() != null ? "You won a game against a bot." : "You lost a game against a bot.")
                        .putAttribute(Notification.NotificationAttributeType.GameId, gameEvent.getId().toString())
                        .build()
        );
    }

    public void onGameSurrender(GameEvent gameEvent) {
        if (gameEvent.getStatus().equals(GameEvent.GameStatus.PLAYER1_SURRENDERED) || gameEvent.getStatus().equals(GameEvent.GameStatus.PLAYER2_SURRENDERED)) {
            Player surrenderer = gameEvent.getStatus() == GameEvent.GameStatus.PLAYER1_SURRENDERED ? gameEvent.getPlayer1() : gameEvent.getPlayer2();
            Player other = null;
            String surrenderMessage = "You surrendered a bot game.";

            if (!gameEvent.getGameType().equals(GameType.AI)) {
                other = gameEvent.getStatus() == GameEvent.GameStatus.PLAYER1_SURRENDERED ? gameEvent.getPlayer2() : gameEvent.getPlayer1();
                surrenderMessage = "You surrendered a game against " + other.getName() + ".";
            }

            saveNotificationAndNotify(
                    new Notification.NotificationBuilder()
                            .setNotificationType(Notification.NotificationType.GameResult)
                            .setSticky(false)
                            .setUser(surrenderer.getUser())
                            .setAdditionalInfo(surrenderMessage)
                            .putAttribute(Notification.NotificationAttributeType.GameId, gameEvent.getId().toString())
                            .build()
            );

            if (!gameEvent.getGameType().equals(GameType.AI)) {
                saveNotificationAndNotify(
                        new Notification.NotificationBuilder()
                                .setNotificationType(Notification.NotificationType.GameResult)
                                .setSticky(false)
                                .setUser(other.getUser())
                                .setAdditionalInfo("Your opponent " + surrenderer.getName() + " surrendered a game against you.")
                                .putAttribute(Notification.NotificationAttributeType.GameId, gameEvent.getId().toString())
                                .build()
                );
            }
        }
    }

    public void onGameComplete(GameEvent gameEvent) {
        String winnerMessage;
        String loserMessage;

        Player winner;
        Player loser;

        if (gameEvent.getStatus() == GameEvent.GameStatus.ENDED) {
            if (gameEvent.getGameType().equals(GameType.AI)) {
                onAIGameComplete(gameEvent);
                return;
            }

            winner = gameEvent.getWinningPlayer();
            loser = gameEvent.getLosingPlayer();
            winnerMessage = "You won a game against " + loser.getName();
            loserMessage = "You lost a game against " + winner.getName();
        } else {
            if(gameEvent.getGameType().equals(GameType.AI)) {
                saveNotificationAndNotify(
                        new Notification.NotificationBuilder()
                                .setNotificationType(Notification.NotificationType.GameResult)
                                .setSticky(false)
                                .setUser(gameEvent.getPlayer1().getUser())
                                .setAdditionalInfo("Your bot game was canceled. You have been refunded 1 ball.")
                                .putAttribute(Notification.NotificationAttributeType.GameId, gameEvent.getId().toString())
                                .build()
                );
                return;
            }
            winner = gameEvent.getPlayer1();
            loser = gameEvent.getPlayer2();
            winnerMessage = "Your game against " + loser.getName() + " has been canceled. You have been refunded 1 ball.";
            loserMessage = "Your game against " + winner.getName() + " has been canceled.";
        }

        saveNotificationAndNotify(
                new Notification.NotificationBuilder()
                        .setNotificationType(Notification.NotificationType.GameResult)
                        .setSticky(false)
                        .setUser(winner.getUser())
                        .setAdditionalInfo(winnerMessage)
                        .putAttribute(Notification.NotificationAttributeType.GameId, gameEvent.getId().toString())
                        .build()
        );

        saveNotificationAndNotify(
                new Notification.NotificationBuilder()
                        .setNotificationType(Notification.NotificationType.GameResult)
                        .setSticky(false)
                        .setUser(loser.getUser())
                        .setAdditionalInfo(loserMessage)
                        .putAttribute(Notification.NotificationAttributeType.GameId, gameEvent.getId().toString())
                        .build()
        );

        List<Notification> sentNotifications = notificationRepository.findByAttributeAndNotificationTypes(
                Notification.NotificationAttributeType.GameId,
                gameEvent.getId().toString(),
                Arrays.asList(Notification.NotificationType.Game, Notification.NotificationType.TournamentGame)
        );

        notificationRepository.delete(sentNotifications);

        sentNotifications.forEach((Notification notification) ->
        {
            notification.setDeleteNotification(true);
            messagingTemplate.convertAndSendToUser(
                    notification.getUser().getUsername(), "/notifications", notification
            );
        });
    }

    public void onTournamentStart(Tournament tournament) {
        Set<Player> players = tournament.getPlayers();
        Set<User> notifiedUsers = new HashSet<>();
        User currentUser;

        for (Player player : players) {
            currentUser = player.getUser();
            if (!notifiedUsers.contains(currentUser)) {
                saveNotificationAndNotify(
                        new Notification.NotificationBuilder()
                                .setUser(player.getUser())
                                .setNotificationType(Notification.NotificationType.Tournament)
                                .putAttribute(Notification.NotificationAttributeType.TournamentId, tournament.getId().toString())
                                .setAdditionalInfo("Tournament " + tournament.getName() + " has started!")
                                .build()
                );
            }
            notifiedUsers.add(currentUser);
        }

    }

    public void onTournamentComplete(User user, Player userPlayer, Long position, Tournament tournament) {
        saveNotificationAndNotify(
                new Notification.NotificationBuilder()
                        .setUser(user)
                        .setNotificationType(Notification.NotificationType.Tournament)
                        .putAttribute(Notification.NotificationAttributeType.TournamentId, tournament.getId().toString())
                        .setAdditionalInfo("Your player " + userPlayer.getName() + " placed " + position + " in " + tournament.getName())
                        .setSticky(false)
                        .build()
        );
    }

    public void onTrainingComplete(TrainingEvent trainingEvent) {
        saveNotificationAndNotify(
                new Notification.NotificationBuilder()
                        .setUser(trainingEvent.getPlayer().getUser())
                        .setNotificationType(Notification.NotificationType.Training)
                        .putAttribute(Notification.NotificationAttributeType.TrainingEventId, trainingEvent.getId().toString())
                        .setAdditionalInfo("Your player " + trainingEvent.getPlayer().getName() + " finished training "
                                + trainingEvent.getTraining().getCode())
                        .setSticky(true)
                        .build()
        );
    }

    public void sendStatUpdate(Player player) {
        try {
            String newStats = new ObjectMapper().writeValueAsString(player.getStats());
            messagingTemplate.convertAndSendToUser(player.getUser().getUsername(), "/notifications/update",
                    new Notification.NotificationBuilder()
                            .setUser(player.getUser())
                            .setNotificationType(Notification.NotificationType.AttributeUpdate)
                            .putAttribute(Notification.NotificationAttributeType.Stats, newStats)
                            .putAttribute(Notification.NotificationAttributeType.PlayerId, player.getId().toString())
                            .build());
        } catch (Exception e) {
            return;
        }
    }

    public void deleteTrainingNotification(TrainingEvent trainingEvent) {
        List<Notification> notifications = notificationRepository.findByAttributeAndNotificationTypes(
                Notification.NotificationAttributeType.TrainingEventId,
                trainingEvent.getId().toString(),
                Arrays.asList(Notification.NotificationType.Training));

        if (notifications.size() != 1) {
            return; //throw new WTFException();
        }

        Notification notification = notifications.get(0);
        notification.setDeleteNotification(true);

        messagingTemplate.convertAndSendToUser(trainingEvent.getPlayer().getUser().getUsername(), "/notifications", notification);
        notificationRepository.delete(notification);
    }

    public void sendTournamentGameNotifications(User challenger, User opponent, GameEvent gameEvent, Tournament tournament) {
        sendGameNotification(
                new Notification.NotificationBuilder()
                        .setUser(challenger)
                        .setNotificationType(Notification.NotificationType.TournamentGame)
                        .putAttribute(Notification.NotificationAttributeType.GameId, gameEvent.getId().toString())
                        .setEventStart(gameEvent.getGameStart())
                        .setAdditionalInfo("You challenged " + gameEvent.getPlayer2().getName() + " in tournament " + tournament.getName())
                        .setSticky(true)
                        .build()
        );

        sendGameNotification(
                new Notification.NotificationBuilder()
                        .setUser(opponent)
                        .setNotificationType(Notification.NotificationType.TournamentGame)
                        .putAttribute(Notification.NotificationAttributeType.GameId, gameEvent.getId().toString())
                        .setEventStart(gameEvent.getGameStart())
                        .setAdditionalInfo("You have been challenged by " + gameEvent.getPlayer1().getName() + " in tournament " + tournament.getName())
                        .setSticky(true)
                        .build()
        );
    }

    public void onAchievementProgress(List<AchievementProgress> progresses) {
        ObjectMapper objectMapper = new ObjectMapper();
        progresses.forEach(p -> {
            try {
                messagingTemplate.convertAndSendToUser(p.getUser().getUsername(), "/notifications/update",
                        new Notification.NotificationBuilder()
                                .setNotificationType(Notification.NotificationType.AchievementProgress)
                                .putAttribute(Notification.NotificationAttributeType.AchievementProgressId, p.getId().toString())
                                .putAttribute(Notification.NotificationAttributeType.AchievementProgress, objectMapper.writeValueAsString(p))
                                .setSticky(false)
                                .build()
                );
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }

    public void sendMMPlayerCount(int count) {
        messagingTemplate.convertAndSend("/matchmaking/active", count);
    }

    public void sendMatchupFail(User user) {
        messagingTemplate.convertAndSendToUser(user.getUsername(), "/notifications", new Notification.NotificationBuilder()
                .setNotificationType(Notification.NotificationType.MatchmakingError)
                .putAttribute(Notification.NotificationAttributeType.CurrentMatchMakingGame, "")
                .setAdditionalInfo("Game server is currently unavailable!")
                .setSticky(false)
                .build()
        );
    }

    public void sendMatchupOpponentDeclinedNotification(User user) {
        messagingTemplate.convertAndSendToUser(user.getUsername(), "/matchmaking/game", new Notification.NotificationBuilder()
                .setNotificationType(Notification.NotificationType.DuelOpponentDeclined)
                .build()
        );
    }

    public void sendMatchupExpiredNotification(User user, String matchupId) {
        messagingTemplate.convertAndSendToUser(user.getUsername(), "/matchup", new Notification.NotificationBuilder()
                .setNotificationType(Notification.NotificationType.DuelExpired)
                .putAttribute(Notification.NotificationAttributeType.DuelId, matchupId)
                .setAdditionalInfo("Duel has expired!")
                .build()
        );
    }

    public void onGameMigrate(GameEvent serverLessGame) {
        List<User> users = Arrays.asList(serverLessGame.getPlayer1().getUser(), serverLessGame.getPlayer2().getUser());
        users.forEach(user ->
                messagingTemplate.convertAndSendToUser(user.getUsername(), "/notifications", new Notification.NotificationBuilder()
                        .setNotificationType(Notification.NotificationType.GameMigrated)
                        .setSticky(false)
                        .putAttribute(Notification.NotificationAttributeType.GameId, serverLessGame.getId().toString())
                        .setAdditionalInfo("Your game has been migrated to a new server, please reopen the game!")
                        .build()
                )
        );
    }

    public void sendPopupNotification(Player player, String text) {
        this.sendPopupNotification(player.getUser(), text);
    }

    public void sendPopupNotification(User user, String text) {
        messagingTemplate.convertAndSendToUser(user.getUsername(), "/notifications", new Notification.NotificationBuilder()
                .setNotificationType(Notification.NotificationType.Popup)
                .setSticky(false)
                .setAdditionalInfo(text)
                .build()
        );
    }

    public void sendMatchupNotification(Player player, String matchupID, Long matchupExpiryTime) {
        User user = userRepository.findByPlayer(player);
        messagingTemplate.convertAndSendToUser(user.getUsername(), "/matchup",
                new Notification.NotificationBuilder()
                        .setNotificationType(Notification.NotificationType.DuelOpponentFound)
                        .putAttribute(Notification.NotificationAttributeType.DuelId, matchupID)
                        .putAttribute(Notification.NotificationAttributeType.PlayerId, player.getId().toString())
                        .putAttribute(Notification.NotificationAttributeType.Expires, matchupExpiryTime.toString())
                        .setSticky(true)
                        .build()
        );
    }
}
