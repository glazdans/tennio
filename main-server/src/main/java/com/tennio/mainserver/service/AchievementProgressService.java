package com.tennio.mainserver.service;

import com.tennio.common.GameType;
import com.tennio.mainserver.domain.Achievement;
import com.tennio.mainserver.domain.AchievementProgress;
import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.repository.AchievementProgressRepository;
import com.tennio.mainserver.security.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class AchievementProgressService {
    @Autowired
    private AchievementProgressRepository achievementProgressRepository;

    @Autowired
    private AchievementService achievementService;

    @Autowired
    private ActivityService activityService;

    public void onGameEnd(GameEvent event) {
        User winnerUser = (event.getPlayer1Score().getMatches() > event.getPlayer2Score().getMatches() ? event.getPlayer1() : event.getPlayer2()).getUser();
        User loserUser = (event.getPlayer1().getUser() == winnerUser ? event.getPlayer2() : event.getPlayer1()).getUser();

        List<AchievementProgress> losingSpreeProgresses = winnerUser.getAchievements().stream()
                .filter(p -> !p.getFinished())
                .filter(p -> Objects.equals(p.getAchievementType().getType(), Achievement.AchievementType.LosingSpree))
                .collect(Collectors.toList());
        losingSpreeProgresses.forEach(achievementProgress -> achievementProgress.setProgress(0));

        List<AchievementProgress> winningSpreeProgresses = loserUser.getAchievements().stream()
                .filter(p -> !p.getFinished())
                .filter(p -> Objects.equals(p.getAchievementType().getType(), Achievement.AchievementType.WinningSpree))
                .collect(Collectors.toList());
        winningSpreeProgresses.forEach(achievementProgress -> achievementProgress.setProgress(0));

        List<AchievementProgress> bothLists = new ArrayList<>(winningSpreeProgresses);
        bothLists.addAll(losingSpreeProgresses);
        achievementProgressRepository.save(bothLists);

        onEvent(Achievement.AchievementType.GamesWon, winnerUser);
        onEvent(Achievement.AchievementType.WinningSpree, winnerUser);

        onEvent(Achievement.AchievementType.GamesLost, loserUser);
        onEvent(Achievement.AchievementType.LosingSpree, loserUser);

        winnerUser.setWonGameCount(winnerUser.getWonGameCount() + 1);
        loserUser.setLostGameCount(loserUser.getLostGameCount() + 1);

        if (event.getGameType().equals(GameType.Matchmaking)) {
            onEvent(Achievement.AchievementType.CompetitiveGamesWon, winnerUser);
        }

        activityService.getUserRepository().save(winnerUser);
        activityService.getUserRepository().save(loserUser);
    }

    public void onTournamentWin(Player player) {
        this.onEvent(Achievement.AchievementType.TournamentsWon, player.getUser());
    }

    public void onEvent(Achievement.AchievementType type, User user) {
        List<Achievement> achievements = achievementService.getAllAchievements().stream()
                .filter(p -> Objects.equals(p.getType(), type)).collect(Collectors.toList());

        List<AchievementProgress> userAchievements = user.getAchievements();
        List<AchievementProgress> updatedAchievements = new ArrayList<>();

        Iterator<Achievement> it = achievements.iterator();
        while (it.hasNext()) {
            Achievement achievement = it.next();
            for(AchievementProgress userAchievementProgress : userAchievements) {
                if (Objects.equals(userAchievementProgress.getAchievementType().getId(), achievement.getId())) {
                    it.remove();
                    if(userAchievementProgress.getFinished()){
                        continue;
                    }
                    userAchievementProgress.setProgress(userAchievementProgress.getProgress() + 1);
                    if (userAchievementProgress.getProgress() >= userAchievementProgress.getAchievementType().getRequirement()) {
                        userAchievementProgress.setFinished(true);
                    }
                    updatedAchievements.add(userAchievementProgress);
                }
            }
        }

        for(Achievement achievement: achievements){
            AchievementProgress achievementProgress = new AchievementProgress();
            achievementProgress.setProgress(1);
            achievementProgress.setUser(user);
            achievementProgress.setAchievementType(achievement);
            updatedAchievements.add(achievementProgress);
        }

        achievementProgressRepository.save(updatedAchievements);
        activityService.getUserRepository().save(user);
        activityService.getNotificationService().onAchievementProgress(updatedAchievements);
    }
}
