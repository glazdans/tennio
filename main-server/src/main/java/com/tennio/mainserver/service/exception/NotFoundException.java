package com.tennio.mainserver.service.exception;

import com.tennio.mainserver.TennioException;

public class NotFoundException extends TennioException {
    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException() {
    }
}
