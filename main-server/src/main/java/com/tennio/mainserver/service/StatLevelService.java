package com.tennio.mainserver.service;

import com.tennio.common.Settings;
import com.tennio.mainserver.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatLevelService {
    @Autowired
    private PlayerService playerService;

    private final long BASE_EXP = 111L;
    private final double SCALE = 1.15d;
    public static final double MAX_LEVEL = Settings.MAX_STAT_LEVEL;

    private long[] maxEXP;

    public StatLevelService() {
        this.calcXPRanges();
    }

    private void calcXPRanges() {
        maxEXP = new long[(int) (MAX_LEVEL + 1)];
        maxEXP[0] = 0;

        for(int i = 1; i <= MAX_LEVEL; i++){
            maxEXP[i] = (long) (maxEXP[i - 1] + Math.ceil(Math.pow((BASE_EXP * i), SCALE)));
        }
    }

    public int getLevel(Long EXP){
        for(int i = 1; i <= MAX_LEVEL; i++){
            if(EXP <= maxEXP[i]){
                return i;
            }
        }
        return (int) MAX_LEVEL;
    }

    public Player setStatLVLs(Player player){
        player.setStats(setStatLVLs(player.getStats(), playerService.getActivePlayerEquipment(player)));
        return player;
    }

    public Stats setStatLVLs(Stats stats){
        stats.setSpeedLVL(getLevel(stats.getSpeed()));
        stats.setBackhandLVL(getLevel(stats.getBackhand()));
        stats.setForehandLVL(getLevel(stats.getForehand()));
        stats.setServerLVL(getLevel(stats.getServer()));
        stats.setPrecisionLVL(getLevel(stats.getPrecision()));
        stats.setArmStrengthLVL(getLevel(stats.getArmStrength()));
        stats.setFitnessLVL(getLevel(stats.getFitness()));

        return stats;
    }

    public Stats getEquipmentStats(PlayerEquipment equipment) {
        Stats equipmentStats = new Stats();

        if(equipment == null) {
            return equipmentStats;
        }

        for(ItemEntity equippedItem : equipment.getEquippedItems().values()) {
            equipmentStats.addStats(equippedItem.getItemType().getStats().normalizeLevels(StatLevelService.MAX_LEVEL * Item.EquipmentType.values().length));
        }

        return equipmentStats;
    }

    public Stats setStatLVLs(Stats stats, PlayerEquipment activeEquipment){
        setStatLVLs(stats);
        stats.addStats(getEquipmentStats(activeEquipment));
        return stats;
    }
}
