package com.tennio.mainserver.service.model;

import java.io.Serializable;
import java.util.List;

public class Ranks implements Serializable { // terrible page wrapper
    List<Rank> ranks;
    int totalPages;
    boolean lastPage;
    boolean firstPage;
    int number;

    public Ranks(List<Rank> ranks, int totalPages, boolean lastPage, boolean firstPage, int number) {
        this.ranks = ranks;
        this.totalPages = totalPages;
        this.lastPage = lastPage;
        this.firstPage = firstPage;
        this.number = number;
    }

    public List<Rank> getRanks() {
        return ranks;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    public boolean isFirstPage() {
        return firstPage;
    }

    public int getNumber() {
        return number;
    }
}