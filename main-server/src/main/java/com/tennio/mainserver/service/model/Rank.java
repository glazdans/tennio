package com.tennio.mainserver.service.model;

import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.security.model.User;

import java.io.Serializable;

public class Rank implements Serializable {
    long elo;
    Player player;
    long won;
    long lost;
    String username;

    public Rank(long elo, Player player, User user) {
        this.elo = elo;
        this.player = player;
        this.username = user.getUsername();
        this.lost = user.getLostGameCount();
        this.won = user.getWonGameCount();
    }

    public long getElo() {
        return elo;
    }

    public Player getPlayer() {
        return player;
    }

    public String getUsername() {
        return username;
    }

    public long getWon() {
        return won;
    }

    public long getLost() {
        return lost;
    }

    @Override
    public String toString() {
        return "Rank{" +
                "elo=" + elo +
                ", player=" + player +
                ", won=" + won +
                ", lost=" + lost +
                ", username='" + username + '\'' +
                '}';
    }
}
