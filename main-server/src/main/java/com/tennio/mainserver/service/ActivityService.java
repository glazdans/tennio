package com.tennio.mainserver.service;

import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.repository.GameEventRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class ActivityService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private GameEventRepository gameEventRepository;

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public NotificationService getNotificationService() {
        return notificationService;
    }

    @CachePut(value = "usersByUsername", key = "#user.getUsername()")
    @CacheEvict(value = "usersByPlayers", allEntries = true)
    public User decrementBalls(User user) {
        userRepository.decrementBalls(user);
        notificationService.sendBallsCountUpdate(user);
        return user;
    }

    @Caching(evict = {@CacheEvict(value = "usersByUsername", allEntries = true), @CacheEvict(value = "usersByPlayers", allEntries = true)})
    public void incrementBalls() {
        userRepository.incrementBalls();
        notificationService.sendBallsIncrement();
    }

    public void refundBall(User user) {
        user.setBalls((short) (user.getBalls() + 1));
        userRepository.save(user);
        notificationService.sendBallsCountUpdate(user);
    }

    public void addMoney(Player winner, Player loser, long winnerGains, long loserGains) {
        List<User> users = Arrays.asList(winner.getUser(), loser.getUser());
        users.get(0).setMoney(users.get(0).getMoney() + winnerGains);
        users.get(1).setMoney(users.get(1).getMoney() + loserGains);

        userRepository.save(users.get(0));
        userRepository.save(users.get(1));

        notificationService.sendMoneyUpdate(users);
    }

    public void addMoney(User user, long money) {
        user.setMoney(user.getMoney() + money);
        userRepository.save(user);
    }

    public GameEventRepository getGameEventRepository() {
        return gameEventRepository;
    }
}
