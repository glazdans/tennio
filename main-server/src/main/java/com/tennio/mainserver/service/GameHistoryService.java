package com.tennio.mainserver.service;

import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.security.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameHistoryService extends TService<GameEvent> {
    private ActivityService activityService;

    @Autowired
    public GameHistoryService(ActivityService activityService) {
        this.repository = activityService.getGameEventRepository();
        this.activityService = activityService;
    }

    public List<GameEvent> getUserFinishedGameHistory(String username, boolean recent) {
        User user = activityService.getUserRepository().findByUsername(username);
        List<Player> players = user.getPlayers();
        if (recent) {
            return getRecentGameHistory(players, username);
        } else {
            return getGameHistory(players, username);
        }
    }

    @Cacheable(cacheNames = {"allGameHistory"}, key = "#username") //no clue why we cant cache the upper method
    public List<GameEvent> getGameHistory(List<Player> players, String username) {
        return activityService.getGameEventRepository().findAllNonActiveGamesByPlayers(players);
    }

    @Cacheable(cacheNames = {"recentGameHistory"}, key = "#username") //no clue why we cant cache the upper method
    public List<GameEvent> getRecentGameHistory(List<Player> players, String username) {
        return activityService.getGameEventRepository().findRecentNonActiveGamesByPlayers(players);
    }
}
