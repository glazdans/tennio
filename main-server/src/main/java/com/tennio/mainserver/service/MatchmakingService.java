package com.tennio.mainserver.service;

import com.tennio.common.GameType;
import com.tennio.mainserver.TennioException;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.security.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class MatchmakingService {
    private Map<Long, Player> currentPlayersInQueue;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private MatchupService matchupService;

    @PostConstruct
    public void init() {
        currentPlayersInQueue = new HashMap<>();
    }

    public void addToQueue(Player player) {
        if (!currentPlayersInQueue.containsValue(player)) {
            currentPlayersInQueue.put(player.getId(), player);
            notificationService.sendMMPlayerCount(currentPlayersInQueue.size());
        }
        if (currentPlayersInQueue.size() >= 2) {
            List<Player> players;
            if (currentPlayersInQueue.size() == 2) {
                Iterator<Map.Entry<Long, Player>> i = currentPlayersInQueue.entrySet().iterator();
                players = Arrays.asList(i.next().getValue(), i.next().getValue());
            } else {
                Player opponent = getPlayerWithClosestElo(player);
                if (opponent == null) {
                    throw new TennioException("Couldn't get opponent player");
                }
                players = Arrays.asList(player, opponent);
            }

            matchupService.newMatchup(players, GameType.Matchmaking);

            currentPlayersInQueue.remove(players.get(0).getId());
            currentPlayersInQueue.remove(players.get(1).getId());
        }
    }

    private Player getPlayerWithClosestElo(Player player) {
        Player closestPlayer = null;
        Long closestAbsEloDifference = Long.MAX_VALUE;
        Long playerElo = player.getPlayerSeasonList().get(player.getPlayerSeasonList().size() - 1).getElo();

        for (Player opponent : currentPlayersInQueue.values()) {
            if (!Objects.equals(opponent.getId(), player.getId())) {
                Long opponentElo = opponent.getPlayerSeasonList().get(opponent.getPlayerSeasonList().size() - 1).getElo();
                Long eloDiff = Math.abs(playerElo - opponentElo);
                if (eloDiff < closestAbsEloDifference) {
                    closestAbsEloDifference = eloDiff;
                    closestPlayer = opponent;
                }
            }
        }

        return closestPlayer;
    }

    public boolean isUserInQueue(User user) {
        boolean result = false;
        for (Player player :
                user.getPlayers()) {
            result = currentPlayersInQueue.get(player.getId()) != null;
            if (result) {
                break;
            }
        }
        return result;
    }

    public void removeFromQueue(Player player) {
        currentPlayersInQueue.remove(player.getId());
        notificationService.sendMMPlayerCount(currentPlayersInQueue.size());
    }

    public MatchupService getMatchupService() {
        return matchupService;
    }
}
