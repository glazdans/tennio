package com.tennio.mainserver.service;

import com.tennio.common.GameType;
import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.domain.Tournament;
import com.tennio.mainserver.domain.websocket.MatchupDecisionPrincipal;
import com.tennio.mainserver.repository.TournamentRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.service.exception.GenericErrorException;
import com.tennio.mainserver.service.exception.ServerNotAvailableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MatchupService {
    private HashMap<String, GameMatchup> matchups = new HashMap<>();

    @Autowired
    private MatchmakingService matchmakingService;

    @Autowired
    private GameService gameService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private TournamentRepository tournamentRepository;

    private final static Map<GameType, Long> MAX_RESPONSE_TIME = new HashMap<GameType, Long>() {{
        put(GameType.Matchmaking, 15000L);
        put(GameType.Tournament, 30000L);
        put(GameType.AI, 0L);
    }};

    public void setMatchupDecision(MatchupDecisionPrincipal principal, User user) {
        this.setMatchupDecision(principal.playerId, principal.matchupId, principal.decision, user);
    }

    public void setMatchupDecision(Long playerId, String matchupId, boolean decision, User user) {
        if (user.getPlayerById(playerId) == null) {
            return;
        }
        GameMatchup matchup = this.matchups.get(matchupId);
        if (matchup != null) {
            if (Objects.equals(matchup.player1.getId(), playerId)) {
                matchup.player1Accepted = decision;
                if (!decision) {
                    notificationService.sendMatchupOpponentDeclinedNotification(matchup.player2.getUser());
                }
            }
            if (Objects.equals(matchup.player2.getId(), playerId)) {
                matchup.player2Accepted = decision;
                if (!decision) {
                    notificationService.sendMatchupOpponentDeclinedNotification(matchup.player1.getUser());
                }
            }
            this.onPlayerResponse(matchup, matchupId);
        }
    }

    private void onDeclineResponse(GameMatchup matchup, Player undeclinedPlayer) {
        switch (matchup.getGameType()) {
            case Matchmaking:
                matchmakingService.addToQueue(undeclinedPlayer);
                break;
            case Tournament:
                notificationService.sendMatchupOpponentDeclinedNotification(undeclinedPlayer.getUser());
                break;
        }
    }

    private void onPlayerResponse(GameMatchup matchup, String matchupId) {
        if (matchup.player1Accepted != null && matchup.player1Accepted.equals(false)) {
            if (matchup.player2Accepted == null || !matchup.player2Accepted.equals(false)) {
                onDeclineResponse(matchup, matchup.player2);
            }
            this.matchups.remove(matchupId);
            return;
        }

        if (matchup.player2Accepted != null && matchup.player2Accepted.equals(false)) {
            if (matchup.player1Accepted == null || !matchup.player1Accepted.equals(false)) {
                onDeclineResponse(matchup, matchup.player1);
            }
            this.matchups.remove(matchupId);
            return;
        }

        if (matchup.player1Accepted == null || matchup.player2Accepted == null) {
            return;
        }

        if (matchup.player1Accepted && matchup.player2Accepted) {
            GameEvent gameEvent;
            switch (matchup.getGameType()) {
                case Matchmaking:
                    gameEvent = gameService.createMatchmakingGame(matchup.player1, matchup.player2);
                    break;
                case Tournament:
                    gameEvent = this.gameService.createTournamentGame(matchup.player1, matchup.player2, Long.parseLong(matchup.getId().replace("tournament#", "")));
                    break;
                default:
                    return;
            }
            try {
                gameService.initializeGame(gameEvent.getId(), false);
            } catch (ServerNotAvailableException | GenericErrorException | RestClientException e) {
                gameService.deleteEvent(gameEvent);
                notificationService.sendMatchupFail(matchup.player1.getUser());
                notificationService.sendMatchupFail(matchup.player2.getUser());
                this.matchups.remove(matchupId);
                return;
            }

            this.onGameCreated(matchup, gameEvent);
        }
    }

    private void onGameCreated(GameMatchup matchup, GameEvent gameEvent) {
        Date currentTime = new Date();

        for (Player curPlayer : Arrays.asList(matchup.player1, matchup.player2)) {
            switch (matchup.getGameType()) {
                case Matchmaking:
                    notificationService.sendMMGameNotification(curPlayer, currentTime, gameEvent);
                    break;
                case Tournament:
                    long tournamentId = Long.parseLong(matchup.getId().replace("tournament#", ""));
                    Tournament tournament = tournamentRepository.findOne(tournamentId);
                    notificationService.sendTournamentGameNotifications(matchup.player1.getUser(), matchup.player2.getUser(), gameEvent, tournament);
                    break;
            }
        }

        this.matchups.remove(matchup.getId());
    }

    private static long getGameTypeMaxResponseTime(GameType gameType) {
        return MAX_RESPONSE_TIME.get(gameType);
    }

    @Scheduled(fixedRate = 1000)
    private void closeUnacceptedMatchups() {
        Long time = System.currentTimeMillis();
        matchups.entrySet().stream()
                .filter(matchupEntry -> time - matchupEntry.getValue().matchupTime >= getGameTypeMaxResponseTime(matchupEntry.getValue().getGameType()))
                .collect(Collectors.toList()) // collect in a new list so we can remove the current element from the original
                .forEach(matchupEntry -> {
                    GameMatchup matchup = matchupEntry.getValue();

                    Set<Player> acceptedPlayers = new HashSet<>();
                    Set<Player> failedToAccept = new HashSet<>();

                    if (matchup.player1Accepted != null && matchup.player1Accepted) {
                        acceptedPlayers.add(matchup.player1);
                    }
                    if (matchup.player2Accepted != null && matchup.player2Accepted) {
                        acceptedPlayers.add(matchup.player2);
                    }

                    acceptedPlayers.forEach(player -> {
                        onDeclineResponse(matchup, player);
                        if (matchup.getGameType().equals(GameType.Matchmaking)) {
                            notificationService.sendMatchupOpponentDeclinedNotification(player.getUser());
                        }
                    });

                    if (matchup.player1Accepted == null && !acceptedPlayers.contains(matchup.player1)) {
                        failedToAccept.add(matchup.player1);
                    }
                    if (matchup.player2Accepted == null && !acceptedPlayers.contains(matchup.player2)) {
                        failedToAccept.add(matchup.player2);
                    }

                    failedToAccept.forEach(player -> notificationService.sendMatchupExpiredNotification(player.getUser(), matchupEntry.getKey()));
                    this.matchups.remove(matchupEntry.getKey());
                });
    }

    private void sendMatchupNotification(GameMatchup matchup) {
        long time = MAX_RESPONSE_TIME.get(matchup.gameType) + 500 + matchup.matchupTime;
        switch (matchup.getGameType()) {
            case Matchmaking:
                notificationService.sendMatchupNotification(matchup.player1, matchup.getId(), time);
                notificationService.sendMatchupNotification(matchup.player2, matchup.getId(), time);
                break;
            case Tournament:
                notificationService.sendMatchupNotification(matchup.player2, matchup.getId(), time);
                break;
        }
    }

    public String newMatchup(List<Player> players, GameType gameType) {
        String matchupID = java.util.UUID.randomUUID().toString();
        return newMatchup(matchupID, players, gameType);
    }

    public String newMatchup(String id, List<Player> players, GameType gameType) {
        long matchupTime = new Date().getTime();
        GameMatchup matchup = new GameMatchup(id, players, matchupTime, gameType);
        if (matchup.getGameType().equals(GameType.Tournament)) {
            matchup.player1Accepted = true;
        }
        this.sendMatchupNotification(matchup);
        this.matchups.put(id, matchup);

        return id;
    }

    class GameMatchup {
        private String id;
        private Player player1;
        private Player player2;
        private Boolean player1Accepted = null;
        private Boolean player2Accepted = null;
        private long matchupTime;
        private GameType gameType;

        public GameMatchup(String id, List<Player> players, long matchupTime, GameType gameType) {
            this.id = id;
            this.player1 = players.get(0);
            this.player2 = players.get(1);
            this.matchupTime = matchupTime;
            this.gameType = gameType;
        }

        public boolean isPlayer1Accepted() {
            return player1Accepted;
        }

        public void setPlayer1Accepted(boolean player1Accepted) {
            this.player1Accepted = player1Accepted;
        }

        public boolean isPlayer2Accepted() {
            return player2Accepted;
        }

        public void setPlayer2Accepted(boolean player2Accepted) {
            this.player2Accepted = player2Accepted;
        }

        public long getMatchupTime() {
            return matchupTime;
        }

        public GameType getGameType() {
            return gameType;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
