package com.tennio.mainserver;

/**
 * Created by PC on 2016.08.17..
 */
public class TennioException extends RuntimeException {
    public TennioException(String message) {
        super(message);
    }

    public TennioException() {
    }
}
