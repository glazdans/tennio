package com.tennio.mainserver.security.model;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN, ROLE_SERVER
}