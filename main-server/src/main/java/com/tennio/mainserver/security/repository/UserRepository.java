package com.tennio.mainserver.security.repository;

import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.security.model.User;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.access.method.P;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

public interface UserRepository extends JpaRepository<User,Long> {
    Long countByUsername(String username);
    Long countByEmail(String email);

    @Cacheable(value = "usersByUsername")
    User findByUsername(String username);

    User findByEmail(String email);


    @Query("SELECT u FROM User u JOIN u.players p WHERE ?1 in p")
    @Cacheable(value = "usersByPlayer", condition = "#player != null", key = "#player.getId()")
    User findByPlayer(@P("player") Player player);

    @CachePut(value = "usersByUsername", key = "#result.getUsername()")
    User save(User var1);

    @Modifying
    @Transactional
    @Query("UPDATE User P SET P.balls = P.balls + 1 WHERE P.balls < 5")
    void incrementBalls();

    @Modifying
    @Transactional
    @Query("UPDATE User P SET P.balls = P.balls - 1 WHERE P = ?1")
    void decrementBalls(User user);

    @Query(value = "SELECT country_code, COUNT(*) AS cnt FROM users WHERE country_code IS NOT NULL GROUP BY country_code ORDER BY cnt DESC", nativeQuery = true)
    List<CountryUserCount> getCountriesByCount();

    public class CountryUserCount implements Serializable {
        public long country_code;
        public long cnt;
    }
}
