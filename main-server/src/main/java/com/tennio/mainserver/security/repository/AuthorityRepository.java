package com.tennio.mainserver.security.repository;

import com.tennio.mainserver.security.model.Authority;
import com.tennio.mainserver.security.model.AuthorityName;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority,Long> {
    public static final String KEY = "authorities";

    @Cacheable(cacheNames = {"authorities"}, key = "#root.target.KEY")
    Authority findByName(AuthorityName authorityName);
}
