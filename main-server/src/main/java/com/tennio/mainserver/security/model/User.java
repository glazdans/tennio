package com.tennio.mainserver.security.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tennio.mainserver.domain.AchievementProgress;
import com.tennio.mainserver.domain.Country;
import com.tennio.mainserver.domain.ItemEntity;
import com.tennio.mainserver.domain.Player;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "user_seq", allocationSize = 1)
    private Long id;

    @Column(name = "USERNAME", length = 50, unique = true)
    @NotNull
    @Size(min = 4, max = 50)
    private String username;

    @JsonIgnore
    @Column(name = "PASSWORD", length = 100)
    @NotNull
    @Size(min = 4, max = 100)
    private String password;

    @Column(name = "FIRSTNAME", length = 50)
    @Size(min = 4, max = 50)
    private String firstname;

    @Column(name = "LASTNAME", length = 50)
    @Size(min = 4, max = 50)
    private String lastname;

    @Column(name = "EMAIL", length = 256)
    private String email;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_code")
    private Country country;

    @Column(name = "ENABLED")
    @NotNull
    private Boolean enabled;

    @Column(nullable = false)
    private Long money;

    @Column(name = "balls")
    private Short balls = 5;

    @JsonIgnore
    @Column(name = "LASTPASSWORDRESETDATE")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date lastPasswordResetDate;

    @OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(
            name = "USER_PLAYERS",
            joinColumns = {@JoinColumn(table = "user_players", name = "user_id", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(table = "user_players", name = "players", referencedColumnName = "ID")})
    private List<Player> players;

    @Column(name = "ACTIVE_PLAYER")
    private Long activePlayerId;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "USER_AUTHORITY",
            joinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "AUTHORITY_ID", referencedColumnName = "ID")})
    private List<Authority> authorities;

    @OneToMany(fetch = FetchType.EAGER)
    private List<ItemEntity> items;

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private List<AchievementProgress> achievements;

    @Column(name = "won_games")
    private Integer wonGameCount = 0;

    @Column(name = "lost_games")
    private Integer lostGameCount = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(Date lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", enabled=" + enabled +
                ", lastPasswordResetDate=" + lastPasswordResetDate +
                ", authorities=" + authorities +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return id != null ? id.equals(user.id) : user.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public Short getBalls() {
        return balls;
    }

    public void setBalls(Short balls) {
        this.balls = balls;
    }

    public Player getActivePlayer() {
        if (activePlayerId == null && !this.players.isEmpty()) {
            return null;
        }

        return this.getPlayerById(activePlayerId);
    }

    public Player getPlayerById(Long id) {
        for (Player player :
                this.players) {
            if (Objects.equals(player.getId(), id)) {
                return player;
            }
        }
        return null;
    }

    public void setActivePlayerId(Long id) {
        this.activePlayerId = id;
    }

    public List<ItemEntity> getItems() {
        return items;
    }

    public void setItems(List<ItemEntity> items) {
        this.items = items;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<AchievementProgress> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<AchievementProgress> achievements) {
        this.achievements = achievements;
    }

    public Integer getLostGameCount() {
        return lostGameCount;
    }

    public void setLostGameCount(Integer lostGameCount) {
        this.lostGameCount = lostGameCount;
    }

    public Integer getWonGameCount() {
        return wonGameCount;
    }

    public void setWonGameCount(Integer wonGameCount) {
        this.wonGameCount = wonGameCount;
    }
}