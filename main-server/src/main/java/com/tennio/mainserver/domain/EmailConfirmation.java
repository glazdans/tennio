package com.tennio.mainserver.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tennio.mainserver.security.model.User;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "email_confirmation")
public class EmailConfirmation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "email_confirmation_seq")
    @SequenceGenerator(name = "email_confirmation_seq", sequenceName = "email_confirmation_seq", allocationSize = 1)
    private Long id;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "confirmation_string", nullable = false)
    private String confirmationString;

    @Column(name = "confirmation_type", nullable = false)
    private ConfirmationType type;

    @Column(name = "time_sent", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date whenSent;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public EmailConfirmation() {
    }

    public ConfirmationType getType() {
        return type;
    }

    public void setType(ConfirmationType type) {
        this.type = type;
    }

    public String getConfirmationString() {
        return confirmationString;
    }

    public void setConfirmationString(String confirmationString) {
        this.confirmationString = confirmationString;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getWhenSent() {
        return whenSent;
    }

    public void setWhenSent(Date whenSent) {
        this.whenSent = whenSent;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public enum ConfirmationType {
        NEW_EMAIL, PASSWORD_RESET, CHANGED_EMAIL
    }
}
