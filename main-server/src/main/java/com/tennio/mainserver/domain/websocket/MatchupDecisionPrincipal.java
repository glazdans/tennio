package com.tennio.mainserver.domain.websocket;

public class MatchupDecisionPrincipal {
    public long playerId;
    public String matchupId;
    public boolean decision;
}