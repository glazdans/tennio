package com.tennio.mainserver.domain.websocket;

public class GameFoundEvent {
    private String message;
    private Long gameId;

    public GameFoundEvent() {
    }

    public GameFoundEvent(String message, Long gameId) {
        this.message = message;
        this.gameId = gameId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }
}
