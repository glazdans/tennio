package com.tennio.mainserver.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * JSON example
 * {"id":null,   "code":"ARM_STRENGTH_PRACTICE",   "length":30,   "statIncrease": {"armStrength":100} }
 */

@Entity
@Table(name = "trainings", uniqueConstraints = {@UniqueConstraint(columnNames = "CODE")})
public class Training implements Serializable {
    public enum TrainingType {
        FREE, PAID
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "CODE", nullable = false, unique = true)
    private String code;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE, optional = false)
    private Stats statIncrease;

    @Column(nullable = false)
    private Long length;

    @Enumerated(EnumType.STRING)
    private TrainingType trainingType;

    private Long cost;

    @Column(name = "EXP_DELTA")
    private float expDelta;

    public Training() {
    }

    public Training(String code, Long length) {
        this.code = code;
        this.length = length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Training training = (Training) o;

        if (id != null ? !id.equals(training.id) : training.id != null) return false;
        if (code != null ? !code.equals(training.code) : training.code != null) return false;
        if (statIncrease != null ? !statIncrease.equals(training.statIncrease) : training.statIncrease != null)
            return false;
        return !(length != null ? !length.equals(training.length) : training.length != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (statIncrease != null ? statIncrease.hashCode() : 0);
        result = 31 * result + (length != null ? length.hashCode() : 0);
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Stats getStatIncrease() {
        return statIncrease;
    }

    public void setStatIncrease(Stats statIncrease) {
        this.statIncrease = statIncrease;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public TrainingType getTrainingType() {
        return trainingType;
    }

    public void setTrainingType(TrainingType trainingType) {
        this.trainingType = trainingType;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public float getExpDelta() {
        return expDelta;
    }

    public void setExpDelta(float expDelta) {
        this.expDelta = expDelta;
    }

}
