package com.tennio.mainserver.domain;

public class TrainingFinishEvent {
    private Stats statIncrease;

    public TrainingFinishEvent(Stats statIncrease) {
        this.statIncrease = statIncrease;
    }

    public Stats getStatIncrease() {
        return statIncrease;
    }

    public void setStatIncrease(Stats statIncrease) {
        this.statIncrease = statIncrease;
    }
}
