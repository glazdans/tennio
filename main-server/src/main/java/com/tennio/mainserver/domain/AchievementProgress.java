package com.tennio.mainserver.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tennio.mainserver.security.model.User;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "achievement_progress")
public class AchievementProgress implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "achievement_progress_seq")
    @SequenceGenerator(name = "achievement_progress_seq", sequenceName = "achievement_progress_seq", allocationSize = 1)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="achievement_type_id")
    private Achievement achievementType;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    private User user;

    @Column(name="progress")
    private Integer progress = 0;

    @Column(name="finished")
    private Boolean finished = false;

    public AchievementProgress() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Achievement getAchievementType() {
        return achievementType;
    }

    public void setAchievementType(Achievement achievementType) {
        this.achievementType = achievementType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    @Override
    public String toString() {
        return "AchievementProgress{" +
                "id=" + id +
                ", achievementType=" + achievementType +
                ", user=" + user +
                ", progress=" + progress +
                '}';
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }
}
