package com.tennio.mainserver.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.tennio.common.GameType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@JsonAutoDetect
@Table(name = "game_events")
public class GameEvent implements Serializable {
    public enum GameStatus {
        WAITING, STARTED, ENDED, PLAYER1_SURRENDERED, PLAYER2_SURRENDERED
    }

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    private Player player1;

    @ManyToOne(fetch = FetchType.EAGER)
    private Player player2;

    private GameStatus status;

    @OneToOne(cascade = CascadeType.ALL)
    private Score player1Score;

    @OneToOne(cascade = CascadeType.ALL)
    private Score player2Score;

    @Basic(optional = true)
    @Column(name = "gameStart", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date gameStart;

    @Column(name = "game_type")
    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private GameType gameType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public Score getPlayer1Score() {
        return player1Score;
    }

    public Player getWinningPlayer() {
        try {
            if (player1Score.getMatches() > player2Score.getMatches()) {
                return player1;
            } else {
                return player2;
            }
        } catch (NullPointerException w) {
            return null;
        }
    }

    public Player getLosingPlayer() {
        try {
            if (player1Score.getMatches() < player2Score.getMatches()) {
                return player1;
            } else {
                return player2;
            }
        } catch (NullPointerException w) {
            return null;
        }
    }

    public void setPlayer1Score(Score player1Score) {
        this.player1Score = player1Score;
    }

    public Score getPlayer2Score() {
        return player2Score;
    }

    public void setPlayer2Score(Score player2Score) {
        this.player2Score = player2Score;
    }

    public Date getGameStart() {
        return gameStart;
    }

    public void setGameStart(Date gameStart) {
        this.gameStart = gameStart;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameEvent gameEvent = (GameEvent) o;

        return !(id != null ? !id.equals(gameEvent.id) : gameEvent.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Entity
    public static class Score {
        @Id
        @GeneratedValue
        private Long id;
        @Column(name="in_game")
        private Integer games;
        @Column(name="in_set")
        private Integer sets;
        @Column(name="in_match")
        private Integer matches;

        public Score(Integer games, Integer sets, Integer matches) {
            this.games = games;
            this.sets = sets;
            this.matches = matches;
        }

        public Score() {
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Score score = (Score) o;

            if (id != null ? !id.equals(score.id) : score.id != null) return false;
            if (games != null ? !games.equals(score.games) : score.games != null) return false;
            if (sets != null ? !sets.equals(score.sets) : score.sets != null) return false;
            return matches != null ? matches.equals(score.matches) : score.matches == null;
        }

        @Override
        public int hashCode() {
            int result = id != null ? id.hashCode() : 0;
            result = 31 * result + (games != null ? games.hashCode() : 0);
            result = 31 * result + (sets != null ? sets.hashCode() : 0);
            result = 31 * result + (matches != null ? matches.hashCode() : 0);
            return result;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Integer getGames() {
            return games;
        }

        public void setGames(Integer games) {
            this.games = games;
        }

        public Integer getSets() {
            return sets;
        }

        public void setSets(Integer sets) {
            this.sets = sets;
        }

        public Integer getMatches() {
            return matches;
        }

        public void setMatches(Integer matches) {
            this.matches = matches;
        }
    }
}
