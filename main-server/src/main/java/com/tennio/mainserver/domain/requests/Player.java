package com.tennio.mainserver.domain.requests;

import com.tennio.mainserver.domain.Stats;

public class Player extends com.tennio.common.requests.Player {
    private Stats stats;

    public Player(String name, long id, Stats stats) {
        super(name, id);
        this.stats = stats;
    }

    public Player() {
        super();
    }

    public Player(com.tennio.mainserver.domain.Player player){
        super(player.getName(), player.getId());
        this.stats = player.getStats();
    }

    public Player(com.tennio.mainserver.domain.Player player, String username){
        super(player.getName(), player.getId());
        this.stats = player.getStats();
        this.username = username;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return stats.equals(player.stats);
    }

    @Override
    public int hashCode() {
        return stats.hashCode();
    }
}
