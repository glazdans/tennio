package com.tennio.mainserver.domain.requests;

import javax.validation.constraints.NotNull;

public class UserPasswordChangeRequest {

    @NotNull
    private String password;
    @NotNull
    private String email;
    @NotNull
    private String key;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
