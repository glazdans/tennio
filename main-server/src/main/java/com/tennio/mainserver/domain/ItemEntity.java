package com.tennio.mainserver.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "item_entity")
public class ItemEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_entity_seq")
    @SequenceGenerator(name = "item_entity_seq", sequenceName = "item_entity_seq", allocationSize = 1)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="item_type_id")
    private Item itemType;

    @Column(name="times_used")
    private int timesUsed = 0;

    @Column(name="player_id")
    private Long playerId = null;

    @Column(name="equipment_id")
    private Long equipmentId = null;

    public ItemEntity() {

    }

    public ItemEntity(Item itemType) {
        this.itemType = itemType;
    }

    public Item getItemType() {
        return itemType;
    }

    public void setItemType(Item itemType) {
        this.itemType = itemType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTimesUsed() {
        return timesUsed;
    }

    public void setTimesUsed(int timesUsed) {
        this.timesUsed = timesUsed;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Long getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Long equipmentId) {
        this.equipmentId = equipmentId;
    }
}
