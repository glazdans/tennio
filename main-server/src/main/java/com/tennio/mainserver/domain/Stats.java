package com.tennio.mainserver.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;

@Entity
public class Stats implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    private Long speed;
    private Long backhand;
    private Long forehand;
    private Long server;
    private Long precision;
    private Long armStrength;
    private Long fitness;
    private Long recovery;

    @Transient
    private int speedLVL;
    @Transient
    private int backhandLVL;
    @Transient
    private int forehandLVL;
    @Transient
    private int serverLVL;
    @Transient
    private int precisionLVL;
    @Transient
    private int armStrengthLVL;
    @Transient
    private int fitnessLVL;
    @Transient
    private int recoveryLVL;

    public Stats() {
        this(0L);
    }

    public Stats(Long startingValue) {
        speed = startingValue;
        backhand = startingValue;
        forehand = startingValue;
        server = startingValue;
        precision = startingValue;
        armStrength = startingValue;
        fitness = startingValue;
        recovery = startingValue;
    }

    public Stats addStats(Stats stats) {
        speed += stats.getSpeed();
        backhand += stats.getBackhand();
        forehand += stats.getForehand();
        server += stats.getServer();
        precision += stats.getPrecision();
        armStrength += stats.getArmStrength();
        fitness += stats.getFitness();
        recovery += stats.getRecovery();
        return this;
    }

    public Stats normalizeLevels(double divisor) {
        speedLVL = (int) (speedLVL / divisor);
        backhandLVL = (int) (backhandLVL / divisor);
        forehandLVL = (int) (forehandLVL / divisor);
        serverLVL = (int) (serverLVL / divisor);
        precisionLVL = (int) (precisionLVL / divisor);
        armStrengthLVL = (int) (armStrengthLVL / divisor);
        fitnessLVL = (int) (fitnessLVL / divisor);
        recoveryLVL = (int) (recoveryLVL / divisor);
        return this;
    }

    public Stats clone(Stats stats) {
        speed = stats.getSpeed();
        backhand = stats.getBackhand();
        forehand = stats.getForehand();
        server = stats.getServer();
        precision = stats.getPrecision();
        armStrength = stats.getArmStrength();
        fitness = stats.getFitness();
        recovery = stats.getRecovery();
        return this;
    }

    public Stats addFraction(float multiplier) {
        speed += ((long) (speed * multiplier));
        backhand += ((long) (backhand * multiplier));
        forehand += ((long) (forehand * multiplier));
        server += ((long) (server * multiplier));
        precision += ((long) (precision * multiplier));
        armStrength += ((long) (armStrength * multiplier));
        fitness += ((long) (fitness * multiplier));
        recovery += ((long) (recovery * multiplier));
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }

    public Long getBackhand() {
        return backhand;
    }

    public void setBackhand(Long backhand) {
        this.backhand = backhand;
    }

    public Long getForehand() {
        return forehand;
    }

    public void setForehand(Long forehand) {
        this.forehand = forehand;
    }

    public Long getServer() {
        return server;
    }

    public void setServer(Long server) {
        this.server = server;
    }

    public Long getPrecision() {
        return precision;
    }

    public void setPrecision(Long precision) {
        this.precision = precision;
    }

    public Long getArmStrength() {
        return armStrength;
    }

    public void setArmStrength(Long armStrength) {
        this.armStrength = armStrength;
    }


    public Long getFitness() {
        return fitness;
    }

    public void setFitness(Long fitness) {
        this.fitness = fitness;
    }

    public Long getRecovery() {
        return recovery;
    }

    public void setRecovery(Long recovery) {
        this.recovery = recovery;
    }

    public int getRecoveryLVL() {
        return recoveryLVL;
    }

    public void setRecoveryLVL(int recoveryLVL) {
        this.recoveryLVL = recoveryLVL;
    }

    public int getFitnessLVL() {
        return fitnessLVL;
    }

    public void setFitnessLVL(int fitnessLVL) {
        this.fitnessLVL = fitnessLVL;
    }

    public int getArmStrengthLVL() {
        return armStrengthLVL;
    }

    public void setArmStrengthLVL(int armStrengthLVL) {
        this.armStrengthLVL = armStrengthLVL;
    }

    public int getPrecisionLVL() {
        return precisionLVL;
    }

    public void setPrecisionLVL(int precisionLVL) {
        this.precisionLVL = precisionLVL;
    }

    public int getServerLVL() {
        return serverLVL;
    }

    public void setServerLVL(int serverLVL) {
        this.serverLVL = serverLVL;
    }

    public int getForehandLVL() {
        return forehandLVL;
    }

    public void setForehandLVL(int forehandLVL) {
        this.forehandLVL = forehandLVL;
    }

    public int getBackhandLVL() {
        return backhandLVL;
    }

    public void setBackhandLVL(int backhandLVL) {
        this.backhandLVL = backhandLVL;
    }

    public int getSpeedLVL() {
        return speedLVL;
    }

    public void setSpeedLVL(int speedLVL) {
        this.speedLVL = speedLVL;
    }
}
