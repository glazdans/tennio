package com.tennio.mainserver.domain.requests;

import javax.validation.constraints.NotNull;

public class UserUpdateRequest {

    @NotNull
    private String password;
    private String newPassword = "";
    private String rnewPassword = "";
    private String firstName = "";
    private String lastName = "";
    private String email = "";
    private String countryCode = "";

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRnewPassword() {
        return rnewPassword;
    }

    public void setRnewPassword(String rnewPassword) {
        this.rnewPassword = rnewPassword;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
