package com.tennio.mainserver.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "item")
public class Item implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_seq")
    @SequenceGenerator(name = "item_seq", sequenceName = "item_seq", allocationSize = 1)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE, optional = false)
    private Stats stats;

    @Column(name="name", nullable = false)
    private String name;

    @Column(name="price")
    private int price;

    @Column(name="for_sale")
    private boolean forSale;

    @Column(name="item_type", nullable = false)
    private ItemType type;

    // set only if type == EQUIPABLE
    @Column(name="equipment_type")
    private EquipmentType equipmentType;

    // 1 usage = 1 game, set only if consumable
    @Column(name="usages")
    private int usages;

    public Item() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(EquipmentType equipmentType) {
        this.equipmentType = equipmentType;
    }

    public int getUsages() {
        return usages;
    }

    public void setUsages(int usages) {
        this.usages = usages;
    }

    public boolean isForSale() {
        return forSale;
    }

    public void setForSale(boolean forSale) {
        this.forSale = forSale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return id.equals(item.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", stats=" + stats +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", forSale=" + forSale +
                ", type=" + type +
                ", equipmentType=" + equipmentType +
                ", usages=" + usages +
                '}';
    }

    public enum ItemType {
        EQUIPABLE, CONSUMABLE
    }

    public enum EquipmentType {
        HEAD, TORSO, ARMS, HANDS, LEGS, FEET
    }
}
