package com.tennio.mainserver.domain.requests;

import com.tennio.common.GameType;

public class GameCreationRequest {
    private Player playerRed;
    private Player playerBlue;
    private GameType gameType;

    public GameCreationRequest() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameCreationRequest that = (GameCreationRequest) o;

        if (!playerRed.equals(that.playerRed)) return false;
        return playerBlue.equals(that.playerBlue);
    }

    @Override
    public int hashCode() {
        int result = playerRed.hashCode();
        result = 31 * result;
        result += playerBlue != null ? playerBlue.hashCode() : 0;
        return result;
    }

    public Player getPlayerRed() {
        return playerRed;
    }

    public void setPlayerRed(Player playerRed) {
        this.playerRed = playerRed;
    }

    public void setPlayerRed(com.tennio.mainserver.domain.Player playerRed) {
        this.playerRed = new Player(playerRed);
    }

    public void setPlayerBlue(com.tennio.mainserver.domain.Player playerBlue) {
        this.playerBlue = new Player(playerBlue);
    }

    public Player getPlayerBlue() {
        return playerBlue;
    }

    public void setPlayerBlue(Player playerBlue) {
        this.playerBlue = playerBlue;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }
}
