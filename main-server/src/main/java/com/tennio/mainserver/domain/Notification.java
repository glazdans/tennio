package com.tennio.mainserver.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tennio.mainserver.security.model.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.tennio.mainserver.domain.Notification.NotificationType.Game;

@Entity
public class Notification implements Serializable {
    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", notificationType=" + notificationType +
                ", additionalInfo='" + additionalInfo + '\'' +
                ", sticky=" + sticky +
                ", notificationEventStart=" + notificationEventStart +
                ", tennioUser=" + tennioUser +
                ", notificationAttributes=" + notificationAttributes +
                ", deleteNotification=" + deleteNotification +
                '}';
    }

    /** @implNote Should be mirrored in ts @ web/src/app/component/menu/notification.ts */
    public enum NotificationType {
        Game, TournamentGame, Tournament, Training, AttributeUpdate, Misc, GameResult, Duel, AchievementProgress,
        MatchmakingError, GameMigrated, GameCanceled, Popup, DuelOpponentFound, DuelOpponentDeclined, DuelExpired
    }

    /** @implNote Should be mirrored in ts @ web/src/app/component/menu/notification.ts */
    public enum NotificationAttributeType {
        TournamentId, GameId, TournamentName, OpponentName, TrainingName, Money, TrainingStats, Balls, TrainingEventId,
        ELO, PlayerId, Stats, AchievementProgressId, AchievementProgress, CurrentMatchMakingGame, DuelId, Expires
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notification_seq")
    @SequenceGenerator(name = "notification_seq", sequenceName = "notification_seq", allocationSize = 1)
    private Long id;

    @Basic(optional = false)
    private NotificationType notificationType;
    private String additionalInfo;

    @Basic(optional = true)
    private Boolean sticky;

    @Basic(optional = false)
    @Column(name = "gameStart", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date notificationEventStart;

    @ManyToOne(optional = false)
    @JsonIgnore
    private User tennioUser;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable
    @Column
    @MapKeyColumn
    @MapKeyEnumerated(EnumType.STRING)
    private Map<NotificationAttributeType, String> notificationAttributes;

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private Boolean deleteNotification;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @JsonIgnore
    public User getUser() {
        return tennioUser;
    }

    public void setUser(User user) {
        this.tennioUser = user;
    }

    public Boolean getSticky() {
        return sticky;
    }

    public void setSticky(Boolean sticky) {
        this.sticky = sticky;
    }

    public Date getNotificationEventStart() {
        return notificationEventStart;
    }

    public void setNotificationEventStart(Date notificationEventStart) {
        this.notificationEventStart = notificationEventStart;
    }

    public Boolean getDeleteNotification() {
        return deleteNotification;
    }

    public void setDeleteNotification(Boolean deleteNotification) {
        this.deleteNotification = deleteNotification;
    }

    public Map<NotificationAttributeType, String> getNotificationAttributes() {
        return notificationAttributes;
    }

    public void setNotificationAttributes(Map<NotificationAttributeType, String> notificationAttributes) {
        this.notificationAttributes = notificationAttributes;
    }

    public static class NotificationBuilder {
        private Notification notification;
        Map<Notification.NotificationAttributeType, String> attributeTypeStringMap;
        private Notification.NotificationType defaultType = Game;

        public NotificationBuilder() {
            attributeTypeStringMap = new HashMap<>();
            this.notification = new Notification();
        }

        public NotificationBuilder setUser(User user) {
            this.notification.setUser(user);
            return this;
        }

        public NotificationBuilder setEventStart(Date eventStart) {
            this.notification.setNotificationEventStart(eventStart);
            return this;
        }

        public NotificationBuilder setSticky(boolean sticky) {
            this.notification.setSticky(sticky);
            return this;
        }

        public NotificationBuilder setAdditionalInfo(String additionalInfo) {
            this.notification.setAdditionalInfo(additionalInfo);
            return this;
        }

        public NotificationBuilder setNotificationType(Notification.NotificationType type) {
            this.notification.setNotificationType(type);
            return this;
        }

        public NotificationBuilder putAttribute(NotificationAttributeType type, String value) {
            attributeTypeStringMap.put(type, value);
            return this;
        }

        public NotificationBuilder setDeleteNotification(boolean delete) {
            this.notification.setDeleteNotification(delete);
            return this;
        }

        public Notification build() {
            if (this.notification.getNotificationType() == null) {
                this.setNotificationType(this.defaultType);
            }

            if (this.notification.sticky == null) {
                this.notification.sticky = true;
            }

            notification.setNotificationAttributes(attributeTypeStringMap);
            return notification;
        }
    }
}
