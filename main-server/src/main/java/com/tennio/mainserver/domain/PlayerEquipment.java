package com.tennio.mainserver.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Map;

@Entity
@Table(name="player_equipment")
public class PlayerEquipment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "player_equipment_seq")
    @SequenceGenerator(name = "player_equipment_seq", sequenceName = "player_equipment_seq", allocationSize = 1)
    private Long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "player_id", nullable = false)
    private Player player;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable
    @Column
    @MapKeyColumn
    @MapKeyEnumerated(EnumType.STRING)
    private Map<Item.EquipmentType, ItemEntity> equippedItems;

    @Column(name = "active", nullable = false)
    private boolean active;

    public PlayerEquipment() {
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Map<Item.EquipmentType, ItemEntity> getEquippedItems() {
        return equippedItems;
    }

    public void equipItem(ItemEntity itemEntity){
        this.equippedItems.put(itemEntity.getItemType().getEquipmentType(), itemEntity);
    }

    public void setEquippedItems(Map<Item.EquipmentType, ItemEntity> equippedItems) {
        this.equippedItems = equippedItems;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
