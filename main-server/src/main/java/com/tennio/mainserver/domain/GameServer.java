package com.tennio.mainserver.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class GameServer {
    public enum ServerStatus {
        AVAILABLE, UNAVAILABLE
    }

    public GameServer() {
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    private String ip;

    @Column(nullable = false)
    private ServerStatus serverStatus;

    @Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastResponse;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<GameEvent> currentGames;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameServer that = (GameServer) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (ip != null ? !ip.equals(that.ip) : that.ip != null) return false;
        if (serverStatus != that.serverStatus) return false;
        if (lastResponse != null ? !lastResponse.equals(that.lastResponse) : that.lastResponse != null) return false;
        return !(currentGames != null ? !currentGames.equals(that.currentGames) : that.currentGames != null);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (serverStatus != null ? serverStatus.hashCode() : 0);
        result = 31 * result + (lastResponse != null ? lastResponse.hashCode() : 0);
        result = 31 * result + (currentGames != null ? currentGames.hashCode() : 0);
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public ServerStatus getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(ServerStatus serverStatus) {
        this.serverStatus = serverStatus;
    }

    public Set<GameEvent> getCurrentGames() {
        return currentGames;
    }

    public void setCurrentGames(Set<GameEvent> currentGames) {
        this.currentGames = currentGames;
    }

    public Date getLastResponse() {
        return lastResponse;
    }

    public void setLastResponse(Date lastResponse) {
        this.lastResponse = lastResponse;
    }
}
