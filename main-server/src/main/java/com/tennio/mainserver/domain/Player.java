package com.tennio.mainserver.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tennio.mainserver.security.model.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "players")
public class Player implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE, optional = false)
    private Stats stats;

    @JoinTable(name = "USER_PLAYERS",
            joinColumns = {@JoinColumn(name = "players")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")})
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private User user;

    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    private List<PlayerSeason> playerSeasonList;

    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private List<PlayerEquipment> equipments = new ArrayList<>();

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="country_code")
    private Country country;

    public Player() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return !(id != null ? !id.equals(player.id) : player.id != null);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<PlayerSeason> getPlayerSeasonList() {
        return playerSeasonList;
    }

    public void setPlayerSeasonList(List<PlayerSeason> playerSeasonList) {
        this.playerSeasonList = playerSeasonList;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", stats=" + stats +
                ", user=" + user +
                '}';
    }

    public List<PlayerEquipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(List<PlayerEquipment> equipments) {
        this.equipments = equipments;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
