package com.tennio.mainserver.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tennio.mainserver.serializers.CustomPlayerPositionsSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@Entity
public class Tournament implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Set<Player> players;

    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn()
    @Column()
    @JsonSerialize(using = CustomPlayerPositionsSerializer.class)
    private Map<Player, Long> playerPositions;

    private Long maxPlayers;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JsonIgnore
    private Set<GameEvent> tournamentGames;

    @Column(name = "PRIZE_POOL")
    private int prizePool;

    @Column(name = "START_DATE", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "END_DATE", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Column(name = "type")
    private TournamentType tournamentType;

    @Column(name = "entry_fee")
    private Long entryFee;

    private TournamentPhase tournamentPhase;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public Map<Player, Long> getPlayerPositions() {
        return playerPositions;
    }

    public void setPlayerPositions(Map<Player, Long> playerPositions) {
        this.playerPositions = playerPositions;
    }

    public Long getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Long maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    @JsonIgnore
    public Set<GameEvent> getTournamentGames() {
        return tournamentGames;
    }

    public void setTournamentGames(Set<GameEvent> tournamentGames) {
        this.tournamentGames = tournamentGames;
    }

    public TournamentPhase getTournamentPhase() {
        return tournamentPhase;
    }

    public void setTournamentPhase(TournamentPhase tournamentPhase) {
        this.tournamentPhase = tournamentPhase;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public static class PlayerPosition {
        private Player player;
        private Long position;

        public PlayerPosition() {
        }

        public PlayerPosition(Player player) {
            this.player = player;
        }

        public PlayerPosition(Player player, Long position) {
            this.player = player;
            this.position = position;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PlayerPosition that = (PlayerPosition) o;

            return !(player != null ? !player.equals(that.player) : that.player != null);

        }

        @Override
        public int hashCode() {
            return player != null ? player.hashCode() : 0;
        }

        public Player getPlayer() {
            return player;
        }

        public void setPlayer(Player player) {
            this.player = player;
        }

        public Long getPosition() {
            return position;
        }

        public void setPosition(Long position) {
            this.position = position;
        }
    }

    public int getPrizePool() {
        return prizePool;
    }

    public void setPrizePool(int prizePool) {
        this.prizePool = prizePool;
    }

    public TournamentType getTournamentType() {
        return tournamentType;
    }

    public void setTournamentType(TournamentType tournamentType) {
        this.tournamentType = tournamentType;
    }

    public Long getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(Long entryFee) {
        this.entryFee = entryFee;
    }

    public enum TournamentPhase {
        REGISTER, START, END
    }

    public enum TournamentType {
        NORMAL, QUICKFIRE
    }
}
