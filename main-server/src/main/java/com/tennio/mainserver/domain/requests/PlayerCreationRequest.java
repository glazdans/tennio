package com.tennio.mainserver.domain.requests;

public class PlayerCreationRequest {
    public String name;
    public String countryCode;

    public PlayerCreationRequest() {
    }
}
