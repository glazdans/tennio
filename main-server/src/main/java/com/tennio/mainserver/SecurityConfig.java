package com.tennio.mainserver;

import com.tennio.mainserver.security.jwt.JwtAuthenticationEntryPoint;
import com.tennio.mainserver.security.jwt.JwtAuthenticationTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
        JwtAuthenticationTokenFilter authenticationTokenFilter = new JwtAuthenticationTokenFilter();
        authenticationTokenFilter.setAuthenticationManager(authenticationManagerBean());
        return authenticationTokenFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http    .csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

                .authorizeRequests().antMatchers(HttpMethod.GET, "/stats/frontpage").permitAll().and()
                .authorizeRequests().antMatchers("/login/auth/").permitAll().and()
                .authorizeRequests().antMatchers("/login/refresh/").permitAll().and()
                .authorizeRequests().antMatchers("/user/**").permitAll().and()
                //.authorizeRequests().antMatchers("/tournament/**").permitAll().and()
                //.authorizeRequests().antMatchers("/trainings/**").permitAll().and()
                .authorizeRequests().antMatchers("/server/**").hasRole("SERVER").and()
                .authorizeRequests().antMatchers(HttpMethod.OPTIONS,"/admin/**").permitAll().and()
                .authorizeRequests().antMatchers("/admin/**").hasRole("ADMIN").and()
                .authorizeRequests().antMatchers("/hello/**").permitAll().and()
                .authorizeRequests().antMatchers("/game/add-server").permitAll().and()
                .authorizeRequests().antMatchers("/game/end-game").permitAll().and()
                //Swagger docs
                .authorizeRequests().antMatchers("/swagger-ui.html").permitAll().and()
                .authorizeRequests().antMatchers("/webjars/**").permitAll().and()
                .authorizeRequests().antMatchers("/swagger-resources").permitAll().and()
                .authorizeRequests().antMatchers("/v2/**").permitAll().and()
                .authorizeRequests().antMatchers("/configuration/**").permitAll().and()
                .authorizeRequests().antMatchers(HttpMethod.OPTIONS,"/**").permitAll() // On rest request to different domain it sends OPTIONS
                .anyRequest().authenticated()
                .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/login/logout")).permitAll().and();

        http.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
        http.headers().cacheControl();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }

}
