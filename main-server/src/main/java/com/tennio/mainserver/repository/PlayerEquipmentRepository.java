package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.PlayerEquipment;
import org.springframework.data.repository.CrudRepository;

public interface PlayerEquipmentRepository extends CrudRepository<PlayerEquipment, Long> {
    //@Cacheable(value = "playerEquipment", key = "#result.getId()")
    PlayerEquipment findOne(Long var1);

    //@CachePut(value = "playerEquipment", key = "#var1.id")
    PlayerEquipment save(PlayerEquipment var1);
}
