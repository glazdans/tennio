package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player,Long> {
}
