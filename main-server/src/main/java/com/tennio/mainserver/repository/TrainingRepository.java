package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.Training;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;

public interface TrainingRepository extends CrudRepository<Training, Long> {
    Long countByCode(String code);

    @Cacheable(value = "trainingsById")
    Training findOne(Long var1);
}
