package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.Tournament;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TournamentRepository extends CrudRepository<Tournament,Long> {
    Tournament findByTournamentGames(GameEvent gameEvent);

    @Query("SELECT u FROM Tournament u WHERE u.tournamentType = 1")
    List<Tournament> findQFTournaments();

    @Query("SELECT count(u) FROM Tournament u WHERE u.tournamentType = 1")
    long findQFTournamentCount();

    @Query("SELECT u FROM Tournament u WHERE u.tournamentType = 0")
    List<Tournament> findNormal();

    @Query("SELECT u FROM Tournament u WHERE u.tournamentType = 1 AND u.tournamentPhase = 2")
    List<Tournament> findEndedQFTournaments();

    @Query("SELECT u FROM Tournament u WHERE u.tournamentType = 1 AND u.tournamentPhase <> 2")
    List<Tournament> findActiveQFTournaments();
}
