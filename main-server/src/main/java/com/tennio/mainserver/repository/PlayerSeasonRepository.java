package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.domain.PlayerSeason;
import com.tennio.mainserver.domain.Season;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerSeasonRepository extends JpaRepository<PlayerSeason, Long> {
    PlayerSeason findBySeasonAndPlayer(Season season, Player player);
    Page<PlayerSeason> getBySeason(Season season, Pageable pageRequest);
}
