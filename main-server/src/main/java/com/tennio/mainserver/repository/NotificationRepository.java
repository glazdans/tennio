package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.Notification;
import com.tennio.mainserver.security.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Map;

public interface NotificationRepository extends JpaRepository<Notification, Long> {
    List<Notification> findByTennioUser(User user);

    @Query("SELECT n FROM Notification n JOIN n.notificationAttributes a " +
            "WHERE n.notificationType IN ?3 AND key(a) = ?1 AND value(a) = ?2")
    List<Notification> findByAttributeAndNotificationTypes(
            Notification.NotificationAttributeType attributeType,
            String attributeValue,
            List<Notification.NotificationType> notificationTypes
    );
}
