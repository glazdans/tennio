package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.EmailConfirmation;
import com.tennio.mainserver.domain.Item;
import com.tennio.mainserver.security.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmailConfirmationRepository extends CrudRepository<EmailConfirmation, Long> {
    EmailConfirmation findByEmailAndConfirmationString(String email, String confirmationString);
    EmailConfirmation findByEmailAndType(String email, EmailConfirmation.ConfirmationType type);
    Long countByEmail(String email);
    List<EmailConfirmation> findByUser(User user);
}
