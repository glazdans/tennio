package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.Achievement;
import com.tennio.mainserver.domain.AchievementProgress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AchievementProgressRepository extends JpaRepository<AchievementProgress, Long> {
    void deleteAllByAchievementType(Achievement achievementType);
}