package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.ItemEntity;
import org.springframework.data.repository.CrudRepository;

public interface ItemEntityRepository extends CrudRepository<ItemEntity, Long> {

}
