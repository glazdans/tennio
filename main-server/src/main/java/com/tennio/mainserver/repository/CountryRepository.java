package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
}