package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.Season;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeasonRepository extends JpaRepository<Season, Long> {
    public static final String KEY = "activeSeason";

    @Cacheable(cacheNames = {"activeSeason"}, key = "#root.target.KEY")
    Season findByEndDateIsNull();
}
