package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.GameServer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface ServerRepository extends CrudRepository<GameServer,Long> {
    GameServer findByIp(String ip);

    GameServer findByCurrentGames(GameEvent gameEvent);

    @Query(value = "SELECT gs.*, COUNT(gg.current_games) FROM game_server gs " +
            "LEFT JOIN  game_server_current_games gg ON gs.id = gg.game_server " +
            "WHERE gs.server_status = 0 " +
            "GROUP BY gs.id, gg.game_server " +
            "ORDER BY COUNT(gg.current_games) ASC " +
            "LIMIT 1",nativeQuery = true)
    GameServer findServerWithLowestGames();

    @Query(value = "SELECT gs FROM GameServer gs " +
            "WHERE gs.serverStatus = 0" +
            "AND gs.lastResponse < ?1")
    List<GameServer> findAvailableByLastResponseLessThan(Date date);

    @Query(value = "SELECT gs.*, COUNT(gg.current_games) FROM game_server gs " +
            "LEFT JOIN game_server_current_games gg ON gs.id = gg.game_server " +
            "WHERE gs.server_status = 1 " +
            "GROUP BY gs.id " +
            "HAVING COUNT(gg.current_games) > 0", nativeQuery = true)
    List<GameServer> findUnavailableWithGames();
}
