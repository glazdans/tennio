package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.Achievement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AchievementRepository extends JpaRepository<Achievement, Long> {
    public Achievement findByName(String name);
}