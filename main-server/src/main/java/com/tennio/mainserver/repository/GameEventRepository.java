package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.domain.Tournament;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GameEventRepository extends JpaRepository<GameEvent,Long> {
    List<GameEvent> findByStatusNot(GameEvent.GameStatus status);

    @Query("SELECT e FROM GameEvent e , Tournament t WHERE (e.player1=?1 OR e.player2 =?1)" +
            " AND e.status < 2 AND e MEMBER OF t.tournamentGames AND t = ?2")
    List<GameEvent> findActiveGamesByPlayerAndTournament(Player player, Tournament tournament);

    @Query("SELECT e FROM GameEvent e WHERE (e.player1=?1 OR e.player2 =?1)" +
            " AND e.status < 2 ")
    List<GameEvent> findActiveGamesByPlayer(Player player);

    @Query("SELECT e FROM GameEvent e WHERE (e.player1 IN (?1) OR e.player2 IN (?1))" +
            " AND e.status < 2 ORDER BY e.gameStart DESC")
    List<GameEvent> findActiveGamesByPlayers(List<Player> players);

    @Query("SELECT e FROM GameEvent e WHERE (e.player1 IN (?1) OR e.player2 IN (?1)) " +
            "AND e.status >= 2 " +
            "ORDER BY e.gameStart DESC")
    List<GameEvent> findNonActiveGamesByPlayers(List<Player> player, Pageable pageable);

    default List<GameEvent> findRecentNonActiveGamesByPlayers(List<Player> player) {
        return this.findNonActiveGamesByPlayers(player, new PageRequest(0, 5));
    }

    default List<GameEvent> findAllNonActiveGamesByPlayers(List<Player> player) {
        return this.findNonActiveGamesByPlayers(player, new PageRequest(0, 150));
    }

    @Caching(evict = {
            @CacheEvict(cacheNames = {"recentGameHistory", "allGameHistory"}, condition = "#result.getPlayer1() != null", key = "#result.getPlayer1().getUser().getUsername()"),
            @CacheEvict(cacheNames = {"recentGameHistory", "allGameHistory"}, condition = "#result.getPlayer2() != null", key = "#result.getPlayer2().getUser().getUsername()")
    })
    GameEvent save(GameEvent var1);
}
