package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.ScheduleEvent;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface ScheduleEventRepository extends CrudRepository<ScheduleEvent,Long>{
    List<ScheduleEvent> findByDateBefore(Date date);
    @Transactional
    Long deleteByEventIdInAndEventTypeIn(Long eventId, ScheduleEvent.EventType eventType);
    ScheduleEvent findByEventIdInAndEventTypeIn(Long eventId, ScheduleEvent.EventType eventType);
}
