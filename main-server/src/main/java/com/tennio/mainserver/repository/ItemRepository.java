package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ItemRepository extends JpaRepository<Item, Long> {
    @Query
    List<Item> findByForSaleTrue();
}
