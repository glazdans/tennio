package com.tennio.mainserver.repository;

import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.domain.TrainingEvent;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TrainingEventsRepository extends CrudRepository<TrainingEvent, Long> {
    @Query("SELECT T FROM TrainingEvent T JOIN T.player P WHERE P in ?1")
    public List<TrainingEvent> findByPlayers(List<Player> players);
}
