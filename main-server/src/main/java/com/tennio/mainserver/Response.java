package com.tennio.mainserver;

/**
 * Response wrapper for data changing requests.
 * @param <T> object to send when the request was legit
 */
public class Response<T> {
    private T object;
    private ResponseCode code;
    private String message = "";

    public Response() {
    }

    public Response(T object) {
        this.object = object;
        this.code = ResponseCode.OK;
    }

    public Response(T object, ResponseCode code) {
        this.object = object;
        this.code = code;
    }

    public Response(T object, ResponseCode code, String message) {
        this.object = object;
        this.code = code;
        this.message = message;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    public ResponseCode getCode() {
        return code;
    }

    public void setCode(ResponseCode code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
