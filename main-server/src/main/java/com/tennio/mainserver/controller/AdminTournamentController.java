package com.tennio.mainserver.controller;

import com.tennio.mainserver.domain.ScheduleEvent;
import com.tennio.mainserver.domain.Tournament;
import com.tennio.mainserver.service.SchedulerService;
import com.tennio.mainserver.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@CrossOrigin
@RequestMapping("/admin/tournament")
public class AdminTournamentController {
    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private SchedulerService schedulerService;

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public Tournament createOrUpdateTournament(@RequestBody TournamentCreation tournamentCreation) {
        if(tournamentCreation.getId() == null) {
            return tournamentService.createTournamentInstance(tournamentCreation.getName(),
                    tournamentCreation.getMaxPlayers(), tournamentCreation.getPrizePool(),
                    tournamentCreation.getStartDate(), tournamentCreation.getEndDate(), Tournament.TournamentType.NORMAL, tournamentCreation.getEntryFee());
        } else {
            return tournamentService.updateTournament(tournamentCreation.getId(), tournamentCreation.getName(),
                    tournamentCreation.getMaxPlayers(), tournamentCreation.getPrizePool(),
                    tournamentCreation.getStartDate(), tournamentCreation.getEndDate(), tournamentCreation.getEntryFee());
        }
    }

    @RequestMapping(path = "/delete/{id}", method = RequestMethod.GET)
    public boolean deleteTournament(@PathVariable Long id) {
        try {
            tournamentService.deleteTournament(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @RequestMapping(path = "/end/{id}", method = RequestMethod.GET)
    public boolean endTournament(@PathVariable Long id) {
        try {
            schedulerService.removeSchedule(ScheduleEvent.EventType.TournamentEnd, id);
            tournamentService.endTournament(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @RequestMapping(path = "/start/{id}", method = RequestMethod.GET)
    public boolean startTournament(@PathVariable Long id) {
        try {
            schedulerService.removeSchedule(ScheduleEvent.EventType.TournamentStart, id);
            tournamentService.startTournament(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    // This class is only used for rest json body parsing
    private static class TournamentCreation {
        private Long id;
        private String name;
        private Long maxPlayers;
        private int prizePool;
        private Date startDate;
        private Date endDate;

        public long getEntryFee() {
            return entryFee;
        }

        public void setEntryFee(long entryFee) {
            this.entryFee = entryFee;
        }

        private long entryFee;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getMaxPlayers() {
            return maxPlayers;
        }

        public void setMaxPlayers(Long maxPlayers) {
            this.maxPlayers = maxPlayers;
        }

        public int getPrizePool() {
            return prizePool;
        }

        public void setPrizePool(int prizePool) {
            this.prizePool = prizePool;
        }

        public Date getStartDate() {
            return startDate;
        }

        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }
}
