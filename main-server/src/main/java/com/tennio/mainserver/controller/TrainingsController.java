package com.tennio.mainserver.controller;

import com.tennio.mainserver.Response;
import com.tennio.mainserver.domain.Training;
import com.tennio.mainserver.domain.TrainingEvent;
import com.tennio.mainserver.domain.TrainingFinishEvent;
import com.tennio.mainserver.service.TrainingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/trainings")
public class TrainingsController {
    @Autowired
    private TrainingsService trainingsService;

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public List<Training> getAllTrainings(){
        return trainingsService.getAllTrainings();
    }

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public List<TrainingEvent> getTrainingEvents(Principal principal){
        String username = principal.getName();
        return trainingsService.getActiveEvents(username);
    }

    @RequestMapping(path = "/find/{id}", method = RequestMethod.GET)
    public TrainingEvent findEvent(@PathVariable Long id){
        return trainingsService.findEvent(id);
    }

    @RequestMapping(path = "/start/{id}", method = RequestMethod.POST)
    public Response<TrainingEvent> startTraining(@PathVariable Long id, Principal principal){
        String name = principal.getName();
        return new Response<>(trainingsService.newTrainingEvent(id, name));
    }

    @RequestMapping(path = "finish/{id}", method = RequestMethod.POST)
    public Response<TrainingFinishEvent> finishTraining(@PathVariable Long id, Principal principal){
        String name = principal.getName();
        return new Response<>(trainingsService.finishTraining(id, name));
    }
}
