package com.tennio.mainserver.controller;

import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/admin/player")
public class AdminPlayerController extends TController<Player> {
    @Autowired
    public AdminPlayerController(PlayerService playerService) {
        this.service = playerService;
    }
}