package com.tennio.mainserver.controller;

import com.tennio.mainserver.domain.GameServer;
import com.tennio.common.requests.ServerInfoRequest;
import com.tennio.mainserver.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.util.Objects;

@RestController
@CrossOrigin
@RequestMapping("/server")
public class ServerController {

    @Autowired
    private ServerService serverService;

    @Value("${tennio.production}")
    private boolean production;

    @RequestMapping(path = "/", method = RequestMethod.PUT)
    public boolean addServer(HttpServletRequest request) {
        try {
            String uri = request.getHeader("Address");
            serverService.addServer(uri);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @RequestMapping(path = "/{gameId}",method = RequestMethod.GET)
    public ServerInfoRequest getServerForGame(@PathVariable Long gameId){
        GameServer gameServer = serverService.serverForGame(gameId);
        ServerInfoRequest serverInfoRequest = new ServerInfoRequest();
        serverInfoRequest.setIp(gameServer.getIp());
        serverInfoRequest.setPort("8081");
        return serverInfoRequest;
    }
}
