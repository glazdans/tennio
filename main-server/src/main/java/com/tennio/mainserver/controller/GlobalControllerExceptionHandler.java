package com.tennio.mainserver.controller;

import com.tennio.mainserver.Response;
import com.tennio.mainserver.ResponseCode;
import com.tennio.mainserver.service.exception.AlreadyExistsException;
import com.tennio.mainserver.service.exception.GenericErrorException;
import com.tennio.mainserver.service.exception.ServerNotAvailableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(value = GenericErrorException.class)
    private Response<?> handleException(GenericErrorException e) {
        return new Response<>(null, ResponseCode.ERROR, e.getMessage());
    }

    @ExceptionHandler(value = AlreadyExistsException.class)
    private Response<?> handleAlreadyExistsException(AlreadyExistsException e) {
        return new Response<>(null, ResponseCode.FOUND, e.getMessage());
    }

    @ExceptionHandler(value = ServerNotAvailableException.class)
    private Response<?> ServerNotAvailableException(AlreadyExistsException e) {
        return new Response<>(null, ResponseCode.ERROR, "No servers available");
    }
}
