package com.tennio.mainserver.controller;

import com.tennio.mainserver.TennioException;
import com.tennio.mainserver.service.TService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

public abstract class TController<T> {
    protected TService<T> service;

    @RequestMapping(
            value = "/",
            params = { "page", "size" },
            method = RequestMethod.GET
    )
    public Page<T> findPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
        Page<T> resultPage = service.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
            throw new TennioException();
        }

        resultPage.getContent();
        return resultPage;
    }
}