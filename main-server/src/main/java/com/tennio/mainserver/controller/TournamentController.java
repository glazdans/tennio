package com.tennio.mainserver.controller;

import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.domain.Tournament;
import com.tennio.mainserver.repository.PlayerRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.security.repository.UserRepository;
import com.tennio.mainserver.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/tournament")
public class TournamentController {

    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public List<Tournament> getTournaments() {
        return tournamentService.getAllTournaments();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Tournament getTournament(@PathVariable Long id) {
        return tournamentService.getTournament(id);
    }

    @RequestMapping(path = "/add-player/{tournamentId}", method = RequestMethod.GET)
    public Tournament addPlayer(@PathVariable Long tournamentId, Principal principal) {
        String username = principal.getName();
        Player player = userRepository.findByUsername(username).getActivePlayer();
        if(player == null){
            return null;
        } else {
            return tournamentService.addPlayerToTournament(tournamentId, player);
        }
    }

    @RequestMapping(path = "/get-opponents/{tournamentId}", method = RequestMethod.GET)
    public List<Player> getOpponents(@PathVariable Long tournamentId, Principal principal) {
        Player player = userRepository.findByUsername(principal.getName()).getActivePlayer();
        return tournamentService.getTournamentOpponents(tournamentId, player);
    }

    @RequestMapping(path = "/challenge/{tournamentId}", method = RequestMethod.POST)
    public boolean challengeOpponent(@PathVariable Long tournamentId,
                                  @RequestParam(value="challengerId") Long challengerId,
                                  @RequestParam(value="opponentId") Long opponentId, Principal principal) {
        User user = userRepository.findByUsername(principal.getName());
        Player player = user.getPlayerById(challengerId);
        Player opponentPlayer = playerRepository.findOne(opponentId);

        return (player != null && opponentPlayer != null) && tournamentService.challenge(player, opponentPlayer, tournamentId);
    }
}
