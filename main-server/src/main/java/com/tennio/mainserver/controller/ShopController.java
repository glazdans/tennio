package com.tennio.mainserver.controller;

import com.tennio.mainserver.domain.Item;
import com.tennio.mainserver.domain.ItemEntity;
import com.tennio.mainserver.security.repository.UserRepository;
import com.tennio.mainserver.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/shop")
public class ShopController {

    @Autowired
    private ShopService shopService;

    @RequestMapping(path = "/items", method = RequestMethod.GET)
    public List<Item> getItems() {
       return shopService.getItemsForSale();
    }

    @RequestMapping(path = "/items/buy/{itemId}", method = RequestMethod.GET)
    public ItemEntity buyItem(@PathVariable Long itemId, Principal principal) {
        return shopService.buyItem(principal.getName(), itemId);
    }
}
