package com.tennio.mainserver.controller;

import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/admin/user")
public class AdminUserController extends TController<User> {
    @Autowired
    public AdminUserController(UserService userService) {
        this.service = userService;
    }
}