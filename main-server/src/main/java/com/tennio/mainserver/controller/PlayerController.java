package com.tennio.mainserver.controller;

import com.tennio.mainserver.Response;
import com.tennio.mainserver.ResponseCode;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.domain.requests.PlayerCreationRequest;
import com.tennio.mainserver.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/player")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @RequestMapping(path = "/create", method = RequestMethod.POST)
    public Response<Player> createPlayer(@RequestBody PlayerCreationRequest request, Principal principal) {
        return new Response<>(playerService.createPlayer(principal.getName(), request.name, request.countryCode));
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public List<Player> getPlayers(Principal principal) {
        return playerService.getPlayers(principal.getName());
    }

    @RequestMapping(path = "/equipment/equip/{itemEntityId}", method = RequestMethod.GET)
    public boolean equip(Principal principal, @PathVariable long itemEntityId) {
        return playerService.equipItem(principal.getName(), itemEntityId);
    }

    @RequestMapping(path = "/equipment/unequip/{itemEntityId}", method = RequestMethod.GET)
    public boolean unequip(Principal principal, @PathVariable long itemEntityId) {
        return playerService.unequipItem(principal.getName(), itemEntityId);
    }
}
