package com.tennio.mainserver.controller;

import com.tennio.mainserver.domain.Item;
import com.tennio.mainserver.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/admin/item")
public class AdminItemController extends TController<Item> {
    @Autowired
    private ShopService shopService;

    @Autowired
    public AdminItemController(ShopService shopService) {
        this.service = shopService;
    }

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public Item createOrUpdateItem(@RequestBody Item item) {
        if(item.getId() == null) {
            return shopService.createNewItem(item);
        } else {
            return shopService.updateItem(item);
        }
    }
}