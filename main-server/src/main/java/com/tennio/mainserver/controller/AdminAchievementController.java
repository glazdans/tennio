package com.tennio.mainserver.controller;

import com.tennio.mainserver.domain.Achievement;
import com.tennio.mainserver.service.AchievementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/admin/achievement")
public class AdminAchievementController extends TController<Achievement> {
    private AchievementService achievementService;

    @Autowired
    public AdminAchievementController(AchievementService achievementService) {
        this.achievementService = achievementService;
        this.service = achievementService;
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void deleteAchievement(@PathVariable Long id) {
        this.achievementService.deleteAchievement(id);
    }

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public Achievement createOrUpdateItem(@RequestBody Achievement achievement) {
        if(achievement.getId() == null) {
            return achievementService.createAchievement(achievement);
        } else {
            return achievementService.updateAchievement(achievement);
        }
    }
}