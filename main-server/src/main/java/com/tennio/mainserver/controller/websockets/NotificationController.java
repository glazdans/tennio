package com.tennio.mainserver.controller.websockets;

import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.Player;
import com.tennio.mainserver.repository.GameEventRepository;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.service.ActivityService;
import com.tennio.mainserver.service.MatchmakingService;
import com.tennio.mainserver.websockets.entity.BasicResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.util.Date;
import java.util.List;

@Controller
public class NotificationController {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private MatchmakingService matchmakingService;

    @Autowired
    private GameEventRepository gameEventRepository;

    @SubscribeMapping("/notifications")
    public UserNotifications getActiveNotifications(Principal principal) {
        UserNotifications data = new UserNotifications();

        User user = activityService.getUserRepository().findByUsername(principal.getName());
        data.notifications = activityService.getNotificationService().getNotifications(user);

        List<GameEvent> gameEvents = gameEventRepository.findActiveGamesByPlayers(user.getPlayers());

        if(!gameEvents.isEmpty()) {
            data.game = gameEvents.get(0);
        }

        data.currentServerTime = new Date().getTime();

        data.inMMQueue = matchmakingService.isUserInQueue(user);

        return data;
        // Active games
        // Upcoming tournaments?
    }

    @SubscribeMapping("/notifications/delete")
    @SendToUser("/notifications/delete")
    public BasicResponse deleteNotification(@Payload(required = false) Long id, Principal principal) {
        if (id == null) {
            return null;
        }
        BasicResponse resp = new BasicResponse();
        resp.id = id;
        User user = activityService.getUserRepository().findByUsername(principal.getName());
        resp.success = activityService.getNotificationService().deleteUserNotification(id, user);
        return resp;
    }

    @SubscribeMapping("/notifications/activeplayer")
    @SendToUser("/notifications/activeplayer")
    public BasicResponse setActivePlayer(@Payload(required = false) Long id, Principal principal) {
        if (id == null) {
            return null;
        }
        BasicResponse resp = new BasicResponse();
        resp.id = id;

        User user = activityService.getUserRepository().findByUsername(principal.getName());

        if(user != null){
            Player player = user.getPlayerById(id);
            if(player == null){
                resp.success = false;
            } else {
                user.setActivePlayerId(id);
                try {
                    activityService.getUserRepository().save(user);
                    resp.success = true;
                } catch (Exception e) {
                    resp.success = false;
                }
            }
        }
        return resp;
    }
}
