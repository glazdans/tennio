package com.tennio.mainserver.controller;

import com.tennio.mainserver.service.StatsService;
import com.tennio.mainserver.service.model.FrontPageStats;
import com.tennio.mainserver.service.model.Ranks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;

@RestController
@CrossOrigin
@RequestMapping("/stats")
public class StatsController {
    @Autowired
    private StatsService statsService;

    @RequestMapping(path = "/frontpage", method = RequestMethod.GET)
    public FrontPageStats frontPageStats() {
        return statsService.getFrontPageStats();
    }

    @RequestMapping(path="/rankings", method = RequestMethod.GET)
    @ResponseBody
    public Ranks rankStats(@RequestParam("page") int page, @RequestParam("size") @Max(50) int size) {
        return statsService.getPlayerRankingPage(page, size);
    }
}