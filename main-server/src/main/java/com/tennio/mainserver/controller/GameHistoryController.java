package com.tennio.mainserver.controller;


import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.service.GameHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/gamehistory")
public class GameHistoryController extends TController<GameEvent> {

    @Autowired
    private GameHistoryService gameHistoryService;

    @Autowired
    public GameHistoryController(GameHistoryService gameHistoryService) {
        this.service = gameHistoryService;
    }

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public List<GameEvent> history(Principal principal) {
        return gameHistoryService.getUserFinishedGameHistory(principal.getName(), false);
    }

    @RequestMapping(path = "/recent", method = RequestMethod.GET)
    public List<GameEvent> recentHistory(Principal principal) {
        return gameHistoryService.getUserFinishedGameHistory(principal.getName(), true);
    }

}
