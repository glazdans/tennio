package com.tennio.mainserver.controller;

import com.tennio.common.requests.GameEndEvent;
import com.tennio.common.requests.ServerInfoRequest;
import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.security.repository.UserRepository;
import com.tennio.mainserver.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/game")
public class GameController {

    @Autowired
    private GameService gameService;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(path = "/events", method = RequestMethod.GET)
    public List<GameEvent> getAllEvents() {
        return gameService.getAllEvents();
    }


    @RequestMapping(path = "/active-events", method = RequestMethod.GET)
    public List<GameEvent> getPlayerActiveGames(Principal principal) {
        return gameService.getPlayerActiveGames(userRepository.findByUsername(principal.getName()).getActivePlayer());
    }

    @RequestMapping(path = "/end-game", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_SERVER')")
    public void endGame(@RequestBody GameEndEvent gameEndEvent) {
        gameService.endGame(gameEndEvent);
    }

    @RequestMapping(path = "/get-game/{gameId}", method = RequestMethod.POST)
    public ServerInfoRequest createInstance(@PathVariable Long gameId) {
        return gameService.initializeGame(gameId, false);
    }

    @RequestMapping(path = "/aigame", method = RequestMethod.GET)
    public ServerInfoRequest startAIGame(Principal principal) {
        return gameService.newAIGame(userRepository.findByUsername(principal.getName()));
    }
}
