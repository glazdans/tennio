package com.tennio.mainserver.controller;

import com.tennio.mainserver.domain.Achievement;
import com.tennio.mainserver.domain.Item;
import com.tennio.mainserver.domain.ItemEntity;
import com.tennio.mainserver.service.AchievementService;
import com.tennio.mainserver.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/achievement")
public class AchievementController {

    @Autowired
    private AchievementService achievementService;

    @RequestMapping(path = "/items", method = RequestMethod.GET)
    public List<Achievement> getItems() {
       return achievementService.getAllAchievements();
    }
}
