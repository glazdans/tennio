package com.tennio.mainserver.controller;

import com.tennio.mainserver.Response;
import com.tennio.mainserver.domain.requests.UserPasswordChangeRequest;
import com.tennio.mainserver.domain.requests.UserRequest;
import com.tennio.mainserver.domain.requests.UserUpdateRequest;
import com.tennio.mainserver.security.model.User;
import com.tennio.mainserver.security.repository.UserRepository;
import com.tennio.mainserver.service.EmailConfirmationService;
import com.tennio.mainserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailConfirmationService confirmationService;

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public Response<Boolean> registerUser(@RequestBody UserRequest userRequest) {
        return new Response<>(userService.registerNewUser(userRequest));
    }

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public User getUser(Principal principal) {
        User user = userRepository.findByUsername(principal.getName());
        return user;
    }

    @RequestMapping(path="/update", method = RequestMethod.POST)
    public Response<User> updateUser(Principal principal, @RequestBody UserUpdateRequest userUpdateRequest) {
        return new Response<>(userService.updateUser(userRepository.findByUsername(principal.getName()), userUpdateRequest));
    }

    @RequestMapping(path="/confirm", method = RequestMethod.GET)
    public Response confirmEmail(@RequestParam("email") String email, @RequestParam("key") String key) {
        return new Response<>(confirmationService.handleConfirmationAttempt(email, key));
    }

    @RequestMapping(path="/resend", method = RequestMethod.GET)
    public Response resendEmail(Principal principal) {
        return new Response<>(confirmationService.handleResendRequest(userRepository.findByUsername(principal.getName())));
    }

    @RequestMapping(path="/reset", method = RequestMethod.GET)
    public boolean resetPassword(@RequestParam("email") String email) {
        userService.handlePasswordResetRequest(email);
        return true;
    }

    @RequestMapping(path="/changepw", method = RequestMethod.POST)
    public boolean changePassword(@RequestBody UserPasswordChangeRequest request) {
        return userService.handlePasswordChangeRequest(request.getEmail(), request.getKey(), request.getPassword()) != null;
    }
}