package com.tennio.mainserver.controller.websockets;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tennio.mainserver.domain.GameEvent;
import com.tennio.mainserver.domain.Notification;

import java.util.ArrayList;
import java.util.List;

class UserNotifications {
    @JsonSerialize
    List<Notification> notifications = new ArrayList<>();
    @JsonSerialize
    GameEvent game = null;
    @JsonSerialize
    boolean inMMQueue = false;
    @JsonSerialize
    long currentServerTime = 0;
}
