package com.tennio.mainserver.controller;

import com.tennio.mainserver.domain.Training;
import com.tennio.mainserver.service.TrainingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/trainings")
@CrossOrigin
public class AdminTrainingsController {

    @Autowired
    private TrainingsService trainingsService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Training saveTraining(@RequestBody Training training) {
        if(training.getId() == null){
            return trainingsService.createTraining(training);
        } else {
            return trainingsService.updateTraining(training);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Long deleteTraining(@PathVariable Long id){
        trainingsService.deleteTraining(id);
        return id;
    }
}
