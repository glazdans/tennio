package com.tennio.mainserver.controller.websockets;

import com.tennio.mainserver.domain.websocket.MatchupDecisionPrincipal;
import com.tennio.mainserver.security.repository.UserRepository;
import com.tennio.mainserver.service.MatchmakingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import java.security.Principal;

@Controller
public class MatchmakingController {
    @Autowired
    private MatchmakingService matchmakingService;

    @Autowired
    private UserRepository userRepository;

    @MessageMapping("/match/join")
    public void addPlayerToQueue(Principal principal){
        matchmakingService.addToQueue(userRepository.findByUsername(principal.getName()).getActivePlayer());
    }

    @MessageMapping("/match/leave")
    public void removePlayerToQueue(Principal principal){
        matchmakingService.removeFromQueue(userRepository.findByUsername(principal.getName()).getActivePlayer());
    }

    @MessageMapping("/match/decide")
    public void acceptMatch(MatchupDecisionPrincipal decisionPrincipal, Principal principal) {
        matchmakingService.getMatchupService().setMatchupDecision(decisionPrincipal, userRepository.findByUsername(principal.getName()));
    }
}
