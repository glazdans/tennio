package com.tennio.mainserver.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.tennio.mainserver.domain.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CustomPlayerPositionsSerializer extends JsonSerializer<Map<Player,Long>> {
    @Override
    public void serialize(Map<Player, Long> playerLongMap, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        List<PlayerPositionsJson> jsons = new ArrayList<>();
        playerLongMap.forEach((player, aLong) -> jsons.add(new PlayerPositionsJson(player,aLong)));
        jsonGenerator.writeObject(jsons);
    }

    private static class PlayerPositionsJson{
        private Player player;
        private Long position;

        public PlayerPositionsJson() {
        }

        public PlayerPositionsJson(Player player, Long position) {
            this.player = player;
            this.position = position;
        }

        public Player getPlayer() {
            return player;
        }

        public void setPlayer(Player player) {
            this.player = player;
        }

        public Long getPosition() {
            return position;
        }

        public void setPosition(Long position) {
            this.position = position;
        }
    }
}
