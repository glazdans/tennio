package com.tennio.mainserver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CacheConfig extends CachingConfigurerSupport {
    @Value("${tennio.cache}")
    private boolean cache;

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private String port;

    private final static long UNLIMITED = 0L;

    private final static Map<String, Long> REGION_EXPIRE = new HashMap<String, Long>() {{
        put("activeSeason", UNLIMITED);
        put("trainingsById", UNLIMITED);
        put("allTrainings", UNLIMITED);
        put("trainingsById", UNLIMITED);
        put("authorities", UNLIMITED);
        put("playerRanks", UNLIMITED);
        put("allTournaments", UNLIMITED);
        put("tournamentsById", UNLIMITED);

        put("usersByPlayer", 1800L);
        put("usersByUsername", 1800L);
        put("recentGameHistory", 1200L);
    }};

    @Bean
    public JedisConnectionFactory redisConnectionFactory() {
        JedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory();

        // Defaults
        redisConnectionFactory.setHostName(host);
        redisConnectionFactory.setPort(Integer.parseInt(port));
        return redisConnectionFactory;
    }

    @Bean
    public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory cf) {
        RedisTemplate<String, String> redisTemplate = new RedisTemplate<String, String>();

        redisTemplate.setConnectionFactory(cf);
        return redisTemplate;
    }

    @Bean
    public CacheManager cacheManager(RedisTemplate redisTemplate) {
        if (cache) {
            RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate);

            // Number of seconds before expiration. Defaults to unlimited (0)
            cacheManager.setDefaultExpiration(300);
            cacheManager.setExpires(REGION_EXPIRE);
            return cacheManager;
        } else {
            return new NoOpCacheManager();
        }
    }


}