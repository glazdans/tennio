ALTER TABLE stats
    DROP COLUMN technique,
    DROP COLUMN mental_strength,
    DROP COLUMN reaction;