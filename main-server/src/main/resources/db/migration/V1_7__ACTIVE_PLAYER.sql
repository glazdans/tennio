ALTER TABLE users
    ADD active_player BIGINT DEFAULT NULL REFERENCES players(id);