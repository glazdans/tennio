ALTER TABLE item_entity
    DROP COLUMN in_use,
    ADD COLUMN player_id BIGINT DEFAULT NULL,
    ADD COLUMN equipment_id BIGINT DEFAULT NULL,
    ADD FOREIGN KEY(player_id) REFERENCES players(id),
    ADD FOREIGN KEY(equipment_id) REFERENCES player_equipment(id);