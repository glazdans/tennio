CREATE SEQUENCE email_confirmation_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

CREATE TABLE email_confirmation
(
    id BIGINT PRIMARY KEY NOT NULL,
    email VARCHAR(255) NOT NULL,
    confirmation_string VARCHAR(255) NOT NULL,
    confirmation_type int NOT NULL,
    time_sent TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    user_id BIGINT NOT NULL REFERENCES users(id)
);

CREATE UNIQUE INDEX ON email_confirmation(email, confirmation_string);

