INSERT INTO authority VALUES (1, 'ROLE_ADMIN');
INSERT INTO authority VALUES (2, 'ROLE_USER');
INSERT INTO authority VALUES (5, 'ROLE_SERVER');

INSERT INTO stats VALUES (167, 100, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0);
INSERT INTO stats VALUES (397, 0, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0);
INSERT INTO stats VALUES (446, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100);
INSERT INTO stats VALUES (2, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO stats VALUES (458, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0);
INSERT INTO stats VALUES (461, 1, 2, 30, 1, 2, 5, 4, 4, 1, 1, 0);
INSERT INTO stats VALUES (92, 200, 1000, 200, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 200);
INSERT INTO stats VALUES (58, 400, 1100, 100, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 200);
INSERT INTO stats VALUES (60, 500, 1000, 800, 400, 0, 500, 80, 700, 1020, 70, 200);
INSERT INTO stats VALUES (15, 200, 1110, 1000, 800, 0, 1000, 1000, 1000, 1000, 1000, 200);
INSERT INTO stats VALUES (492, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 200);
INSERT INTO stats VALUES (4, 800, 901, 700, 700, 700, 700, 700, 800, 900, 701, 800);

INSERT INTO players VALUES (91, 1000, 'Lops', NULL, 92);
INSERT INTO players VALUES (14, 1200, 'Pedro', NULL, 15);
INSERT INTO players VALUES (57, 1800, 'qwerty', NULL, 58);
INSERT INTO players VALUES (59, 1320, 'Lunio', NULL, 60);
INSERT INTO players VALUES (3, 1310, 'Frederick Gonzalez', NULL, 4);

INSERT INTO tournament VALUES (46, NULL, 1, 'Wimbladin Championships');

INSERT INTO tournament_player_positions VALUES (46, 2, 57);
INSERT INTO tournament_player_positions VALUES (46, 3, 3);
INSERT INTO tournament_player_positions VALUES (46, 1, 59);
INSERT INTO tournament_player_positions VALUES (46, 4, 91);
INSERT INTO tournament_player_positions VALUES (46, 5, 14);

INSERT INTO tournament_players VALUES (46, 3);
INSERT INTO tournament_players VALUES (46, 14);
INSERT INTO tournament_players VALUES (46, 57);
INSERT INTO tournament_players VALUES (46, 59);
INSERT INTO tournament_players VALUES (46, 91);

INSERT INTO trainings VALUES (166, 'ARM_STRENGTH_PRACTICE', 30, 167, 'FREE', NULL);
INSERT INTO trainings VALUES (396, 'SPEED_PARCTICE', 1, 397, 'FREE', NULL);
INSERT INTO trainings VALUES (445, 'SPECIAL_ALL_PRACTICE', 10, 446, 'PAID', 50);
INSERT INTO trainings VALUES (1, 'BACK_HAND_PRACTICE', 3, 2, 'FREE', NULL);
INSERT INTO trainings VALUES (457, 'EXPENSIVE_SERVE_PRACTICE', 1, 458, 'PAID', 100);
INSERT INTO trainings VALUES (460, 'USELESS_EXPENSIVE_TEST', 100, 461, 'PAID', 1100);
INSERT INTO trainings VALUES (491, 'RECOVERY_PRACTICE', 2, 492, 'PAID', 50);


INSERT INTO users VALUES (1, 'g.lazdans@gmail.com', true, 'Georgs', '2016-07-06 13:19:16.83', 'Lazdāns', '$2a$10$94q22eDzqRW6FSpnhktBjOj0uBmBvr4lkec6wKflxE4zDfBC/FzMi', 'admin', 3);
INSERT INTO users VALUES (2, 'g.lazdans1@gmail', true, 'Georgs', '2016-07-06 14:52:16.79', 'Lazdāns', '$2a$10$SQjud062OxONxPa7itXQYe7m4swAqP6ymlC7qGjbqSISwAcIm/yYe', 'admin1', 14);
INSERT INTO users VALUES (3, 'qwerty', true, 'qwerty', '2016-07-13 14:19:22.977', 'qwerty', '$2a$10$tGuMkNN8QbedYrFAPHWume/9fRL80JCeVTOdOC9Apw831wHzCBKxC', 'qwerty', 57);
INSERT INTO users VALUES (5, 'q124124', true, 'qwerty123124', '2016-07-13 15:31:15.203', 'q241212', '$2a$10$g.T3phg6iYoxLnY.9X0oneL2h156tALBUMmhwtarVZYmbjB1X/hSi', 'qwerty1', 59);
INSERT INTO users VALUES (6, 'eqweqwe', true, 'qweqw', '2016-07-14 15:41:23.044', 'eqwew', '$2a$10$vz6zUPaN6KhQ7c19eHHwLe/q9pMsLWUcljFXlWvDH8sgNCQAjJCwK', 'qwerty2', 91);
INSERT INTO users VALUES (7, 'DONT_SEND', true, 'Game', '2016-07-21 20:46:13.067', 'Server', '$2a$10$PqJ7d271OolPaS5J2tQibuuK7zp8cFG92Ci8YF1kP.zAxCjQLD7ZC', 'game_server', NULL);

INSERT INTO user_authority VALUES (1, 2);
INSERT INTO user_authority VALUES (1, 1);
INSERT INTO user_authority VALUES (2, 2);
INSERT INTO user_authority VALUES (3, 2);
INSERT INTO user_authority VALUES (5, 2);
INSERT INTO user_authority VALUES (6, 2);
INSERT INTO user_authority VALUES (7, 2);
INSERT INTO user_authority VALUES (7, 5);