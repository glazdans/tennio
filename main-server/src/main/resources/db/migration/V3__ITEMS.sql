CREATE SEQUENCE item_entity_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

CREATE SEQUENCE item_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

CREATE SEQUENCE player_equipment_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

CREATE TABLE item
(
    id BIGINT PRIMARY KEY NOT NULL,
    stats BIGINT NOT NULL REFERENCES stats(id),
    name VARCHAR(255),
    price INT DEFAULT NULL,
    item_type INT NOT NULL,
    equipment_type INT DEFAULT NULL,
    consumable BOOLEAN NOT NULL,
    usages INT DEFAULT NULL
);

CREATE TABLE player_equipment
(
    id BIGINT PRIMARY KEY NOT NULL,
    player_id BIGINT NOT NULL REFERENCES players(id),
    active BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE item_entity
(
    id BIGINT PRIMARY KEY NOT NULL,
    item_type_id BIGINT REFERENCES item(id),
    in_use BOOLEAN DEFAULT false,
    times_used INT DEFAULT 0
);

CREATE TABLE player_equipment_equipped_items
(
    player_equipment BIGINT NOT NULL,
    equipped_items BIGINT NOT NULL REFERENCES item_entity(id),
    equipped_items_key VARCHAR(255) NOT NULL,
    PRIMARY KEY (player_equipment, equipped_items_key)
);

CREATE TABLE users_items
(
  users BIGINT REFERENCES users(id),
  items BIGINT REFERENCES item_entity(id)
);
