CREATE TABLE authority
(
    id BIGINT PRIMARY KEY NOT NULL,
    name VARCHAR(50) NOT NULL
);
CREATE TABLE game_event$score
(
    id BIGINT PRIMARY KEY NOT NULL,
    in_game INT,
    in_match INT,
    in_set INT
);
CREATE TABLE game_events
(
    id BIGINT PRIMARY KEY NOT NULL,
    game_start TIMESTAMP DEFAULT now(),
    status INT,
    player1 BIGINT,
    player1score BIGINT,
    player2 BIGINT,
    player2score BIGINT
);
CREATE TABLE game_server
(
    id BIGINT PRIMARY KEY NOT NULL,
    ip VARCHAR(255) NOT NULL,
    server_status INT NOT NULL,
    last_response TIMESTAMP DEFAULT now()
);
CREATE TABLE game_server_current_games
(
    game_server BIGINT NOT NULL,
    current_games BIGINT NOT NULL,
    PRIMARY KEY (game_server, current_games)
);
CREATE TABLE notification
(
    id BIGINT PRIMARY KEY NOT NULL,
    additional_info VARCHAR(255),
    entity_id BIGINT,
    game_start TIMESTAMP DEFAULT now(),
    notification_type INT NOT NULL,
    sticky BOOL NOT NULL,
    tennio_user BIGINT NOT NULL
);
CREATE TABLE notification_notification_attributes
(
    notification BIGINT NOT NULL,
    notification_attributes VARCHAR(255),
    notification_attributes_key VARCHAR(255) NOT NULL,
    PRIMARY KEY (notification, notification_attributes_key)
);
CREATE TABLE players
(
    id BIGINT PRIMARY KEY NOT NULL,
    money BIGINT NOT NULL,
    name VARCHAR(255) NOT NULL,
    stat BIGINT,
    stats BIGINT NOT NULL
);
CREATE TABLE schedule_event
(
    id BIGINT PRIMARY KEY NOT NULL,
    date TIMESTAMP DEFAULT now() NOT NULL,
    event_id BIGINT NOT NULL,
    event_type INT NOT NULL
);
CREATE TABLE stats
(
    id BIGINT PRIMARY KEY NOT NULL,
    arm_strength BIGINT,
    backhand BIGINT,
    fitness BIGINT,
    forehand BIGINT,
    mental_strength BIGINT,
    precision BIGINT,
    reaction BIGINT,
    server BIGINT,
    speed BIGINT,
    technique BIGINT,
    recovery BIGINT
);
CREATE TABLE tournament
(
    id BIGINT PRIMARY KEY NOT NULL,
    max_players BIGINT,
    tournament_phase INT,
    name VARCHAR(255)
);
CREATE TABLE tournament_player_positions
(
    tournament BIGINT NOT NULL,
    player_positions BIGINT,
    player_positions_key BIGINT NOT NULL,
    PRIMARY KEY (tournament, player_positions_key)
);
CREATE TABLE tournament_players
(
    tournament BIGINT NOT NULL,
    players BIGINT NOT NULL
);
CREATE TABLE tournament_tournament_games
(
    tournament BIGINT NOT NULL,
    tournament_games BIGINT NOT NULL
);
CREATE TABLE training_events
(
    id BIGINT PRIMARY KEY NOT NULL,
    complete_date TIMESTAMP DEFAULT now() NOT NULL,
    start_date TIMESTAMP DEFAULT now(),
    player BIGINT,
    training BIGINT
);
CREATE TABLE trainings
(
    id BIGINT PRIMARY KEY NOT NULL,
    code VARCHAR(255) NOT NULL,
    length BIGINT,
    stat_increase BIGINT NOT NULL,
    training_type VARCHAR(255),
    cost BIGINT
);
CREATE TABLE user_authority
(
    user_id BIGINT NOT NULL,
    authority_id BIGINT NOT NULL
);
CREATE TABLE users
(
    id BIGINT PRIMARY KEY NOT NULL,
    email VARCHAR(256),
    enabled BOOL NOT NULL,
    firstname VARCHAR(50) NOT NULL,
    lastpasswordresetdate TIMESTAMP NOT NULL,
    lastname VARCHAR(50) NOT NULL,
    password VARCHAR(100) NOT NULL,
    username VARCHAR(50) NOT NULL,
    player BIGINT
);
CREATE UNIQUE INDEX uk_jdeu5vgpb8k5ptsqhrvamuad2 ON authority (name);
ALTER TABLE game_events ADD FOREIGN KEY (player2score) REFERENCES game_event$score (id);
ALTER TABLE game_events ADD FOREIGN KEY (player1score) REFERENCES game_event$score (id);
ALTER TABLE game_events ADD FOREIGN KEY (player2) REFERENCES players (id);
ALTER TABLE game_events ADD FOREIGN KEY (player1) REFERENCES players (id);
CREATE UNIQUE INDEX uk_px7m25cpys2bqgt856xgs38d0 ON game_server (ip);
ALTER TABLE game_server_current_games ADD FOREIGN KEY (current_games) REFERENCES game_events (id);
ALTER TABLE game_server_current_games ADD FOREIGN KEY (game_server) REFERENCES game_server (id);
CREATE UNIQUE INDEX uk_ge00327q03kan0m5g6rew7q25 ON game_server_current_games (current_games);
ALTER TABLE notification ADD FOREIGN KEY (tennio_user) REFERENCES users (id);
ALTER TABLE notification_notification_attributes ADD FOREIGN KEY (notification) REFERENCES notification (id);
ALTER TABLE players ADD FOREIGN KEY (stats) REFERENCES stats (id);
CREATE UNIQUE INDEX uk_3mryl9et0bc3w5f44syvu2p9g ON players (stats);
ALTER TABLE tournament_player_positions ADD FOREIGN KEY (player_positions_key) REFERENCES players (id);
ALTER TABLE tournament_player_positions ADD FOREIGN KEY (tournament) REFERENCES tournament (id);
ALTER TABLE tournament_players ADD FOREIGN KEY (players) REFERENCES players (id);
ALTER TABLE tournament_players ADD FOREIGN KEY (tournament) REFERENCES tournament (id);
CREATE UNIQUE INDEX uk_i2rwd7rb052jp26tnqd7dl9nc ON tournament_players (players);
ALTER TABLE tournament_tournament_games ADD FOREIGN KEY (tournament_games) REFERENCES game_events (id);
ALTER TABLE tournament_tournament_games ADD FOREIGN KEY (tournament) REFERENCES tournament (id);
CREATE UNIQUE INDEX uk_im78pwmi7f9ffvo2oeugo6ra8 ON tournament_tournament_games (tournament_games);
ALTER TABLE training_events ADD FOREIGN KEY (player) REFERENCES players (id);
ALTER TABLE training_events ADD FOREIGN KEY (training) REFERENCES trainings (id);
ALTER TABLE trainings ADD FOREIGN KEY (stat_increase) REFERENCES stats (id);
CREATE UNIQUE INDEX uk_50ien3k5n6cfto0eqmjs01cli ON trainings (stat_increase);
CREATE UNIQUE INDEX uk_j5ogghjxpb6vjl73vwkbj40m9 ON trainings (code);
ALTER TABLE user_authority ADD FOREIGN KEY (authority_id) REFERENCES authority (id);
ALTER TABLE user_authority ADD FOREIGN KEY (user_id) REFERENCES users (id);
ALTER TABLE users ADD FOREIGN KEY (player) REFERENCES players (id);
CREATE UNIQUE INDEX uk_r43af9ap4edm43mmtq01oddj6 ON users (username);