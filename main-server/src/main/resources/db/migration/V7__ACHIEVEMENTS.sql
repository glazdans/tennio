CREATE SEQUENCE achievement_progress_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

CREATE SEQUENCE achievement_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

CREATE TABLE achievement
(
    id BIGINT PRIMARY KEY NOT NULL,
    name VARCHAR(255),
    description TEXT,
    reward INT DEFAULT NULL,
    type INT NOT NULL,
    requirement INT NOT NULL
);

CREATE TABLE achievement_progress
(
    id BIGINT PRIMARY KEY NOT NULL,
    achievement_type_id BIGINT REFERENCES achievement(id),
    user_id BIGINT NOT NULL REFERENCES users(id),
    PROGRESS INT DEFAULT 0
);

