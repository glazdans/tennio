ALTER TABLE users
    DROP COLUMN player,
    ADD money BIGINT DEFAULT 1000,
    ADD balls SMALLINT DEFAULT 5;

ALTER TABLE players
    DROP COLUMN money,
    DROP COLUMN balls;

CREATE TABLE user_players (
    "user" BIGINT REFERENCES users(id),
    players BIGINT REFERENCES players(id)
);


