package com.tennio.common.requests;

import java.util.HashMap;

public class GameEndEvent {
    protected Long gameId;
    protected Double time;
    protected HashMap<Long, Score> scores = new HashMap<>();
    protected GameEndCause gameEndCause;
    protected Long gameEndCauserId;

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    public HashMap<Long, Score> getScores() {
        return scores;
    }

    public void setScores(HashMap<Long, Score> scores) {
        this.scores = scores;
    }

    public GameEndCause getGameEndCause() {
        return gameEndCause;
    }

    public void setGameEndCause(GameEndCause gameEndCause) {
        this.gameEndCause = gameEndCause;
    }

    public Long getGameEndCauserId() {
        return gameEndCauserId;
    }

    public void setGameEndCauserId(Long gameEndCauserId) {
        this.gameEndCauserId = gameEndCauserId;
    }

    @Override
    public String toString() {
        return "GameEndEvent{" +
                "gameId=" + gameId +
                ", time=" + time +
                ", scores=" + scores +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameEndEvent that = (GameEndEvent) o;

        if (!gameId.equals(that.gameId)) return false;
        if (!time.equals(that.time)) return false;
        return scores.equals(that.scores);
    }

    @Override
    public int hashCode() {
        int result = gameId.hashCode();
        result = 31 * result + time.hashCode();
        result = 31 * result + scores.hashCode();
        return result;
    }

    public enum GameEndCause {
        PlayerWon, PlayerSurrender
    }
}
