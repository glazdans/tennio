package com.tennio.common.requests;

public class Score {
    public Integer points;
    public Integer games;
    public Integer sets;
    public Integer matches;

    public Score() {
    }

    public Score(Integer games, Integer sets, Integer matches){
        this.games = games;
        this.sets = sets;
        this.matches = matches;
    }

    @Override
    public String toString() {
        return "Score{" +
                "points=" + points +
                ", games=" + games +
                ", sets=" + sets +
                ", matches=" + matches +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Score score = (Score) o;

        if (points != score.points) return false;
        if (games != score.games) return false;
        if (sets != score.sets) return false;
        return matches == score.matches;
    }

    @Override
    public int hashCode() {
        int result = points;
        result = 31 * result + games;
        result = 31 * result + sets;
        result = 31 * result + matches;
        return result;
    }
}