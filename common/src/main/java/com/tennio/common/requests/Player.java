package com.tennio.common.requests;


public class Player {
    protected String name;
    protected long id;
    protected String username;

    public Player(String name, long id) {
        this.name = name;
        this.id = id;
    }

    public Player() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String toJSON() {
        return "{" +
                    "\"name\":\"" + name + '"' +
                    ", \"id\":" + id +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
