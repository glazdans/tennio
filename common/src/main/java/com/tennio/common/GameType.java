package com.tennio.common;

public enum GameType {
    Matchmaking, AI, Tournament, TournamentQuickFire
}
